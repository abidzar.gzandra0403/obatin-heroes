import 'package:obatin/importPackage.dart';

class Preferences {
  Preferences._();
  static Preferences instance = Preferences._();

  //== SAVE DARKMODE SETTINGS ====================================================
  savePrefsDarkMode({@required int? mode}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setInt('darkmode', mode!);
  }

  getPrefsDarkMode(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    int initialDarkMode =
        _prefs.getInt('darkmode') != null ? _prefs.getInt('darkmode')! : 1;
    if (initialDarkMode == 2) {
      context.read<SettingsSwitchBloc>().add(SwitchThemeMode());
    }
  }

  //== SAVE LANGUAGE SETTINGS ====================================================
  //langId
  //1 = Indonesia
  //2 = Inggris
  savePrefsLanguage({@required int? langId}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setInt('language', langId!);
  }

  getPrefsLanguage(BuildContext context) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    int initLanguageID =
        _prefs.getInt('language') != null ? _prefs.getInt('language')! : 1;
    if (initLanguageID == 2) {
      context.read<SettingsSwitchBloc>().add(SwitchLang());
    }
  }

  //== SAVE NAMA & EMAIL =========================================================
  savePrefsName({@required String? name, @required String? email}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setString('user_name', name!);
    _prefs.setString('user_email', email!);
  }

  getPrefsName<List>() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return [_prefs.getString('user_email'), _prefs.getString('user_name')]
        .toList();
  }

  //== SAVE STATUS LOGIN =========================================================
  savePrefsAlreadyLogin({@required bool? alreadyLogin}) async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    _prefs.setBool('alreadyLogin', alreadyLogin!);
  }

  getPrefsAlreadyLogin<bool>() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    return _prefs.getBool('alreadyLogin');
  }
}
