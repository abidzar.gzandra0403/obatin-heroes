import 'package:obatin/importPackage.dart';
import 'package:http/http.dart' as http;

@immutable
abstract class UserState {
  UserState();
}

class UserInitial extends UserState {
  UserInitial() : super();
}

class UserResult extends UserState {
  UserResult() : super();
}

@immutable
abstract class UserEvent {}

class LoginUserHeroes extends UserEvent {
  final BuildContext context;
  final String? email;
  final String? password;
  LoginUserHeroes(this.context,
      {@required this.email, @required this.password});
}

class CheckIfAlreadyUser extends UserEvent {
  final BuildContext context;
  final bool? createNEW;
  final PageController? pageController;
  final String? email;
  final String? password;
  final VoidCallback? functionIfHeroesAlreadyAsUser;

  CheckIfAlreadyUser(
    this.context, {
    @required this.createNEW,
    @required this.pageController,
    @required this.email,
    @required this.password,
    this.functionIfHeroesAlreadyAsUser,
  });
}

class RegisterUserHeroes extends UserEvent {
  final BuildContext context;
  final String? email;
  final String? password;
  final String? passwordconfirm;
  final String? namalengkap;
  final String? tglahir;
  final String? tmptlahir;
  final String? nostr;
  final String? berlakudari;
  final String? berlakusampai;
  final String? idmitralevel;

  RegisterUserHeroes(this.context,
      {@required this.email,
      @required this.password,
      @required this.passwordconfirm,
      @required this.namalengkap,
      @required this.tmptlahir,
      @required this.tglahir,
      @required this.nostr,
      @required this.berlakudari,
      @required this.berlakusampai,
      @required this.idmitralevel});
}

class ResetPasswordUser extends UserEvent {
  final BuildContext context;
  final String? email;
  final String? passwordbaru;
  ResetPasswordUser(this.context,
      {@required this.email, @required this.passwordbaru});
}

class EditBiodataUser extends UserEvent {
  final BuildContext context;
  final String? nama;
  final String? tempatlahir;
  final String? tanggallahir;
  final String? email;
  final String? nik;
  EditBiodataUser(this.context,
      {@required this.nama,
      @required this.tempatlahir,
      @required this.tanggallahir,
      @required this.email,
      @required this.nik});
}

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc() : super(UserInitial());

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    //===============================================================================
    //FUNGSI LOGIN USER =============================================================
    //===============================================================================
    if (event is LoginUserHeroes) {
      var apiUrl =
          Uri.parse("http://103.226.139.253:32258/v1/auth/loginpenggunaheroes");
      CustomLoadingDialog(event.context);

      try {
        DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
        String merk = '';
        String imei = '';
        String jenis = '';
        String tipe = '';
        if (Platform.isAndroid) {
          AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
          merk = androidInfo.brand!;
          imei = androidInfo.androidId!;
          jenis = "android" + androidInfo.version.release!;
          tipe = androidInfo.model!;
        } else if (Platform.isIOS) {
          IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
          merk = "Apple";
          imei = iosInfo.identifierForVendor!;
          jenis = iosInfo.systemName!;
          tipe = iosInfo.model!;
        }
        var result = await http.post(apiUrl, headers: {
          "Accept": "application/json"
        }, body: {
          "email": event.email,
          "password": event.password,
          "merk": merk,
          "imei": imei,
          "jenis": jenis,
          "tipe": tipe,
        });
        var jsonObject = await json.decode(result.body);
        if (jsonObject["code"] == 0) {
          var itemList = jsonObject["data"]["items"];
          var itemNakesList = jsonObject["data"]["itemsnakes"];
          event.context.read<SQFLITEUserDataBloc>().add(AddUserData(
              event.context,
              idpengguna: itemList["Id_pengguna"],
              idnakes: itemNakesList["Id_nakes"],
              idlevel: itemNakesList["Id_level"],
              nostr: itemNakesList["No_str"],
              strberlakudari: itemNakesList["Berlaku_dari"],
              strberlakusampai: itemNakesList["Berlaku_sampai"],
              email: itemList["Email"],
              nik: itemList["Nik"],
              nama: itemList["Nama"],
              tempatlahir: itemList["Tempat_lahir"],
              tanggallahir: itemList["Tanggal_lahir"],
              alamat: itemList["Alamat"],
              kelurahan: itemList["Kelurahan"],
              kecamatan: itemList["Kecamatan"],
              kota: itemList["Kota"],
              provinsi: itemList["Propinsi"]));

          // ========================================================

          Navigator.pop(event.context);
          Navigator.pushReplacement(
              event.context,
              FadeRoute(
                  page: HomeSideMenu(
                loadNewData: true,
                idpengguna: itemList["Id_pengguna"],
                idnakes: itemNakesList["Id_nakes"],
              )));
        } else {
          print(jsonObject);
          Navigator.pop(event.context);
          CustomBottomDialog(event.context,
              txt:
                  "Akun tidak ditemukan, periksa kembali email dan password anda",
              prefixIcon: Icons.warning_amber_outlined);
        }
      } catch (e) {
        Navigator.pop(event.context);
        print("user_bloc - LoginUser get Error = $e");
        CustomBottomDialog(event.context,
            txt: "Masalah Pada Jaringan",
            prefixIcon: Icons.warning_amber_outlined);
      }
    }
    //===============================================================================
    //FUNGSI CHECK APAKAH HEROES SUDAH ADA AKUN OBATIN USER =========================
    //===============================================================================
    else if (event is CheckIfAlreadyUser) {
      var apiUrlCheckUser =
          Uri.parse("http://103.226.139.253:32258/v1/auth/loginpengguna");
      CustomLoadingDialog(event.context);

      try {
        var result = await http.post(apiUrlCheckUser, headers: {
          "Accept": "application/json"
        }, body: {
          "email": event.email,
          "password": event.password,
          "merk": '',
          "imei": '',
          "jenis": '',
          "tipe": '',
        });
        var jsonObject = await json.decode(result.body);

        if ((jsonObject["code"] == 0 && jsonObject["data"] != null) ||
            (jsonObject["code"] == 0 && jsonObject["data"] == null)) {
          Navigator.pop(event.context);
          print(" Sudah menjadi pengguna" + jsonObject.toString());
          if (event.createNEW == false) {
            if (jsonObject["data"] != null) {
              event.pageController!.nextPage(
                  duration: Duration(milliseconds: 500), curve: Curves.ease);
            } else {
              CustomBottomDialog(event.context,
                  txt: "Email atau Password Salah");
            }
          } else {
            CustomAlertDialog(event.context,
                title: "Email sudah terdaftar di OBAT-in",
                contentText:
                    "Sepertinya email kamu sudah terdaftar di OBAT-in nih, kamu tinggal memasukkan Email dan Password yang terdaftar lalu melengkapi data untuk menjadi HEROES!",
                txtButton1: "Login dan Daftar Heroes",
                txtButton2: "Batal",
                funcButton1: event.functionIfHeroesAlreadyAsUser!);
          }
        } else {
          Navigator.pop(event.context);
          print("Belum menjadi pengguna" + jsonObject.toString());
          if (event.createNEW == false) {
            CustomAlertDialog(event.context,
                title: "Akun tidak ditemukan",
                contentText:
                    "Pastikan Email dan Password benar, jika belum membuat akun OBAT-in maka kamu bisa lansung membuat akun HEROES",
                txtButton1: "Baik",
                txtButton3: "Buat Akun HEROES",
                funcButton1: () {});
          } else {
            event.pageController!.nextPage(
                duration: Duration(milliseconds: 500), curve: Curves.ease);
          }
        }
      } catch (e) {
        print("user_bloc - Checkifalready get Error = $e");
        CustomBottomDialog(event.context,
            txt: "Masalah Pada Jaringan",
            prefixIcon: Icons.warning_amber_outlined);
      }
    }
    //===============================================================================
    //FUNGSI REGISTER USER ==========================================================
    //===============================================================================
    else if (event is RegisterUserHeroes) {
      successDialog() {
        return CustomAlertDialog(
          event.context,
          title: "Pendaftaran Akun Sukses",
          funcDismiss: () {
            Navigator.pop(event.context);
          },
          contentIcon: Icons.check_circle_outline,
          contentText: "Anda sudah bisa login menggunakan akun anda",
        );
      }

      var apiUrl = Uri.parse(
          "http://103.226.139.253:32258/v1/auth/registerpenggunaheroes");

      if (event.email == '' ||
          event.password == '' ||
          event.namalengkap == '' ||
          event.tglahir == '' ||
          event.tmptlahir == '' ||
          event.nostr == '' ||
          event.berlakudari == '' ||
          event.berlakusampai == '') {
        CustomBottomDialog(event.context, txt: "Mohon Lengkapi Data");
      } else if (event.password != event.passwordconfirm) {
        CustomBottomDialog(event.context, txt: "Password tidak cocok");
      } else {
        try {
          CustomLoadingDialog(event.context);
          var result = await http.post(apiUrl, headers: {
            "Accept": "application/json"
          }, body: {
            "email": event.email,
            "password": event.password,
            "nama": event.namalengkap,
            "tempat_lahir": event.tmptlahir,
            "tanggal_lahir": event.tglahir,
            "nostr": event.nostr,
            "berlaku_dari": event.berlakudari,
            "berlaku_sampai": event.berlakusampai,
            "id_level": event.idmitralevel,
          });
          var jsonObject = await json.decode(result.body);
          print(jsonObject);
          if (jsonObject["code"] == 1) {
            Navigator.pop(event.context);
            CustomAlertDialog(event.context,
                title: "Ops.. email sudah terdaftar",
                contentIcon: Icons.info_outline,
                contentFitHeight: true,
                contentWidget: Column(
                  children: [
                    CustomText(
                        txt: "Email " +
                            event.email! +
                            " sudah terdaftar di OBAT-in sebagai HEROES"),
                    Row(
                      children: [
                        CustomText(txt: "Lupa password?"),
                        CustomButton(
                          styleId: 1,
                          bColor: Colors.transparent,
                          onPressed: () {},
                          btnText: "Reset Password",
                          fsize: 0.034,
                          fColor: defaultcolor2,
                        )
                      ],
                    ),
                  ],
                ),
                txtButton1: "Oke");
          } else if (jsonObject["code"] == 0) {
            Navigator.pop(event.context);
            successDialog();
          }
          print(jsonObject);
        } catch (e) {
          print("user_bloc - Register get Error = $e");
        }
      }
    }

    //===============================================================================
    //FUNGSI RESET PASSWORD USER ====================================================
    //===============================================================================
    else if (event is ResetPasswordUser) {
      String apiUrl = "http://103.226.139.253:32258/v1/auth/lupapassword";
      CustomAlertDialog(
        event.context,
        dismissable: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        width: 0.1,
        contentWidget: SpinKitPouringHourGlass(color: Colors.white),
      );
      try {
        var result = await http.post(Uri.parse(apiUrl), headers: {
          "Accept": "application/json"
        }, body: {
          "email": event.email,
          "passwordbaru": event.passwordbaru,
        });
        var jsonObject = json.decode(result.body);
        print(jsonObject);
        if (jsonObject["code"] == 1) {
          Navigator.pop(event.context);
          CustomBottomDialog(
            event.context,
            txt: "Akun Tidak Ditemukan",
          );
        } else {
          Navigator.pop(event.context);
          Navigator.pushReplacement(
              event.context, SlideRightRoute(page: LoginMain()));
          CustomBottomDialog(
            event.context,
            txt: "Reset Password Berhasil",
          );
        }
      } catch (e) {
        print("user_bloc - Resetpassword get Error = $e");
      }
    }
    //===============================================================================
    //FUNGSI EDIT BIODATA USER ======================================================
    //===============================================================================

    else if (event is EditBiodataUser) {
      successDialog() {
        return CustomAlertDialog(
          event.context,
          title: "Tersimpan",
          contentIcon: Icons.check_circle_outline,
          contentText: "Biodata anda berhasil diubah",
        );
      }

      var apiUrl =
          Uri.parse("http://103.226.139.253:32258/v1/auth/updatepengguna");

      try {
        print(postProceedData_userData[0]['email']);
        CustomLoadingDialog(event.context);
        var result = await http.post(apiUrl, headers: {
          "Accept": "application/json"
        }, body: {
          "nik": postProceedData_userData[0]['nik'],
          "email": postProceedData_userData[0]['email'],
          "nama": postProceedData_userData[0]['nama'],
          "tanggal_lahir": postProceedData_userData[0]['tanggal_lahir']
              .toString()
              .substring(0, 10),
          "tempat_lahir": postProceedData_userData[0]['tempat_lahir']
        });
        var jsonObject = await json.decode(result.body);
        print("SUKSES JOK" + jsonObject.toString());
        if (jsonObject["code"] == 1) {
          Navigator.pop(event.context);
          CustomBottomDialog(event.context, txt: "Terjadi Kesalahan");
        } else {
          // Fungsi Menyimpan Data API ke SQFLITE
          event.context
              .read<SQFLITEUserDataBloc>()
              .add(UpdateUserData(event.context, updateData: {
                "nama": event.nama!,
                "nik": event.nik!,
                "tempat_lahir": event.tempatlahir!,
                "tanggal_lahir": event.tanggallahir!,
                "email": event.email!,
              }));

          // ========================================================

          Navigator.pop(event.context);
          Navigator.pop(event.context);
          successDialog();
        }
      } catch (e) {
        print("user_bloc - Edit bio get Error = $e");
      }
    }
  }
}
