import 'package:http/http.dart' as http;

import 'package:obatin/importPackage.dart';

@immutable
abstract class SipSikState {}

class SipSikInitial extends SipSikState {
  SipSikInitial() : super();
}

class SipSikResult extends SipSikState {
  SipSikResult() : super();
}

@immutable
abstract class SipSikEvent {}

class GetSipSik extends SipSikEvent {
  final BuildContext context;
  final String? idpengguna;
  final String? idnakes;
  GetSipSik(this.context, {@required this.idpengguna, @required this.idnakes});
}

class CreateSipSik extends SipSikEvent {
  final BuildContext context;
  final String? nosip;
  final String? berlakudari;
  final String? berlakusampai;
  final String? kotaberlaku;
  final String? propinsiberlaku;

  CreateSipSik(
    this.context, {
    @required this.nosip,
    @required this.berlakudari,
    @required this.berlakusampai,
    @required this.kotaberlaku,
    @required this.propinsiberlaku,
  });
}

class UpdateSipSik extends SipSikEvent {
  final BuildContext context;
  final int? dataid;
  final String? idSip;
  final String? nosip;
  final String? berlakudari;
  final String? berlakusampai;
  final String? kotaberlaku;
  final String? propinsiberlaku;

  UpdateSipSik(
    this.context, {
    @required this.dataid,
    @required this.idSip,
    @required this.nosip,
    @required this.berlakudari,
    @required this.berlakusampai,
    @required this.kotaberlaku,
    @required this.propinsiberlaku,
  });
}

class DeleteSipSik extends SipSikEvent {
  final BuildContext context;
  final String? idSip;
  final int? dataId;
  DeleteSipSik(this.context, {@required this.idSip, this.dataId});
}

class SipSikBloc extends Bloc<SipSikEvent, SipSikState> {
  SipSikBloc() : super(SipSikInitial());

  @override
  Stream<SipSikState> mapEventToState(
    SipSikEvent event,
  ) async* {
    var mainApiUrl = "http://103.226.139.253:32258/v1/sipnakes/";
    //==================================================================
    // GET SIP / SIK
    //==================================================================
    if (event is GetSipSik) {
      CustomLoadingDialog(event.context);
      var functionApi = mainApiUrl + "get";
      var result = await http.post(Uri.parse(functionApi), headers: {
        "Accept": "application/json"
      }, body: {
        "idpengguna": event.idpengguna,
        "idnakes": event.idnakes,
      });
      var jsonObject = await json.decode(result.body);
      print(jsonObject);
      if (jsonObject["code"] == 0 && jsonObject["data"]["item"] != null) {
        List itemList = (jsonObject["data"]["item"]);
        int i = 0;
        while (i < itemList.length) {
          event.context.read<SQFLITESipSikBloc>().add(AddSipSikData(
              event.context,
              idSip: itemList[i]['id_sip'],
              nosip: itemList[i]['no_sip'],
              berlakudari: itemList[i]['berlaku_dari'],
              berlakusampai: itemList[i]['berlaku_sampai'],
              kotaberlaku: itemList[i]['kota_berlaku'],
              propinsiberlaku: itemList[i]['propinsi_berlaku']));
          i++;
        }
        Navigator.pop(event.context);
      } else {
        Navigator.pop(event.context);
      }
    }
    //==================================================================
    // CREATE SIP / SIK
    //==================================================================
    else if (event is CreateSipSik) {
      CustomLoadingDialog(event.context);
      var functionApi = mainApiUrl + "create";
      var idPengguna = postProceedData_userData[0]["id_pengguna"];
      var idnakes = postProceedData_userData[0]["id_nakes"];
      try {
        var result = await http.post(Uri.parse(functionApi), headers: {
          "Accept": "application/json"
        }, body: {
          "idpengguna": idPengguna,
          "idnakes": idnakes,
          "nosip": event.nosip,
          "berlakudari": event.berlakudari,
          "berlakusampai": event.berlakusampai,
          "kotaberlaku": event.kotaberlaku,
          "propinsiberlaku": event.propinsiberlaku,
        });
        var jsonObject = await json.decode(result.body);
        print(jsonObject);
        if (jsonObject["code"] == 1) {
          CustomBottomDialog(event.context, txt: "Terjadi Kesalahan");
        } else {
          var idSip = jsonObject["data"]["item"][0]["id_sip"];
          event.context.read<SQFLITESipSikBloc>().add(AddSipSikData(
                event.context,
                idSip: idSip,
                nosip: event.nosip,
                berlakudari: event.berlakudari,
                berlakusampai: event.berlakusampai,
                kotaberlaku: event.kotaberlaku,
                propinsiberlaku: event.propinsiberlaku,
              ));
        }
        Navigator.pop(event.context);
        Navigator.pop(event.context);
        CustomBottomDialog(event.context, txt: "Berhasil ditambah");
      } catch (e) {
        Navigator.pop(event.context);
        print("sipsik_bloc - Create Sip sik get error = $e");
      }
    }
    //==================================================================
    // UPDATE SIP / SIK
    //==================================================================
    else if (event is UpdateSipSik) {
      CustomLoadingDialog(event.context);
      var functionApi = mainApiUrl + "update";
      var idPengguna = postProceedData_userData[0]["id_pengguna"];
      var idnakes = postProceedData_userData[0]["id_nakes"];
      try {
        var result = await http.post(Uri.parse(functionApi), headers: {
          "Accept": "application/json"
        }, body: {
          "idsip": event.idSip,
          "idpengguna": idPengguna,
          "idnakes": idnakes,
          "nosip": event.nosip,
          "berlakudari": event.berlakudari,
          "berlakusampai": event.berlakusampai,
          "kotaberlaku": event.kotaberlaku,
          "propinsiberlaku": event.propinsiberlaku,
        });

        var jsonObject = await json.decode(result.body);
        if (jsonObject["code"] == 1) {
          CustomBottomDialog(event.context, txt: "Terjadi Kesalahan");
        } else {
          print(jsonObject);
          event.context.read<SQFLITESipSikBloc>().add(UpdateSipSikData(
                  event.context,
                  dataid: event.dataid!,
                  updateData: {
                    "nosip": event.nosip!,
                    "berlakudari": event.berlakudari!,
                    "berlakusampai": event.berlakusampai!,
                    "kotaberlaku": event.kotaberlaku!,
                    "propinsiberlaku": event.propinsiberlaku!,
                  }));
        }
        Navigator.pop(event.context);
        Navigator.pop(event.context);
        CustomBottomDialog(event.context, txt: "Berhasil diupdate");
      } catch (e) {
        Navigator.pop(event.context);
        print("sipsik_bloc - Update Sip sik get error = $e");
      }
    }
    //==================================================================
    // DELETE SIP / SIK
    //==================================================================
    else if (event is DeleteSipSik) {
      CustomLoadingDialog(event.context);
      var functionApi = mainApiUrl + "delete";
      var idPengguna = postProceedData_userData[0]["id_pengguna"];
      var idnakes = postProceedData_userData[0]["id_nakes"];
      try {
        var result = await http.post(Uri.parse(functionApi), headers: {
          "Accept": "application/json"
        }, body: {
          "idsip": event.idSip,
          "idpengguna": idPengguna,
          "idnakes": idnakes,
        });

        var jsonObject = await json.decode(result.body);
        if (jsonObject["code"] == 1) {
          CustomBottomDialog(event.context, txt: "Terjadi Kesalahan");
        } else {
          event.context
              .read<SQFLITESipSikBloc>()
              .add(DeleteSipSikData(event.context, dataid: event.dataId));
        }
        Navigator.pop(event.context);
        CustomBottomDialog(event.context, txt: "Berhasil dihapus");
      } catch (e) {
        Navigator.pop(event.context);
        print("sipsik_bloc - Delete Sip sik get error = $e");
      }
    }
  }
}
