import 'package:obatin/importPackage.dart';
import 'package:http/http.dart' as http;

@immutable
abstract class RegionSelectorState {
  final List provinsiList;
  final List kabupatenList;
  final List kecamatanList;
  final List kelurahanList;
  RegionSelectorState(this.provinsiList, this.kabupatenList, this.kecamatanList,
      this.kelurahanList);
}

class RegionSelectorInitial extends RegionSelectorState {
  RegionSelectorInitial() : super([], [], [], []);
}

class RegionSelectorResult extends RegionSelectorState {
  RegionSelectorResult(List provinsiListResult, List kabupatenListResult,
      List kecamatanListResult, List kelurahanListResult)
      : super(provinsiListResult, kabupatenListResult, kecamatanListResult,
            kelurahanListResult);
}

@immutable
abstract class RegionSelectorEvent {}

class SelectProvinsi extends RegionSelectorEvent {}

class SelectKabupaten extends RegionSelectorEvent {
  final int? provinsiID;
  SelectKabupaten({@required this.provinsiID});
}

class SelectKecamatan extends RegionSelectorEvent {
  final int? kabupatenID;
  SelectKecamatan({@required this.kabupatenID});
}

class SelectKelurahan extends RegionSelectorEvent {
  final int? kecamatanID;
  SelectKelurahan({@required this.kecamatanID});
}

class RegionSelectorBloc
    extends Bloc<RegionSelectorEvent, RegionSelectorState> {
  RegionSelectorBloc() : super(RegionSelectorInitial());

  @override
  Stream<RegionSelectorState> mapEventToState(
    RegionSelectorEvent event,
  ) async* {
    getToken() async {
      var apiUrl = Uri.parse("https://x.rajaapi.com/poe");
      try {
        var result = await http.get(
          apiUrl,
          headers: {"Accept": "application/json"},
        );
        var jsonResult = await json.decode(result.body);
        return jsonResult['token'];
      } catch (e) {
        print("Error get token : $e");
      }
    }

    var headerUrl = "https://x.rajaapi.com/MeP7c5ne";
    if (event is SelectProvinsi) {
      var token = rajaAPIToken != null ? rajaAPIToken : await getToken();
      var apiUrl = Uri.parse(headerUrl + token + "/m/wilayah/provinsi");
      try {
        var result =
            await http.get(apiUrl, headers: {"Accept": "application/json"});
        var jsonResult = await json.decode(result.body);
        yield RegionSelectorResult(jsonResult['data'], [], [], []);
      } catch (e) {
        print('Error Get Provinsi : $e');
      }
    } else if (event is SelectKabupaten) {
      var token = rajaAPIToken != null ? rajaAPIToken : await getToken();
      var apiUrl = Uri.parse(headerUrl +
          token +
          "/m/wilayah/kabupaten?idpropinsi=" +
          event.provinsiID.toString());
      try {
        var result =
            await http.get(apiUrl, headers: {"Accept": "application/json"});
        var jsonResult = await json.decode(result.body);
        yield RegionSelectorResult([], jsonResult['data'], [], []);
      } catch (e) {
        print('Error Get Kabupaten : $e');
      }
    } else if (event is SelectKecamatan) {
      var token = rajaAPIToken != null ? rajaAPIToken : await getToken();
      var apiUrl = Uri.parse(headerUrl +
          token +
          "/m/wilayah/kecamatan?idkabupaten=" +
          event.kabupatenID.toString());
      try {
        var result =
            await http.get(apiUrl, headers: {"Accept": "application/json"});
        var jsonResult = await json.decode(result.body);
        yield RegionSelectorResult([], [], jsonResult['data'], []);
      } catch (e) {
        print('Error Get Kecamatan : $e');
      }
    } else if (event is SelectKelurahan) {
      var token = rajaAPIToken != null ? rajaAPIToken : await getToken();
      var apiUrl = Uri.parse(headerUrl +
          token +
          "/m/wilayah/kelurahan?idkecamatan=" +
          event.kecamatanID.toString());
      try {
        var result =
            await http.get(apiUrl, headers: {"Accept": "application/json"});
        var jsonResult = await json.decode(result.body);
        yield RegionSelectorResult([], [], [], jsonResult['data']);
      } catch (e) {
        print('Error Get Kelurahan : $e');
      }
    }
  }
}
