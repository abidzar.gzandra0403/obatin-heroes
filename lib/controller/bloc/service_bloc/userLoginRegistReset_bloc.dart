import 'dart:async';
import 'dart:convert';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;
import 'package:obatin/controller/sharedPrefs.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customBottomDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';
import 'package:obatin/view/menu_home/home_sidebutton.dart';

@immutable
abstract class UserLoginRegistResetState {
  UserLoginRegistResetState();
}

class UserLoginRegistResetInitial extends UserLoginRegistResetState {
  UserLoginRegistResetInitial() : super();
}

class Result extends UserLoginRegistResetState {
  Result() : super();
}

@immutable
abstract class Event {}

class LoginUser extends Event {
  final BuildContext context;
  final String? email;
  final String? password;
  LoginUser(this.context, {@required this.email, @required this.password});
}

class RegisterUser extends Event {
  final String? namalengkap;
  final String? email;
  final String? password;
  RegisterUser(
      {@required this.namalengkap,
      @required this.email,
      @required this.password});
}

class ResetPasswordUser extends Event {}

class UserLoginRegistResetBloc extends Bloc<Event, UserLoginRegistResetState> {
  UserLoginRegistResetBloc() : super(UserLoginRegistResetInitial());

  @override
  Stream<UserLoginRegistResetState> mapEventToState(
    Event event,
  ) async* {
    //===============================================================================
    //FUNGSI LOGIN USER =============================================================
    //===============================================================================
    if (event is LoginUser) {
      var apiUrl =
          Uri.parse("http://103.226.139.253:32258/v1/auth/loginpengguna");
      CustomAlertDialog(
        event.context,
        dismissable: false,
        backgroundColor: Colors.transparent,
        elevation: 0,
        width: 0.1,
        contentWidget: SpinKitPouringHourGlass(color: Colors.white),
      );
      try {
        var result = await http.post(apiUrl, headers: {
          "Accept": "application/json"
        }, body: {
          "email": event.email,
          "password": event.password,
        });
        var jsonObject = await json.decode(result.body);
        if (jsonObject["code"] == 0) {
          Preferences.instance.savePrefsName(
              name: jsonObject["data"]["items"]["Nama"],
              email: jsonObject["data"]["items"]["Email"]);
          Navigator.pop(event.context);
          Navigator.pushReplacement(
              event.context,
              SlideRightRoute(
                  page: HomeSideButton(
                xoffsetEnd: getWidth(event.context, -0.5),
              )));
        } else {
          Navigator.pop(event.context);
          CustomBottomDialog(event.context,
              txt: "Email atau Password Salah",
              prefixIcon: Icons.warning_amber_outlined);
        }
      } catch (e) {
        Navigator.pop(event.context);
        CustomBottomDialog(event.context,
            txt: "Masalah Pada Jaringan",
            prefixIcon: Icons.warning_amber_outlined);
      }
    }
    //===============================================================================
    //FUNGSI REGISTER USER =============================================================
    //===============================================================================
    else if (event is RegisterUser) {
      String apiUrl =
          "http://103.226.139.253:32258/v1/auth/registerpenggunaheroes";
      try {
        var result = await http.post(Uri.parse(apiUrl), headers: {
          "Accept": "application/json"
        }, body: {
          "nama": event.namalengkap,
          "email": event.email,
          "password": event.password,
          "nik": "",
          "alamat": "",
          "kelurahan": "",
          "kecamatan": "",
          "propinsi": "",
          "tempat_lahir": "",
          "tanggal_lahir": "2021-10-10",
          "kota": "",
        });
        var jsonObject = json.decode(result.body);
        print(jsonObject);
      } catch (e) {
        print("Sambungan Gagal" + "\nKeterangan :\n" + e.toString());
      }
    } else if (event is ResetPasswordUser) {}
  }
}
