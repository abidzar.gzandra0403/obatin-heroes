import 'package:http/http.dart' as http;

import 'package:obatin/importPackage.dart';

@immutable
abstract class DeskripsiState {}

class DeskripsiInitial extends DeskripsiState {
  DeskripsiInitial() : super();
}

class DeskripsiResult extends DeskripsiState {
  DeskripsiResult() : super();
}

@immutable
abstract class DeskripsiEvent {}

class GetDeskripsi extends DeskripsiEvent {
  final BuildContext context;
  final String? idpengguna;
  final String? idnakes;
  GetDeskripsi(this.context,
      {@required this.idpengguna, @required this.idnakes});
}

class CreateDeskripsi extends DeskripsiEvent {
  final BuildContext context;
  final String? deskripsi;

  CreateDeskripsi(
    this.context, {
    @required this.deskripsi,
  });
}

class DeskripsiBloc extends Bloc<DeskripsiEvent, DeskripsiState> {
  DeskripsiBloc() : super(DeskripsiInitial());

  @override
  Stream<DeskripsiState> mapEventToState(
    DeskripsiEvent event,
  ) async* {
    var mainApiUrl = "http://103.226.139.253:32258/v1/profilnakes/";
    //==================================================================
    // GET Deskripsi
    //==================================================================
    if (event is GetDeskripsi) {
      CustomLoadingDialog(event.context);
      var functionApi = mainApiUrl + "getdeskripsi";
      var result = await http.post(Uri.parse(functionApi), headers: {
        "Accept": "application/json"
      }, body: {
        "idpengguna": event.idpengguna,
        "idnakes": event.idnakes,
      });
      var jsonObject = await json.decode(result.body);
      print(jsonObject);
      if (jsonObject["code"] == 0 && jsonObject["data"]["item"] != null) {
        List itemList = (jsonObject["data"]["item"]);
        int i = 0;
        while (i < itemList.length) {
          // event.context.read<SQFLITESipSikBloc>().add(AddSipSikData(
          //     event.context,
          //     idSip: itemList[i]['id_sip'],
          //     nosip: itemList[i]['no_sip'],
          //     berlakudari: itemList[i]['berlaku_dari'],
          //     berlakusampai: itemList[i]['berlaku_sampai'],
          //     kotaberlaku: itemList[i]['kota_berlaku'],
          //     propinsiberlaku: itemList[i]['propinsi_berlaku']));
          // i++;
        }
        Navigator.pop(event.context);
      } else {
        Navigator.pop(event.context);
      }
    }
    //==================================================================
    // CREATE Deskripsi
    //==================================================================
    else if (event is CreateDeskripsi) {
      var functionApi = mainApiUrl + "createdeskripsi";
      var idPengguna = postProceedData_userData[0]["id_pengguna"];
      var idnakes = postProceedData_userData[0]["id_nakes"];
      try {
        var result = await http.post(Uri.parse(functionApi), headers: {
          "Accept": "application/json"
        }, body: {
          "idpengguna": idPengguna,
          "idnakes": idnakes,
          "deskripsi": event.deskripsi
        });
        var jsonObject = await json.decode(result.body);
        print(jsonObject);
        if (jsonObject["code"] == 1) {
          CustomBottomDialog(event.context, txt: "Terjadi Kesalahan");
        } else {
          // var idSip = jsonObject["data"]["item"][0]["id_sip"];
          // event.context.read<SQFLITESipSikBloc>().add(AddSipSikData(
          //       event.context,
          //       idSip: idSip,
          //       nosip: event.nosip,
          //       berlakudari: event.berlakudari,
          //       berlakusampai: event.berlakusampai,
          //       kotaberlaku: event.kotaberlaku,
          //       propinsiberlaku: event.propinsiberlaku,
          //     ));
        }
        // Navigator.pop(event.context);

        CustomBottomDialog(event.context, txt: "Berhasil Menambah !!");
      } catch (e) {
        // Navigator.pop(event.context);

        print("deskripsi_bloc - Create deskripsi get error = $e");
      }
    }
  }
}
