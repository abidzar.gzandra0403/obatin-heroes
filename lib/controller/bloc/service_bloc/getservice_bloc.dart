import 'package:obatin/importPackage.dart';
import 'package:http/http.dart' as http;

@immutable
abstract class PracticeState {}

class PracticeInitial extends PracticeState {}

@immutable
abstract class PracticeEvent {}

class PracticeGetSip extends PracticeEvent {
  final String? idpengguna;
  final String? idnakes;
  PracticeGetSip({@required this.idpengguna, @required this.idnakes});
}

class PracticeBloc extends Bloc<PracticeEvent, PracticeState> {
  PracticeBloc() : super(PracticeInitial());

  @override
  Stream<PracticeState> mapEventToState(
    PracticeEvent event,
  ) async* {
    var mainApiUrl = "http://103.226.139.253:32258/v1/layanan/";
    if (event is PracticeGetSip) {
      var functionApi = mainApiUrl + "getlayanan";
      var result = await http.post(Uri.parse(functionApi), headers: {
        "Accept": "application/json"
      }, body: {
        "idpengguna": event.idpengguna,
        "idnakes": event.idnakes,
      });
      var jsonObject = await json.decode(result.body);
      print(jsonObject);
    }
  }
}
