import 'package:obatin/importPackage.dart';

@immutable
abstract class SQFLITESipSikState {
  final bool? finishLoading;
  SQFLITESipSikState(this.finishLoading);
}

class SQFLITESipSikInitial extends SQFLITESipSikState {
  SQFLITESipSikInitial() : super(false);
}

class SQFLITESipSikResult extends SQFLITESipSikState {
  SQFLITESipSikResult(bool? finishLoading) : super(finishLoading);
}

@immutable
abstract class SQFLITESipSikEvent {}

class AddSipSikData extends SQFLITESipSikEvent {
  final BuildContext context;
  final String? idSip;
  final String? nosip;
  final String? berlakudari;
  final String? berlakusampai;
  final String? kotaberlaku;
  final String? propinsiberlaku;

  AddSipSikData(
    this.context, {
    @required this.idSip,
    @required this.nosip,
    @required this.berlakudari,
    @required this.berlakusampai,
    @required this.kotaberlaku,
    @required this.propinsiberlaku,
  });
}

class UpdateSipSikData extends SQFLITESipSikEvent {
  final BuildContext context;
  final int? dataid;
  final Map<String, Object>? updateData;
  UpdateSipSikData(this.context,
      {@required this.dataid, @required this.updateData});
}

class DeleteSipSikData extends SQFLITESipSikEvent {
  final BuildContext context;
  final int? dataid;
  DeleteSipSikData(
    this.context, {
    this.dataid,
  });
}

class SQFLITESipSikBloc extends Bloc<SQFLITESipSikEvent, SQFLITESipSikState> {
  SQFLITESipSikBloc() : super(SQFLITESipSikInitial());

  @override
  Stream<SQFLITESipSikState> mapEventToState(
    SQFLITESipSikEvent event,
  ) async* {
    //====================================================================
    //TAMBAH SIP
    //====================================================================
    if (event is AddSipSikData) {
      await LocalDatabase().createData(ModelSipData.tableName, {
        "idSip": event.idSip!,
        "nosip": event.nosip!,
        "berlakudari": event.berlakudari!,
        "berlakusampai": event.berlakusampai!,
        "kotaberlaku": event.kotaberlaku!,
        "propinsiberlaku": event.propinsiberlaku!,
      });
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipData.tableName);
      yield SQFLITESipSikResult(true);
    }
    //====================================================================
    //UPDATE SIP
    //====================================================================
    else if (event is UpdateSipSikData) {
      await LocalDatabase().updateData(ModelSipData.tableName,
          dataid: event.dataid, updateData: event.updateData);
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipData.tableName);
      yield SQFLITESipSikResult(true);
    }
    //====================================================================
    //DELETE SIP
    //====================================================================
    else if (event is DeleteSipSikData) {
      event.dataid != null
          ? await LocalDatabase()
              .deleteData(ModelSipData.tableName, dataid: event.dataid!)
          : await LocalDatabase().deleteData(ModelSipData.tableName);
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipData.tableName);
      yield SQFLITESipSikResult(true);
    }
  }
}
