import 'package:obatin/importPackage.dart';

@immutable
abstract class SQFLITEUserDataState {
  final bool? finishLoading;
  SQFLITEUserDataState(this.finishLoading);
}

class SQFLITEUserDataInitial extends SQFLITEUserDataState {
  SQFLITEUserDataInitial() : super(false);
}

class SQFLITEUserDataResult extends SQFLITEUserDataState {
  SQFLITEUserDataResult(bool? finishLoading) : super(finishLoading);
}

@immutable
abstract class SQFLITEUserDataEvent {}

class AddUserData extends SQFLITEUserDataEvent {
  final BuildContext context;
  final String? idpengguna;
  final String? idnakes;
  final String? idlevel;
  final String? nostr;
  final String? strberlakudari;
  final String? strberlakusampai;
  final String? email;
  final String? nik;
  final String? nama;
  final String? tempatlahir;
  final String? tanggallahir;
  final String? alamat;
  final String? kelurahan;
  final String? kecamatan;
  final String? kota;
  final String? provinsi;
  AddUserData(this.context,
      {@required this.idpengguna,
      @required this.idnakes,
      @required this.idlevel,
      @required this.nostr,
      @required this.strberlakudari,
      @required this.strberlakusampai,
      @required this.email,
      @required this.nik,
      @required this.nama,
      @required this.tempatlahir,
      @required this.tanggallahir,
      @required this.alamat,
      @required this.kelurahan,
      @required this.kecamatan,
      @required this.kota,
      @required this.provinsi});
}

class UpdateUserData extends SQFLITEUserDataEvent {
  final BuildContext context;
  final Map<String, Object>? updateData;
  UpdateUserData(this.context, {@required this.updateData});
}

class DeleteUserData extends SQFLITEUserDataEvent {}

class SQFLITEUserDataBloc
    extends Bloc<SQFLITEUserDataEvent, SQFLITEUserDataState> {
  SQFLITEUserDataBloc() : super(SQFLITEUserDataInitial());

  @override
  Stream<SQFLITEUserDataState> mapEventToState(
    SQFLITEUserDataEvent event,
  ) async* {
    //TAMBAH SIP ====================================================================
    if (event is AddUserData) {
      await LocalDatabase().createData(ModelUserData.tableName, {
        "id_pengguna": event.idpengguna!,
        "id_nakes": event.idnakes!,
        "id_level": event.idlevel!,
        "no_str": event.nostr!,
        "str_berlaku_dari": event.strberlakudari!,
        "str_berlaku_sampai": event.strberlakusampai!,
        "email": event.email!,
        "nik": event.nik!,
        "nama": event.nama!,
        "tempat_lahir": event.tempatlahir!,
        "tanggal_lahir": event.tanggallahir!,
        "alamat": event.alamat!,
        "kelurahan": event.kelurahan!,
        "kecamatan": event.kecamatan!,
        "kota": event.kota!,
        "provinsi": event.provinsi!
      });
      Preferences.instance.savePrefsAlreadyLogin(alreadyLogin: true);
      postProceedData_userData =
          await LocalDatabase().readData(ModelUserData.tableName);
      print(postProceedData_userData);
      yield SQFLITEUserDataResult(true);
    }
    //UPDATE SIP ====================================================================
    else if (event is UpdateUserData) {
      await LocalDatabase().updateData(ModelUserData.tableName,
          dataid: postProceedData_userData[0]["dataid"],
          updateData: event.updateData);
      postProceedData_userData =
          await LocalDatabase().readData(ModelUserData.tableName);
      yield SQFLITEUserDataResult(true);
    }
    //DELETE SIP ===================================================================
    else if (event is DeleteUserData) {
      Preferences.instance.savePrefsAlreadyLogin(alreadyLogin: false);
      postProceedData_userData = [];
      await LocalDatabase().deleteData(ModelUserData.tableName);

      yield SQFLITEUserDataResult(true);
    }
  }

  //FUNCTION
}
