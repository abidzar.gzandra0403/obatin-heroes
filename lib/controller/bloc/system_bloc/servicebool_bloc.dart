import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:obatin/main.dart';
import 'package:obatin/models/system_models/serviceBool.dart';

@immutable
abstract class ServiceBoolState {}

class ServiceBoolInitial extends ServiceBoolState {}

class ServiceBoolResult extends ServiceBoolState {}

@immutable
abstract class ServiceBoolEvent {}

class SetOnloc1 extends ServiceBoolEvent {}

class SetOnloc2 extends ServiceBoolEvent {}

class SetOnloc3 extends ServiceBoolEvent {}

class SetVisit1 extends ServiceBoolEvent {}

class SetVisit2 extends ServiceBoolEvent {}

class SetVisit3 extends ServiceBoolEvent {}

class SetTele1 extends ServiceBoolEvent {}

class SetTele2 extends ServiceBoolEvent {}

class SetTele3 extends ServiceBoolEvent {}

class SetVolunOnloc1 extends ServiceBoolEvent {}

class SetVolunOnloc2 extends ServiceBoolEvent {}

class SetVolunOnloc3 extends ServiceBoolEvent {}

class SetVolunVisit1 extends ServiceBoolEvent {}

class SetVolunVisit2 extends ServiceBoolEvent {}

class SetVolunVisit3 extends ServiceBoolEvent {}

class SetVolunTele1 extends ServiceBoolEvent {}

class SetVolunTele2 extends ServiceBoolEvent {}

class SetVolunTele3 extends ServiceBoolEvent {}

class ServiceBoolBloc extends Bloc<ServiceBoolEvent, ServiceBoolState> {
  ServiceBoolBloc() : super(ServiceBoolInitial());

  @override
  Stream<ServiceBoolState> mapEventToState(
    ServiceBoolEvent event,
  ) async* {
    switch (event.runtimeType) {
      case SetOnloc1:
        onloc1 = !onloc1;
        yield ServiceBoolResult();
        break;
      case SetOnloc2:
        onloc2 = !onloc2;
        yield ServiceBoolResult();
        break;
      case SetOnloc3:
        onloc3 = !onloc3;
        yield ServiceBoolResult();
        break;
      case SetVisit1:
        visit1 = !visit1;
        yield ServiceBoolResult();
        break;
      case SetVisit2:
        visit2 = !visit2;
        yield ServiceBoolResult();
        break;
      case SetVisit3:
        visit3 = !visit3;
        yield ServiceBoolResult();
        break;
      case SetTele1:
        tele1 = !tele1;
        yield ServiceBoolResult();
        break;
      case SetTele2:
        tele2 = !tele2;
        yield ServiceBoolResult();
        break;
      case SetTele3:
        tele3 = !tele3;
        yield ServiceBoolResult();
        break;
      case SetVolunOnloc1:
        volunonloc1 = !volunonloc1;
        yield ServiceBoolResult();
        break;
      case SetVolunOnloc2:
        volunonloc2 = !volunonloc2;
        yield ServiceBoolResult();
        break;
      case SetVolunOnloc3:
        volunonloc3 = !volunonloc3;
        yield ServiceBoolResult();
        break;
      case SetVolunVisit1:
        volunvisit1 = !volunvisit1;
        yield ServiceBoolResult();
        break;
      case SetVolunVisit2:
        volunvisit2 = !volunvisit2;
        yield ServiceBoolResult();
        break;
      case SetVolunVisit3:
        volunvisit3 = !volunvisit3;
        yield ServiceBoolResult();
        break;
      case SetVolunTele1:
        voluntele1 = !voluntele1;
        yield ServiceBoolResult();
        break;
      case SetVolunTele2:
        voluntele2 = !voluntele2;
        yield ServiceBoolResult();
        break;
      case SetVolunTele3:
        voluntele3 = !voluntele3;
        yield ServiceBoolResult();
        break;
    }
  }
}
