import 'package:obatin/importPackage.dart';

@immutable
abstract class PasswordCheckerState {
  final bool warning;
  PasswordCheckerState(this.warning);
}

class PasswordCheckerInitial extends PasswordCheckerState {
  PasswordCheckerInitial() : super(false);
}

class CheckResult extends PasswordCheckerState {
  CheckResult(bool warningcheck) : super(warningcheck);
}

@immutable
abstract class CheckEvent {}

class Check extends CheckEvent {
  final String? txtboxPassword;
  final String? txtboxPasswordConfirm;
  Check({@required this.txtboxPassword, @required this.txtboxPasswordConfirm});
}

class PasswordCheckerBloc extends Bloc<CheckEvent, PasswordCheckerState> {
  PasswordCheckerBloc() : super(PasswordCheckerInitial());

  @override
  Stream<PasswordCheckerState> mapEventToState(
    CheckEvent event,
  ) async* {
    if (event is Check) {
      if (event.txtboxPassword != event.txtboxPasswordConfirm) {
        yield CheckResult(true);
      } else {
        yield CheckResult(false);
      }
    }
  }
}
