import 'package:obatin/importPackage.dart';

@immutable
abstract class SettingsSwitchState {
  SettingsSwitchState();
}

class SettingsSwitchInitial extends SettingsSwitchState {
  SettingsSwitchInitial() : super();
}

class SwitchResult extends SettingsSwitchState {
  SwitchResult() : super();
}

@immutable
abstract class SwitchEvent {}

class SwitchThemeMode extends SwitchEvent {}

class SwitchLang extends SwitchEvent {}

class SwitchAllowReview extends SwitchEvent {}

class SettingsSwitchBloc extends Bloc<SwitchEvent, SettingsSwitchState> {
  SettingsSwitchBloc() : super(SettingsSwitchInitial());

  @override
  Stream<SettingsSwitchState> mapEventToState(
    SwitchEvent event,
  ) async* {
    switch (event.runtimeType) {
      case SwitchThemeMode:
        themeID = themeID == 1 ? 2 : 1;
        Preferences.instance.savePrefsDarkMode(mode: themeID);
        yield SwitchResult();
        break;
      case SwitchLang:
        langID = langID == 1 ? 2 : 1;
        Preferences.instance.savePrefsLanguage(langId: langID);

        yield SwitchResult();
        break;
      case SwitchAllowReview:
        allowreview = !allowreview;
        yield SwitchResult();
        break;
    }
  }
}
