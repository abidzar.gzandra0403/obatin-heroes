import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:obatin/models/localDatabase.dart';
import 'package:obatin/models/models_sip.dart';
import 'package:obatin/models/postProceedData.dart';
import 'package:obatin/view/common_widget/customWidget/customBottomDialog.dart';

@immutable
abstract class CRUDSipState {
  CRUDSipState();
}

class CRUDSipInitial extends CRUDSipState {
  CRUDSipInitial() : super();
}

class Result extends CRUDSipState {
  Result() : super();
}

@immutable
abstract class Event {}

class AddSIP extends Event {
  final BuildContext context;
  final String? noSIP;
  final String? alamat;
  AddSIP(this.context, {@required this.noSIP, @required this.alamat});
}

class UpdateSIP extends Event {
  final BuildContext context;
  final int? dataid;
  final String? alamat;
  UpdateSIP(this.context, {@required this.dataid, @required this.alamat});
}

class DeleteSIP extends Event {
  final BuildContext context;
  final int? dataid;
  DeleteSIP(
    this.context, {
    @required this.dataid,
  });
}

class CRUDSipBloc extends Bloc<Event, CRUDSipState> {
  CRUDSipBloc() : super(CRUDSipInitial());

  @override
  Stream<CRUDSipState> mapEventToState(
    Event event,
  ) async* {
    //TAMBAH SIP ====================================================================
    if (event is AddSIP) {
      await LocalDatabase().createData(ModelSipQuery.tableName,
          {"nosip": event.noSIP!, "alamat": event.alamat!});
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipQuery.tableName);
      yield Result();
    }
    //UPDATE SIP ====================================================================
    else if (event is UpdateSIP) {
      await LocalDatabase().updateData(ModelSipQuery.tableName,
          dataid: event.dataid!, updateData: {'alamat': event.alamat!});
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipQuery.tableName);
      yield Result();
    }
    //DELETE SIP ===================================================================
    else if (event is DeleteSIP) {
      await LocalDatabase()
          .deleteData(ModelSipQuery.tableName, dataid: event.dataid!);
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipQuery.tableName);
      CustomBottomDialog(event.context, txt: "Berhasil Dihapus");
      yield Result();
    }
  }

  //FUNCTION
}
