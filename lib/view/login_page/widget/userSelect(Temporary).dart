import 'package:obatin/importPackage.dart';

userSelectDialog(BuildContext context) {
  loginBtnFunc() {
    goToPage(context, page: TermPrivacyView());
  }

  return NAlertDialog(
    dialogStyle: DialogStyle(titleDivider: true, backgroundColor: bgColor()),
    content: Container(
      height: getHeight(context, 0.3),
      width: getWidth(context, 0.7),
      color: bgColor(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          CustomText(
              textAlign: TextAlign.center,
              txt: "Login Sebagai\n(Menu Sementara)"),
          CustomButton(
            styleId: 1,
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushReplacement(
                  context, SlideRightRoute(page: HomeSideMenu()));
              mainUserType = 1;
            },
            btnText: "Dokter",
          ),
          CustomButton(
            styleId: 1,
            onPressed: () {
              Navigator.pop(context);
              Navigator.pushReplacement(
                  context, SlideRightRoute(page: HomeSideMenu()));
              mainUserType = 2;
            },
            btnText: "Nakes",
          ),
          CustomButton(
            styleId: 1,
            bColor: Colors.grey,
            onPressed: () {},
            btnText: "Penjual",
          ),
        ],
      ),
    ),
    blur: 0,
  ).show(context, transitionType: DialogTransitionType.Bubble);
}
