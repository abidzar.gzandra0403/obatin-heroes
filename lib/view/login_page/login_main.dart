import 'package:obatin/importPackage.dart';

int? mainUserType = 1;
bool initPos = true;

class LoginMain extends StatefulWidget {
  @override
  _LoginMainState createState() => _LoginMainState();
}

class _LoginMainState extends State<LoginMain> {
  TextEditingController txtboxEmail = new TextEditingController();
  TextEditingController txtboxPassword = new TextEditingController();
  bool secureText = true;

  createaccountFunc() {
    Navigator.push(
        context,
        FadeRoute(
            page: RegisterMain(
          createNEW: true,
        )));
  }

  makeaccountfromCurrentFunc() {
    Navigator.push(
        context,
        FadeRoute(
            page: RegisterMain(
          createNEW: false,
        )));
  }

  setinitPos() {
    Timer _timer;
    _timer = new Timer(const Duration(milliseconds: 50), () {
      setState(() {
        initPos = false;
      });
    });
  }

  showHide() {
    setState(() {
      secureText = !secureText;
    });
  }

  loginBtnFunc() async {
    // await loadData();
    context.read<UserBloc>().add(LoginUserHeroes(context,
        email: txtboxEmail.text, password: txtboxPassword.text));

    //Jika Offline
    // Preferences.instance.savePrefsName(
    //     name: "Abidzar Ghifari Zandra", email: "abidzar.gzandra0403@gmail.com");
    // Navigator.pushReplacement(
    //     context,
    //     SlideRightRoute(
    //         page: HomeSideButton(
    //       xoffsetEnd: getWidth(context, -0.5),
    //     )));
  }

  forgetBtnFunc() {
    goToPage(context, page: ResetpassMain());
  }

  Future<bool> handleBack() async {
    return await CustomAlertDialog(context,
            title: "Keluar Aplikasi?",
            contentText:
                "Jika kamu ada masalah kesehatan, OBAT-in siap membantu.",
            txtButton1: "Keluar",
            txtButton2: "Batal",
            funcExitApp: () => Navigator.of(context).pop(true)) ??
        false; //if showDialouge had returned null, then return false
  }

  loadData() async {
    postProceedData_listSIP =
        await LocalDatabase().readData(ModelSipData.tableName);
    setState(() {});
  }

  @override
  void initState() {
    setinitPos();
    loadData();
    txtboxEmail.text = "usertest@gmail.com";
    txtboxPassword.text = "12345678";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) =>
          AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness:
                themeID == 1 ? Brightness.dark : Brightness.light),
        child: WillPopScope(
          onWillPop: handleBack,
          child: Scaffold(
              // backgroundColor: bgColor(),
              body: Stack(children: [
            obatinHeader(context, Str().welcome),
            Container(
              height: getHeight(context, 1.8),
              child: SingleChildScrollView(
                child: loginForm(),
              ),
            ),
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              curve: Curves.fastOutSlowIn,
              color: bgColor(),
              height: getWidth(context, 0.2),
            ),
            SwitchLanguageButton(),
            SwitchThemeButton()
          ])),
        ),
      ),
    );
  }

  loginForm() {
    return Center(
      child: AnimatedContainer(
        duration: Duration(milliseconds: 500),
        curve: Curves.fastOutSlowIn,
        width: getWidth(context, 1),
        padding: EdgeInsets.all(getWidth(context, 0.1)),
        margin: EdgeInsets.only(top: getWidth(context, initPos ? 0 : 0.6)),
        decoration: BoxDecoration(
            color: bgColor(),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(getWidth(context, 0.1)),
                topRight: Radius.circular(getWidth(context, 0.1)))),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            CustomTextField(
              hint: Str().email,
              controller: txtboxEmail,
            ),
            PasswordField(
                hint: Str().password,
                controller: txtboxPassword,
                secureText: secureText,
                onIconPress: () {
                  showHide();
                }),
            Padding(
              padding: EdgeInsets.only(top: getHeight(context, 0.02)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {
                      forgetBtnFunc();
                    },
                    child: Text(
                      Str().forgotpassword,
                      style: hnnormal(context,
                          txtSize: getWidth(context, 0.03),
                          txtColor: Colors.grey,
                          txtStyle: FontStyle.italic),
                    ),
                  ),
                  CustomButton(
                      styleId: 1,
                      btnText: Str().login,
                      onPressed: () {
                        mainUserType = 1;
                        loginBtnFunc();
                      })
                ],
              ),
            ),
            Container(
                width: double.infinity,
                height: getHeight(context, 0.25),
                padding: EdgeInsets.all(0),
                child: Column(
                  children: <Widget>[
                    setHSpacing(20),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(
                            width: 0.8,
                            textAlign: TextAlign.center,
                            txt:
                                "Sudah ada akun OBAT-in? yuk lengkapi data untuk membuat akun OBAT-in HEROES mu",
                            color: Colors.grey),
                        TextButton(
                          onPressed: () {
                            makeaccountfromCurrentFunc();
                          },
                          child: CustomText(
                              txt: "Klik Disini", color: accentColor),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        CustomText(
                            txt: Str().nohaveaccount, color: Colors.grey),
                        TextButton(
                          onPressed: () {
                            createaccountFunc();
                          },
                          child: CustomText(
                              txt: Str().createaccount, color: accentColor),
                        )
                      ],
                    ),
                  ],
                )),
          ],
        ),
      ),
    );
  }
}
