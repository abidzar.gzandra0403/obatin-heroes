// import 'dart:async';

// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';
// import 'package:flutter_switch/flutter_switch.dart';
// 
// import 'package:obatin/view/common/asset_icons.dart';
// import 'package:obatin/view/common/asset_images.dart';
// import 'package:obatin/view/common/colorpalette.dart';
// import 'package:obatin/view/common/defaultvalue.dart';
// import 'package:obatin/view/common/textstyle_manager.dart';
// import 'package:obatin/view/common/transitionanimation.dart';
// import 'package:obatin/view/mainscreen.dart';
// import 'package:obatin/view/menu_activity/activity_latest.dart';

// import 'package:obatin/importPackage.dart';

//class RawatinLandingPage extends StatefulWidget {
//   @override
//   _RawatinLandingPageState createState() => _RawatinLandingPageState();
// }

// import 'package:obatin/importPackage.dart';

//class RawatinLandingPageState extends State<RawatinLandingPage> {
//   int currentSlide = 0;
//   int currentPage = 0;
//   bool boolonlocation = false;
//   bool boolvisitpatient = false;
//   bool booltelemedicine = false;
//   bool boolvolunteer = false;
//   final slideshowController = new PageController(
//     initialPage: 0,
//   );
//   final pageController = new PageController();

//   pageTransition() {
//     if (currentPage == 0) {
//       setState(() {});
//       currentPage = 1;
//       pageController.animateToPage(
//         currentPage,
//         duration: Duration(milliseconds: 300),
//         curve: Curves.easeIn,
//       );
//     } else {
//       gotoRawatin(context);
//     }
//   }

//   gotoRawatin(BuildContext context) {
//     
//   }

//   slideshowTimer(bool stopSlideshow) {
//     bool loop = true;
//     final duration = Duration(seconds: 3);

//     Timer.periodic(duration, (timer) {
//       if (stopSlideshow == false) {
//         if (currentPage == 0) {
//           if (currentSlide == 3) {
//             loop = false;
//           } else if (currentSlide == 0) {
//             loop = true;
//           }
//           if (loop == true && currentSlide < 3) {
//             currentSlide++;
//           } else if (loop == false && currentSlide >= 0) {
//             currentSlide = currentSlide - 1;
//           }

//           slideshowController.animateToPage(
//             currentSlide,
//             duration: Duration(milliseconds: 500),
//             curve: Curves.easeOutQuad,
//           );
//         } else {
//           timer.cancel();
//         }
//       } else {
//         timer.cancel();
//       }
//     });
//   }

//   handleBack() async {
//     showDialog(
//       context: context,
//       builder: (context) => new AlertDialog(
//         title: new Text(txt_cancelregist),
//         content: new Text(txt_confirmcancelregist),
//         actions: <Widget>[
//           TextButton(
//             onPressed: () => Navigator.of(context).pop(),
//             child: new Text(txt_no),
//           ),
//           TextButton(
//             onPressed: () {
//               Navigator.of(context).pop();
//               
//               slideshowTimer(true);
//             },
//             child: new Text(txt_yes),
//           ),
//         ],
//       ),
//     );
//   }

//   @override
//   void initState() {
//     slideshowTimer(false);
//   }

//   @override
//   Widget build(BuildContext context) {
//     SystemChrome.setEnabledSystemUIOverlays([
//       SystemUiOverlay.bottom,
//     ]);
//     return WillPopScope(
//       onWillPop: () async {
//         handleBack();
//         return false;
//       },
//       child: Scaffold(
//         body: Container(
//             height: getHeight(context, 1),
//             width: getWidth(context, 1),
//             padding: EdgeInsets.only(top: getHeight(context, 0.15)),
//             decoration: new BoxDecoration(
//               gradient: new LinearGradient(
//                 colors: [
//                   Color.fromARGB(255, 150, 206, 255),
//                   Color.fromARGB(255, 104, 140, 207)
//                 ],
//                 begin: Alignment.topCenter,
//                 end: Alignment.bottomCenter,
//               ),
//             ),
//             child: pageview(1)),
//       ),
//     );
//   }

//   pageview(int pageNum) {
//     return Column(
//       crossAxisAlignment: CrossAxisAlignment.start,
//       children: [
//         Container(
//           margin: EdgeInsets.only(left: getWidth(context, 0.07)),
//           width: getWidth(context, 0.5),
//           child: img_obatin,
//         ),
//         setHSpacing(getWidth(context, 0.08)),
//         Padding(
//           padding: EdgeInsets.only(left: getWidth(context, 0.07)),
//           child: Text(
//             "RAWAT-in",
//             style: hnnormal(context,
//                 txtSize: getWidth(context, 0.08),
//                 txtWeight: FontWeight.bold,
//                 txtColor: Colors.white),
//           ),
//         ),
//         setHSpacing(getWidth(context, 0.02)),
//         Expanded(
//           child: PageView(
//             physics: NeverScrollableScrollPhysics(),
//             controller: pageController,
//             children: [page1(), page2()],
//           ),
//         ),
//         Align(
//           alignment: Alignment.center,
//           child: Padding(
//             padding: const EdgeInsets.all(20.0),
//             child: currentPage == 0
//                 ? button("Aktifkan Layanan RAWAT-in", 1)
//                 : button("Mulai", 2),
//           ),
//         )
//       ],
//     );
//   }

//   page1() {
//     return SingleChildScrollView(
//       child: Column(
//         mainAxisAlignment: MainAxisAlignment.start,
//         children: [
//           Padding(
//             padding: EdgeInsets.symmetric(horizontal: getHeight(context, 0.04)),
//             child: Column(
//               crossAxisAlignment: CrossAxisAlignment.start,
//               children: [
//                 Text(
//                   "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in ",
//                   style: hnnormal(context,
//                       txtSize: getWidth(context, 0.04),
//                       txtWeight: FontWeight.normal,
//                       txtColor: Colors.white),
//                 ),
//                 setHSpacing(getWidth(context, 0.05)),
//               ],
//             ),
//           ),
//           slideshowFeatures(),
//         ],
//       ),
//     );
//   }

//   page2() {
//     return SingleChildScrollView(
//       child: Padding(
//         padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.075)),
//         child: Column(
//           mainAxisAlignment: MainAxisAlignment.start,
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             setHSpacing(getWidth(context, 0.05)),
//             Text(
//               "Aktifkan layanan anda, anda dapat mengaktifkan atau menonaktifkan layanan di setelan akun.",
//               textAlign: TextAlign.left,
//               style: hnnormal(context,
//                   txtSize: getWidth(context, 0.04),
//                   txtWeight: FontWeight.normal,
//                   txtColor: Colors.white),
//             ),
//             setHSpacing(getWidth(context, 0.02)),
//             featuresSetting(1),
//             featuresSetting(2),
//             // featuresSetting(3),
//             featuresSetting(4),
//           ],
//         ),
//       ),
//     );
//   }

//   button(String txt, int btnId) {
//     return SizedBox(
//       width: getWidth(context, 0.7),
//       child: RawMaterialButton(
//         onPressed: () {
//           pageTransition();
//         },
//         padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
//         fillColor: defaultcolor1,
//         splashColor: defaultcolor2,
//         highlightColor: defaultcolor5,
//         shape: StadiumBorder(),
//         child: Text(
//           txt,
//           style: hnnormal(context,
//               txtSize: getWidth(context, 0.04), txtColor: Colors.white),
//         ),
//       ),
//     );
//   }

//   slideshowFeatures() {
//     slideshowItem(int imgId) {
//       return Column(
//         children: [
//           // Align(
//           //   alignment: Alignment.center,
//           //   child: SizedBox(
//           //     height: getWidth(context, 0.5),
//           //     child: imgId == 1
//           //         ? icon_rawatinonlocation
//           //         : imgId == 2
//           //             ? icon_rawatinvisitpatient
//           //             : imgId == 3
//           //                 ? icon_rawatintelemedicine
//           //                 : icon_rawatinvolunteer,
//           //   ),
//           // ),
//           setHSpacing(getWidth(context, 0.05)),
//           Align(
//             alignment: Alignment.center,
//             child: Text(
//               imgId == 1
//                   ? "Layanan On Location"
//                   : imgId == 2
//                       ? "Layanan Visit Patient"
//                       : imgId == 3
//                           ? "Layanan Telemedicine"
//                           : "Layanan Volunteer",
//               textAlign: TextAlign.center,
//               style: hnnormal(context,
//                   txtSize: getWidth(context, 0.06),
//                   txtWeight: FontWeight.w400,
//                   txtColor: Colors.white),
//             ),
//           ),
//         ],
//       );
//     }

//     return SizedBox(
//       height: getWidth(context, 0.70),
//       width: double.infinity,
//       child: PageView(
//         physics: NeverScrollableScrollPhysics(),
//         controller: slideshowController,
//         children: [
//           slideshowItem(1),
//           slideshowItem(2),
//           // slideshowItem(3),
//           slideshowItem(4),
//         ],
//       ),
//     );
//   }

//   featuresSetting(int btnId) {
//     return Container(
//       width: double.infinity,
//       height: getWidth(context, 0.14),
//       margin: EdgeInsets.symmetric(vertical: 5),
//       child: RawMaterialButton(
//           onPressed: () {
//             setState(() {
//               btnId == 1
//                   ? boolonlocation = !boolonlocation
//                   : btnId == 2
//                       ? boolvisitpatient = !boolvisitpatient
//                       : btnId == 3
//                           ? booltelemedicine = !booltelemedicine
//                           : boolvolunteer = !boolvolunteer;
//             });
//           },
//           elevation: 0,
//           highlightElevation: 0,
//           shape:
//               RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
//           fillColor: Colors.white,
//           padding: EdgeInsets.symmetric(
//               horizontal: getWidth(context, 0.04),
//               vertical: getWidth(context, 0.022)),
//           child: Row(
//             children: [
//               SizedBox(
//                 width: 40,
//                 height: 40,
//                 child: btnId == 1
//                     ? icon_rawatinonlocation2
//                     : btnId == 2
//                         ? icon_rawatinvisitpatient2
//                         : btnId == 3
//                             ? icon_rawatintelemedicine2
//                             : icon_rawatinvolunteer2,
//               ),
//               setWSpacing(15),
//               Text(
//                   btnId == 1
//                       ? "On Location"
//                       : btnId == 2
//                           ? "Visit Patient"
//                           : btnId == 3
//                               ? "Telemedicine"
//                               : "Volunteer",
//                   style: hnnormal(context,
//                       txtSize: getWidth(context, 0.034),
//                       txtWeight: FontWeight.w700)),
//               Expanded(
//                 child: Container(),
//               ),
//               FlutterSwitch(
//                 width: 42.0,
//                 height: 22.0,
//                 switchBorder: Border.all(
//                     color: defaultcolor5, width: getWidth(context, 0.008)),
//                 activeColor: defaultcolor5,
//                 activeToggleColor: Colors.white,
//                 inactiveColor: Colors.white,
//                 inactiveToggleColor: defaultcolor5,
//                 toggleSize: 14,
//                 value: btnId == 1
//                     ? boolonlocation
//                     : btnId == 2
//                         ? boolvisitpatient
//                         : btnId == 3
//                             ? booltelemedicine
//                             : boolvolunteer,
//                 padding: 1,
//                 showOnOff: false,
//                 onToggle: (val) {
//                   setState(() {
//                     btnId == 1
//                         ? boolonlocation = val
//                         : btnId == 2
//                             ? boolvisitpatient = val
//                             : btnId == 3
//                                 ? booltelemedicine = val
//                                 : boolvolunteer = val;
//                   });
//                 },
//               ),
//             ],
//           )),
//     );
//   }
// }
