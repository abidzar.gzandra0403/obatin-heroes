import 'package:obatin/importPackage.dart';

@immutable
class NotificationMain extends StatefulWidget {
  @override
  _NotificationMainState createState() => _NotificationMainState();
}

class _NotificationMainState extends State<NotificationMain> {
  List listdata = [1, 2, 3, 1, 3, 2];
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness:
                themeID == 1 ? Brightness.dark : Brightness.light),
        child: Scaffold(
          backgroundColor: bgColor(),
          appBar: CustomAppBar(
            context,
            Str().notification,
            true,
            rightWidget: Container(
                alignment: Alignment.bottomLeft,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomButton(
                        styleId: 4,
                        shapeId: 4,
                        width: 0.1,
                        isize: 0.05,
                        fColor: defaultcolor2,
                        prefixIcon: Icons.mark_chat_read_outlined,
                        onPressed: () {}),
                    CustomButton(
                        styleId: 4,
                        shapeId: 4,
                        isize: 0.05,
                        width: 0.1,
                        fColor: defaultcolor2,
                        prefixIcon: Icons.delete_outline_outlined,
                        onPressed: () {}),
                    // topbutton(Icons.mark_chat_read_outlined),
                    // topbutton(Icons.delete_outline_outlined)
                  ],
                )),
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(txt: Str().unread, bold: true, color: Colors.grey),
                listview(listdata, 3, title: "", itemlength: 2),
                CustomText(
                    toppad: 0.04,
                    txt: Str().alreadyread,
                    bold: true,
                    color: Colors.grey),
                listview(listdata, 3, title: "", itemlength: 6, read: true)
              ],
            ),
          ),
        ));
  }

  listview(List list, int callId,
      {String? title, int? itemlength, bool? read}) {
    return Container(
      child: ListView.builder(
          padding: EdgeInsets.all(0),
          shrinkWrap: true,
          itemCount: itemlength == null
              ? list.length
              : list.length >= itemlength
                  ? itemlength
                  : list.length,
          itemBuilder: (BuildContext context, int index) {
            return notificationCard(context, "Judul Notifikasi", "Informasi",
                "Tanggal", list[index], callId,
                serviceId: 2, read: read != null ? false : true);
          }),
    );
  }
}
