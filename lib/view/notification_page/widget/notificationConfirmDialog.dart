import 'package:obatin/importPackage.dart';

notificationConfirmDialog(BuildContext context, bool accept) {
  btnFunc(int btnId) {
    if (btnId == 1) {
      Navigator.pop(context);
    } else {
      Navigator.pop(context);
    }
  }

  return CustomAlertDialog(
    context,
    title: accept
        ? "Terima permintaan reschedule?"
        : "Tolak permintaan reschedule?",
    contentWidget:
        accept ? ImgAsset.imgRequestAccept : ImgAsset.imgRequestDecline,
    contentText: accept
        ? "Jadwal praktik akan diperbaharui dan pasien akan mendapatkan konfirmasi bahwa permintaan reschedule telah disetujui oleh anda."
        : "Permintaan reschedule akan ditolak dan pasien akan mendapatkan konfirmasi bahwa permintaan reschedule telah ditolak.",
    txtButton1: accept ? "Terima" : "Tolak",
    txtButton2: "Batal",
    funcButton1: () => btnFunc(accept ? 1 : 2),
  );
}
