import 'package:obatin/importPackage.dart';

notificationCard(BuildContext context, String txt1, String txt2, String txt3,
    int menuId, int callId,
    {int? serviceId, bool? read}) {
  final bgcolor = menuId == 1
      ? Colors.blue.shade50
      : menuId == 2
          ? Colors.orange.shade200
          : Colors.green.shade100;
  final iconset = menuId == 1
      ? Icons.notifications_active_outlined
      : menuId == 2
          ? Icons.local_offer_outlined
          : Icons.feedback_outlined;
  final iconcolor = menuId == 1
      ? Colors.blue
      : menuId == 2
          ? Colors.orange.shade800
          : Colors.green.shade800;
  return SizedBox(
    height: getWidth(context, 0.15),
    child: RawMaterialButton(
      onPressed: () {},
      highlightColor: btnHightlightColor(),
      elevation: 0,
      highlightElevation: 0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            padding: const EdgeInsets.all(8),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      CircleAvatar(
                        backgroundColor: bgcolor,
                        child: Icon(
                          iconset,
                          color: iconcolor,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: getWidth(context, 0.03)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              txt: txt1,
                              bold: read,
                              size: 0.037,
                              color: fontColor1(),
                            ),
                            CustomText(
                              txt: txt2,
                              color: Colors.grey,
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                  CustomText(txt: txt3)
                ]),
          )
        ],
      ),
    ),
  );
}
