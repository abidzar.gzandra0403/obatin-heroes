import 'package:obatin/importPackage.dart';

textField1(
    BuildContext context, String hint, TextEditingController txtController,
    {bool? eyeIcon, bool? center}) {
  return SizedBox(
    height: getWidth(context, 0.12),
    child: TextField(
        textAlign: center == true ? TextAlign.center : TextAlign.left,
        controller: txtController,
        style: hnnormal(context, txtSize: getWidth(context, 0.04)),
        decoration: InputDecoration(
          hintText: hint,
          hintStyle: TextStyle(letterSpacing: 0, color: Colors.black),
          enabledBorder:
              UnderlineInputBorder(borderSide: BorderSide(color: Colors.black)),
          focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: Colors.blue.shade600)),
          suffixIcon: eyeIcon == true
              ? Icon(
                  Icons.remove_red_eye,
                )
              : null,
        )),
  );
}

obatinHeader(BuildContext context, var text) {
  return ClipPath(
    // clipper: CurveClipper2(),
    child: AnimatedContainer(
      duration: Duration(milliseconds: 500),
      curve: Curves.fastOutSlowIn,
      width: getWidth(context, 1),
      // height: getWidth(context, 1),
      padding: EdgeInsets.only(top: getHeight(context, 0.1)),
      color: bgColor(),
      child: Padding(
        padding: EdgeInsets.only(
            left: getWidth(context, 0.07), top: getWidth(context, 0.1)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              width: getWidth(context, 0.6),
              child: ImgAsset.imgObatin,
            ),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: EdgeInsets.only(left: 10),
              child: Text(text,
                  style: hnnormal(
                    context,
                    txtSize: getWidth(context, 0.05),
                    txtColor: fontColor2(),
                  )),
            ),
          ],
        ),
      ),
    ),
  );
}
