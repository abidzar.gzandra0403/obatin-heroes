import 'package:obatin/importPackage.dart';

// ignore: must_be_immutable
class TopNavbarButton extends StatefulWidget {
  List<String>? textBtnList;
  PageController? pageController;
  int? currentIndex;
  Color? btnColor;
  TopNavbarButton(
      {@required this.textBtnList,
      @required this.pageController,
      @required this.currentIndex,
      this.btnColor});
  @override
  _TopNavbarButtonState createState() => _TopNavbarButtonState();
}

class _TopNavbarButtonState extends State<TopNavbarButton> {
  @override
  Widget build(BuildContext context) {
    button(BuildContext context, String text, {@required int? index}) {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
        child: AnimatedContainer(
          duration: Duration(milliseconds: 500),
          curve: Curves.fastOutSlowIn,
          decoration: BoxDecoration(
              color: widget.currentIndex == index
                  ? widget.btnColor != null
                      ? widget.btnColor
                      : defaultcolor1
                  : bgColor(),
              borderRadius: BorderRadius.circular(getWidth(context, 0.05))),
          child: CustomButton(
            styleId: 3,
            bColor: Colors.transparent,
            fColor: widget.currentIndex == index ? Colors.white : null,
            onPressed: () => setState(() {
              widget.currentIndex = index;
              widget.pageController!.animateToPage(index!,
                  duration: Duration(milliseconds: 300),
                  curve: Curves.fastOutSlowIn);
            }),
            btnText: text,
          ),
        ),
      );
    }

    return Container(
        alignment: Alignment.center,
        width: getWidth(context, 1),
        margin: EdgeInsets.only(bottom: getWidth(context, 0.03)),
        padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.02)),
        height: getWidth(context, 0.1),
        child: Center(
          child: ListView.builder(
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: widget.textBtnList!.length,
              itemBuilder: (context, index) =>
                  button(context, widget.textBtnList![index], index: index)),
        ));
  }
}
