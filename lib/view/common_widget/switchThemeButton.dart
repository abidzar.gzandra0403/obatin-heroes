import 'package:obatin/importPackage.dart';

class SwitchThemeButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.topRight,
        child: CustomButton(
          mainAxisAlignment: MainAxisAlignment.end,
          bColor: Colors.transparent,
          width: 0.2,
          vermargin: 0.06,
          hormargin: 0.02,
          height: 0.12,
          // iconhorpad: 0.02,
          styleId: 1,
          fColor: fontColor2(),
          fsize: 0.036,
          isize: 0.04,
          iColor: defaultcolor2,
          onPressed: () =>
              context.read<SettingsSwitchBloc>().add(SwitchThemeMode()),
          btnText: themeID == 1 ? "Terang" : "Gelap",
          suffixIcon: themeID == 1
              ? Icons.light_mode_outlined
              : Icons.dark_mode_outlined,
        ));
  }
}
