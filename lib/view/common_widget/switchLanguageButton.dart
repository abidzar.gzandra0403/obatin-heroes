import 'package:obatin/importPackage.dart';

class SwitchLanguageButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.topLeft,
        child: CustomButton(
          mainAxisAlignment: MainAxisAlignment.start,
          bColor: Colors.transparent,
          width: 0.3,
          vermargin: 0.06,
          hormargin: 0.02,
          height: 0.12,
          // iconhorpad: 0.02,
          styleId: 1,
          fColor: fontColor2(),
          fsize: 0.036,
          isize: 0.04,
          iColor: defaultcolor2,
          onPressed: () => context.read<SettingsSwitchBloc>().add(SwitchLang()),
          btnText: langID == 1 ? "Indonesia" : "English",
          prefixIcon: Icons.language_outlined,
        ));
  }
}
