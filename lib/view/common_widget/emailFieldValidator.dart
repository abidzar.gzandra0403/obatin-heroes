import 'package:obatin/importPackage.dart';

class EmailFieldValidator extends StatefulWidget {
  // final String? email;
  // final TextEditingController? emailController;
  // EmailFieldValidator({@required this.email, @required this.emailController});
  final TextEditingController? emailController;
  EmailFieldValidator({@required this.emailController});
  @override
  _EmailFieldValidatorState createState() => _EmailFieldValidatorState();
}

class _EmailFieldValidatorState extends State<EmailFieldValidator> {
  bool isEmailValid = false;

  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      hint: "Email",
      suffixIcon: widget.emailController!.text.isEmpty
          ? null
          : isEmailValid
              ? Icons.check_circle_outline
              : Icons.block_outlined,
      suffixIconColor: isEmailValid ? Colors.green : Colors.red.shade300,
      onChanged: () {
        setState(() {
          isEmailValid = emailValidCheck(widget.emailController!.text);
        });
      },
      keyboardtype: TextInputType.emailAddress,
      controller: widget.emailController,
    );
  }
}
