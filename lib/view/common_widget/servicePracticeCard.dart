import 'package:obatin/importPackage.dart';

class ServicePracticeCard extends StatelessWidget {
  final int? showOnlySipId;
  final bool? switchToggleOnly;
  final int? styleId;
  ServicePracticeCard(
      {this.showOnlySipId, this.switchToggleOnly, @required this.styleId});
  @override
  Widget build(BuildContext context) {
    return (showOnlySipId != null)
        ? _contentService(
            context,
            postProceedData_listSIP[showOnlySipId!]["nosip"],
            showOnlySipId! + 1,
            styleId: styleId)
        : BlocBuilder<SQFLITESipSikBloc, SQFLITESipSikState>(
            builder: (context, state) => ListView.builder(
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: postProceedData_listSIP.length,
              itemBuilder: (context, index) => _contentService(
                  context, postProceedData_listSIP[index]["nosip"], index + 1,
                  styleId: styleId),
            ),
          );
  }

  _contentService(BuildContext context, String sip, int callId,
      {@required int? styleId}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.012)),
      decoration: styleId == 1
          ? BoxDecoration(
              color: bgColorDarken(),
              borderRadius: BorderRadius.circular(10),
              border: Border.all(
                  width: getWidth(context, 0.005), color: borderBlueColor()))
          : BoxDecoration(color: Colors.transparent),
      padding: EdgeInsets.all(getWidth(context, 0.02)),
      child: showOnlySipId != null
          ? Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  children: [
                    Align(
                      alignment: Alignment.centerRight,
                      child: CustomButton(
                        width: 0.1,
                        height: 0.1,
                        styleId: 3,
                        onPressed: () {
                          Navigator.pop(context);
                          Navigator.push(
                              context, SlideRightRoute(page: ProfileService()));
                        },
                        fsize: 0.034,
                        prefixIcon: Icons.settings_outlined,
                      ),
                    ),
                    Align(
                      alignment: Alignment.centerLeft,
                      child: CustomText(
                          txt: "Surat Izin. " + sip,
                          size: 0.035,
                          color: fontColor1(),
                          bold: true),
                    ),
                  ],
                ),
                _contentExpanded(context, callId: callId, sip: sip)
              ],
            )
          : ExpandablePanel(
              theme: ExpandableThemeData(iconColor: defaultcolor2),
              header: CustomText(
                  txt: sip, size: 0.035, color: fontColor1(), bold: true),
              collapsed: _contentCollapsed(callId: callId),
              expanded: _contentExpanded(context, callId: callId, sip: sip)),
    );
  }

  _contentCollapsed({@required int? callId}) {
    return BlocBuilder<ServiceBoolBloc, ServiceBoolState>(
        builder: (context, state) => Row(
              children: [
                CustomText(
                  txt: Str().onlocation + " ",
                  color: (callId == 1
                              ? onloc1
                              : callId == 2
                                  ? onloc2
                                  : onloc3) ==
                          true
                      ? defaultcolor2
                      : fontColor1(),
                ),
                CustomText(
                  txt: "| " + Str().visitpatient + " ",
                  color: (callId == 1
                              ? visit1
                              : callId == 2
                                  ? visit2
                                  : visit3) ==
                          true
                      ? defaultcolor2
                      : fontColor1(),
                ),
                if (mainUserType == 1)
                  CustomText(
                    txt: "| " + Str().telemedicine + " ",
                    color: (callId == 1
                                ? tele1
                                : callId == 2
                                    ? tele2
                                    : tele3) ==
                            true
                        ? defaultcolor2
                        : fontColor1(),
                  ),
                CustomText(
                  txt: "| " + Str().volunteer,
                  color: (callId == 1
                              ? (volunonloc1 || volunvisit1 || voluntele1)
                              : callId == 2
                                  ? (volunonloc2 || volunvisit2 || voluntele2)
                                  : (volunonloc3 ||
                                      volunvisit3 ||
                                      voluntele3)) ==
                          true
                      ? defaultcolor2
                      : fontColor1(),
                )
              ],
            ));
  }

  _contentExpanded(BuildContext context,
      {@required int? callId, @required String? sip}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          txt: Str().standardservice,
          bold: true,
          color: fontColor5(),
          toppad: 0.04,
        ),
        _contentTile(
            context,
            Str().onlocation,
            callId == 1
                ? 11
                : callId == 2
                    ? 12
                    : 13,
            sip: sip),
        _contentTile(
            context,
            Str().visitpatient,
            callId == 1
                ? 21
                : callId == 2
                    ? 22
                    : 23,
            sip: sip),
        if (mainUserType == 1)
          _contentTile(
              context,
              Str().telemedicine,
              callId == 1
                  ? 31
                  : callId == 2
                      ? 32
                      : 33,
              sip: sip),
        if (switchToggleOnly == null || switchToggleOnly == false)
          Column(
            children: [
              CustomButton(
                styleId: 3,
                shapeId: 2,
                onPressed: () => Navigator.push(
                    context,
                    SlideRightRoute(
                        page: ProfileServiceEdit(
                            noSip: sip,
                            serviceType: 1,
                            editId: 1,
                            allserviceTitle: Str().setrateallstandardservice))),
                btnText: Str().setrateallstandardservice,
                fsize: 0.034,
              ),
              CustomButton(
                styleId: 3,
                shapeId: 2,
                onPressed: () => Navigator.push(
                    context,
                    SlideRightRoute(
                        page: ProfileServiceEdit(
                      noSip: sip,
                      serviceType: 1,
                      editId: 2,
                      allserviceTitle: Str().setworktimeallstandardservice,
                    ))),
                btnText: Str().setworktimeallstandardservice,
                fsize: 0.034,
              ),
            ],
          ),
        Divider(
          height: getWidth(context, 0.08),
          thickness: 1.5,
        ),
        CustomText(
          txt: Str().volunteerservice,
          bold: true,
          color: fontColor5(),
          toppad: 0.04,
        ),
        _contentTile(
            context,
            Str().onlocation,
            callId == 1
                ? 41
                : callId == 2
                    ? 42
                    : 43,
            sip: sip,
            sukarela: true),
        _contentTile(
            context,
            Str().visitpatient,
            callId == 1
                ? 51
                : callId == 2
                    ? 52
                    : 53,
            sip: sip,
            sukarela: true),
        if (mainUserType == 1)
          _contentTile(
              context,
              Str().telemedicine,
              callId == 1
                  ? 61
                  : callId == 2
                      ? 62
                      : 63,
              sip: sip,
              sukarela: true),
        if (switchToggleOnly == null || switchToggleOnly == false)
          CustomButton(
            styleId: 3,
            shapeId: 2,
            onPressed: () => Navigator.push(
                context,
                SlideRightRoute(
                    page: ProfileServiceEdit(
                  noSip: sip,
                  serviceType: 2,
                  editId: 2,
                  allserviceTitle: Str().setworktimeallstandardvolunteer,
                ))),
            btnText: Str().setworktimeallstandardvolunteer,
            fsize: 0.034,
          )
      ],
    );
  }

  _contentTile(BuildContext context, String service, int boolid,
      {@required String? sip, bool? sukarela}) {
    initValue<bool>() {
      if (boolid == 11) {
        return onloc1;
      } else if (boolid == 12) {
        return onloc2;
      } else if (boolid == 13) {
        return onloc3;
      } else if (boolid == 21) {
        return visit1;
      } else if (boolid == 22) {
        return visit2;
      } else if (boolid == 23) {
        return visit3;
      } else if (boolid == 31) {
        return tele1;
      } else if (boolid == 32) {
        return tele2;
      } else if (boolid == 33) {
        return tele3;
      } else if (boolid == 41) {
        return volunonloc1;
      } else if (boolid == 42) {
        return volunonloc2;
      } else if (boolid == 43) {
        return volunonloc3;
      } else if (boolid == 51) {
        return volunvisit1;
      } else if (boolid == 52) {
        return volunvisit2;
      } else if (boolid == 53) {
        return volunvisit3;
      } else if (boolid == 61) {
        return voluntele1;
      } else if (boolid == 62) {
        return voluntele2;
      } else {
        return voluntele3;
      }
    }

    ontoggle() {
      if (boolid == 11) {
        context.read<ServiceBoolBloc>().add(SetOnloc1());
      } else if (boolid == 12) {
        context.read<ServiceBoolBloc>().add(SetOnloc2());
      } else if (boolid == 13) {
        context.read<ServiceBoolBloc>().add(SetOnloc3());
      } else if (boolid == 21) {
        context.read<ServiceBoolBloc>().add(SetVisit1());
      } else if (boolid == 22) {
        context.read<ServiceBoolBloc>().add(SetVisit2());
      } else if (boolid == 23) {
        context.read<ServiceBoolBloc>().add(SetVisit3());
      } else if (boolid == 31) {
        context.read<ServiceBoolBloc>().add(SetTele1());
      } else if (boolid == 32) {
        context.read<ServiceBoolBloc>().add(SetTele2());
      } else if (boolid == 33) {
        context.read<ServiceBoolBloc>().add(SetTele3());
      } else if (boolid == 41) {
        context.read<ServiceBoolBloc>().add(SetVolunOnloc1());
      } else if (boolid == 42) {
        context.read<ServiceBoolBloc>().add(SetVolunOnloc2());
      } else if (boolid == 43) {
        context.read<ServiceBoolBloc>().add(SetVolunOnloc3());
      } else if (boolid == 51) {
        context.read<ServiceBoolBloc>().add(SetVolunVisit1());
      } else if (boolid == 52) {
        context.read<ServiceBoolBloc>().add(SetVolunVisit2());
      } else if (boolid == 53) {
        context.read<ServiceBoolBloc>().add(SetVolunVisit3());
      } else if (boolid == 61) {
        context.read<ServiceBoolBloc>().add(SetVolunTele1());
      } else if (boolid == 62) {
        context.read<ServiceBoolBloc>().add(SetVolunTele2());
      } else {
        context.read<ServiceBoolBloc>().add(SetVolunTele3());
      }
    }

    return Padding(
        padding: EdgeInsets.all(getWidth(context, 0.02)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  txt: service,
                  color: fontColor1(),
                  bold: true,
                ),
                Row(
                  children: [
                    if (switchToggleOnly == null || switchToggleOnly == false)
                      CustomButton(
                        styleId: 4,
                        width: 0.1,
                        height: 0.1,
                        onPressed: () => Navigator.push(
                            context,
                            SlideRightRoute(
                                page: ProfileServiceEdit(
                              service: service,
                              noSip: sip,
                              editId: 0,
                              serviceType:
                                  (sukarela != null && sukarela == true)
                                      ? 2
                                      : 1,
                            ))),
                        prefixIcon: Icons.edit_outlined,
                      ),
                    BlocBuilder<ServiceBoolBloc, ServiceBoolState>(
                      builder: (context, state) => FlutterSwitch(
                        width: getWidth(context, 0.1),
                        height: getWidth(context, 0.05),
                        valueFontSize: 0,
                        toggleSize: getWidth(context, 0.05),
                        value: initValue(),
                        borderRadius: 30.0,
                        padding: getWidth(context, 0.004),
                        activeColor: defaultcolor1,
                        onToggle: (val) => ontoggle(),
                      ),
                    ),
                  ],
                )
              ],
            ),
            _contentOpenTime(context,
                sukarela: sukarela != null ? sukarela : false,
                tarif: Str().price + " " + "Rp." + "60.000",
                txt1: "10:00 - 19:00",
                txt2: "11:00 - 15:00",
                txt4: "08:00 - 17:00")
          ],
        ));
  }

// ignore: unused_element
  _contentOpenTime(BuildContext context,
      {@required String? tarif,
      bool? sukarela,
      String? txt1,
      String? txt2,
      String? txt3,
      String? txt4,
      String? txt5,
      String? txt6,
      String? txt7}) {
    return Padding(
      padding: EdgeInsets.symmetric(
        vertical: getWidth(context, 0),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            txt: sukarela! ? Str().free : tarif!,
            bold: true,
            color: Colors.green,
          ),
          setHSpacing(getWidth(context, 0.01)),
          if (txt1 != null)
            CustomText(txt: Str().monday + ", " + txt1, size: 0.036),
          if (txt2 != null)
            CustomText(txt: Str().tuesday + ", " + txt2, size: 0.036),
          if (txt3 != null)
            CustomText(txt: Str().wednesday + ", " + txt3, size: 0.036),
          if (txt4 != null)
            CustomText(txt: Str().thursday + ", " + txt4, size: 0.036),
          if (txt5 != null)
            CustomText(txt: Str().friday + ", " + txt5, size: 0.036),
          if (txt6 != null)
            CustomText(txt: Str().saturday + ", " + txt6, size: 0.036),
          if (txt7 != null)
            CustomText(txt: Str().sunday + ", " + txt7, size: 0.036),
        ],
      ),
    );
  }
}
