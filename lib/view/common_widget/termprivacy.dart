import 'package:obatin/importPackage.dart';

class TermPrivacyView extends StatefulWidget {
  final bool? showAgreementBtn;
  TermPrivacyView({this.showAgreementBtn});
  @override
  _TermPrivacyViewState createState() => _TermPrivacyViewState();
}

class _TermPrivacyViewState extends State<TermPrivacyView> {
  Future<bool> actionButton(int funcId) async {
    return await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            backgroundColor: bgColor(),
            title: CustomText(
                txt: funcId == 2
                    ? "Dengan menolak syarat & ketentuan anda tidak dapat masuk dan data registrasi akan terhapus. Apakah anda yakin?"
                    : "Dengan menyetujui syarat & ketentuan, berarti anda menyetujui seluruh kebijakan dan tunduk terhadap semua layanan OBAT-in yang dilindungi oleh undang-undang berlaku",
                color: fontColor1(),
                size: 0.038),
            actions: <Widget>[
              CustomButton(
                  styleId: 4,
                  width: 0.2,
                  fColor: defaultcolor1,
                  btnText: Str().no,
                  onPressed: () => Navigator.pop(context)),
              CustomButton(
                  styleId: 4,
                  width: 0.2,
                  btnText: Str().yes,
                  onPressed: () {
                    initPos = true;
                    Navigator.pop(context);
                    Navigator.pop(context);
                    funcId == 1
                        ? Navigator.pushReplacement(
                            context, SlideRightRoute(page: MainScreen()))
                        : Navigator.pushReplacement(
                            context, SlideRightRoute(page: LoginMain()));
                  }),
            ],
          ),
        ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, "", true),
      backgroundColor: bgColor(),
      body: Padding(
        padding: EdgeInsets.all(defaultPaddingContent(context)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: getWidth(context, 0.4),
              child: ImgAsset.imgObatin,
            ),
            setHSpacing(getWidth(context, 0.05)),
            CustomText(
                txt: "Syarat & Ketentuan OBAT-in",
                bold: true,
                size: 0.05,
                color: fontColor1()),
            setHSpacing(getWidth(context, 0.05)),
            Expanded(
                child: SingleChildScrollView(
              child: CustomText(
                  txt: loremIpsum,
                  size: 0.037,
                  textAlign: TextAlign.justify,
                  color: fontColor1()),
            )),
            if (widget.showAgreementBtn == null ||
                widget.showAgreementBtn == true)
              Container(
                padding: EdgeInsets.only(top: getWidth(context, 0.02)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomButton(
                        styleId: 1,
                        btnText: Str().decline,
                        bColor: red,
                        onPressed: () => actionButton(2)),
                    CustomButton(
                        styleId: 1,
                        btnText: Str().accept,
                        onPressed: () => actionButton(1))
                  ],
                ),
              )
          ],
        ),
      ),
    );
  }
}
