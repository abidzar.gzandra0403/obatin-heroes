import 'package:obatin/importPackage.dart';

inputborderStyle(Color color, double circular, double w) {
  return OutlineInputBorder(
      borderSide: BorderSide(width: w, color: color),
      borderRadius: BorderRadius.circular(circular));
}

textFieldStyle1(BuildContext context, String hint, {IconData? icons}) {
  return TextField(
      style: hnnormal(context,
          txtColor: fontColor1(), txtSize: getWidth(context, 0.035)),
      decoration: InputDecoration(
          fillColor: btnColor(),
          prefixIcon: icons == null
              ? null
              : Icon(
                  icons,
                  color: fontColor1(),
                ),
          hintText: hint,
          hintStyle: hnnormal(context,
              txtColor: hintColor(), txtSize: getWidth(context, 0.035)),
          contentPadding: EdgeInsets.only(left: getWidth(context, 0.02)),
          enabledBorder: inputborderStyle(Colors.grey, 10, 1),
          focusedBorder: inputborderStyle(defaultcolor4, 10, 1)));
}

textFieldStyle2(BuildContext context, String hint, {IconData? icons}) {
  return Container(
    padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
    decoration: BoxDecoration(
        color: themeID == 1 ? Colors.black12 : Colors.white12,
        borderRadius: BorderRadius.circular(getWidth(context, 0.05))),
    child: TextFormField(
        minLines: 1,
        maxLines: 6,
        style: hnnormal(context,
            txtColor: fontColor1(), txtSize: getWidth(context, 0.04)),
        decoration: InputDecoration(
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          prefixIcon: icons == null
              ? null
              : Icon(
                  icons,
                  color: fontColor1(),
                ),
          hintText: hint,
          hintStyle: hnnormal(context,
              txtColor: hintColor(), txtSize: getWidth(context, 0.04)),
          contentPadding:
              EdgeInsets.symmetric(horizontal: getWidth(context, 0.03)),
        )),
  );
}
