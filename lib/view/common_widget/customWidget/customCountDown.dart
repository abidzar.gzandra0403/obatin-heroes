import 'package:obatin/importPackage.dart';

Timer? countDownTimerState;
// ignore: must_be_immutable

class CustomCountDown extends StatefulWidget {
  final int? minutes;
  final int? seconds;
  final double? countTextSize;
  final Widget? widgetAfterCount;
  final VoidCallback? onEndCount;
  CustomCountDown(
      {@required this.onEndCount,
      this.widgetAfterCount,
      this.countTextSize,
      this.minutes,
      this.seconds});
  @override
  _CustomCountDownState createState() => _CustomCountDownState();
}

class _CustomCountDownState extends State<CustomCountDown> {
  Duration duration = Duration();
  bool? setButton;
  bool showButton = false;
  @override
  void initState() {
    setButton = widget.widgetAfterCount != null ? true : false;
    duration = widget.minutes != null
        ? Duration(minutes: widget.minutes!)
        : Duration(seconds: widget.seconds!);
    startTimer();
    super.initState();
  }

  void addTimer() {
    final subSecond = 1;
    setState(() {
      final seconds = duration.inSeconds - subSecond;
      if (seconds < 0) {
        if (setButton == true) {
          setState(() {
            showButton = true;
          });
        }
        countDownTimerState?.cancel();
      } else {
        duration = Duration(seconds: seconds);
      }
    });
  }

  void startTimer() {
    countDownTimerState =
        Timer.periodic(Duration(seconds: 1), (_) => addTimer());
  }

  @override
  Widget build(BuildContext context) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    final minutes = twoDigits(duration.inMinutes.remainder(60));
    final seconds = twoDigits(duration.inSeconds.remainder(60));
    return showButton == true
        ? widget.widgetAfterCount!
        : Center(
            child: CustomText(
              txt: minutes + ":" + seconds,
              size: widget.countTextSize != null ? widget.countTextSize : 0.036,
            ),
          );
  }
}
