import 'package:obatin/importPackage.dart';

class CustomDynamicDropDown extends StatelessWidget {
  final BuildContext context;
  final VoidCallback? onTapFunction;
  final bool? disableOnTap;
  final String? dialogTitle;
  final String? hint;
  final double? width;
  final String? value;
  final Widget? contentWidget;
  final int? maxlines;
  final double? height;
  final bool? showIfCondition;
  final String? ifConditionFalseText;
  CustomDynamicDropDown(
    this.context, {
    this.disableOnTap,
    @required this.dialogTitle,
    @required this.hint,
    @required this.contentWidget,
    this.onTapFunction,
    this.value,
    this.width,
    this.maxlines,
    this.height,
    @required this.showIfCondition,
    this.ifConditionFalseText,
  });

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (disableOnTap == null || disableOnTap == false) {
          if (onTapFunction != null) {
            onTapFunction!();
          }
          showIfCondition == true
              ? CustomAlertDialog(context,
                  title: dialogTitle!,
                  contentWidgetHeight: height != null ? height! : null,
                  contentWidget:
                      contentWidget != null ? contentWidget! : SizedBox())
              : CustomBottomDialog(context,
                  txt: ifConditionFalseText != null
                      ? ifConditionFalseText
                      : "Error");
        }
      },
      child: Container(
        width: width != null ? getWidth(context, width!) : null,
        margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
        child: Stack(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: getWidth(context, 0.125)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  Expanded(
                      // width: getWidth(context, 0.7),
                      child: Padding(
                          padding:
                              EdgeInsets.only(left: getWidth(context, 0.03)),
                          child: CustomText(
                              txt: value != null ? value! : '',
                              size: 0.04,
                              color:
                                  (disableOnTap != null && disableOnTap == true)
                                      ? Colors.grey
                                      : fontColor1()))),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getWidth(context, 0.02)),
                    child: Icon(Icons.keyboard_arrow_down_rounded,
                        color: (disableOnTap != null && disableOnTap == true)
                            ? Colors.grey
                            : fontColor1()),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: getWidth(context, 0.02)),
              padding:
                  EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
              decoration: BoxDecoration(color: bgColor()),
              child: CustomText(
                  txt: hint != null ? hint! : "",
                  bold: true,
                  color: defaultcolor5),
            ),
          ],
        ),
      ),
    );
  }
}
