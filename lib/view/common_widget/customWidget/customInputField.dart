import 'package:obatin/importPackage.dart';

class CustomInputField extends StatelessWidget {
  final String? hint;
  final IconData? icon;
  final int? maxlines;
  final double? width;
  final TextInputType? keyboardtype;
  final VoidCallback? onPressed;
  CustomInputField(
      {@required this.onPressed,
      this.hint,
      this.icon,
      this.maxlines,
      this.width,
      this.keyboardtype});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width != null ? getWidth(context, width!) : double.infinity,
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
      child: Stack(
        children: [
          Container(
              constraints: BoxConstraints(minHeight: getWidth(context, 0.12)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: TextField(
                keyboardType:
                    keyboardtype != null ? keyboardtype! : TextInputType.text,
                readOnly: true,
                minLines: 1,
                maxLines: maxlines != null ? maxlines : 1,
                textInputAction: TextInputAction.done,
                style: hnnormal(context, txtSize: getWidth(context, 0.04)),
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(getWidth(context, 0.03)),
                  suffixIcon: Container(
                      width: getWidth(context, 0.15),
                      // height: getWidth(context, 0.1),
                      decoration: BoxDecoration(
                          color: defaultcolor2,
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(6),
                              bottomRight: Radius.circular(6))),
                      child: RawMaterialButton(
                        onPressed: () => onPressed!(),
                        child: Icon(
                          icon,
                          size: getWidth(context, 0.06),
                          color: Colors.white,
                        ),
                      )),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              )),
          Container(
            margin: EdgeInsets.only(left: getWidth(context, 0.02)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
            decoration: BoxDecoration(color: bgColor()),
            child: CustomText(
                txt: hint != null ? hint! : "",
                bold: true,
                color: defaultcolor5),
          ),
        ],
      ),
    );
  }
}
