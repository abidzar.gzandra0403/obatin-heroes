import 'package:flutter_switch/flutter_switch.dart';
import 'package:obatin/importPackage.dart';

class CustomSwitch extends StatelessWidget {
  final VoidCallback? onToggle;
  final bool? value;
  final double? topmargin;
  final double? bottommargin;
  final double? leftmargin;
  final double? rightmargin;
  CustomSwitch(
      {@required this.onToggle,
      @required this.value,
      this.topmargin,
      this.bottommargin,
      this.leftmargin,
      this.rightmargin});
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        top: getWidth(context, topmargin != null ? topmargin! : 0),
        bottom: getWidth(context, bottommargin != null ? bottommargin! : 0),
        left: getWidth(context, leftmargin != null ? leftmargin! : 0),
        right: getWidth(context, rightmargin != null ? rightmargin! : 0),
      ),
      child: FlutterSwitch(
          height: getWidth(context, 0.07),
          padding: getWidth(context, 0.005),
          toggleSize: getWidth(context, 0.045),
          width: getWidth(context, 0.13),
          activeColor: defaultcolor4,
          activeToggleColor: bgColor(),
          activeSwitchBorder:
              Border.all(width: getWidth(context, 0.01), color: defaultcolor4),
          inactiveColor: bgColor(),
          inactiveToggleColor: defaultcolor4,
          inactiveSwitchBorder:
              Border.all(width: getWidth(context, 0.01), color: defaultcolor4),
          value: value!,
          onToggle: (value) => onToggle!()),
    );
  }
}
