import 'package:flutter/material.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class CustomDropDown extends StatelessWidget {
  final String? hint;
  final double? width;
  final int? maxlines;
  CustomDropDown({this.hint, this.width, this.maxlines});
  @override
  Widget build(BuildContext context) {
    return Container(
      width: width != null ? getWidth(context, width!) : null,
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
      child: Stack(
        children: [
          Container(
              constraints: BoxConstraints(minHeight: getWidth(context, 0.12)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: GestureDetector(
                onTap: () {
                  print("aho");
                },
                child: Row(
                  children: [
                    Expanded(
                        // width: getWidth(context, 0.7),
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: getWidth(context, 0.03)),
                            child: CustomText(txt: ""))),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getWidth(context, 0.02)),
                      child: Icon(Icons.keyboard_arrow_down_rounded),
                    )
                  ],
                ),
              )),
          Container(
            margin: EdgeInsets.only(left: getWidth(context, 0.02)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
            decoration: BoxDecoration(color: bgColor()),
            child: CustomText(
                txt: hint != null ? hint! : "",
                bold: true,
                color: defaultcolor5),
          ),
        ],
      ),
    );
  }
}
