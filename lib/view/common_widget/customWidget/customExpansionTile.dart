import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names

class CustomExpansionTile extends StatefulWidget {
  final String? title;
  final bool? bold;
  final Color? backgroundColor;
  final Color? borderColor;
  final double? borderSize;
  final Widget? headerWidget;
  final List<Widget>? children;
  final bool? hideIcon;
  CustomExpansionTile(
      {this.title,
      this.bold,
      this.backgroundColor,
      this.borderColor,
      this.borderSize,
      this.headerWidget,
      this.children,
      this.hideIcon});

  @override
  _CustomExpansionTileState createState() => _CustomExpansionTileState();
}

class _CustomExpansionTileState extends State<CustomExpansionTile> {
  bool expanded = false;
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: widget.backgroundColor != null
              ? widget.backgroundColor!
              : bgColorDarken(),
          borderRadius: BorderRadius.circular(getWidth(context, 0.03)),
          border: Border.all(
              width: getWidth(context,
                  widget.borderSize != null ? widget.borderSize! : 0.005),
              color: widget.borderColor != null
                  ? widget.borderColor!
                  : borderBlueColor())),
      child: Theme(
        data: Theme.of(context).copyWith(
            dividerColor: Colors.transparent,
            highlightColor: Colors.transparent,
            splashColor: Colors.transparent),
        child: ExpansionTile(
          onExpansionChanged: (bool isExpanded) =>
              setState(() => expanded = isExpanded),
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          collapsedIconColor: fontColor1(),
          trailing: (widget.hideIcon != null && widget.hideIcon == true)
              ? Icon(Icons.check, color: Colors.transparent)
              : Icon(expanded
                  ? Icons.arrow_drop_up_rounded
                  : Icons.arrow_drop_down_rounded),
          title: widget.title != null
              ? CustomText(
                  txt: widget.title,
                  size: 0.035,
                  bold: widget.bold != null ? widget.bold : false,
                  color: fontColor1())
              : widget.headerWidget != null
                  ? widget.headerWidget!
                  : SizedBox(),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: widget.children!,
        ),
      ),
    );
  }
}
