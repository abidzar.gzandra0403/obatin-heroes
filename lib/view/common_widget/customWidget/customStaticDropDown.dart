import 'package:obatin/importPackage.dart';

// data id
// 0 = semua dokter
// 1 = bidan
// 2 = perawat
// 3 = fisioterapis
// 4 = ahli gizi
// 5 = terapis wicara
// 6 = akupunturis
// 7 = psikolog
// 8 = lainnya
// 9 = semua nakes

// ignore: must_be_immutable
// ignore: camel_case_types

class CustomStaticDropdown extends StatefulWidget {
  final List? itemList;
  final String? title;
  final Function(String value)? onSelectedItem;
  final bool? enabled;
  final int? styleId;
  final double? width;
  final double? height;
  CustomStaticDropdown(
      {@required this.itemList,
      @required this.title,
      this.enabled,
      @required this.onSelectedItem,
      this.styleId,
      this.width,
      this.height});
  @override
  _CustomStaticDropdownState createState() => _CustomStaticDropdownState();
}

// ignore: must_be_immutable
// ignore: camel_case_types

class _CustomStaticDropdownState extends State<CustomStaticDropdown> {
  String initText = '';
  int initStyleId = 1;
  @override
  void initState() {
    super.initState();
    initText =
        (widget.styleId != null && widget.styleId == 2) ? '' : widget.title!;
    initStyleId = widget.styleId != null ? widget.styleId! : 1;
  }

  specialistFilter(BuildContext context) {
    List data = widget.itemList!;
    list() {
      return Container(
        child: ListView.builder(
            padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.03)),
            itemCount: data.length,
            itemBuilder: (BuildContext context, int index) {
              return RawMaterialButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                padding: EdgeInsets.only(
                    left: getWidth(context, 0.03),
                    right: getWidth(context, 0.03),
                    top: getWidth(context, 0.03)),
                onPressed: () {
                  setState(() {
                    initText = data[index];
                  });
                  widget.onSelectedItem!(data[index]);
                  Navigator.pop(context);
                },
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        Icon(
                          Icons.circle_rounded,
                          size: getWidth(context, 0.025),
                          color: defaultcolor2,
                        ),
                        setWSpacing(getWidth(context, 0.02)),
                        Expanded(
                            child: CustomText(txt: data[index], size: 0.037))
                      ],
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                  ],
                ),
              );
            }),
      );
    }

    return NAlertDialog(
      dialogStyle: DialogStyle(titleDivider: true, backgroundColor: btnColor()),
      content: Container(
        height: getWidth(context, 1.4),
        width: getWidth(context, 0.7),
        margin: EdgeInsets.only(top: getWidth(context, 0.04)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              widget.title!,
              textAlign: TextAlign.center,
              style: hnnormal(context,
                  txtColor: fontColor1(),
                  txtWeight: FontWeight.bold,
                  txtSize: getWidth(context, 0.04)),
            ),
            Expanded(
              child: list(),
            )
          ],
        ),
      ),
      blur: 0,
    ).show(context,
        transitionType: DialogTransitionType.BottomToTop,
        transitionDuration: Duration(milliseconds: 200));
  }

  style1() {
    return CustomButton(
      styleId: 3,
      shapeId: 2,
      onPressed: () => specialistFilter(context),
      textWidth: 0.7,
      btnText: initText,
      suffixIcon: Icons.arrow_drop_down,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }

  style2() {
    return GestureDetector(
      onTap: () => specialistFilter(context),
      child: Container(
        width: widget.width != null ? getWidth(context, widget.width!) : null,
        margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
        child: Stack(
          children: [
            Container(
              constraints: BoxConstraints(minHeight: getWidth(context, 0.125)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                children: [
                  Expanded(
                      // width: getWidth(context, 0.7),
                      child: Padding(
                          padding:
                              EdgeInsets.only(left: getWidth(context, 0.03)),
                          child: CustomText(
                              txt: initText,
                              overflow: TextOverflow.ellipsis,
                              size: 0.04,
                              color: (widget.enabled != null &&
                                      widget.enabled == true)
                                  ? Colors.grey
                                  : fontColor1()))),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getWidth(context, 0.02)),
                    child: Icon(Icons.keyboard_arrow_down_rounded,
                        color:
                            (widget.enabled != null && widget.enabled == true)
                                ? Colors.grey
                                : fontColor1()),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: getWidth(context, 0.02)),
              padding:
                  EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
              decoration: BoxDecoration(color: bgColor()),
              child: CustomText(
                  txt: widget.title != null ? widget.title! : "",
                  bold: true,
                  color: defaultcolor5),
            ),
          ],
        ),
      ),
    );
  }

  styleSelect() {
    if (initStyleId == 1) {
      return style1();
    } else
      return style2();
  }

  @override
  Widget build(BuildContext context) {
    return styleSelect();
  }
}
