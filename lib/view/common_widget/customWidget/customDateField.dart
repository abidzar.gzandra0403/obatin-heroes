import 'package:obatin/importPackage.dart';
import 'package:intl/intl.dart';

// ignore: must_be_immutable
class CustomDateField extends StatelessWidget {
  TextEditingController? textEditingController;
  bool? maxDateIsNow;
  String? hint;
  int? styleId;
  CustomDateField(
      {@required this.textEditingController,
      @required this.hint,
      this.styleId,
      this.maxDateIsNow});
  @override
  Widget build(BuildContext context) {
    return CustomTextField(
      controller: textEditingController,
      styleId: styleId != null ? styleId : 1,
      readonly: true,
      onTap: () {
        DatePicker.showDatePicker(context,
            showTitleActions: true,
            minTime: DateTime(1940, 1, 1),
            maxTime: (maxDateIsNow != null && maxDateIsNow!)
                ? DateTime.now()
                : null, onConfirm: (date) {
          textEditingController!.text = DateFormat('yyyy-MM-dd').format(date);
        },
            theme: DatePickerTheme(
                containerHeight: getWidth(context, 0.9),
                itemHeight: getWidth(context, 0.25),
                headerColor: bgColor(),
                backgroundColor: bgColor(),
                itemStyle: TextStyle(
                    color: fontColor1(), fontSize: getWidth(context, 0.036)),
                doneStyle: TextStyle(
                    color: defaultcolor1, fontSize: getWidth(context, 0.038)),
                cancelStyle: TextStyle(
                    color: Colors.red, fontSize: getWidth(context, 0.038))),
            currentTime: DateTime.now().subtract(new Duration(days: 5475)),
            locale: LocaleType.id);
      },
      hint: hint!,
      suffixIcon: Icons.date_range_outlined,
    );
  }
}
