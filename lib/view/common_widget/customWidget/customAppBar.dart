import 'package:obatin/importPackage.dart';
//Button ID
// 1 = goto rawatin settings

buttonFunction(BuildContext context, int btnId) {
  if (btnId == 1) {}
}

// ignore: non_constant_identifier_names
CustomAppBar(BuildContext context, String title, bool showback,
    {int? btnId,
    Widget? rightWidget,
    Widget? leftWidget,
    Brightness? brightness,
    Color? backgroundColor,
    Color? fontColor,
    VoidCallback? customBackFunc}) {
  sideButton() {
    return btnId != null
        ? SizedBox(
            width: getWidth(context, 0.1),
            height: getWidth(context, 0.1),
            child: RawMaterialButton(
              padding: EdgeInsets.all(getWidth(context, 0.01)),
              onPressed: () {
                buttonFunction(context, btnId);
              },
              shape: StadiumBorder(),
              child: Icon(
                Icons.settings,
                color: Colors.black,
                size: getWidth(context, 0.06),
              ),
            ),
          )
        : SizedBox(
            width: getWidth(context, 0.1), height: getWidth(context, 0.1));
  }

  return AppBar(
    automaticallyImplyLeading: false,
    toolbarHeight: getWidth(
        context,
        showback == true
            ? title == ""
                ? 0.13
                : 0.22
            : title == ""
                ? 0.08
                : 0.22),
    backgroundColor: backgroundColor != null ? backgroundColor : bgColor(),
    systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
      statusBarIconBrightness: brightness != null
          ? brightness
          : themeID == 1
              ? Brightness.dark
              : Brightness.light,
    ),
    elevation: 0.0,
    title: Padding(
      padding: EdgeInsets.only(
          top: getWidth(
              context,
              title == ""
                  ? 0.08
                  : showback
                      ? 0.03
                      : 0.12),
          left: getWidth(context, 0.02),
          right: getWidth(context, 0.02),
          bottom: getWidth(context, title == "" ? 0 : 0.04)),
      child: Column(children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            if (showback == true)
              IconButton(
                  padding: EdgeInsets.all(0),
                  alignment: Alignment.centerLeft,
                  hoverColor: Colors.transparent,
                  color: Colors.transparent,
                  focusColor: Colors.transparent,
                  splashColor: Colors.transparent,
                  highlightColor: Colors.transparent,
                  disabledColor: Colors.transparent,
                  onPressed: () {
                    customBackFunc != null
                        ? customBackFunc()
                        : Navigator.pop(context);
                  },
                  icon: Icon(
                    Icons.arrow_back_ios_new_outlined,
                    color: brightness == Brightness.light
                        ? Colors.white
                        : brightness == Brightness.dark
                            ? Colors.black
                            : fontColor1(),
                    size: getWidth(context, 0.05),
                  )),
            setWSpacing(getWidth(context, 0.1)),
            if (showback == true)
              rightWidget != null
                  ? rightWidget
                  : btnId != null
                      ? sideButton()
                      : SizedBox(),
          ],
        ),
        SizedBox(
          height: getWidth(context, 0.1),
          child: Row(
            mainAxisAlignment: showback == true
                ? MainAxisAlignment.center
                : rightWidget != null
                    ? MainAxisAlignment.spaceBetween
                    : MainAxisAlignment.center,
            children: [
              CustomText(
                txt: title,
                size: 0.05,
                spacing: 2,
                bold: true,
                color: fontColor != null ? fontColor : fontColor1(),
              ),
              if (showback != true)
                rightWidget != null
                    ? rightWidget
                    : btnId != null
                        ? sideButton()
                        : SizedBox(),
            ],
          ),
        )
      ]),
    ),
  );
}
