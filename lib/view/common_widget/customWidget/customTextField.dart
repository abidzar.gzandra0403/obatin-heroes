import 'package:obatin/importPackage.dart';

class CustomTextField extends StatelessWidget {
  final TextEditingController? controller;
  final String? hint;
  final String? hint2;
  final bool? readonly;
  final int? maxlines;
  final double? minHeight;
  final int? maxLength;
  final bool? obsecure;
  final double? width;
  final double? height;
  final int? styleId;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Color? suffixIconColor;
  final VoidCallback? onChanged;
  final VoidCallback? onTap;
  final TextInputType? keyboardtype;
  final TextInputAction? keyboardAction;
  CustomTextField(
      {this.controller,
      this.hint,
      this.hint2,
      this.readonly,
      this.maxlines,
      this.minHeight,
      this.maxLength,
      this.obsecure,
      this.width,
      this.height,
      this.styleId,
      this.prefixIcon,
      this.suffixIcon,
      this.suffixIconColor,
      this.onChanged,
      this.onTap,
      this.keyboardtype,
      this.keyboardAction});

  @override
  Widget build(BuildContext context) {
    contentTextInput() {
      return TextFormField(
        controller: controller,
        onTap: () => onTap != null ? onTap!() : null,
        onChanged: (s) => onChanged != null ? onChanged!() : null,
        keyboardType: keyboardtype != null ? keyboardtype! : TextInputType.text,
        obscureText: obsecure != null ? obsecure! : false,
        readOnly: readonly != null ? readonly! : false,
        minLines: 1,
        maxLength: maxLength != null ? maxLength! : null,
        maxLines: maxlines != null ? maxlines : 1,
        textInputAction:
            keyboardAction != null ? keyboardAction! : TextInputAction.done,
        style: hnnormal(context,
            txtSize: getWidth(context, 0.04), txtColor: fontColor1()),
        decoration: InputDecoration(
          prefixIcon: prefixIcon != null
              ? Icon(
                  prefixIcon,
                  color: fontColor2(),
                )
              : null,
          suffixIcon: suffixIcon != null
              ? Icon(
                  suffixIcon,
                  color:
                      suffixIconColor != null ? suffixIconColor : fontColor2(),
                )
              : null,
          hintText: (styleId == 1 || styleId == null)
              ? hint2 != null
                  ? hint2
                  : ""
              : hint != null
                  ? hint
                  : null,
          hintStyle: TextStyle(color: fontColor2()),
          contentPadding: (styleId == 1 || styleId == null)
              ? EdgeInsets.all(getWidth(context, 0.03))
              : EdgeInsets.symmetric(
                  vertical: getWidth(context, 0.01),
                  horizontal: getWidth(context, prefixIcon != null ? 0 : 0.02)),
          border: InputBorder.none,
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
          disabledBorder: OutlineInputBorder(
            borderSide: BorderSide(width: 0, color: Colors.transparent),
          ),
        ),
      );
    }

    style1() {
      return Stack(
        children: [
          Container(
              constraints: BoxConstraints(
                  minHeight:
                      getWidth(context, minHeight != null ? minHeight! : 0.12)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: contentTextInput()),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            curve: Curves.fastOutSlowIn,
            margin: EdgeInsets.only(left: getWidth(context, 0.02)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
            decoration: BoxDecoration(color: bgColor()),
            child: CustomText(
                txt: hint != null ? hint! : "",
                bold: true,
                color: defaultcolor5),
          ),
        ],
      );
    }

    style2() {
      return Container(
        constraints: BoxConstraints(
            minHeight:
                getWidth(context, minHeight != null ? minHeight! : 0.12)),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(getWidth(context, 0.02)),
            border: Border.all(width: 1, color: Colors.grey)),
        child: contentTextInput(),
      );
    }

    //================================================================================
    //Main
    //================================================================================

    styleSelect() {
      if (styleId == null) {
        return style1();
      } else {
        if (styleId == 1) {
          return style1();
        } else {
          return style2();
        }
      }
    }

    contentMain() {
      return Container(
          height: height != null ? getWidth(context, height!) : null,
          width: getWidth(context, width != null ? width! : 1),
          margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.012)),
          child: styleSelect());
    }

    return contentMain();
  }
}
