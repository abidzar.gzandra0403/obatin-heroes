import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';

// ignore: non_constant_identifier_names
CustomBottomSheet(context, {@required Widget? content}) {
  return showMaterialModalBottomSheet(
      expand: false,
      isDismissible: true,
      bounce: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (builder) {
        return new Container(
            margin: EdgeInsets.only(top: getWidth(context, 0.1)),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.08),
                vertical: getWidth(context, 0.02)),
            decoration: new BoxDecoration(
                color: bgDialogColor(),
                borderRadius: new BorderRadius.only(
                    topLeft: const Radius.circular(30.0),
                    topRight: const Radius.circular(30.0))),
            child: SingleChildScrollView(child: content!));
      });
}
