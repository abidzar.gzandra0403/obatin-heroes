import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names
CustomLoadingDialog(BuildContext context, {Widget? customSpinKit}) {
  return CustomAlertDialog(context,
      dismissable: false,
      backgroundColor: Colors.transparent,
      elevation: 0,
      width: 0.2,
      contentWidget: customSpinKit != null
          ? customSpinKit
          : Stack(
              children: [
                SpinKitDualRing(
                  color: Colors.white,
                  size: getWidth(context, 0.2),
                ),
                Center(
                  child: SizedBox(
                    width: getWidth(context, 0.14),
                    child: ImgAsset.splashObatin1,
                  ),
                )
              ],
            ));
}
