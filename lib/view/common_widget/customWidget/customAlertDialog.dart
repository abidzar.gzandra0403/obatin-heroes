import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names
CustomAlertDialog(
  BuildContext context, {
  double? width,
  String? title,
  String? contentText,
  Widget? customBody,
  IconData? contentIcon,
  Widget? contentWidget,
  double? contentWidgetHeight,
  bool? contentFitHeight,
  String? txtButton1,
  String? txtButton2,
  String? txtButton3,
  double? elevation,
  Color? backgroundColor,
  bool? dismissable,
  VoidCallback? funcButton1,
  VoidCallback? funcButton2,
  VoidCallback? funcButton3,
  VoidCallback? funcExitApp,
  VoidCallback? funcDismiss,
}) {
  return NAlertDialog(
    dismissable: dismissable != null ? dismissable : true,
    dialogStyle: DialogStyle(
        backgroundColor:
            backgroundColor != null ? backgroundColor : bgDialogColor(),
        elevation: elevation != null ? elevation : 1),
    onDismiss: () => funcDismiss != null ? funcDismiss() : null,
    content: Container(
      width: getWidth(context, width != null ? width : 0.7),
      padding: EdgeInsets.only(top: getWidth(context, 0.04)),
      child: SingleChildScrollView(
        child: customBody != null
            ? customBody
            : Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  if (title != null)
                    CustomText(
                      botpad: 0.02,
                      txt: title,
                      bold: true,
                      size: 0.04,
                    ),
                  if (contentIcon != null)
                    Icon(
                      contentIcon,
                      size: getWidth(context, 0.2),
                      color: defaultcolor2,
                    ),
                  if (contentWidget != null)
                    Container(
                      padding: EdgeInsets.symmetric(
                          vertical: getWidth(context, 0.02)),
                      height:
                          (contentFitHeight != null && contentFitHeight == true)
                              ? null
                              : getWidth(
                                  context,
                                  contentWidgetHeight != null
                                      ? contentWidgetHeight
                                      : 0.4),
                      child: contentWidget,
                    ),
                  if (contentText != null)
                    CustomText(
                      botpad: 0.02,
                      txt: contentText,
                      size: 0.036,
                    ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      if (txtButton1 != null)
                        CustomButton(
                          styleId: 1,
                          onPressed: () {
                            if (funcExitApp != null) {
                              funcExitApp();
                            } else {
                              Navigator.pop(context);
                              if (funcButton1 != null) funcButton1();
                            }
                          },
                          btnText: txtButton1,
                        ),
                      if (txtButton2 != null)
                        CustomButton(
                          styleId: 1,
                          onPressed: () {
                            Navigator.pop(context);
                            if (funcButton2 != null) funcButton2();
                          },
                          btnText: txtButton2,
                        ),
                      if (txtButton3 != null)
                        CustomButton(
                          styleId: 1,
                          onPressed: () {
                            Navigator.pop(context);
                            if (funcButton3 != null) funcButton3();
                          },
                          btnText: txtButton3,
                        )
                    ],
                  )
                ],
              ),
      ),
    ),
    blur: 0,
  ).show(context,
      transitionType: DialogTransitionType.Bubble,
      transitionDuration: Duration(milliseconds: 500));
}
