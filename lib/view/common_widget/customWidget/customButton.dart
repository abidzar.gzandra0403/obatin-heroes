import 'package:obatin/importPackage.dart';

class CustomButton extends StatelessWidget {
  final int? styleId;
  final String? btnText;
  final String? btnText2;
  final VoidCallback? onPressed;
  final int? shapeId;
  final Alignment? alignment;
  final MainAxisAlignment? mainAxisAlignment;
  final IconData? prefixIcon;
  final IconData? suffixIcon;
  final Widget? prefixWidget;
  final Widget? suffixWidget;
  final double? iconhorpad;
  final double? height;
  final double? width;
  final double? textWidth;
  final double? horpad;
  final double? verpad;
  final double? hormargin;
  final double? vermargin;
  final double? topmargin;
  final double? bottommargin;
  final double? leftmargin;
  final double? rightmargin;
  final double? borderSize;
  final Color? borderColor;
  final double? fsize;
  final double? isize;
  final double? wsize;
  final bool? bold;
  final Color? bColor;
  final Color? fColor;
  final Color? highlighColor;
  final Color? iColor;
  final double? elevation;
  final Widget? customContent;
  CustomButton(
      {@required this.styleId,
      this.btnText = "",
      this.btnText2,
      @required this.onPressed,
      this.shapeId,
      this.alignment,
      this.mainAxisAlignment,
      this.prefixIcon,
      this.suffixIcon,
      this.prefixWidget,
      this.suffixWidget,
      this.iconhorpad,
      this.height,
      this.width,
      this.textWidth,
      this.horpad,
      this.verpad,
      this.hormargin,
      this.vermargin,
      this.topmargin,
      this.bottommargin,
      this.leftmargin,
      this.rightmargin,
      this.borderSize,
      this.borderColor,
      this.fsize,
      this.isize,
      this.wsize,
      this.bold,
      this.bColor,
      this.fColor,
      this.highlighColor,
      this.iColor,
      this.elevation,
      this.customContent});

  @override
  Widget build(BuildContext context) {
    //====================================================================================
    //Init State
    //====================================================================================
    contentStyle(Color? color) {
      String txt1 = btnText != null ? btnText! : "";
      return Padding(
        padding: EdgeInsets.symmetric(
            vertical: getWidth(context, verpad != null ? vermargin! : 0.01),
            horizontal: getWidth(context, horpad != null ? hormargin! : 0.02)),
        child: Row(
          mainAxisAlignment: mainAxisAlignment != null
              ? mainAxisAlignment!
              : MainAxisAlignment.center,
          children: [
            Row(children: [
              if (prefixIcon != null)
                Padding(
                    padding: EdgeInsets.only(
                        right: getWidth(context, txt1.isNotEmpty ? 0.01 : 0)),
                    child: Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getWidth(
                              context, iconhorpad != null ? iconhorpad! : 0)),
                      child: Icon(prefixIcon!,
                          color: iColor != null
                              ? iColor
                              : color != null
                                  ? color
                                  : Colors.white,
                          size: getWidth(
                              context, isize != null ? isize! : 0.045)),
                    )),
              if (prefixWidget != null)
                Padding(
                    padding: EdgeInsets.only(
                        right: getWidth(context, btnText != null ? 0.01 : 0)),
                    child: prefixWidget!),
              if (btnText != null)
                CustomText(
                    width: textWidth != null ? textWidth! : null,
                    txt: btnText!,
                    bold: bold != null ? bold : false,
                    color: color != null ? color : Colors.white,
                    size: fsize != null ? fsize! : 0.037),
            ]),
            if (suffixIcon != null)
              Padding(
                padding: EdgeInsets.only(left: getWidth(context, 0.01)),
                child: Icon(
                  suffixIcon!,
                  color: iColor != null
                      ? iColor
                      : color != null
                          ? color
                          : Colors.white,
                  size: getWidth(context, isize != null ? isize! : 0.045),
                ),
              ),
            if (suffixWidget != null)
              Padding(
                  padding: EdgeInsets.only(
                      right: getWidth(context, btnText != null ? 0.01 : 0)),
                  child: suffixWidget!),
          ],
        ),
      );
    }

    hgColor<Colors>(bool nofillColor) {
      return themeID == 1
          ? nofillColor
              ? Color.fromARGB(50, 0, 100, 255)
              : Color.fromARGB(20, 0, 0, 0)
          : nofillColor
              ? Color.fromARGB(50, 0, 100, 255)
              : Color.fromARGB(50, 255, 255, 255);
    }

    shapeIdBtn({BorderSide? side}) {
      return shapeId == 1
          ? RoundedRectangleBorder(
              side: borderSize != null
                  ? BorderSide(
                      width: getWidth(context, borderSize!),
                      color: borderColor != null
                          ? borderColor!
                          : Colors.transparent)
                  : BorderSide.none)
          : shapeId == 2
              ? RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(getWidth(context, 0.02)),
                  side: borderSize != null
                      ? BorderSide(
                          width: getWidth(context, borderSize!),
                          color: borderColor != null
                              ? borderColor!
                              : Colors.transparent)
                      : BorderSide.none)
              : shapeId == 3
                  ? StadiumBorder(
                      side: side != null
                          ? side
                          : borderSize != null
                              ? BorderSide(
                                  width: getWidth(context, borderSize!),
                                  color: borderColor != null
                                      ? borderColor!
                                      : Colors.transparent)
                              : BorderSide.none)
                  : CircleBorder(
                      side: side != null
                          ? side
                          : borderSize != null
                              ? BorderSide(
                                  width: getWidth(context, borderSize!),
                                  color: borderColor != null
                                      ? borderColor!
                                      : Colors.transparent)
                              : BorderSide.none);
    }

    //====================================================================================
    //Button Style
    //====================================================================================

    style1() {
      return RawMaterialButton(
          padding: EdgeInsets.symmetric(
              vertical: getWidth(context, verpad != null ? verpad! : 0.0),
              horizontal: getWidth(context, horpad != null ? horpad! : 0)),
          onPressed: () => onPressed!(),
          shape: shapeId != null
              ? shapeIdBtn()
              : StadiumBorder(
                  side: borderSize != null
                      ? BorderSide(
                          width: getWidth(context, borderSize!),
                          color: borderColor != null
                              ? borderColor!
                              : Colors.transparent)
                      : BorderSide.none),
          fillColor: bColor != null ? bColor! : defaultcolor1,
          splashColor: Colors.transparent,
          highlightColor:
              highlighColor != null ? highlighColor! : hgColor(false),
          elevation: elevation != null ? elevation! : 0,
          highlightElevation: 0,
          child: customContent != null
              ? customContent
              : contentStyle(fColor != null ? fColor! : Colors.white));
    }

    style2() {
      return RawMaterialButton(
        padding: EdgeInsets.symmetric(
            vertical: getWidth(context, verpad != null ? verpad! : 0.0),
            horizontal: getWidth(context, horpad != null ? horpad! : 0)),
        onPressed: () => onPressed!(),
        shape: shapeId != null
            ? shapeIdBtn(
                side: BorderSide(
                    width: getWidth(context, 0.008),
                    color: bColor != null ? bColor! : defaultcolor1))
            : StadiumBorder(
                side: BorderSide(
                    width: getWidth(context, 0.008),
                    color: bColor != null ? bColor! : defaultcolor1)),
        fillColor: Colors.transparent,
        splashColor: Colors.transparent,
        highlightColor: highlighColor != null ? highlighColor! : hgColor(false),
        elevation: elevation != null ? elevation! : 0,
        highlightElevation: 0,
        child: customContent != null
            ? customContent
            : contentStyle(fColor != null ? fColor! : defaultcolor1),
      );
    }

    style3() {
      return RawMaterialButton(
          onPressed: () => onPressed!(),
          padding: EdgeInsets.symmetric(
              vertical: getWidth(context, verpad != null ? verpad! : 0.0),
              horizontal: getWidth(context, horpad != null ? horpad! : 0)),
          shape: shapeId != null
              ? shapeIdBtn()
              : StadiumBorder(
                  side: borderSize != null
                      ? BorderSide(
                          width: getWidth(context, borderSize!),
                          color: borderColor != null
                              ? borderColor!
                              : Colors.transparent)
                      : BorderSide.none),
          fillColor:
              bColor != null ? bColor! : Color.fromARGB(70, 54, 180, 247),
          splashColor: Colors.transparent,
          highlightColor:
              highlighColor != null ? highlighColor! : hgColor(false),
          elevation: elevation != null ? elevation! : 0,
          highlightElevation: 0,
          child: customContent != null
              ? customContent
              : contentStyle(
                  fColor != null ? fColor! : fontColor5(),
                ));
    }

    style4() {
      return RawMaterialButton(
          onPressed: () => onPressed!(),
          padding: EdgeInsets.symmetric(
              vertical: getWidth(context, verpad != null ? verpad! : 0.0),
              horizontal: getWidth(context, horpad != null ? horpad! : 0)),
          shape: shapeId != null
              ? shapeIdBtn()
              : StadiumBorder(
                  side: borderSize != null
                      ? BorderSide(
                          width: getWidth(context, borderSize!),
                          color: borderColor != null
                              ? borderColor!
                              : Colors.transparent)
                      : BorderSide.none),
          fillColor: bColor != null ? bColor! : Colors.transparent,
          splashColor: Colors.transparent,
          highlightColor:
              highlighColor != null ? highlighColor! : hgColor(true),
          elevation: elevation != null ? elevation! : 0,
          highlightElevation: 0,
          child: customContent != null
              ? customContent
              : contentStyle(
                  fColor != null ? fColor! : fontColor1(),
                ));
    }

    style5() {
      return RawMaterialButton(
          onPressed: () => onPressed!(),
          elevation: elevation != null ? elevation! : 0,
          highlightColor:
              highlighColor != null ? highlighColor! : hgColor(true),
          fillColor: bColor != null ? bColor : Colors.transparent,
          highlightElevation: 0,
          padding: EdgeInsets.symmetric(
              vertical: getWidth(context, verpad != null ? verpad! : 0),
              horizontal: getWidth(context, horpad != null ? horpad! : 0)),
          shape: shapeId != null
              ? shapeIdBtn()
              : RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10),
                  side: borderSize != null
                      ? BorderSide(
                          width: getWidth(context, borderSize!),
                          color: borderColor != null
                              ? borderColor!
                              : Colors.transparent)
                      : BorderSide.none),
          child: customContent != null
              ? customContent
              : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    if (prefixIcon != null)
                      SizedBox(
                        width: getWidth(context, 0.1),
                        child: Icon(prefixIcon,
                            size: getWidth(
                                context, isize != null ? isize! : 0.05),
                            color: iColor != null
                                ? iColor
                                : fColor != null
                                    ? fColor
                                    : defaultcolor2),
                      ),
                    if (prefixWidget != null) prefixWidget!,
                    Expanded(
                        child: Padding(
                            padding: const EdgeInsets.only(left: 10),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CustomText(
                                    txt: btnText!,
                                    bold: bold != null ? bold : false,
                                    size: fsize != null ? fsize : 0.035,
                                    color:
                                        fColor != null ? fColor : fontColor1()),
                                if (btnText2 != null)
                                  CustomText(
                                      txt: btnText2!,
                                      size: fsize != null
                                          ? (fsize! - 0.003)
                                          : 0.032,
                                      color:
                                          fColor != null ? fColor : Colors.grey)
                              ],
                            ))),
                    suffixWidget != null
                        ? suffixWidget!
                        : SizedBox(
                            width: getWidth(context, 0.1),
                            child: Align(
                              alignment: Alignment.centerRight,
                              child: Icon(
                                Icons.arrow_forward_ios_sharp,
                                color: iColor != null
                                    ? iColor
                                    : fColor != null
                                        ? fColor
                                        : defaultcolor2,
                                size: getWidth(context, 0.04),
                              ),
                            ))
                  ],
                ));
    }

    style6() {
      return RawMaterialButton(
        onPressed: () => onPressed!(),
        padding: EdgeInsets.symmetric(
            vertical: getWidth(context, verpad != null ? verpad! : 0.0),
            horizontal: getWidth(context, horpad != null ? horpad! : 0)),
        elevation: elevation != null ? elevation! : 0,
        highlightElevation: 0,
        fillColor: bColor != null ? bColor : defaultcolor1,
        highlightColor: highlighColor != null ? highlighColor! : Colors.white10,
        hoverColor: Colors.white10,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: borderSize != null
                ? BorderSide(
                    width: getWidth(context, borderSize!),
                    color:
                        borderColor != null ? borderColor! : Colors.transparent)
                : BorderSide.none),
        child: customContent != null
            ? customContent
            : SizedBox(
                child: Column(
                  mainAxisAlignment: mainAxisAlignment != null
                      ? mainAxisAlignment!
                      : MainAxisAlignment.center,
                  children: [
                    if (prefixIcon != null)
                      SizedBox(
                          child: Icon(
                        prefixIcon,
                        size: getWidth(context, isize != null ? isize! : 0.080),
                        color: iColor != null ? iColor : Colors.white,
                      )),
                    if (prefixWidget != null) prefixWidget!,
                    if (btnText != null)
                      CustomText(
                        txt: btnText!,
                        bold: bold != null ? bold : false,
                        size: fsize != null ? fsize : 0.035,
                        textAlign: TextAlign.center,
                        color: fColor != null ? fColor : fontColor1(),
                      ),
                  ],
                ),
              ),
      );
    }

    //================================================================================
    //Main
    //================================================================================

    styleSelect() {
      if (styleId == 1) {
        return style1();
      } else if (styleId == 2) {
        return style2();
      } else if (styleId == 3) {
        return style3();
      } else if (styleId == 4) {
        return style4();
      } else if (styleId == 5) {
        return style5();
      } else if (styleId == 6) {
        return style6();
      }
    }

    contentMain() {
      return Padding(
        padding: EdgeInsets.only(
            top: getWidth(context, topmargin != null ? topmargin! : 0),
            bottom: getWidth(context, bottommargin != null ? bottommargin! : 0),
            left: getWidth(context, leftmargin != null ? leftmargin! : 0),
            right: getWidth(context, rightmargin != null ? rightmargin! : 0)),
        child: Container(
            margin: EdgeInsets.symmetric(
                vertical: getWidth(context, vermargin != null ? vermargin! : 0),
                horizontal:
                    getWidth(context, hormargin != null ? hormargin! : 0)),
            height: height != null ? getWidth(context, height!) : null,
            width: width != null ? getWidth(context, width!) : null,
            child: styleSelect()),
      );
    }

    return alignment != null
        ? Align(alignment: alignment!, child: contentMain())
        : contentMain();
  }
}
