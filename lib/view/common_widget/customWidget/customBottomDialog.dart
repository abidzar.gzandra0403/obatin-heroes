// ignore: non_constant_identifier_names
import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names
CustomBottomDialog(BuildContext context,
    {@required String? txt,
    IconData? prefixIcon,
    IconData? suffixIcon,
    int? durationInMilliSec}) {
  return ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      shape: StadiumBorder(),
      behavior: SnackBarBehavior.floating,
      backgroundColor: Color.fromARGB(180, 67, 115, 196),
      duration: Duration(
          milliseconds: durationInMilliSec != null ? durationInMilliSec : 1500),
      content: SizedBox(
        // width: getWidth(context, 0.9),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (prefixIcon != null)
              Padding(
                padding: EdgeInsets.only(right: getWidth(context, 0.02)),
                child: Icon(
                  prefixIcon,
                  color: Colors.blue.shade100,
                  size: getWidth(context, 0.06),
                ),
              ),
            CustomText(width: 0.7, txt: txt, color: Colors.white, bold: true),
            if (suffixIcon != null)
              Padding(
                padding: EdgeInsets.only(left: getWidth(context, 0.02)),
                child: Icon(
                  suffixIcon,
                  color: Colors.blue.shade100,
                  size: getWidth(context, 0.06),
                ),
              ),
          ],
        ),
      )));
}
