import 'package:obatin/importPackage.dart';

class CustomText extends StatelessWidget {
  final String? txt;
  final Color? color;
  final double? size;
  final bool? bold;
  final IconData? prefixIcon;
  final Color? iconColor;
  final double? iconSize;
  final double? iconSpacing;
  final TextAlign? textAlign;
  final Alignment? align;
  final TextOverflow? overflow;
  final double? spacing;
  final double? width;
  final double? height;
  final double? toppad;
  final double? botpad;
  final double? leftpad;
  final double? rightpad;
  CustomText(
      {@required this.txt,
      this.color,
      this.size,
      this.bold,
      this.prefixIcon,
      this.iconColor,
      this.iconSize,
      this.iconSpacing,
      this.textAlign,
      this.align,
      this.overflow,
      this.spacing,
      this.width,
      this.height,
      this.toppad,
      this.botpad,
      this.leftpad,
      this.rightpad});
  @override
  Widget build(BuildContext context) {
    bool initbold = bold == null ? false : bold!;
    contentContainer({@required child}) {
      return Container(
          alignment: textAlign == TextAlign.center ? Alignment.center : null,
          width: width != null ? getWidth(context, width!) : null,
          height: height != null ? getWidth(context, height!) : null,
          margin: EdgeInsets.only(
            top: getWidth(context, toppad != null ? toppad! : 0),
            bottom: getWidth(context, botpad != null ? botpad! : 0),
            left: getWidth(context, leftpad != null ? leftpad! : 0),
            right: getWidth(context, rightpad != null ? rightpad! : 0),
          ),
          child: child);
    }

    contentText() {
      return Text(
        txt!,
        textAlign: textAlign != null ? textAlign : TextAlign.start,
        overflow: overflow != null ? overflow! : TextOverflow.clip,
        style: hnnormal(
          context,
          spacing: spacing != null ? spacing : 0,
          txtColor: color != null ? color : fontColor1(),
          txtWeight: initbold == true ? FontWeight.bold : FontWeight.normal,
          txtSize: getWidth(context, size != null ? size! : 0.034),
        ),
      );
    }

    contentMain() {
      return prefixIcon != null
          ? contentContainer(
              child: Row(
              children: [
                Icon(
                  prefixIcon!,
                  color: iconColor != null ? iconColor : fontColor1(),
                  size: getWidth(context, iconSize != null ? iconSize! : 0.035),
                ),
                setWSpacing(getWidth(
                    context, iconSpacing != null ? iconSpacing! : 0.01)),
                contentText(),
              ],
            ))
          : contentContainer(child: contentText());
    }

    return align != null
        ? Align(
            alignment: align!,
            child: contentMain(),
          )
        : contentMain();
  }
}
