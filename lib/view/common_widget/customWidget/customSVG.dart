import 'package:flutter_svg/flutter_svg.dart';
import 'package:obatin/importPackage.dart';

class CustomSVG extends StatelessWidget {
  final String? svgName;
  final Color? color;
  final double? size;
  final double? verpad;
  final double? horpad;
  final double? allpad;
  final double? vermargin;
  final double? hormargin;
  final double? allmargin;
  CustomSVG(
      {@required this.svgName,
      this.color,
      this.size,
      this.verpad,
      this.horpad,
      this.allpad,
      this.vermargin,
      this.hormargin,
      this.allmargin});
  @override
  Widget build(BuildContext context) {
    return Container(
      height: getWidth(context, size != null ? size! : 0.05),
      width: getWidth(context, size != null ? size! : 0.05),
      margin: allmargin != null
          ? EdgeInsets.all(getWidth(context, allmargin!))
          : EdgeInsets.symmetric(
              horizontal: getWidth(context, hormargin != null ? hormargin! : 0),
              vertical: getWidth(context, vermargin != null ? vermargin! : 0)),
      padding: allpad != null
          ? EdgeInsets.all(getWidth(context, allpad!))
          : EdgeInsets.symmetric(
              horizontal: getWidth(context, horpad != null ? horpad! : 0),
              vertical: getWidth(context, verpad != null ? verpad! : 0)),
      child: SvgPicture.asset(
        "assets/svg/" + svgName!,
        semanticsLabel: 'svgName',
        fit: BoxFit.scaleDown,
        color: color != null ? color : fontColor1(),
      ),
    );
  }
}

//Static Data =================================

final svg_gopaytransfer = "gopaytransfer.svg";
final svg_gopaytopup = "gopaytopup.svg";
final svg_gopayhistory = "gopayhistory.svg";
final svg_iconactivity = "iconactivity.svg";
final svg_iconinbox = "iconinbox.svg";
final svg_iconprofile = "iconprofile.svg";
final svg_iconsettings = "iconsettings.svg";
final svg_iconsobatin = "iconsobatin.svg";
final svg_iconsupport = "iconsupport.svg";
final svg_iconpracticesetting = "iconpracticesetting.svg";
final svg_iconedittitle = "iconedittitle.svg";
final svg_iconreview = "iconreview.svg";
final svg_icondescription = "icondescription.svg";
final svg_iconpassword = "iconpassword.svg";
final svg_iconnotifsettings = "iconnotifsettings.svg";
final svg_iconbankaccount = "iconbankaccount.svg";
final svg_iconsupportcenter = "iconsupportcenter.svg";
final svg_iconcomplains = "iconcomplains.svg";
final svg_iconconsumencomplains = "iconconsumencomplains.svg";
final svg_iconintelectualrights = "iconintelectualrights.svg";
final svg_iconobatin = "iconobatin.svg";
final svg_iconabout = "iconabout.svg";
final svg_icontermcondition = "icontermcondition.svg";
final svg_iconprivacypolicy = "iconprivacypolicy.svg";
final svg_iconreviewapp = "iconreviewapp.svg";
final svg_iconajakin = "iconajakin.svg";
final svg_iconsharein = "iconsharein.svg";
final svg_iconcarein = "iconcarein.svg";
final svg_iconsip = "iconsip.svg";
final svg_iconsip1 = "iconsip1.svg";
final svg_iconsip2 = "iconsip2.svg";
final svg_iconsip3 = "iconsip3.svg";
final svg_anam = "obatin3.svg";
final svg_listproduct = "obatlist.svg";
final svg_iconmedicalproduct = "iconmedicalproduct.svg";
final svg_iconlistdoctor = "iconlistdoctor.svg";
final svg_iconnurse = "iconnurse.svg";
final svg_iconhospital = "iconhospital.svg";
final svg_iconwallet = "iconwallet.svg";
final svg_iconwaitingschedule = "iconwaitingschedule.svg";
final svg_iconreschedule = "iconreschedule.svg";
final svg_iconordercomplete = "iconordercomplete.svg";
final svg_iconcancel = "iconcancel.svg";
final svg_iconcompaint = "iconcompaint.svg";
final svg_icontolak = "icontolak.svg";
final svg_iconneworder = "iconneworder.svg";
final svg_iconbeingserved = "iconbeingserved.svg";
final svg_iconmakereferral = "iconmakereferral.svg";
final svg_icondoreferal = "icondoreferal.svg";

//==============================================