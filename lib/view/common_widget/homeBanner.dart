import 'package:obatin/importPackage.dart';

class HomeBanner extends StatefulWidget {
  @override
  _HomeBannerState createState() => _HomeBannerState();
}

class _HomeBannerState extends State<HomeBanner> {
  int indexNow = 0;
  @override
  Widget build(BuildContext context) {
    double height = getWidth(context, 0.72);
    double width = getWidth(context, 0.55);

    return Padding(
      padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.02)),
      child: SizedBox(
        width: width,
        height: height,
        child: ClipRRect(
            borderRadius: BorderRadius.circular(getWidth(context, 0.02)),
            child: Stack(
              children: [
                CarouselSlider(
                  options: CarouselOptions(
                      height: height,
                      aspectRatio: 1,
                      onPageChanged: (index, reason) {
                        setState(() {
                          indexNow = index;
                        });
                      },
                      autoPlay: true,
                      autoPlayInterval: Duration(seconds: 5),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlayCurve: Curves.fastOutSlowIn,
                      pauseAutoPlayOnTouch: true,
                      enlargeCenterPage: true,
                      viewportFraction: 1,
                      disableCenter: true),
                  items: bannerImageLoad,
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(
                        bottom: getWidth(context, 0.02),
                        left: getWidth(context, 0.06)),
                    height: getWidth(context, 0.012),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        shrinkWrap: true,
                        itemCount: bannerImageLoad.length,
                        itemBuilder: (context, index) => Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: getWidth(context, 0.003)),
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(99),
                                  color: (index == indexNow)
                                      ? defaultcolor3
                                      : Colors.white),
                              width: getWidth(context, 0.012),
                            )),
                  ),
                )
              ],
            )),
      ),
    );
  }
}
