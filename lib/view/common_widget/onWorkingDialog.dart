import 'package:obatin/importPackage.dart';

onWorkingDialog(BuildContext context) {
  CustomBottomDialog(
    context,
    txt: "On Working",
    prefixIcon: Icons.gavel_sharp,
  );
}
