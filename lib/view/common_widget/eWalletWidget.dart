import 'package:obatin/importPackage.dart';

eWalletWidget(BuildContext context) {
  iconButton(var svgName, String text, {@required Widget? gotopage}) {
    return CustomButton(
      styleId: 6,
      width: 0.15,
      onPressed: () => goToPage(context, page: gotopage!),
      btnText: text,
      fColor: Colors.white,
      fsize: 0.03,
      verpad: 0,
      bColor: Colors.transparent,
      prefixWidget: CustomSVG(
        svgName: svgName,
        color: Colors.white,
        size: 0.06,
      ),
    );
  }

  return GestureDetector(
    onTap: () => goToPage(context, page: EWalletMain()),
    child: Container(
      height: getWidth(context, 0.15),
      decoration: BoxDecoration(
          color: gopayBlueColor1(),
          borderRadius: BorderRadius.circular(getWidth(context, 0.03))),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            width: getWidth(context, 0.49),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.04)),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(getWidth(context, 0.03)),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(width: getWidth(context, 0.13), child: icon_gopay),
                setHSpacing(getWidth(context, 0.01)),
                CustomText(
                  txt: "Rp. 455.000",
                  size: 0.032,
                  bold: true,
                  color: Colors.black,
                ),
                CustomText(
                  txt: Str().clickfordetail,
                  size: 0.022,
                  bold: true,
                  color: Colors.grey,
                )
              ],
            ),
          ),
          Row(
            children: [
              iconButton(svg_gopaytransfer, Str().pay, gotopage: EmptyPage()),
              iconButton(svg_gopaytopup, Str().topup, gotopage: EmptyPage()),
            ],
          )
        ],
      ),
    ),
  );
}
