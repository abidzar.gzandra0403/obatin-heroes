import 'package:obatin/importPackage.dart';

reschedulebottomsheet(BuildContext context, int serviceId) {
  return CustomBottomSheet(
    context,
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Container(
            height: getWidth(context, 0.02),
            width: 40,
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: Colors.black12, borderRadius: BorderRadius.circular(50)),
          ),
        ),
        Center(
            child: CustomText(
          txt: "Permintaan Reschedule",
          bold: true,
          size: 0.04,
        )),
        setHSpacing(getWidth(context, 0.04)),
        Container(
          // height: getWidth(context, 0.5),
          child: Row(
            children: [
              Container(
                width: getWidth(context, 0.3),
                height: getWidth(context, 0.45),
                decoration: BoxDecoration(
                    color: Colors.blue.shade50,
                    borderRadius: BorderRadius.circular(10)),
                child: Center(child: Icon(Icons.person, color: Colors.white)),
              ),
              Container(
                width: getWidth(context, 0.52),
                padding: EdgeInsets.only(left: getWidth(context, 0.025)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                        child: CustomText(
                      txt: "Tn. Abidzar Ghifari Zandra",
                      bold: true,
                      size: 0.05,
                    )),
                    initialServiceRequest(context, serviceId),
                    Container(
                      height: getWidth(context, 0.008),
                      width: getWidth(context, 0.5),
                      margin: EdgeInsets.only(bottom: getWidth(context, 0.01)),
                      decoration: BoxDecoration(
                          color: lightgrey,
                          borderRadius: BorderRadius.circular(10)),
                    ),
                    informationCustomer(context, ObatinIconpack.identity,
                        "1502110404990001", iconcoloridentity),
                    informationCustomer(context, ObatinIconpack.gender, "Pria",
                        iconcolorgenderred),
                    informationCustomer(context, ObatinIconpack.ages,
                        "28 Tahun", iconcolorageyellow),
                    informationCustomer(context, ObatinIconpack.bodyheight,
                        "168 Cm", iconcolorbodyheight),
                    informationCustomer(context, ObatinIconpack.bodyweight,
                        "55 Kg", iconcolorbodyweight),
                  ],
                ),
              )
            ],
          ),
        ),
        setHSpacing(getWidth(context, 0.04)),
        if (serviceId == 2)
          detailinformationCustomer(context, Str().address,
              "Sendangsari Utara Raya, Pedurungan Semarang", true,
              icon: ObatinIconpack.distancepoint,
              icontxt: "2.4 Km dari lokasi anda"),
        Container(
          margin: EdgeInsets.only(top: getWidth(context, 0.04)),
          padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.02)),
          decoration: BoxDecoration(
              color: Color.fromARGB(50, 0, 179, 255),
              borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              detailinformationCustomer(
                  context, "Jadwal Sebelumnya", "16:00, 15 Juni 2021", false),
              Icon(
                Icons.arrow_right_outlined,
                color: defaultcolor2,
                size: getWidth(context, 0.14),
              ),
              detailinformationCustomer(
                  context, "Reschedule", "16:00, 16 Juni 2021", false),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.04)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(txt: Str().pricedetail, bold: true, size: 0.035),
              CustomText(txt: "Rp. 120.000,-", size: 0.07),
              SizedBox(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      txt: "- Rp. 40.000,- Biaya Transport",
                      size: 0.035,
                      color: Colors.green.shade600,
                    ),
                    CustomText(
                      txt: "- Rp. 80.000,- Biaya Layanan",
                      size: 0.035,
                      color: Colors.green.shade600,
                    ),
                  ],
                ),
              ),
              SizedBox(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    CustomButton(
                      styleId: 1,
                      onPressed: () => notificationConfirmDialog(context, true),
                      btnText: "Terima",
                    ),
                    setWSpacing(getWidth(context, 0.04)),
                    CustomButton(
                      styleId: 1,
                      bColor: red,
                      onPressed: () =>
                          notificationConfirmDialog(context, false),
                      btnText: "Tolak",
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    ),
  );
}

informationCustomer(
    BuildContext context, IconData icon, String text, Color color) {
  return SizedBox(
    height: getWidth(context, 0.043),
    child: Row(
      children: [
        Icon(
          icon,
          size: getWidth(context, 0.032),
          color: color,
        ),
        setWSpacing(10),
        CustomText(
          txt: text,
          size: 0.03,
        )
      ],
    ),
  );
}

initialServiceRequest(BuildContext context, int serviceId) {
  if (serviceId == 1) {
    return informationCustomer(context, ObatinIconpack.onlocation,
        Str().onlocationreq, iconcolorservicesblue);
  } else if (serviceId == 2) {
    return informationCustomer(context, ObatinIconpack.visit,
        Str().visitpatientreq, iconcolorservicesblue);
  } else if (serviceId == 3) {
    return informationCustomer(context, ObatinIconpack.telemedicine,
        Str().telemedicinereq, iconcolorservicesblue);
  } else {
    return informationCustomer(context, ObatinIconpack.volunteer,
        Str().volunteerreq, iconcolorservicesblue);
  }
}

detailinformationCustomer(
    BuildContext context, String txt1, String txt2, bool detailIcon,
    {IconData? icon, String? icontxt}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          txt: txt1,
          size: 0.035,
          bold: true,
        ),
        CustomText(
          txt: txt2,
          size: 0.035,
        ),
        if (detailIcon == true)
          informationCustomer(context, ObatinIconpack.distancepoint,
              "2.4 Km dari lokasi anda", iconcolordistance)
      ],
    ),
  );
}
