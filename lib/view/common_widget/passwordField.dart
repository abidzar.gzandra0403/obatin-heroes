import 'package:obatin/importPackage.dart';

class PasswordField extends StatelessWidget {
  final String? hint;
  final TextEditingController? controller;
  final bool? secureText;
  final VoidCallback? onIconPress;
  const PasswordField(
      {@required this.hint,
      @required this.controller,
      @required this.secureText,
      @required this.onIconPress});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
      child: Stack(
        children: [
          Container(
              constraints: BoxConstraints(minHeight: getWidth(context, 0.12)),
              margin: EdgeInsets.only(top: getWidth(context, 0.015)),
              decoration: BoxDecoration(
                  border: Border.all(
                      width: getWidth(context, 0.006), color: borderColor()),
                  borderRadius: BorderRadius.circular(10)),
              child: TextField(
                obscureText: secureText != null ? secureText! : false,
                controller: controller,
                textInputAction: TextInputAction.done,
                style: hnnormal(context,
                    txtSize: getWidth(context, 0.036), txtColor: fontColor1()),
                decoration: InputDecoration(
                  suffixIcon: IconButton(
                    onPressed: onIconPress!,
                    icon: Icon(
                      (secureText != null && secureText == true)
                          ? Icons.visibility_off
                          : Icons.visibility,
                      color: fontColor1(),
                      size: getWidth(context, 0.055),
                    ),
                  ),
                  contentPadding: EdgeInsets.all(getWidth(context, 0.03)),
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                ),
              )),
          AnimatedContainer(
            duration: Duration(milliseconds: 500),
            curve: Curves.fastOutSlowIn,
            margin: EdgeInsets.only(left: getWidth(context, 0.02)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
            decoration: BoxDecoration(color: bgColor()),
            child: CustomText(txt: hint, color: defaultcolor5, bold: true),
          ),
        ],
      ),
    );
  }
}
