import 'package:obatin/importPackage.dart';

dateRangePicker(BuildContext context,
    {@required GlobalKey? formKey, @required DateTimeRange? dateTimeRange}) {
  return Form(
    key: formKey,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          txt: "Berdasarkan Tanggal",
          size: 0.035,
        ),
        Align(
          alignment: Alignment.topLeft,
          child: SizedBox(
            width: getWidth(context, 0.7),
            height: getWidth(context, 0.2),
            child: DateRangeField(
                saveText: "Konfirmasi",
                helpText: "Pilih Jangka Tanggal",
                cancelText: "Batal",
                confirmText: "Konfirmasi",
                errorFormatText: "Format Salah",
                errorInvalidText: "Diluar Jangka",
                errorInvalidRangeText: "Jangka Salah",
                fieldStartHintText: "Tanggal Mulai",
                fieldEndHintText: "Tanggal Akhir",
                fieldStartLabelText: "Tanggal Mulai",
                fieldEndLabelText: "Tanggal Akhir",
                firstDate: DateTime(1990),
                initialValue:
                    DateTimeRange(start: DateTime.now(), end: DateTime.now()),
                enabled: true,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(getWidth(context, 0.03)),
                  border: OutlineInputBorder(
                      borderRadius:
                          BorderRadius.circular(getWidth(context, 0.03))),
                ),
                validator: (value) {
                  if (value!.start.isBefore(DateTime.now())) {
                    return 'Pilih Tanggal Akhir';
                  }
                  return null;
                },
                onSaved: (value) {
                  // setState(() {
                  //   dateTimeRange = value!;
                  // });
                }),
          ),
        ),
      ],
    ),
  );
}
