import 'package:obatin/importPackage.dart';

supportCareinAppBar(BuildContext context) {
  return AppBar(
    systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
      statusBarIconBrightness:
          themeID == 1 ? Brightness.dark : Brightness.light,
    ),
    automaticallyImplyLeading: false,
    toolbarHeight: getWidth(context, 0.2),
    backgroundColor: bgColor(),
    elevation: 0.0,
    title: Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.03)),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        IconButton(
            padding: EdgeInsets.all(0),
            alignment: Alignment.centerLeft,
            hoverColor: Colors.transparent,
            color: Colors.transparent,
            focusColor: Colors.transparent,
            splashColor: Colors.transparent,
            highlightColor: Colors.transparent,
            disabledColor: Colors.transparent,
            onPressed: () {
              Navigator.pop(context);
            },
            icon: Icon(
              Icons.arrow_back_ios_new_outlined,
              color: fontColor1(),
              size: getWidth(context, 0.05),
            )),
        Expanded(
          child: CustomText(
              txt: "CARE-in", size: 0.04, color: fontColor1(), bold: true),
        ),
        CustomButton(
          styleId: 4,
          shapeId: 4,
          // width: 0.2,
          onPressed: () {},
          prefixIcon: Icons.call_outlined,
          isize: 0.07,
        )
      ]),
    ),
  );
}
