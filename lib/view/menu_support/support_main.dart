import 'package:obatin/importPackage.dart';

class SupportMain extends StatefulWidget {
  @override
  _SupportMainState createState() => _SupportMainState();
}

class _SupportMainState extends State<SupportMain> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) =>
          AnnotatedRegion<SystemUiOverlayStyle>(
              value: SystemUiOverlayStyle(
                statusBarColor: Colors.transparent,
                statusBarIconBrightness:
                    themeID == 1 ? Brightness.dark : Brightness.light,
              ),
              child: Scaffold(
                backgroundColor: bgColor(),
                appBar: CustomAppBar(context, Str().support, true),
                floatingActionButton: CustomButton(
                  styleId: 1,
                  width: 0.3,
                  height: 0.15,
                  onPressed: () => goToPage(context, page: SupportCarein()),
                  prefixWidget: CustomSVG(
                    svgName: svg_iconcarein,
                    size: 0.07,
                    color: Colors.white,
                  ),
                  btnText: "CARE-in",
                ),
                body: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.2)),
                  child: Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: defaultPaddingContent(context)),
                    child: Column(
                      children: [
                        settingsBtn(
                            gotopage: SupportHelpCenter(),
                            title: Str().support$supportcenter,
                            info: Str().support$supportcenterdesc,
                            svgName: svg_iconsupportcenter),
                        settingsBtn(
                            gotopage: SupportComplains(),
                            title: Str().support$complain,
                            info: Str().support$complaindesc,
                            svgName: svg_iconcomplains),
                        settingsBtn(
                            gotopage: SupportConsumerComplains(),
                            title: Str().support$customercomplain,
                            info: Str().support$customercomplaindesc,
                            svgName: svg_iconconsumencomplains),
                        settingsBtn(
                            gotopage: SupportAboutObatin(),
                            title: Str().support$aboutobatin,
                            info: Str().support$aboutobatindesc,
                            svgName: svg_iconabout),
                        settingsBtn(
                            func: () => Navigator.push(
                                context,
                                SlideRightRoute(
                                    page: TermPrivacyView(
                                  showAgreementBtn: false,
                                ))),
                            title: Str().support$termNcondition,
                            info: Str().support$termNconditiondesc,
                            svgName: svg_icontermcondition),
                        settingsBtn(
                            gotopage: SupportPrivacyPolicy(),
                            title: Str().support$privacypolicy,
                            info: Str().support$privacypolicydesc,
                            svgName: svg_iconprivacypolicy),
                        settingsBtn(
                            gotopage: SupportIntellectualRights(),
                            title: Str().support$intellectualrights,
                            info: Str().support$intellectualrightsdesc,
                            svgName: svg_iconintelectualrights),
                        settingsBtn(
                            func: () => onWorkingDialog(context),
                            title: Str().support$reviewapp,
                            info: Str().support$reviewappdesc,
                            svgName: svg_iconreviewapp),
                        settingsBtn(
                            gotopage: SupportOfficialWebView(),
                            title: Str().support$officialobatinwebsite,
                            info: Str().support$officialobatinwebsitedesc,
                            svgName: svg_iconobatin),
                      ],
                    ),
                  ),
                ),
              )),
    );
  }

  settingsBtn(
      {@required String? title,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
        styleId: 5,
        verpad: 0.02,
        btnText: title,
        btnText2: info != null ? info : null,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        prefixWidget: CustomSVG(
          svgName: svgName,
          color: defaultcolor3,
          hormargin: 0.02,
          size: 0.06,
        ),
        onPressed: () {
          if (gotopage != null) {
            goToPage(context, page: gotopage);
          } else {
            func!();
          }
        });
  }
}
