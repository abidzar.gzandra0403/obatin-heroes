import 'package:obatin/importPackage.dart';

class SupportHelpCenter extends StatefulWidget {
  @override
  _SupportHelpCenterState createState() => _SupportHelpCenterState();
}

class _SupportHelpCenterState extends State<SupportHelpCenter> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().support$supportcenter, true),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                setHSpacing(getWidth(context, 0.05)),
                CustomText(
                    txt: Str().support$welcometojelasin +
                        "\n" +
                        Str().support$introductionmistero),
                CustomText(
                  toppad: 0.05,
                  bold: true,
                  txt: Str().support$anyhelpmistero,
                ),
                CustomTextField(
                  hint: Str().support$searchtopic,
                  styleId: 2,
                  prefixIcon: Icons.search,
                ),
                CustomText(
                  toppad: 0.05,
                  botpad: 0.02,
                  bold: true,
                  txt: Str().support$selecttopicmistero + " :",
                ),
                GridView.count(
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 4,
                  shrinkWrap: true,
                  childAspectRatio: 1.4,
                  crossAxisSpacing: getWidth(context, 0.02),
                  mainAxisSpacing: getWidth(context, 0.02),
                  children: [
                    buttonTopic("Akun"),
                    buttonTopic("Pesanan"),
                    buttonTopic("Pembayaran"),
                    buttonTopic("Pengiriman"),
                    buttonTopic("Komplain"),
                    buttonTopic("Promosi"),
                    buttonTopic("Menu OBAT-in"),
                    buttonTopic("Syarat & Ketentuan"),
                  ],
                ),
                CustomText(
                  toppad: 0.05,
                  botpad: 0.02,
                  bold: true,
                  txt: Str().support$frequentquestion + " :",
                ),
                ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    buttonFaq("Bagaimana cara kerja aplikasi?"),
                    buttonFaq("Gabung Mitra Obatin?"),
                    buttonFaq("Cara Pembayaran?"),
                    buttonFaq("Apa itu layanan unggulan dan spesialisasi?"),
                    buttonFaq("Bagaimana mekanisme layanan Visit Patient?"),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buttonTopic(String txt) {
    return CustomButton(
      horpad: 0.01,
      styleId: 6,
      onPressed: () {},
      btnText: txt,
      fColor: Colors.white,
    );
  }

  buttonFaq(String txt) {
    return CustomButton(
      styleId: 5,
      onPressed: () => Navigator.push(
          context, SlideRightRoute(page: SupportFAQAnswer(title: txt))),
      btnText: txt,
    );
  }
}



  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     backgroundColor: bgColor(),
  //     appBar: CustomAppBar(context, "Layanan Konsumen", true),
  //     body: SingleChildScrollView(
  //       child: Padding(
  //         padding:
  //             EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
  //         child: Column(
  //           children: [
  //             Container(
  //               child: Column(
  //                 crossAxisAlignment: CrossAxisAlignment.start,
  //                 children: [
  //                   new Row(
  //                     //Digunakan agar widget mengisi ruang kosong pada layar
  //                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                     children: <Widget>[
  //                       CustomButton(
  //                         styleId: 6,
  //                         width: 0.25,
  //                         onPressed: () {},
  //                         btnText: "Hubungi Kami",
  //                         fColor: Colors.white,
  //                         prefixIcon: Icons.call_outlined,
  //                       ),
  //                       CustomButton(
  //                         styleId: 6,
  //                         width: 0.25,
  //                         onPressed: () {},
  //                         fColor: Colors.white,
  //                         btnText: "Pusat Bantuan",
  //                         prefixIcon: Icons.help_center_outlined,
  //                       ),
  //                       CustomButton(
  //                         styleId: 6,
  //                         width: 0.25,
  //                         onPressed: () {},
  //                         btnText: "Aduan Konsumen",
  //                         fColor: Colors.white,
  //                         prefixIcon: Icons.report_outlined,
  //                       ),
  //                     ],
  //                   ),
  //                   SizedBox(
  //                     height: 8,
  //                   ),
  //                   CustomText(
  //                     toppad: 0.05,
  //                     width: getWidth(context, 1),
  //                     align: TextAlign.center,
  //                     txt: "FAQ",
  //                     bold: true,
  //                   ),
  //                   Container(
  //                     padding: EdgeInsets.all(10),
  //                     width: getWidth(context, 1),
  //                     child: Column(
  //                       children: [
  //                         CustomButton(
  //                           styleId: 5,
  //                           onPressed: () {},
  //                           btnText: "Bagaimana cara kerja aplikasi ?",
  //                         ),
  //                         CustomButton(
  //                           styleId: 5,
  //                           onPressed: () {},
  //                           btnText: "Gabung Mitra Obatin ?",
  //                         ),
  //                         CustomButton(
  //                           styleId: 5,
  //                           onPressed: () {},
  //                           btnText: "Cara Pembayaran ?",
  //                         ),
  //                         CustomButton(
  //                           styleId: 5,
  //                           onPressed: () {},
  //                           btnText:
  //                               "Apa itu layanan unggulan dan spesialisasi ?",
  //                         ),
  //                         CustomButton(
  //                           styleId: 5,
  //                           onPressed: () {},
  //                           btnText: "Berapa lama proses verifikasi SIP ?",
  //                         ),
  //                       ],
  //                     ),
  //                   ),
  //                 ],
  //               ),
  //             )
  //           ],
  //         ),
  //       ),
  //     ),
  //   );
  // }

