import 'package:obatin/importPackage.dart';

class SupportPrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, "", true),
      backgroundColor: bgColor(),
      body: Padding(
        padding: EdgeInsets.all(defaultPaddingContent(context)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              width: getWidth(context, 0.4),
              child: ImgAsset.imgObatin,
            ),
            setHSpacing(getWidth(context, 0.05)),
            CustomText(
                txt: "Kebijakan Privasi",
                bold: true,
                size: 0.05,
                color: fontColor1()),
            setHSpacing(getWidth(context, 0.05)),
            Expanded(
                child: SingleChildScrollView(
              child: CustomText(
                  txt: loremIpsum,
                  size: 0.037,
                  textAlign: TextAlign.justify,
                  color: fontColor1()),
            )),
          ],
        ),
      ),
    );
  }
}
