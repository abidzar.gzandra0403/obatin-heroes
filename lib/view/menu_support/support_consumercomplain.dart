import 'package:obatin/importPackage.dart';

class SupportConsumerComplains extends StatefulWidget {
  @override
  _SupportConsumerComplainsState createState() =>
      _SupportConsumerComplainsState();
}

class _SupportConsumerComplainsState extends State<SupportConsumerComplains> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().support$customercomplain, true),
        body: SingleChildScrollView(
            child: Center(
          child: Container(
            width: getWidth(context, 1),
            child: Column(
              children: <Widget>[
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: getWidth(context, 0.1)),
                  child: Column(
                    children: <Widget>[
                      button(Icons.location_on_outlined,
                          "Gedung I Lantai 3 Jalan M.I Ridwan Rais No.5 Jakarta Pusat 10110"),
                      button(Icons.mail_outline, "contact.us@kemendag.go.id"),
                      button(Icons.phone_callback_outlined,
                          "+62-21-3858171 / +62-21-3451692"),
                    ],
                  ),
                ),
              ],
            ),
          ),
        )),
      ),
    );
  }

  button(IconData icon, String txt) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.03)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          CircleAvatar(
            child: Icon(
              icon,
              size: getWidth(context, 0.06),
              color: Colors.white,
            ),
          ),
          setWSpacing(getWidth(context, 0.03)),
          CustomText(width: 0.6, txt: txt),
        ],
      ),
    );
  }
}
