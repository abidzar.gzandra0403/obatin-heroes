import 'package:obatin/importPackage.dart';

class SupportAboutObatin extends StatefulWidget {
  @override
  _SupportAboutObatinState createState() => _SupportAboutObatinState();
}

class _SupportAboutObatinState extends State<SupportAboutObatin> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, "", true,
          backgroundColor: defaultcolor3,
          brightness: Brightness.dark,
          fontColor: Colors.white),
      backgroundColor: defaultcolor3,
      body: Container(
        width: getWidth(context, 1),
        padding: EdgeInsets.symmetric(
            horizontal: getWidth(context, 0.04),
            vertical: getWidth(context, 0.02)),
        margin: EdgeInsets.only(
            left: getWidth(context, 0.05),
            right: getWidth(context, 0.05),
            bottom: getWidth(context, 0.05)),
        decoration: BoxDecoration(
            color: bgColor(),
            borderRadius: BorderRadius.circular(getWidth(context, 0.04))),
        child: Column(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: Container(
                width: getWidth(context, 0.4),
                margin: EdgeInsets.only(
                    top: getWidth(context, 0.04),
                    bottom: getWidth(context, 0.04)),
                child: ImgAsset.imgObatin,
              ),
            ),
            CustomText(
              botpad: 0.02,
              txt: "Mengenal OBAT-in dan OBAT-in Heroes",
              bold: true,
              size: 0.038,
            ),
            Expanded(
                child: SingleChildScrollView(
              child: CustomText(
                txt: "OBAT-in adalah " + loremIpsum,
                textAlign: TextAlign.justify,
              ),
            ))
            // Container(
            //   height: getWidth(context, 0.2),
            //   decoration: BoxDecoration(color: defaultcolor3),
            // )
          ],
        ),
      ),
    );
  }
}
