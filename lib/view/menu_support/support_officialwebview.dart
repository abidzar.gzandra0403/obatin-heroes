import 'package:obatin/importPackage.dart';

class SupportOfficialWebView extends StatefulWidget {
  @override
  _SupportOfficialWebViewState createState() => _SupportOfficialWebViewState();
}

class _SupportOfficialWebViewState extends State<SupportOfficialWebView> {
  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  var officialUrl = "http://obatin.id/";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, "", true,
          rightWidget: CustomButton(
            styleId: 1,
            bColor: defaultcolor2,
            onPressed: () async {
              if (await canLaunch(officialUrl))
                await launch(officialUrl);
              else
                print("error");
            },
            suffixIcon: Icons.open_in_new_outlined,
            btnText: "Buka di browser",
          ),
          backgroundColor: defaultcolor1,
          brightness: Brightness.light,
          fontColor: Colors.white),
      backgroundColor: Colors.grey.shade50,
      body: Stack(children: [
        Container(
          child: WebView(
            onWebResourceError: (e) {
              Navigator.pop(context);
              CustomBottomDialog(context,
                  txt: "Terjadi Kesalahan Saat Memuat",
                  prefixIcon: Icons.warning_amber_outlined);
            },
            onPageFinished: (e) => Navigator.pop(context),
            onWebViewCreated: (i) => CustomAlertDialog(
              context,
              dismissable: false,
              backgroundColor: Colors.transparent,
              elevation: 0,
              contentWidget: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SpinKitWave(
                      color: Colors.blue.shade50, size: getWidth(context, 0.1)),
                  CustomText(
                    toppad: 0.03,
                    txt: "Memuat Website OBAT-in",
                    color: Colors.white,
                    bold: true,
                  )
                ],
              ),
            ),
            initialUrl: officialUrl,
            javascriptMode: JavascriptMode.unrestricted,
          ),
        )
      ]),
    );
  }
}
