import 'package:obatin/importPackage.dart';

class SupportComplains extends StatefulWidget {
  const SupportComplains({Key? key}) : super(key: key);

  @override
  _SupportComplainsState createState() => _SupportComplainsState();
}

class _SupportComplainsState extends State<SupportComplains> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().complaint, true),
        body: SingleChildScrollView(
          padding: EdgeInsets.symmetric(
              horizontal: defaultPaddingContent(context),
              vertical: getWidth(context, 0.04)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                txt: Str().support$canobatinhelp,
                bold: true,
                size: 0.036,
                color: defaultcolor1,
              ),
              CustomText(
                  toppad: 0.04,
                  botpad: 0.02,
                  bold: true,
                  txt: Str().support$tellyourproblem),
              CustomTextField(
                hint: Str().support$problemdetail,
                minHeight: 0.3,
                maxlines: 10,
              ),
              CustomText(
                txt: Str().support$mincharacter,
                color: Colors.grey,
              ),
              CustomText(
                toppad: 0.04,
                botpad: 0.02,
                txt: Str().support$transactionproblem,
                bold: true,
              ),
              SizedBox(
                height: getWidth(context, 0.15),
                child: DottedBorder(
                    borderType: BorderType.RRect,
                    dashPattern: [10, 5],
                    color: Colors.grey,
                    radius: Radius.circular(getWidth(context, 0.03)),
                    child: Center(
                      child: CustomButton(
                        styleId: 3,
                        width: 0.4,
                        onPressed: () {},
                        btnText: Str().support$selecttransaction,
                        prefixIcon: Icons.add_outlined,
                      ),
                    )),
              ),
              CustomText(
                toppad: 0.04,
                botpad: 0.02,
                txt: Str().support$attachment,
                bold: true,
              ),
              CustomText(
                botpad: 0.03,
                txt: Str().support$attachmentdesc,
              ),
              SizedBox(
                height: getWidth(context, 0.2),
                child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: 1,
                  itemBuilder: (BuildContext context, int index) => SizedBox(
                    height: getWidth(context, 0.2),
                    width: getWidth(context, 0.15),
                    child: DottedBorder(
                        borderType: BorderType.RRect,
                        dashPattern: [10, 5],
                        color: Colors.grey,
                        radius: Radius.circular(getWidth(context, 0.03)),
                        child: Center(
                          child: CustomButton(
                            styleId: 3,
                            width: 0.1,
                            height: 0.1,
                            onPressed: () {},
                            prefixIcon: Icons.add_outlined,
                          ),
                        )),
                  ),
                ),
              ),
              CustomText(
                toppad: 0.03,
                txt: Str().support$format1,
                color: defaultcolor2,
                size: 0.03,
              ),
              CustomText(
                  txt: Str().support$format2, color: defaultcolor2, size: 0.03),
              CustomButton(
                  styleId: 1,
                  vermargin: 0.04,
                  onPressed: () => CustomAlertDialog(context,
                      title: Str().send,
                      contentText: Str().support$complaincheckdialog,
                      contentIcon: Icons.check_circle_outline,
                      funcDismiss: () => Navigator.pop(context)),
                  btnText: Str().send)
            ],
          ),
        ));
  }
}
