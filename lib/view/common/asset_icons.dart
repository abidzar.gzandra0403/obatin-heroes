import 'package:obatin/importPackage.dart';

setIcon(var icnName) {
  var iconDir = "assets/icon/";
  return Image.asset(iconDir + icnName);
}

// final icon_google = setIcon("logos_google-icon.png");
// final icon_facebook = setIcon("logos_facebook.png");
// final icon_stars = setIcon("stars.png");

//homepage
final icon_linkaja = setIcon("linkaja_logo.png");
final icon_gopay = setIcon("icon_gopay.png");
// final icon_linkajareceive = setIcon("linkaja_receive.png");
// final icon_linkajascan = setIcon("linkaja_scan.png");
// final icon_linkajatopup = setIcon("linkaja_topup.png");
// final icon_linkajatransfer = setIcon("linkaja_transfer.png");
// final icon_message = setIcon("message.png");
// final icon_newreview = setIcon("newreview.png");
// final icon_ordercomplains = setIcon("ordercomplains.png");

//rawatin_landingpage
// final icon_rawatinonlocation = setIcon("rawatin_onlocation.png");
// final icon_rawatintelemedicine = setIcon("rawatin_telemedicine.png");
// final icon_rawatinvisitpatient = setIcon("rawatin_visitpatient.png");
// final icon_rawatinvolunteer = setIcon("rawatin_volunteer.png");
final icon_rawatinonlocation2 = setIcon("rawatin_onlocation2.png");
final icon_rawatintelemedicine2 = setIcon("rawatin_telemedicine2.png");
final icon_rawatinvisitpatient2 = setIcon("rawatin_visitpatient2.png");
final icon_rawatinvolunteer2 = setIcon("rawatin_volunteer2.png");
