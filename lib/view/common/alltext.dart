// //global
// import 'package:obatin/main.dart';

// final txt_login = 'Login';
// final txt_username = 'Username';
// final txt_pass = 'Password';
// final txt_email = 'Email';
// final txt_passconfirm = "Konfirmasi Password"; //'Password Confirmation';
// final txt_submit = 'Submit';
// final txt_confirmexit = "Keluar Aplikasi?";
// final txt_yes = "Ya";
// final txt_no = "Batal";
// final txt_accept = "Terima";
// final txt_decline = "Tolak";

// //intro
// final txt_intro1 =
//     'Anda Adalah Pahlawan dari Misi Besar Integrated Health System. Kami Menyebut Anda, OBAT-in Heroes';
// final txt_intro2 =
//     'Anda Bebas Menentukan Jenis Layanan, Waktu Layanan dan Tarif Layanan';
// final txt_intro3 =
//     'Selain Bisa Konsultasi dan Berobat, Anda Juga Bisa Belanja Produk Kesehatan dari Mitra & Mitra Plus Kami';
// final txt_intro1b =
//     'Dokter • Dokter Spesialis • Dokter Konsultan • Dokter Gigi • Dokter Gigi Spesialis • Dokter Gigi Konsultan • Perawat • Bidan • Fisioterapis • Akupunkturis • Psikolog • Ahli Gizi • Terapis Wicara • Terapis Okupasi • dan lainnya';
// final txt_intro2b = 'On Location • Visit Pasien • Telemedicine • Sukarela';
// final txt_intro3b =
//     'Apotek • Alat Kesehatan • PKRT • Optik • Kebutuhan Pendidikan Kesehatan • Produk Ergonomis • Produk UMKM Kesehatan';
// final txt_next = "Selanjutnya"; //'Next';
// final txt_finish = "Selesai"; //'Finish';
// final txt_skip = "Lewati"; //'Skip';

// //login
// final txt_welcome1 = "Selamat Datang"; //'Welcome Back!';
// final txt_forgot = "Lupa Password?"; //'Forgot Password?';
// final txt_noaccount = "Tidak Punya Akun?"; //'No Have Account?';
// final txt_createaccount = "Buat Akun"; //'Create Account';
// final txt_signwith = 'Sign Up With';

// //register
// final txt_notmatch = "Tidak Cocok"; //'Not Match';
// final txt_register = "Daftar"; //'Register';
// final txt_phone = "Nomor Telepon"; //'Phone Number';
// final txt_phonecheck =
//     "Pastikan nomor anda aktif"; //"'Make sure your phone is active';
// final txt_enterotp = "Masukkan 4 kode digit"; //'Enter 4 digits code';
// final txt_resend = "Kirim Ulang"; //'Resend';
// final txt_cancelregist = "Batal Mendaftar?"; //'Cancel Register?';
// final txt_confirmcancelregist =
//     "Semua perubahan tidak akan disimpan"; //'Are you sure unsave changes?';

// //forget
// final txt_forget = "Lupa Kata Sandi"; //'Forget Password';
// final txt_reset = 'Reset Password';

// //reset
// final txt_newpass = "Password Baru"; //'New Password';
// final txt_cancelresetpass = "Batal"; // 'Cancel Reset Password?';
// final txt_confirmcancelresetpass =
//     "Semua perubahan tidak akan disimpan"; //'Are you sure unsave changes?';

// //homepage
// final txt_hai = 'Hai, ';
// final txt_welcome2 = "Selamat Datang"; //'Welcome Back!';
// final txt_unreadmessage = "Pesan Belum Dibaca";
// final txt_newreview = "Review Baru Pelanggan";
// final txt_ordercomplain = "Pesanan Dikomplain";
// final txt_customerrating = "Rating Pelanggan";
// final txt_viewdetail = "Lihat Selengkapnya";
// //mainscreen

// //rawatin
// final txt_active = "Aktif";
// final txt_nonactive = "Nonaktif";
// final txt_newrequest = "Permintaan Baru";
// final txt_pleaseapplyrequestpatient =
//     "Mohon segera konfirmasi permintaan pasien";
// final txt_visitpatientreq = "Permintaan Visit Patient";
// final txt_onlocationreq = "Permintaan On Location";
// final txt_telemedicinereq = "Permintaan Telemedicine";
// final txt_volunteerreq = "Permintaan Suka Rela";
// final txt_clicktodetail = "Ketuk Untuk Lihat Detail";
// final txt_historyreq = "Riwayat Permintaan";
// final txt_confirmrequest = "Konfirmasi Permintaan Pasien";
// final txt_gendermanpatient = "Pria";
// final txt_genderwomanpatient = "Pria";
// final txt_address = "Alamat";
// final txt_servicereq = "Layanan Diminta";
// final txt_patientnotes = "Catatan Pasien";
// final txt_detailpricing = "Detail Harga";

// //kenalin
// final txt_akun = "Akun Pengguna";
// final txt_customer = "Customer care";
// final txt_tentang = "Tentang Kami";
// //final txt_ = "";

// txtconfirmexit<String>() {
//   return indLang == false ? "Keluar Aplikasi?" : "Exit Apps?";
// }

// txtyes<String>() {
//   return indLang == false ? "Ya" : "Yes";
// }

// txtno<String>() {
//   return indLang == false ? "Tidak" : "No";
// }

// txtactivity<String>() {
//   return indLang == false ? "Aktifitas" : "Activity";
// }

// txtorderhistory<String>() {
//   return indLang == false ? "Riwayat Pesanan" : "Order History";
// }

// txtongoing<String>() {
//   return indLang == false ? "Sedang Berlangsung" : "On Going";
// }

// txtupcoming<String>() {
//   return indLang == false ? "Mendatang" : "Upcoming";
// }

// txtrecently<String>() {
//   return indLang == false ? "Baru-baru ini" : "Recently";
// }

// txtall<String>() {
//   return indLang == false ? "Semua" : "All";
// }

// txtsearchchat<String>() {
//   return indLang == false ? "Cari Percakapan" : "Search Conversation";
// }

// txtwritemessage<String>() {
//   return indLang == false ? "Tulis Pesan" : "Write Message";
// }

// txtcustomercare<String>() {
//   return indLang == false ? "Layanan Pelanggan" : "Customer Care";
// }

// txtabout<String>() {
//   return indLang == false ? "Tentang Kami" : "About Us";
// }

// txtlanguage<String>() {
//   return indLang == false ? "Bahasa" : "Language";
// }

// txtdarkmode<String>() {
//   return indLang == false ? "Mode Gelap" : "Dark Mode";
// }

// txtcallus<String>() {
//   return indLang == false ? "Hubungi Kami" : "Call Us";
// }

// txtsupportcenter<String>() {
//   return indLang == false ? "Pusat Bantuan" : "Support Center";
// }

// txtconcomplain<String>() {
//   return indLang == false ? "Aduan Konsumen" : "Customer Complain";
// }

// txthowappwork<String>() {
//   return indLang == false
//       ? "Bagaimana cara kerja aplikasi?"
//       : "How does the application work?";
// }

// txtmitraobatin<String>() {
//   return indLang == false ? "Mitra OBAT-in?" : "OBAT-in Partner?";
// }

// txthowpayment<String>() {
//   return indLang == false ? "Bagaimana Cara pembayaran?" : "How to Payment";
// }

// txtofficialwebsite<String>() {
//   return indLang == false ? "Situs Web Resmi" : "Official Website";
// }

// txttermservice<String>() {
//   return indLang == false ? "Ketentuan Layanan" : "Terms of Service";
// }

// txtprivacypolicy<String>() {
//   return indLang == false ? "Kebijakan Privasi" : "Privacy Policy";
// }

// txtcopyright<String>() {
//   return indLang == false ? "Hak Cipta" : "Copyright";
// }

// txtdigitalwallet<String>() {
//   return indLang == false ? "Dompet Digital" : "E-Wallet";
// }

// txtpoint<String>() {
//   return indLang == false ? "Poin" : "Points";
// }

// txtpay<String>() {
//   return indLang == false ? "Bayar" : "Pay";
// }

// txttopup<String>() {
//   return indLang == false ? "Top Up" : "Top Up";
// }

// txttransfer<String>() {
//   return indLang == false ? "Kirim" : "Transfer";
// }

// txtreceive<String>() {
//   return indLang == false ? "Terima" : "Receive";
// }

// txtpayment<String>() {
//   return indLang == false ? "Pembayaran" : "Payment";
// }

// txtlasttransaction<String>() {
//   return indLang == false ? "Transaksi Terakhir" : "Last Transaction";
// }

// txtinfopengguna<String>() {
//   return indLang == false ? "Informasi Pengguna" : "User Information";
// }
