import 'package:obatin/importPackage.dart';

class ImgAsset {
  //General String
  static setImg(var imgName) {
    var imageDir = "assets/images/";
    return Image.asset(
      imageDir + imgName,
    );
  }

  static setImgSplash(var imgName) {
    var imageDir = "assets/splash/";
    return Image.asset(imageDir + imgName);
  }

//obatinlogo
  static var imgObatin = setImg("obatin.png");
//promo

//ajakin
  static var imgAjakinBanner = setImg("ajakinbanner.jpg");
//splash
  static var splashObatin1 = setImgSplash("obatin1.png");
  static var splashObatin2 = setImgSplash("obatin2.png");
  static var splashObatin3 = setImgSplash("obatin3.png");
//intro
  static var imgIntro1 = setImg("introimage1.png");
  static var imgIntro2 = setImg("introimage2.png");
  static var imgIntro3 = setImg("introimage3.png");
//other
  static var imgSobatin = setImg("sobatin.png");
  static var imgPraktik1 = setImg("praktik1.png");
  static var imgPraktik2 = setImg("praktik2.png");
  static var imgPraktik3 = setImg("praktik3.png");
  static var imgPhoneverification = setImg("phoneverificationImg.png");
  static var imgMaps = "maps.jpg";
  static var imgDokterExample = setImg("dokter.png");
  static var imgWaitingPayment = setImg("waitingpayment.png");
  static var imgRequestAccept = setImg("requestaccept.png");
  static var imgRequestDecline = setImg("requestdecline.png");
  static var imgVolunteerApreciation = setImg("volunterappreciation.png");
  static var imgSettings = setImg("settings.png");
  static var profilePhotoExample = setImg("profilephotoexample.png");
  static var imgRegisterdoctor = setImg("registerdoctor.png");
  static var imgRegisternakes = setImg("registernakes.png");
  static var imgRegisterpharmacy = setImg("registerpharmacy.png");
}
