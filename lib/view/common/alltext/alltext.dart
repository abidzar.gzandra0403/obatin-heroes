// //global
// import 'package:obatin/main.dart';

// txtlogin<String>() {
//   return indLang == true ? "Masuk" : "Login";
// }

// final txt_username = 'Username';
// final txtpass = 'Password';
// final txtemail = 'Email';
// final txt_login = 'Login';
// final txt_pass = 'Password';
// final txt_email = 'Email';
// final txt_passconfirm = "Konfirmasi Password"; //'Password Confirmation';
// final txt_submit = 'Submit';
// final txt_confirmexit = "Keluar Aplikasi?";
// final txt_yes = "Ya";
// final txt_no = "Batal";
// final txt_accept = "Terima";
// final txt_decline = "Tolak";

// //intro

// //login
// final txt_welcome1 = "Selamat Datang"; //'Welcome Back!';
// final txt_forgot = "Lupa Password?"; //'Forgot Password?';
// final txt_noaccount = "Tidak Punya Akun?"; //'No Have Account?';
// final txt_createaccount = "Buat Akun"; //'Create Account';
// final txt_signwith = 'Sign Up With';

// //register
// final txt_notmatch = "Tidak Cocok"; //'Not Match';
// final txt_register = "Daftar"; //'Register';
// final txt_phone = "Nomor Telepon"; //'Phone Number';
// final txt_phonecheck =
//     "Untuk aktivasi, Kami akan mengirim Kode OTP ke nomor HP tersebut. Pastikan HP kamu aktif."; //"'Make sure your phone is active';
// final txt_enterotp = "Masukkan 4 kode digit"; //'Enter 4 digits code';
// final txt_resend = "Kirim Ulang"; //'Resend';
// final txt_cancelregist = "Batal Mendaftar?"; //'Cancel Register?';
// final txt_confirmcancelregist =
//     "Semua perubahan tidak akan disimpan"; //'Are you sure unsave changes?';

// //forget
// final txt_forget = "Lupa Kata Sandi"; //'Forget Password';
// final txt_reset = 'Reset Password';

// //reset
// final txt_newpass = "Password Baru"; //'New Password';
// final txt_cancelresetpass = "Batal"; // 'Cancel Reset Password?';
// final txt_confirmcancelresetpass = "Semua perubahan tidak akan disimpan";

// txtpassconfirm<String>() {
//   return indLang == true ? "Konfirmasi Password" : "Password Confirmation";
// }

// txtsubmit<String>() {
//   return indLang == true ? "Submit" : "Submit";
// }

// txtconfirmexit<String>() {
//   return indLang == true ? "Keluar Aplikasi?" : "Exit Apps?";
// }

// txtyes<String>() {
//   return indLang == true ? "Ya" : "Yes";
// }

// txtno<String>() {
//   return indLang == true ? "Tidak" : "No";
// }

// txtaccept<String>() {
//   return indLang == true ? "Terima" : "Accept";
// }

// txtdecline<String>() {
//   return indLang == true ? "Tolak" : "Decline";
// }

// //intro
// final txt_intro1 = 'Dapatkan Layanan Kapanpun dan di Manapun';
// final txt_intro2 = 'Bebas Pilih Jenis Layanan Sesuai Kebutuhan';
// final txt_intro3 = 'Belanja Produk Kesehatan Dari Mitra & Mitra Plus Kami';
// final txt_intro1b =
//     'Dokter | Dokter Spesialis | Dokter Konsultan | Dokter Gigi | Dokter Gigi Spesialis | Dokter Gigi Konsultan | Perawat | Bidan | Fisioterapis | Akupunkturis | Psikolog | Ahli Gizi | Terapis Wicara | Terapis Okupasi | dan lainnya';
// final txt_intro2b = 'On Location | Visit Pasien | Telemedicine | Sukarela';
// final txt_intro3b =
//     'Apotek | Alat Kesehatan | PKRT | Optik | Kebutuhan Pendidikan Kesehatan | Produk Ergonomis | Produk UMKM Kesehatan';
// txtnext<String>() {
//   return indLang == true ? "Selanjutnya" : "Next";
// }

// txtfinish<String>() {
//   return indLang == true ? "Selesai" : "Finish";
// }

// txtskip<String>() {
//   return indLang == true ? "Lewati" : "Skip";
// }

// //login
// txtnoaccount<String>() {
//   return indLang == true ? "Tidak Punya Akun?" : "No Have Account?";
// }

// txtcreateaccount<String>() {
//   return indLang == true ? "Buat Akun" : "Create Account";
// }

// //register
// txtnotmatch<String>() {
//   return indLang == true ? "Tidak Cocok" : "Not Match";
// }

// txtregister<String>() {
//   return indLang == true ? "Daftar" : "Register";
// }

// txtphonenumber<String>() {
//   return indLang == true ? "Nomor Telepon" : "Phone Number";
// }

// txtphonecheck<String>() {
//   return indLang == true
//       ? "Pastikan nomor anda aktif"
//       : "Make sure yout phone is active";
// }

// txtenterotp<String>() {
//   return indLang == true ? "Masukkan 4 kode digit" : "Enter 4 digits code";
// }

// txtresend<String>() {
//   return indLang == true ? "Kirim Ulang" : "Resend";
// }

// //forget
// txtforgetpass<String>() {
//   return indLang == true ? "Lupa Kata Sandi?" : "Forget Password?";
// }

// txtresetpass<String>() {
//   return indLang == true ? "Reset Password" : "Reset Password";
// }

// //reset
// txtnewpass<String>() {
//   return indLang == true ? "Password Baru" : "New Password";
// }

// txtcancel<String>() {
//   return indLang == true ? "Batal" : "Cancel";
// }

// txtconfirmunsavechanges<String>() {
//   return indLang == true
//       ? "Semua perubahan tidak akan disimpan"
//       : "All changes not be saved";
// }

// //homepage
// txtwelcome<String>() {
//   return indLang == true ? "Selamat Datang" : "Welcome Back";
// }

// txthai<String>() {
//   return indLang == true ? "Hai, " : "Hi, ";
// }

// //kenalin
// txtaccount<String>() {
//   return indLang == true ? "Akun Pengguna" : "User Account";
// }

// txtcustomercare<String>() {
//   return indLang == true ? "Layanan Konsumen" : "Customer Care";
// }

// txtabout<String>() {
//   return indLang == true ? "Tentang Kami" : "About Us";
// }

// txtaddress<String>() {
//   return indLang == true ? "Kelola Alamat" : "Manage Address";
// }

// txtsettings<String>() {
//   return indLang == true ? "Pengaturan" : "Settings";
// }

// //final txt_ = "";
// txtserviceforyou<String>() {
//   return indLang == true ? "Layanan Untuk Anda" : "Service For You";
// }

// txtobatinpromo<String>() {
//   return indLang == true ? "Promosi Dari OBAT-in" : "OBAT-in Promo";
// }

// txtneeddoctor<String>() {
//   return indLang == true ? "Butuh Dokter?" : "Need a Doctor?";
// }

// txtonlocationdesc<String>() {
//   return indLang == true
//       ? "Buat janji ketemuan dengan dokter"
//       : "Make an appointment with Doctor";
// }

// txtvisitpatientdesc<String>() {
//   return indLang == true
//       ? "Tidak bisa keluar? Biar dokter kerumahmu"
//       : "Can't get out? Let the doctor come to your house";
// }

// txttelemedicinedesc<String>() {
//   return indLang == true
//       ? "Konsultasi dengan dokter lewat smarthone mu"
//       : "Consultation with a doctor via your smartphone";
// }

// txtvolunteerdesc<String>() {
//   return indLang == true
//       ? "Temukan tenaga relawan disekitar anda"
//       : "Find volunteers near you";
// }

// txtmoredetail<String>() {
//   return indLang == true ? "Selengkapnya" : "More Details";
// }

// txtordersomething<String>() {
//   return indLang == true ? "Mau pesan sesuatu?" : "Want to order something?";
// }

// txtneedservice<String>() {
//   return indLang == true
//       ? "Layanan apa yang kamu butuhkan?"
//       : "What services do you need?";
// }

// txtvolunteer<String>() {
//   return indLang == true ? "Suka Rela" : "Volunteer";
// }

// txtnakesnear<String>() {
//   return indLang == true
//       ? "Nakes Disekitar Anda"
//       : "Medical Personnel Near You";
// }

// txtallnakes<String>() {
//   return indLang == true ? "Semua Nakes" : "All Medical Personnel";
// }

// txtyear<String>() {
//   return indLang == true ? "Tahun" : "Year";
// }

// txtsearchnakes<String>() {
//   return indLang == true ? "Cari Nakes" : "Search By Name";
// }

// txtservicetype<String>() {
//   return indLang == true ? "Jenis Layanan" : "Service";
// }

// txtnakestype<String>() {
//   return indLang == true ? "Tenaga Kesehatan" : "Medical Personnel";
// }

// txtages<String>() {
//   return indLang == true ? "Usia" : "Ages";
// }

// txtweightbody<String>() {
//   return indLang == true ? "Berat (Kg)" : "Weight (Kg)";
// }

// txtheightbody<String>() {
//   return indLang == true ? "Tinggi (Cm)" : "Height (Cm)";
// }

// txtuserinfo<String>() {
//   return indLang == true ? "Informasi Pengguna" : "User Information";
// }

// txtsettingsandsupport<String>() {
//   return indLang == true ? "Pengaturan & Bantuan" : "Settings & Support";
// }

// txttransaction<String>() {
//   return indLang == true ? "Transaksi" : "Transaction";
// }

// txtorder<String>() {
//   return indLang == true ? "Dipesan" : "Order";
// }

// txtpacked<String>() {
//   return indLang == true ? "Dikemas" : "Packed";
// }

// txtsent<String>() {
//   return indLang == true ? "Dikirim" : "Sent";
// }

// txtreview<String>() {
//   return indLang == true ? "Ulasan" : "Review";
// }

// txtmyaccount<String>() {
//   return indLang == true ? "Akun Saya" : "My Account";
// }

// txtupdatebio<String>() {
//   return indLang == true ? "Update Biodata" : "Biodata Update";
// }

// txtchangepass<String>() {
//   return indLang == true ? "Ubah Password" : "Change Password";
// }

// txtphotoprofile<String>() {
//   return indLang == true ? "Ubah Foto Profil" : "Change Profile Photos";
// }

// txtdigitalwallet<String>() {
//   return indLang == true ? "Dompet Digital" : "E-Wallet";
// }

// txttransactionhistory<String>() {
//   return indLang == true ? "Riwayat Transaksi" : "Transaction History";
// }

// txtcomplain<String>() {
//   return indLang == true ? "Komplain" : "Complain";
// }

// txtupdateaddress<String>() {
//   return indLang == true ? "Perbaharui Alamat" : "Update Address";
// }

// txtaddaddress<String>() {
//   return indLang == true ? "Tambahkan Alamat" : "Add Address";
// }

// txtlanguage<String>() {
//   return indLang == true ? "Bahasa" : "Language";
// }

// txtdarkmode<String>() {
//   return indLang == true ? "Mode Gelap" : "Dark Mode";
// }

// txtcallus<String>() {
//   return indLang == true ? "Hubungi Kami" : "Call Us";
// }

// txtsupportcenter<String>() {
//   return indLang == true ? "Pusat Bantuan" : "Support Center";
// }

// txtconcomplain<String>() {
//   return indLang == true ? "Aduan Konsumen" : "Customer Complain";
// }

// txthowappwork<String>() {
//   return indLang == true
//       ? "Bagaimana cara kerja aplikasi?"
//       : "How does the application work?";
// }

// txtmitraobatin<String>() {
//   return indLang == true ? "Mitra OBAT-in?" : "OBAT-in Partner?";
// }

// txthowpayment<String>() {
//   return indLang == true ? "Bagaimana Cara pembayaran?" : "How to Payment";
// }

// txtofficialwebsite<String>() {
//   return indLang == true ? "Situs Web Resmi" : "Official Website";
// }

// txttermservice<String>() {
//   return indLang == true ? "Ketentuan Layanan" : "Terms of Service";
// }

// txtprivacypolicy<String>() {
//   return indLang == true ? "Kebijakan Privasi" : "Privacy Policy";
// }

// txtcopyright<String>() {
//   return indLang == true ? "Hak Cipta" : "Copyright";
// }

// txtpersonalinfo<String>() {
//   return indLang == true ? "Data Pribadi" : "Personal Information";
// }

// txtfullname<String>() {
//   return indLang == true ? "Nama Lengkap" : "Full Name";
// }

// txtbirthdate<String>() {
//   return indLang == true ? "Tanggal Lahir" : "Birth Date";
// }

// txtbodyweight<String>() {
//   return indLang == true ? "Berat Badan (Kg)" : "Weight (Kg)";
// }

// txtbodyheight<String>() {
//   return indLang == true ? "Tinggi Badan (Cm)" : "Height (Cm)";
// }

// txtidnumber<String>() {
//   return indLang == true ? "Nomor Kartu Identitas" : "Identity Number";
// }

// txtgender<String>() {
//   return indLang == true ? "Jenis Kelamin" : "Gender";
// }

// txtforsecurityyouraccount<String>() {
//   return indLang == true
//       ? "Untuk mengamankan akun, silahkan verifikasi identitas dengan memasukkan password"
//       : "To fill out an account, please verify your identity by entering your password";
// }

// txtpay<String>() {
//   return indLang == true ? "Bayar" : "Pay";
// }

// txttopup<String>() {
//   return indLang == true ? "Top Up" : "Top Up";
// }

// txttransfer<String>() {
//   return indLang == true ? "Kirim" : "Transfer";
// }

// txtreceive<String>() {
//   return indLang == true ? "Terima" : "Receive";
// }

// txtpayment<String>() {
//   return indLang == true ? "Pembayaran" : "Payment";
// }

// txtlasttransaction<String>() {
//   return indLang == true ? "Transaksi Terakhir" : "Last Transaction";
// }

// txtpoint<String>() {
//   return indLang == true ? "Poin" : "Points";
// }

// txtactivity<String>() {
//   return indLang == true ? "Aktifitas" : "Activity";
// }

// txtactivityhistory<String>() {
//   return indLang == true ? "Riwayat Aktifitas" : "Activity History";
// }

// txtongoing<String>() {
//   return indLang == true ? "Sedang Berlansung" : "On Going";
// }

// txtupcoming<String>() {
//   return indLang == true ? "Mendatang" : "Upcoming";
// }

// txtrecently<String>() {
//   return indLang == true ? "Riwayat" : "Histori";
// }

// txtall<String>() {
//   return indLang == true ? "Semua" : "All";
// }

// txtsearchchat<String>() {
//   return indLang == true ? "Cari Percakapan" : "Search Conversation";
// }

// txtwritemessage<String>() {
//   return indLang == true ? "Tulis Pesan" : "Write Message";
// }

// txt<String>() {
//   return indLang == true ? "" : "";
// }

// final txt_active = "Aktif";
// final txt_nonactive = "Nonaktif";
// final txt_newrequest = "Permintaan Baru";
// final txt_pleaseapplyrequestpatient =
//     "Mohon segera konfirmasi permintaan pasien";
// final txt_visitpatientreq = "Permintaan Visit Patient";
// final txt_onlocationreq = "Permintaan On Location";
// final txt_telemedicinereq = "Permintaan Telemedicine";
// final txt_volunteerreq = "Permintaan Suka Rela";
// final txt_clicktodetail = "Ketuk Untuk Lihat Detail";
// final txt_historyreq = "Riwayat Permintaan";
// final txt_confirmrequest = "Konfirmasi Permintaan Pasien";
// final txt_gendermanpatient = "Pria";
// final txt_genderwomanpatient = "Pria";
// final txt_address = "Alamat";
// final txt_servicereq = "Layanan Diminta";
// final txt_layananreq = "Layanan Unggulan";
// final txt_patientnotes = "Catatan Pasien";
// final txt_detailpricing = "Detail Harga";
