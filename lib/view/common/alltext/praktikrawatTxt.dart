// import 'package:obatin/main.dart';

// //S BOTTOM SHEET INI UNTUK PRAKTIKIN DAN RAWATIN

// praktikrawatTxt_ButuhLayanan<String>() {
//   return indLang == true
//       ? "Layanan apa yang kamu butuhkan?"
//       : "What services do you need?";
// }

// praktikrawatTxt_PilihSpesialisasi<String>() {
//   return indLang == true ? "Pilih Spesialisasi" : "Choose Specialization";
// }

// praktikrawatTxt_MulaiDari<String>() {
//   return indLang == true ? "Mulai Dari" : "Start From";
// }

// praktikrawatTxt_Onloc1<String>() {
//   return indLang == true
//       ? "Adalah Jenis Layanan yang hanya dapat dilayani di tempat praktik Dokter/Nakes."
//       : "Types of Services that can only be served at the doctor's practice.";
// }

// praktikrawatTxt_Onloc2<String>() {
//   return indLang == true
//       ? "Harga yang tertera adalah harga untuk jasa Dokter / Nakes saja."
//       : "Prices listed are prices for medical services only.";
// }

// praktikrawatTxt_Visit1<String>() {
//   return indLang == true
//       ? "Adalah Jenis Layanan yang dapat dilayani oleh Dokter / Nakes di tempat Pasien."
//       : "Types of Services that can be served by the Doctor / Health Worker  at the Patient's place.";
// }

// praktikrawatTxt_Visit2<String>() {
//   return indLang == true
//       ? "Harga yang tertera adalah harga untuk jasa Dokter / Nakes saja."
//       : "Prices listed are prices for medical services only.";
// }

// praktikrawatTxt_Visit3<String>() {
//   return indLang == true
//       ? "Pasien akan dikenakan biaya transportasi dokter Pergi Pulang (PP) sesuai jarak tempuh dari tempat praktik Dokter / Nakes."
//       : "Patients will be charged a doctor's transportation fee round-trip according to the distance from the Doctor's / Health Worker's practice.";
// }

// praktikrawatTxt_Tele1<String>() {
//   return indLang == true
//       ? "Adalah Jenis Layanan konsultasi melalui Chat / Telpon / Video Call."
//       : "Types of consulting services via Chat / Phone / Video Call.";
// }

// praktikrawatTxt_Tele2<String>() {
//   return indLang == true
//       ? "Pastikan sinyal HP dalam keadaan optimal."
//       : "Make sure the HP signal is in optimal condition.";
// }

// praktikrawatTxt_Sukarela1<String>() {
//   return indLang == true
//       ? "Adalah Jenis Layanan tidak berbayar (gratis) yang disediakan oleh Dokter / Nakes untuk beramal, terdiri dari:"
//       : "Types of services that are not paid (free) provided by Doctors / Health Workers for charity, consist of:";
// }

// praktikrawatTxt_Sukarela2<String>() {
//   return indLang == true
//       ? "Sukarela On Location\nAdalah Jenis Layanan gratis yang hanya dapat dilayani di tempat praktik Dokter."
//       : "On Location Volunteer\nFree consultation service via Chat / Phone / Video Call.";
// }

// praktikrawatTxt_Sukarela3<String>() {
//   return indLang == true
//       ? "Sukarela Visit Pasien\nAdalah Jenis Layanan gratis yang dapat dilayani oleh Dokter di tempat Pasien."
//       : "Visit Patient Volunteer\nFree service that can be served by the Doctor / Health Worker at the patient's place.";
// }

// praktikrawatTxt_Sukarela4<String>() {
//   return indLang == true
//       ? "Sukarela Telemedicine\nAdalah Jenis Layanan gratis konsultasi melalui Chat / Telpon / Video Call."
//       : "Telemedicine Volunteer\nFree service that can only be served at the Doctor's / Health Worker's office.";
// }

// praktikrawatTxt_<String>() {
//   return indLang == true ? "" : "";
// }

// praktikrawatTxt_Kebijakan0<String>() {
//   return indLang == true
//       ? "Kebijakan Reschedule, Pembatalan dan Pengembalian Dana :"
//       : "Reschedule, Cancellation and Refund Policy :";
// }

// praktikrawatTxt_Kebijakan1<String>() {
//   return indLang == true
//       ? "Pasien dan dokter dapat melakukan perubahan jadwal masing-masing hanya satu kali."
//       : "Patients and doctors can make changes to each schedule only once.";
// }

// praktikrawatTxt_Kebijakan2<String>() {
//   return indLang == true
//       ? "Pembatalan dapat dilakukan melalui aplikasi OBAT-in maksimal 24 Jam sebelum waktu layanan yang telah ditentukan."
//       : "Cancellation can be done through the drug-in application a maximum of 24 hours before the specified service time.";
// }

// praktikrawatTxt_Kebijakan3<String>() {
//   return indLang == true
//       ? "Pengembalian dana dilakukan melalui dompet digital membutuhkan waktu maksimal 3 hari kerja."
//       : "Refunds made through digital wallets take a maximum of 3 working days.";
// }

// praktikrawatTxt_Kebijakan4<String>() {
//   return indLang == true
//       ? "Apabila pengembalian dana belum diterima sesuai batas waktu, silahkan hubungi customer service kami."
//       : "If the refund has not been received within the time limit, please contact our customer service.";
// }
// //========================================================