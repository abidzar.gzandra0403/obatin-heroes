import 'package:obatin/main.dart';
import 'package:obatin/models/system_models/settingData.dart';

rawatinTxt_Bidan<String>() {
  return langID == 1 ? "Bidan" : "Midwife";
}

rawatinTxt_Perawat<String>() {
  return langID == 1 ? "Perawat" : "Nurse";
}

rawatinTxt_Fisioterapis<String>() {
  return langID == 1 ? "Fisioterapis" : "Physiotherapist";
}

rawatinTxt_AhliGizi<String>() {
  return langID == 1 ? "Ahli Gizi" : "Nutritionists";
}

rawatinTxt_TerapisWicara<String>() {
  return langID == 1 ? "Terapis Wicara" : "Speech Therapist";
}

rawatinTxt_Akupunturis<String>() {
  return langID == 1 ? "Akupunturis" : "Acupuncture";
}

rawatinTxt_Psikolog<String>() {
  return langID == 1 ? "Psikolog" : "Psychology";
}

rawatinTxt_NakesLainnya<String>() {
  return langID == 1 ? "Nakes Lainnya" : "Other Health Worker";
}

rawatinTxt_LihatSemuaNakes<String>() {
  return langID == 1 ? "Lihat Semua Nakes" : "See All Health Workers";
}

rawatinTxt_SemuaNakes<String>() {
  return langID == 1 ? " Semua Nakes" : "All Health Workers";
}

rawatinTxt_NakesTerdekat<String>() {
  return langID == 1 ? "Nakes Terdekat" : "Nearest Health Workers";
}

rawatinTxt_CariNakes<String>() {
  return langID == 1 ? "Cari Nakes" : "Search Health Worker";
}

final txt_onlocationreq = "Permintaan On Location";
final txt_telemedicinereq = "Permintaan Telemedicine";
final txt_volunteerreq = "Permintaan Suka Rela";
final txt_visitpatientreq = "Permintaan Visit Patient";
