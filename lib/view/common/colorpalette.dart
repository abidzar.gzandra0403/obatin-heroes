import 'package:obatin/importPackage.dart';

themeLoader(Color darkColor, Color lightColor) {
  return themeID == 1 ? lightColor : darkColor;
}

bgColor<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 0, 11, 28), Color.fromARGB(255, 245, 252, 255));
}

borderBlueColor<Colors>() {
  return themeLoader(
      Color.fromARGB(70, 74, 180, 255), Color.fromARGB(20, 74, 180, 255));
}

bgDialogColor<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 40, 66, 92), Color.fromARGB(255, 245, 252, 255));
}

bgColorDarken<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 0, 7, 18), Color.fromARGB(255, 247, 253, 255));
}

bgBlue1Color<Colors>() {
  return themeLoader(Color.fromARGB(255, 47, 75, 120), defaultcolor2);
}

bgBlue2Color<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 47, 75, 120), Color.fromARGB(100, 150, 192, 229));
}

bgDefault1Color<Colors>() {
  return themeLoader(Color.fromARGB(150, 67, 115, 196), defaultcolor1);
}

bgDefault2Color<Colors>() {
  return themeLoader(Color.fromARGB(200, 104, 140, 207), defaultcolor3);
}

bgLightBlueColor<Colors>() {
  return themeLoader(
      Color.fromARGB(55, 81, 98, 125), Color.fromARGB(255, 222, 240, 255));
}

bwColor<Colors>({int? opacity}) {
  return themeLoader(Color.fromARGB(opacity != null ? opacity : 255, 0, 0, 0),
      Color.fromARGB(opacity != null ? opacity : 255, 255, 255, 255));
}

btnColor<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 0, 23, 38), Color.fromARGB(255, 252, 254, 255));
}

btnHightlightColor<Colors>() {
  return themeLoader(
      Color.fromARGB(100, 92, 186, 228), Color.fromARGB(100, 92, 186, 228));
}

gopayBlueColor1<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 0, 85, 112), Color.fromARGB(255, 130, 215, 237));
}

gopayBlueColor2<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 8, 70, 115), Color.fromARGB(255, 57, 201, 237));
}

btnBlueColor<Colors>() {
  return themeLoader(Color.fromARGB(255, 47, 75, 120), defaultcolor5);
}

fontColor1<Color>() {
  return themeLoader(Colors.white, Colors.black87);
}

fontColor2<Color>() {
  return themeLoader(Colors.white60, Colors.grey.shade500);
}

fontColor3<Color>() {
  return themeLoader(Colors.black87, Colors.white);
}

fontColor4<Color>() {
  return themeLoader(Colors.white24, Colors.black54);
}

fontColor5<Colors>() {
  return themeLoader(
      Color.fromARGB(255, 176, 226, 255), Color.fromARGB(255, 50, 136, 186));
}

hintColor<Color>() {
  return themeLoader(Colors.white60, Colors.grey.shade500);
}

borderColor<Color>() {
  return themeLoader(Colors.white60, Colors.grey.shade300);
}

Color defaultcolor1 = Color.fromARGB(255, 67, 115, 196);
Color defaultcolor1Darken = Color.fromARGB(255, 24, 41, 71);
Color defaultcolor2 = Color.fromARGB(255, 68, 164, 219);
Color defaultcolor3 = Color.fromARGB(255, 150, 192, 229);
Color defaultcolor4 = Color.fromARGB(255, 92, 186, 228);
Color defaultcolor5 = Color.fromARGB(255, 104, 140, 207);
Color accentColor = new Color.fromARGB(255, 0, 162, 222);
Color redlinkaja = Color.fromARGB(255, 213, 59, 53);

Color yellow = Color.fromARGB(255, 255, 215, 73);
Color red = Color.fromARGB(255, 255, 87, 87);
Color lightblue = Color.fromARGB(255, 92, 186, 228);
Color blue = Color.fromARGB(255, 67, 97, 203);
Color green = Color.fromARGB(255, 60, 204, 117);
Color purple = Color.fromARGB(255, 128, 88, 244);
Color lightgrey = Color.fromARGB(10, 0, 0, 0);
Color lightred = Color.fromARGB(
  255,
  255,
  65,
  65,
);

Color descriptionBlue1 = Color.fromARGB(255, 132, 207, 207);
Color descriptionBlue2 = Color.fromARGB(255, 114, 205, 252);
Color praktik1Red1 = Color.fromARGB(255, 237, 107, 107);
Color praktik2Green1 = Color.fromARGB(255, 129, 191, 79);
Color praktik3Orange1 = Color.fromARGB(255, 253, 190, 108);
Color praktik1Red2 = Color.fromARGB(255, 237, 100, 100);
Color praktik2Green2 = Color.fromARGB(255, 77, 190, 105);
Color praktik3Orange2 = Color.fromARGB(255, 255, 163, 71);

Color iconcolorclockgreen = Color.fromARGB(255, 120, 228, 110);
Color iconcolorservicesblue = Color.fromARGB(255, 64, 123, 255);
Color iconcolorgenderred = Color.fromARGB(255, 255, 82, 82);
Color iconcolorageyellow = Color.fromARGB(255, 255, 212, 60);
Color iconcolordistance = Color.fromARGB(255, 92, 209, 90);
Color iconcolorbodyheight = Color.fromARGB(255, 60, 192, 210);
Color iconcolorbodyweight = Color.fromARGB(255, 121, 121, 121);
Color iconcoloridentity = Color.fromARGB(255, 48, 45, 165);
