//Text
import 'package:obatin/importPackage.dart';

hnnormal(BuildContext context,
    {Color? txtColor,
    double? txtSize,
    FontWeight? txtWeight,
    FontStyle? txtStyle,
    double? spacing}) {
  return TextStyle(
      color: txtColor == null ? Colors.black : txtColor,
      fontFamily: "Helvetica Neue",
      letterSpacing: spacing == null ? 0 : spacing,
      fontStyle: txtStyle == null ? FontStyle.normal : txtStyle,
      fontSize: txtSize == null ? getWidth(context, 0.034) : txtSize,
      fontWeight: txtWeight == null ? FontWeight.normal : txtWeight);
}
