import 'package:obatin/importPackage.dart';

//serviceId
//1=Onlocation
//2=VisitPatient
//3=Telemedicine
//4=Volunteer
serviceTypeSelect<String>({@required int? serviceId, bool? withrequest}) {
  return (withrequest != null && withrequest == true)
      ? (serviceId == 1
          ? Str().onlocationreq
          : serviceId == 2
              ? Str().visitpatientreq
              : serviceId == 3
                  ? Str().telemedicinereq
                  : Str().volunteerreq)
      : (serviceId == 1
          ? Str().onlocation
          : serviceId == 2
              ? Str().visitpatient
              : serviceId == 3
                  ? Str().telemedicine
                  : Str().volunteer);
}
