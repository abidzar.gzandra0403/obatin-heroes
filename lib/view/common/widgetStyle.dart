import 'package:obatin/importPackage.dart';

inputborderStyle(Color color) {
  return OutlineInputBorder(
      borderSide: BorderSide(width: 1, color: color),
      borderRadius: BorderRadius.circular(10));
}

textFieldStyle1(BuildContext context, String hint, {IconData? icons}) {
  return TextField(
      style: hnnormal(context,
          txtSize: getWidth(context, 0.035), txtColor: fontColor1()),
      decoration: InputDecoration(
          prefixIcon: icons == null
              ? null
              : Icon(
                  icons,
                  color: fontColor2(),
                ),
          hintText: hint,
          hintStyle: hnnormal(context,
              txtSize: getWidth(context, 0.035), txtColor: fontColor2()),
          contentPadding: EdgeInsets.only(left: getWidth(context, 0.02)),
          enabledBorder: inputborderStyle(Colors.grey),
          focusedBorder: inputborderStyle(defaultcolor4)));
}
