import 'package:obatin/importPackage.dart';

getHeight(BuildContext context, double h) {
  return MediaQuery.of(context).size.height * h;
}

getWidth(BuildContext context, double w) {
  return MediaQuery.of(context).size.width * w;
}

setHSpacing(double h) {
  return SizedBox(
    height: h,
  );
}

setWSpacing(double w) {
  return SizedBox(
    width: w,
  );
}

setInfinitySpacing() {
  return Expanded(child: SizedBox());
}

defaultPaddingContent(BuildContext context) {
  return getWidth(context, 0.06);
}

goToPage(BuildContext context,
    {@required Widget? page, int? animationRouteID, bool? pushReplace}) {
  bool pushReplaceInit = pushReplace != null ? pushReplace : false;
  int routeID = animationRouteID != null
      ? (animationRouteID > 3 ? 3 : animationRouteID)
      : 1;
  return pushReplaceInit
      ? Navigator.pushReplacement(
          context,
          routeID == 1
              ? SlideRightRoute(page: page!)
              : routeID == 2
                  ? SlideLeftRoute(page: page!)
                  : FadeRoute(page: page!))
      : Navigator.push(
          context,
          routeID == 1
              ? SlideRightRoute(page: page!)
              : routeID == 2
                  ? SlideLeftRoute(page: page!)
                  : FadeRoute(page: page!));
}
