import 'package:obatin/importPackage.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  var imageDir = "assets/images/";
  bool animate1 = false;
  bool animate2 = false;
  bool animate3 = false;
  late Animation<double> xOffsetTween;
  late Animation<double> xOffsetTween2;
  late Animation<double> xOffsetTween3;
  late AnimationController xOffsetAnim;

  // =================================================================================================
  // OBATIN SPLASHSCREEN ANIMATION ===================================================================
  // =================================================================================================
  xoffsetAnimate() {
    xOffsetAnim = new AnimationController(
      duration: Duration(milliseconds: 500),
      vsync: this,
    )..addListener(() => setState(() {}));
    xOffsetTween = Tween(begin: 0.4, end: 0.0)
        .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
    xOffsetTween2 = Tween(begin: 0.0, end: 0.2)
        .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
    xOffsetTween3 = Tween(begin: 0.1, end: 0.09)
        .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
  }

  loadinganimate() {
    // ignore: unused_local_variable
    Timer timer;
    timer = new Timer(const Duration(milliseconds: 1200), () {
      setState(() {
        animate3 = true;
      });
    });
  }

  loadinganimateObatin() {
    // ignore: unused_local_variable
    Timer timer;
    timer = new Timer(const Duration(milliseconds: 800), () {
      setState(() {
        animate1 = true;
        animate2 = true;
        xOffsetAnim.forward();
      });
    });
  }

  // =================================================================================================
  // MAIN LOGIC IF STATUS IS LOGIN OR NOT ============================================================
  // =================================================================================================

  loadProgress() async {
    bannerImageLoad = [
      BannerContainer(imgName: "promo1.jpg"),
      BannerContainer(imgName: "promo2.jpg")
    ];
    bool alreadyLogin =
        await Preferences.instance.getPrefsAlreadyLogin() != null
            ? await Preferences.instance.getPrefsAlreadyLogin()
            : false;
    if (alreadyLogin == true) {
      postProceedData_listSIP =
          await LocalDatabase().readData(ModelSipData.tableName);
      print(postProceedData_listSIP);
      postProceedData_userData =
          await LocalDatabase().readData(ModelUserData.tableName);
    }

    // ignore: unused_local_variable
    Timer timer;
    timer = new Timer(const Duration(milliseconds: 1300), () {
      if (alreadyLogin == true) {
        // Navigator.pushReplacement(
        //     context, MaterialPageRoute(builder: (context) => HomeSideMenu()));
        goToPage(context,
            pushReplace: true, animationRouteID: 1, page: HomeSideMenu());
      } else if (alreadyLogin == false) {
        goToPage(context, pushReplace: true, page: IntroMain());
      }
    });
  }

  @override
  void initState() {
    loadProgress();
    xoffsetAnimate();
    Preferences.instance.getPrefsDarkMode(context);
    Preferences.instance.getPrefsLanguage(context);
    loadinganimate();
    loadinganimateObatin();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: new MediaQueryData(),
      child: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.dark),
        child: Scaffold(
          body: Stack(
            children: [
              AnimatedContainer(
                duration: Duration(milliseconds: 500),
                color: bgColor(),
              ),
              Align(
                alignment: Alignment.centerRight,
                child: AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    curve: Curves.ease,
                    padding: EdgeInsets.only(bottom: getWidth(context, 0.008)),
                    alignment: Alignment.centerLeft,
                    width: getWidth(context, animate1 ? 0.6 : 0.34),
                    height: getWidth(context, 0.1),
                    child: ClipRRect(
                      child: Padding(
                        padding:
                            EdgeInsets.only(left: getWidth(context, 0.035)),
                        child: Transform.translate(
                          offset:
                              Offset(-getWidth(context, xOffsetTween.value), 0),
                          child: ImgAsset.splashObatin2,
                        ),
                      ),
                    )),
              ),
              Align(
                alignment: Alignment.center,
                child: Transform.translate(
                  offset: Offset(getWidth(context, xOffsetTween2.value),
                      getWidth(context, xOffsetTween3.value)),
                  child: AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    height: getWidth(context, 0.07),
                    child: ImgAsset.splashObatin3,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.centerLeft,
                child: AnimatedContainer(
                    duration: Duration(milliseconds: 500),
                    curve: Curves.ease,
                    alignment: Alignment.centerRight,
                    decoration: BoxDecoration(
                        borderRadius:
                            BorderRadius.circular(getWidth(context, 0.4))),
                    width: getWidth(context, animate1 ? 0.43 : 0.623),
                    height: getWidth(context, 0.11),
                    child: ImgAsset.splashObatin1),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
