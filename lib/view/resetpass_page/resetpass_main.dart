import 'package:obatin/importPackage.dart';

class ResetpassMain extends StatefulWidget {
  @override
  _ResetpassMainState createState() => _ResetpassMainState();
}

class _ResetpassMainState extends State<ResetpassMain> {
  final resetpassTxtboxEmail = new TextEditingController();
  final resetpassTxtboxPass = new TextEditingController();
  final resetpassTxtboxPConfirm = new TextEditingController();
  String buttonText = "Selanjutnya";

  int indexNow = 1;
  final controller = new PageController(initialPage: 0, keepPage: true);

  void pageChanged(int index) {
    setState(() {
      indexNow = index + 1;
    });
  }

  buttonNext() {
    controller.nextPage(
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  Future handleBack(int btnId) async {
    btnId == 1
        ? goToPage(context, pushReplace: true, page: LoginMain())
        : CustomAlertDialog(context,
            title: Str().cancel,
            contentText: Str().allchangenotbesave,
            txtButton1: Str().yes,
            txtButton2: Str().no, funcButton1: () {
            resetpassTxtboxPass.text = "";
            resetpassTxtboxPConfirm.text = "";
            buttonText = Str().next;
            controller.animateToPage(0,
                duration: Duration(milliseconds: 500), curve: Curves.ease);
          });
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness: Brightness.dark),
        child: Scaffold(
          backgroundColor: bgColor(),
          body: pageviewItem(context),
        ));
  }

  pageviewItem(BuildContext context) {
    return Stack(children: [
      obatinHeader(context, Str().resetpassword),
      Padding(
        padding: EdgeInsets.only(top: getWidth(context, 0.6)),
        child: Container(
          // height: getWidth(context, 1),

          width: getWidth(context, 1),
          padding: EdgeInsets.only(top: getWidth(context, 0.1)),
          decoration: BoxDecoration(
              color: bgColor(),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(getWidth(context, 0.1)),
                  topRight: Radius.circular(getWidth(context, 0.1)))),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: controller,
                  onPageChanged: (index) {
                    pageChanged(index);
                  },
                  children: <Widget>[forgetForm(), resetpassForm()],
                ),
              ),
              CustomButton(
                  vermargin: 0.04,
                  styleId: 1,
                  width: 0.9,
                  height: 0.1,
                  btnText: buttonText,
                  onPressed: () {
                    setState(() {
                      if (indexNow == 1) {
                        buttonText = Str().resetpassword;
                        buttonNext();
                      } else if (indexNow == 2) {
                        CustomAlertDialog(context,
                            title: Str().success,
                            contentIcon: Icons.check_circle_outline,
                            contentText: Str().successchangepassword,
                            funcDismiss: () => Navigator.pop(context));
                      } else {
                        buttonText = Str().resetpassword;
                        buttonNext();
                      }
                    });
                  })
            ],
          ),
        ),
      ),
    ]);
  }

  forgetForm() {
    return WillPopScope(
      onWillPop: () async {
        handleBack(1);
        return false;
      },
      child: Container(
        width: getWidth(context, 1),
        height: getWidth(context, 0.4),
        padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.1)),
        child: CustomTextField(hint: "Email"),
      ),
    );
  }

  resetpassForm() {
    return WillPopScope(
        onWillPop: () async {
          handleBack(2);
          return false;
        },
        child: Center(
          child: Container(
            width: getWidth(context, 1),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.1)),
            child: Column(
              children: <Widget>[
                passwordFieldValidator(
                    context, Str().password, resetpassTxtboxPass),
                passwordFieldValidator(
                    context, Str().passwordconfirm, resetpassTxtboxPConfirm,
                    txtId: 2, targetTxtbox: resetpassTxtboxPass),
              ],
            ),
          ),
        ));
  }
}
