import 'package:obatin/importPackage.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  IndexController controller = new IndexController();
  int index = 0;

  Future<bool> handleBack() async {
    return await CustomAlertDialog(context,
            title: "Keluar Aplikasi?",
            contentText:
                "Jika kamu ada masalah kesehatan, OBAT-in siap membantu.",
            txtButton1: "Keluar",
            txtButton2: "Batal",
            funcExitApp: () => Navigator.of(context).pop(true)) ??
        false; //if showDialouge had returned null, then return false
  }

  pageSwitch(int currentIndex) {
    setState(() {
      index = currentIndex;
    });
    controller.move(currentIndex, animation: false);
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> listmenu = [
      // HomeMain(),
      ActivityLatest(),
      ProfileMain(),
    ];
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, state) => WillPopScope(
          onWillPop: handleBack,
          child: Scaffold(
            backgroundColor: bgBlue1Color(),
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
                automaticallyImplyLeading: false,
                backgroundColor: bgColor(),
                brightness: themeID == 1 ? Brightness.light : Brightness.dark,
                elevation: 0.0,
                title: CustomButton(
                    alignment: Alignment.centerRight,
                    width: 0.28,
                    styleId: 3,
                    prefixIcon: Icons.notifications_none_outlined,
                    btnText: "Notifikasi",
                    onPressed: () {
                      // Navigator.push(
                      //     context, SlideRightRoute(page: NotificationView()));
                    })),
            body: Stack(
              children: [
                Align(
                  alignment: Alignment.bottomCenter,
                  child: navbar(),
                ),
                Container(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
                  margin: EdgeInsets.only(bottom: getWidth(context, 0.18)),
                  decoration: BoxDecoration(
                      color: bgColor(),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.black38,
                          blurRadius: 10,
                        )
                      ],
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(getWidth(context, 0.05)),
                          bottomRight:
                              Radius.circular(getWidth(context, 0.05)))),
                  child: TransformerPageView(
                      controller: controller,
                      physics: NeverScrollableScrollPhysics(),
                      duration: Duration(milliseconds: 700),
                      curve: Curves.ease,
                      pageSnapping: false,
                      index: 0,
                      itemBuilder: (BuildContext context, int index) {
                        return listmenu[index];
                      },
                      itemCount: listmenu.length),
                ),
              ],
            ),
          )),
    );
  }

  navbar() {
    return Container(
        width: getWidth(context, 0.94),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(getWidth(context, 0.04))),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.0)),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: Colors.transparent,
            elevation: 0,
            showSelectedLabels: false,
            showUnselectedLabels: false,
            currentIndex: index,
            selectedItemColor: Colors.white,
            unselectedItemColor: Colors.blue[100],
            onTap: (index) {
              pageSwitch(index);
            },
            items: [
              bottomnavbar(false, Icons.home_outlined, "Beranda"),
              bottomnavbar(false, Icons.shopping_cart_outlined, "Pesanan",
                  orderbtn: true),
              bottomnavbar(
                  false, Icons.chat_bubble_outline_rounded, "Percakapan"),
              bottomnavbar(false, Icons.settings_outlined, "Pengaturan"),
              bottomnavbar(false, Icons.more_horiz_outlined, "Lainnya"),
            ],
          ),
        ));
  }

  bottomnavbar(bool shownotif, IconData icon, String txt, {bool? orderbtn}) {
    return BottomNavigationBarItem(
      tooltip: txt,
      icon: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Column(
            children: [
              Icon(
                icon,
                size: getWidth(context, 0.06),
              ),
              CustomText(
                  txt: txt, color: Colors.white, size: 0.029, bold: false)
            ],
          ),
          Container(
            height: getWidth(context, 0.02),
            width: getWidth(context, 0.02),
            decoration: BoxDecoration(
                color: shownotif ? Colors.red.shade400 : Colors.transparent,
                borderRadius: BorderRadius.circular(50)),
          ),
        ],
      ),
      label: '',
    );
  }
}
