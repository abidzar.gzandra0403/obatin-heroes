import 'package:obatin/importPackage.dart';

class ActivityExampleMap extends StatefulWidget {
  final int? serviceId;
  final String? patientName;
  final String? clockString;

  ActivityExampleMap(
      {@required this.serviceId,
      @required this.patientName,
      @required this.clockString});
  @override
  _ActivityExampleMapState createState() => _ActivityExampleMapState();
}

class _ActivityExampleMapState extends State<ActivityExampleMap> {
  int btnIdInit = 0;
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        body: Column(children: [
          Expanded(
              child: Stack(
            children: [
              new Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage('assets/images/mapgif.gif'),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: EdgeInsets.only(bottom: getWidth(context, 0.05)),
                  padding:
                      EdgeInsets.symmetric(horizontal: getWidth(context, 0.05)),
                  height: getWidth(context, 0.3),
                  child: activityCard(context,
                      title: "Sedang Berlansung",
                      callId: 1,
                      patientName: widget.patientName,
                      clock: widget.clockString,
                      serviceId: widget.serviceId),
                ),
              ),
              CustomButton(
                alignment: Alignment.bottomCenter,
                hormargin: 0.05,
                shapeId: 2,
                styleId: 1,
                onPressed: () {
                  setState(() {
                    btnIdInit = btnIdInit + 1;
                  });
                  if (btnIdInit == 1) {
                  } else if (btnIdInit == 2) {
                  } else {
                    Navigator.of(context).pop();
                  }
                },
                btnText: btnIdInit == 0
                    ? "Mulai Layanan"
                    : btnIdInit == 1
                        ? "Sudah Dilokasi"
                        : "Selesai Layanan",
              ),
              CustomButton(
                alignment: Alignment.topLeft,
                styleId: 1,
                height: 0.12,
                width: 0.12,
                vermargin: 0.1,
                hormargin: 0.06,
                shapeId: 4,
                onPressed: () => Navigator.pop(context),
                prefixIcon: Icons.arrow_back_ios_new_outlined,
              )
            ],
          )),
        ]),
      ),
    );
  }
}
