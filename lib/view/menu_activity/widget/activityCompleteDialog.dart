import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names
activityCompleteDialog(BuildContext context) {
  return CustomAlertDialog(
    context,
    title: "Telah menyelesaikan layanan?",
    contentWidget: ImgAsset.imgRequestAccept,
    contentText:
        "Kami akan meminta konfirmasi kepada pasien apakah benar telah menyelesaikan layanan. Mohon ditunggu ya.",
    txtButton1: "Ya",
    txtButton2: "Batal",
    funcButton1: () => Navigator.pop(context),
  );
}
