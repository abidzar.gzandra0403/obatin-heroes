import 'package:obatin/importPackage.dart';
import 'package:obatin/view/menu_activity/widget/activityDetailBottomSheet.dart';

activityCard(
  BuildContext context, {
  @required String? title,
  @required int? callId,
  @required int? serviceId,
  String? noreg,
  String? patientName,
  String? clock,
  Widget? bottomWidget,
}) {
  rowText({IconData? icon, String? clockString, Color? color}) {
    return SizedBox(
      height: getWidth(context, 0.04),
      child: Row(
        children: [
          Icon(icon, size: getWidth(context, 0.03), color: color),
          setWSpacing(5),
          if (clockString != null)
            CustomText(
              txt: clockString,
              size: 0.03,
            ),
        ],
      ),
    );
  }

  return Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child: RawMaterialButton(
      onPressed: () {
        activityDetailBottomSheet(context,
            patientName: patientName,
            title: title,
            callId: callId,
            noreg: noreg,
            clockString: clock,
            serviceId: serviceId);
      },
      fillColor: bgColorDarken(),
      highlightColor: Colors.blue.shade50,
      elevation: 0,
      highlightElevation: 0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(width: 1, color: borderBlueColor())),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(getWidth(context, 0.02)),
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: btnBlueColor(),
                  radius: getWidth(context, 0.08),
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                          txt: patientName != null ? patientName : "",
                          bold: true,
                          size: 0.042),
                      if (noreg != null)
                        CustomText(
                          txt: "No Reg. " + noreg,
                          size: 0.03,
                        ),
                      rowText(
                          icon: ObatinIconpack.clock,
                          clockString: "04:00",
                          color: iconcolorclockgreen),
                      rowText(
                        color: defaultcolor1,
                        icon: serviceId == 1
                            ? ObatinIconpack.onlocation
                            : serviceId == 2
                                ? ObatinIconpack.visit
                                : serviceId == 3
                                    ? ObatinIconpack.telemedicine
                                    : ObatinIconpack.volunteer,
                        clockString: serviceId == 1
                            ? Str().onlocationreq
                            : serviceId == 2
                                ? Str().visitpatientreq
                                : serviceId == 3
                                    ? Str().telemedicinereq
                                    : Str().volunteerreq,
                      ),
                      bottomWidget != null ? bottomWidget : SizedBox(),
                    ],
                  ),
                )),
                SizedBox(
                  width: getWidth(context, 0.12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Icon(
                        Icons.open_in_new,
                        color: fontColor2(),
                        size: getWidth(context, 0.05),
                      ),
                      if (callId != 1 && callId != 2)
                        CustomText(
                          toppad: 0.02,
                          txt: "11:00",
                          color: fontColor2(),
                        )
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    ),
  );
}
