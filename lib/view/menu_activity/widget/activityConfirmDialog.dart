import 'package:obatin/importPackage.dart';

// ignore: non_constant_identifier_names
activityConfirmDialog(BuildContext context, bool accept, int serviceId) {
  return CustomAlertDialog(
    context,
    title: accept ? "Terima permintaan pasien?" : "Tolak permintaan pasien?",
    contentWidget: Column(
      children: [
        accept ? ImgAsset.imgRequestAccept : ImgAsset.imgRequestDecline,
        if (accept != true)
          Padding(
            padding: EdgeInsets.only(top: getWidth(context, 0.02)),
            child: TextField(
              decoration: new InputDecoration(
                contentPadding: EdgeInsets.all(getWidth(context, 0.05)),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: defaultcolor2),
                ),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: defaultcolor2),
                ),
                hintText: 'Alasan Penolakan..',
              ),
              maxLines: 1,
            ),
          ),
      ],
    ),
    contentWidgetHeight: accept ? 0.4 : 0.57,
    contentText: accept
        ? "Permintaan pasien akan terdaftar ke jadwal kamu, jika terdapat kendala atau reschedule silahkan konfirmasi sebelum jadwal permintaan pasien"
        : "Permintaan akan ditolak dan pasien akan diinformasikan bahwa anda telah menolak permintaan",
    txtButton1: accept ? "Terima" : "Tolak",
    txtButton2: "Batal",
    funcButton1: () {
      if (accept == true) {
        waitingpaymentDialog(context, serviceId);
      } else {
        Navigator.pop(context);
      }
      ;
    },
  );
}

waitingpaymentDialog(BuildContext context, int serviceId) {
  String firstWord = serviceId == 4
      ? "Terima kasih telah mengaktifkan layanan sukarela, pasien mu pasti akan senang."
      : "Kami akan segera memberitahu jika pasien telah melakukan pembayaran.";
  String secondWord = "\nJika pembayaran telah dilakukan ";
  String serviceName = serviceId == 1
      ? secondWord +
          "mohon untuk melayani pasien pada jadwal yang telah disepakati"
      : serviceId == 2
          ? secondWord + "anda akan diarahkan ke lokasi pasien"
          : serviceId == 3
              ? secondWord +
                  "segera menghubungi pasien melalui videocall sesuai jadwal yang telah disepakati."
              : "";
  String title =
      serviceId != 4 ? "Menunggu Konfirmasi Dari Pasien" : "Kamu terbaik!";

  return CustomAlertDialog(context,
      funcDismiss: () => Navigator.pop(context),
      title: title,
      contentWidget: serviceId != 4
          ? ImgAsset.imgWaitingPayment
          : ImgAsset.imgVolunteerApreciation,
      contentText: firstWord +
          serviceName +
          "\nMohon cek notifikasi secara berkala.\nSalam Sehat dari OBAT-in",
      txtButton1: "Baik",
      funcButton1: () => Navigator.pop(context));
}
