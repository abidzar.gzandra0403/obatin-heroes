import 'package:obatin/importPackage.dart';
import 'package:obatin/view/menu_activity/widget/reschedule.dart';

activityDetailBottomSheet(BuildContext context,
    {@required String? title,
    @required int? callId,
    @required String? patientName,
    @required String? clockString,
    @required int? serviceId,
    String? noreg}) {
  //LAYANAN SERVICE YANG DIMINTA =====================================
  initialServiceRequest(BuildContext context, int serviceId) {
    if (serviceId == 1) {
      return customerInfo(context, ObatinIconpack.onlocation,
          Str().onlocationreq, iconcolorservicesblue);
    } else if (serviceId == 2) {
      return customerInfo(context, ObatinIconpack.visit, Str().visitpatientreq,
          iconcolorservicesblue);
    } else if (serviceId == 3) {
      return customerInfo(context, ObatinIconpack.telemedicine,
          Str().telemedicinereq, iconcolorservicesblue);
    } else {
      return customerInfo(context, ObatinIconpack.volunteer, Str().volunteerreq,
          iconcolorservicesblue);
    }
  }
  //===================================================================

  return CustomBottomSheet(
    context,
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Container(
            height: getWidth(context, 0.02),
            width: 40,
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: Colors.black12, borderRadius: BorderRadius.circular(50)),
          ),
        ),
        Center(child: CustomText(txt: title, bold: true, size: 0.04)),
        setHSpacing(getWidth(context, 0.04)),
        Container(
          // height: getWidth(context, 0.5),
          child: Row(
            children: [
              CircleAvatar(
                child: Icon(Icons.person, color: Colors.white),
                radius: getWidth(context, 0.12),
                backgroundColor: defaultcolor3,
              ),
              Container(
                width: getWidth(context, 0.52),
                padding: EdgeInsets.only(left: getWidth(context, 0.025)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      txt: patientName,
                      size: 0.05,
                      bold: true,
                    ),
                    initialServiceRequest(context, serviceId!),
                    Divider(
                      height: getWidth(
                        context,
                        0.03,
                      ),
                      color: Colors.black,
                    ),
                    customerInfo(context, ObatinIconpack.identity,
                        "1502110404990001", iconcoloridentity),
                    customerInfo(context, ObatinIconpack.gender, "Pria",
                        iconcolorgenderred),
                    customerInfo(context, ObatinIconpack.ages, "25 Tahun",
                        iconcolorageyellow),
                  ],
                ),
              )
            ],
          ),
        ),
        setHSpacing(getWidth(context, 0.04)),
        if (noreg != null)
          detailinformationCustomer(context, Str().registnumber, noreg, false),
        detailinformationCustomer(
            context, Str().bookingtime, "16:00, 15 Juni 2021", false),
        if (serviceId == 2)
          detailinformationCustomer(context, Str().address,
              "Sendangsari Utara Raya, Pedurungan Semarang", true,
              icon: ObatinIconpack.distancepoint,
              icontxt: "2.4 Km dari lokasi anda"),
        Padding(
          padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.04)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(txt: Str().pricedetail, size: 0.035, bold: true),
              Column(
                children: [
                  CustomText(
                    txt: serviceId == 4 ? "Gratis" : "Rp. 120.000,-",
                    size: 0.07,
                  ),
                  if (serviceId != 4)
                    SizedBox(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            txt: "- Rp. 40.000,- Biaya Transport",
                            size: 0.035,
                            color: Colors.green.shade600,
                          ),
                          CustomText(
                            txt: "- Rp. 80.000,- Biaya Layanan",
                            size: 0.035,
                            color: Colors.green.shade600,
                          ),
                        ],
                      ),
                    )
                ],
              )
            ],
          ),
        ),
        activityserviceButton(context,
            callId: callId,
            serviceId: serviceId,
            patientName: patientName,
            clockString: clockString),
      ],
    ),
  );
}

customerInfo(BuildContext context, IconData icon, String text, Color color) {
  return SizedBox(
    height: getWidth(context, 0.043),
    child: Row(
      children: [
        SizedBox(
          width: getWidth(context, 0.03),
          child: Icon(
            icon,
            size: getWidth(context, 0.032),
            color: color,
          ),
        ),
        setWSpacing(10),
        CustomText(
          leftpad: 0.01,
          txt: text,
          size: 0.03,
        )
      ],
    ),
  );
}

detailinformationCustomer(
    BuildContext context, String txt1, String txt2, bool detailIcon,
    {IconData? icon, String? icontxt}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(txt: txt1, size: 0.035, bold: true),
        CustomText(txt: txt2, size: 0.035),
        if (detailIcon == true)
          customerInfo(context, ObatinIconpack.distancepoint,
              "2.4 Km dari lokasi anda", iconcolordistance)
      ],
    ),
  );
}

activityserviceButton(BuildContext context,
    {@required int? callId,
    @required int? serviceId,
    @required String? patientName,
    @required String? clockString}) {
  gotovisitmaps() {
    Navigator.of(context).pop();
    Navigator.push(
        context,
        FadeRoute(
            page: ActivityExampleMap(
          patientName: patientName,
          clockString: clockString,
          serviceId: serviceId,
        )));
  }

  buttonRow() {
    btnFunc(int btnFunc) {
      if (btnFunc == 3) {
        gotovisitmaps();
      } else if (btnFunc == 0) {
        activityCompleteDialog(context);
      }
    }

    return Container(
        child: Column(
      children: [
        if (callId == 1)
          Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.03)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: getWidth(context, 0.55),
                      child: CustomText(txt: Str().referraldesc),
                    ),
                    CustomButton(
                        styleId: 1,
                        shapeId: 2,
                        width: 0.25,
                        btnText: Str().referral,
                        suffixIcon: Icons.change_circle_outlined,
                        onPressed: () => btnFunc(0)),
                  ],
                ),
              ),
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                      width: getWidth(context, 0.55),
                      child: CustomText(txt: Str().servicedonedesc),
                    ),
                    CustomButton(
                        styleId: 1,
                        shapeId: 2,
                        width: 0.25,
                        btnText: Str().complete,
                        suffixIcon: Icons.check_outlined,
                        onPressed: () => btnFunc(0)),
                  ],
                ),
              )
            ],
          ),
        if (callId == 2)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                  styleId: 1,
                  hormargin: 0.02,
                  bColor: lightred,
                  btnText: Str().decline,
                  onPressed: () =>
                      activityConfirmDialog(context, false, serviceId!)),
              CustomButton(
                  styleId: 1,
                  bColor: lightblue,
                  btnText: Str().accept,
                  onPressed: () =>
                      activityConfirmDialog(context, true, serviceId!)),
            ],
          ),
        if (callId == 3)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  btnText: Str().chat,
                  prefixIcon: ObatinIconpack.chatbubble,
                  onPressed: () => btnFunc(2)),
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  hormargin: 0.02,
                  bColor: lightred,
                  btnText: Str().reschedule,
                  prefixIcon: Icons.change_circle_outlined,
                  // onPressed: () => settingsTimePicker(context, 0),
                  onPressed: () => rescheduleorder(context, 0)),
            ],
          ),
        if (callId == 4)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                  styleId: 1,
                  hormargin: 0.02,
                  bColor: lightred,
                  btnText: Str().cancel,
                  onPressed: () =>
                      activityConfirmDialog(context, false, serviceId!)),
            ],
          ),
        if (callId == 5)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  btnText: Str().chat,
                  onPressed: () => btnFunc(2)),
              CustomButton(
                  styleId: 1,
                  hormargin: 0.02,
                  shapeId: 2,
                  bColor: lightred,
                  btnText: Str().cancel,
                  onPressed: () =>
                      activityConfirmDialog(context, false, serviceId!)),
            ],
          ),
        if (callId! == 6)
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  btnText: Str().locationroutes,
                  prefixIcon: ObatinIconpack.visit,
                  onPressed: () => btnFunc(3)),
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  hormargin: 0.02,
                  btnText: Str().call,
                  prefixIcon: ObatinIconpack.call,
                  onPressed: () => btnFunc(1)),
              CustomButton(
                  styleId: 1,
                  shapeId: 2,
                  btnText: Str().chat,
                  prefixIcon: ObatinIconpack.chatbubble,
                  onPressed: () => btnFunc(2)),
            ],
          ),
      ],
    ));
  }

  if (serviceId == 1) {
    return buttonRow();
  } else {
    return buttonRow();
  }
}
