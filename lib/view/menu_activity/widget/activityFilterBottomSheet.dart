import 'package:obatin/importPackage.dart';

activityFilterBottomSheet(BuildContext context, int callId) {
  buttonFilter(
    String txt,
  ) {
    return SizedBox(
      width: getWidth(context, 0.25),
      height: getWidth(context, 0.09),
      child: RawMaterialButton(
        onPressed: () {},
        fillColor: Colors.transparent,
        elevation: 0,
        highlightElevation: 0,
        shape: RoundedRectangleBorder(
            side: BorderSide(width: 1, color: defaultcolor2),
            borderRadius: BorderRadius.circular(10)),
        child: CustomText(txt: txt, size: 0.035, color: defaultcolor2),
      ),
    );
  }

  datepicker() {
    return Row(
      children: [
        CustomButton(
            styleId: 1,
            shapeId: 2,
            height: 0.09,
            prefixIcon: Icons.date_range,
            onPressed: () => showDatePicker(
                  helpText: "Pilih Tanggal",
                  cancelText: "Batal",
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2025),
                )),
        setWSpacing(getWidth(context, 0.02)),
        SizedBox(
          width: getWidth(context, 0.4),
          height: getWidth(context, 0.09),
          child: textFieldStyle1(context, "DD/mm/yyyy"),
        )
      ],
    );
  }

  return CustomBottomSheet(
    context,
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: CustomText(
            txt: Str().filter,
            bold: true,
            size: 0.04,
          ),
        ),
        CustomText(txt: Str().servicetype, bold: true),
        GridView.count(
          crossAxisCount: 3,
          shrinkWrap: true,
          crossAxisSpacing: getWidth(context, 0.02),
          mainAxisSpacing: getWidth(context, 0.02),
          childAspectRatio: 3,
          children: [
            buttonFilter(Str().onlocation),
            buttonFilter(Str().visitpatient),
            if (mainUserType == 1) buttonFilter(Str().telemedicine),
            buttonFilter(Str().volunteer)
          ],
        ),
        setHSpacing(getWidth(context, 0.04)),
        CustomText(txt: Str().sortbytime, bold: true),
        GridView.count(
          crossAxisCount: 3,
          shrinkWrap: true,
          crossAxisSpacing: getWidth(context, 0.02),
          mainAxisSpacing: getWidth(context, 0.02),
          childAspectRatio: 3,
          children: [
            buttonFilter(Str().newest),
            buttonFilter(Str().oldest),
          ],
        ),
        setHSpacing(getWidth(context, 0.04)),
        CustomText(txt: Str().sortbydate, bold: true),
        datepicker(),
        setHSpacing(getWidth(context, 0.1)),
        CustomButton(
            styleId: 1,
            shapeId: 4,
            isize: 0.06,
            height: 0.1,
            prefixIcon: Icons.check,
            onPressed: () => Navigator.pop(context))
      ],
    ),
  );
}
