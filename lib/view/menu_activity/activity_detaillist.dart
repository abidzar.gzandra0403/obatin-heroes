import 'package:obatin/importPackage.dart';

class ActivityDetailList extends StatefulWidget {
  final String? title;
  final int? callId;
  final Widget? bottomWidget;
  ActivityDetailList(
      {@required this.title, @required this.callId, this.bottomWidget});
  @override
  _ActivityDetailListState createState() => _ActivityDetailListState();
}

class _ActivityDetailListState extends State<ActivityDetailList> {
  List listdata = [
    "Abidzar",
    "Wahyu",
    "Anam",
    "Wahyu",
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, widget.title!, true),
      body: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
          child: Column(
            children: [
              Container(
                height: getWidth(context, 0.1),
                margin: EdgeInsets.only(bottom: getWidth(context, 0.08)),
                child: Row(
                  children: [
                    Expanded(
                      child: SizedBox(
                          height: getWidth(context, 0.1),
                          child: textFieldStyle1(context, Str().searchpatient,
                              icons: Icons.search)),
                    ),
                    setWSpacing(getWidth(context, 0.02)),
                    CustomButton(
                        styleId: 1,
                        height: 0.1,
                        shapeId: 2,
                        bold: true,
                        btnText: Str().filter,
                        onPressed: () =>
                            activityFilterBottomSheet(context, widget.callId!))
                  ],
                ),
              ),
              Expanded(child: list(listdata))
            ],
          )),
    );
  }

  list(List list) {
    return Container(
      child: ListView.builder(
          padding: EdgeInsets.all(0),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return activityCard(context,
                title: widget.title,
                callId: widget.callId,
                patientName: list[index],
                clock: "11:00",
                serviceId: 1,
                bottomWidget: widget.bottomWidget);
          }),
    );
  }
}
