import 'package:obatin/importPackage.dart';

class ActivityHistory extends StatefulWidget {
  @override
  _ActivityHistoryState createState() => _ActivityHistoryState();
}

class _ActivityHistoryState extends State<ActivityHistory> {
  bool onlocation = true;
  bool visit = true;
  bool sukarela = true;
  List listdata = [1, 2, 4, 1, 3];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: getWidth(context, 0.03)),
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
                txt: Str().activity$orderlist,
                bold: true,
                color: Colors.grey,
                toppad: 0.02),
            button(
                txt: Str().beingserved,
                info: Str().beingserveddesc,
                svgName: svg_iconbeingserved,
                callId: 1,
                btnId: 1),
            button(
              txt: Str().neworder,
              callId: 2,
              btnId: 1,
              info: Str().newsharedesc,
              svgName: svg_iconneworder,
            ),
            button(
              txt: Str().waitingschedule,
              callId: 3,
              btnId: 2,
              info: Str().waitingpaymentDesc,
              svgName: svg_iconwaitingschedule,
            ),
            button(
              txt: Str().waitingpayment,
              callId: 4,
              btnId: 3,
              info: Str().waitingscheduleDesc,
              svgName: svg_iconwallet,
            ),
            button(
              txt: Str().reschedule,
              callId: 5,
              btnId: 4,
              info: Str().rescheduleDesc,
              svgName: svg_iconreschedule,
            ),
            button(
              txt: Str().ordercomplete,
              callId: 7,
              btnId: 5,
              info: Str().ordercompleteDesc,
              svgName: svg_iconordercomplete,
            ),
            button(
              txt: Str().orderrejected,
              callId: 7,
              btnId: 6,
              info: Str().activity$ProductCashbackListDesc,
              svgName: svg_icontolak,
            ),
            button(
              txt: Str().ordercomplain,
              callId: 7,
              btnId: 7,
              info: Str().ordercomplainDesc,
              svgName: svg_iconcompaint,
            ),
            button(
              txt: Str().ordercanceled,
              callId: 7,
              btnId: 8,
              info: Str().ordercanceledDesc,
              svgName: svg_iconcancel,
            ),
            CustomText(
                txt: Str().activity$referencelist,
                bold: true,
                color: Colors.grey,
                toppad: 0.02),
            button(
              txt: Str().makereferrals,
              callId: 7,
              btnId: 9,
              info: Str().activity$ProductCashbackListDesc,
              svgName: svg_iconmakereferral,
            ),
            button(
              txt: Str().receivereferrals,
              callId: 7,
              btnId: 10,
              info: Str().activity$ProductCashbackListDesc,
              svgName: svg_icondoreferal,
            ),
          ],
        ),
      ),
    );
  }

  bottomWidget({@required int? btnId}) {
    String txt = btnId == 4
        ? Str().changeschedule + ":" + " Selasa, 15 Juli 2021 10:00 WIB"
        : btnId == 5
            ? "INV/0023/TC/01052022"
            : btnId == 7
                ? Str().complaint + ": " + " Kamis, 01 Mei 2022 07:50 WIB"
                : btnId == 8
                    ? Str().cancellation + ": " + "Senin, 05 Mei 2022 12:20 WIB"
                    : btnId == 9
                        ? Str().referto +
                            " " +
                            "dr. Amal Sepanjang Hayat, Sp.BS"
                        : btnId == 10
                            ? Str().referfrom + " " + "dr. Ridho Ilahi, Sp.B"
                            : "";
    Color color = btnId == 4
        ? Colors.green
        : btnId == 5
            ? defaultcolor2
            : btnId == 7
                ? Colors.orange
                : btnId == 8
                    ? Colors.red
                    : btnId == 9
                        ? Colors.lightBlue
                        : btnId == 10
                            ? Colors.green
                            : defaultcolor3;
    return txt == ""
        ? SizedBox()
        : Container(
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.02),
                vertical: getWidth(context, 0.01)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  getWidth(context, 0.03),
                ),
                color: color),
            child: CustomText(
              txt: txt,
              size: 0.028,
              color: Colors.white,
            ),
          );
  }

  button(
      {@required String? txt,
      @required int? callId,
      @required int? btnId,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
      styleId: 5,
      verpad: 0.02,
      btnText: txt,
      btnText2: info != null ? info : null,
      suffixWidget: suffixWidget != null ? suffixWidget : null,
      horpad: 0.03,
      prefixWidget: CustomSVG(
        svgName: svgName,
        color: defaultcolor3,
        size: 0.06,
      ),
      onPressed: () => Navigator.push(
          context,
          SlideRightRoute(
              page: ActivityDetailList(
                  title: txt,
                  callId: callId,
                  bottomWidget: bottomWidget(btnId: btnId)))),
    );
  }
}
