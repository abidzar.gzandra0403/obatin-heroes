import 'package:obatin/importPackage.dart';

@immutable
class ActivityMain extends StatefulWidget {
  @override
  _ActivityMainState createState() => _ActivityMainState();
}

class _ActivityMainState extends State<ActivityMain> {
  List listdata = [1, 2, 4, 1, 3];
  int currentPage = 0;
  List<String> btnTextList = [
    Str().latest,
    Str().all,
  ];
  PageController pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().activity, true),
      body: Column(
        children: [
          TopNavbarButton(
              textBtnList: btnTextList,
              pageController: pageController,
              currentIndex: currentPage),
          Expanded(
            child: PageView(
              controller: pageController,
              onPageChanged: (index) {
                setState(() {
                  currentPage = index;
                });
              },
              children: [
                ActivityLatest(),
                ActivityHistory(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
