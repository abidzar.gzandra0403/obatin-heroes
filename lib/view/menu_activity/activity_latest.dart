import 'package:obatin/importPackage.dart';

class ActivityLatest extends StatefulWidget {
  @override
  _ActivityLatestState createState() => _ActivityLatestState();
}

class _ActivityLatestState extends State<ActivityLatest> {
  bool onlocation = true;
  bool visit = true;
  bool sukarela = true;
  List listdata = ["Anam", "Abid", "Zandra", "Anisa"];
  List listdata2 = [1, 2, 3, 4];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: getWidth(context, 0.03)),
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          children: [
            listview(listdata, 1, Str().ongoing, itemlength: 1),
            listview(listdata, 6, Str().incoming, itemlength: 1),
            listview(listdata, 2, Str().newrequest, itemlength: 4)
          ],
        ),
      ),
    );
  }

  listview(List list, int callId, String title, {int? itemlength}) {
    return Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.02)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomText(
                  txt: title,
                  bold: true,
                  textAlign: TextAlign.left,
                  size: 0.036,
                  color: Colors.grey),
              if (callId >= 2)
                CustomButton(
                    styleId: 4,
                    btnText: Str().moredetail,
                    bold: true,
                    fsize: 0.035,
                    fColor: defaultcolor2,
                    onPressed: () => Navigator.push(
                        context,
                        SlideRightRoute(
                            page: ActivityDetailList(
                          title: title,
                          callId: callId,
                        ))))
            ],
          ),
          setHSpacing(getWidth(context, 0.02)),
          Container(
            child: ListView.builder(
                padding: EdgeInsets.all(0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemlength == null
                    ? list.length
                    : list.length >= itemlength
                        ? itemlength
                        : list.length,
                itemBuilder: (BuildContext context, int index) {
                  return activityCard(context,
                      title: title,
                      callId: callId,
                      serviceId: listdata2[index],
                      patientName: list[index],
                      clock: "04:00",
                      noreg: callId <= 2 ? "OBTN172212" : null);
                }),
          ),
        ],
      ),
    );
  }
}
