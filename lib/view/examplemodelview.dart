import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/model_exampledata.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class ExampleView extends StatefulWidget {
  @override
  _ExampleViewState createState() => _ExampleViewState();
}

class _ExampleViewState extends State<ExampleView> {
  ExampleData? exampleData;
  @override
  void initState() {
    // ExampleData.connectAPI("2").then((value) {
    //   exampleData = value;
    // });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: ListView(children: <Widget>[
      new FutureBuilder<List<ExampleData>>(
          future: ExampleData.connectAPI("2"),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              List<ExampleData> posts = snapshot.data!;
              return new Column(
                  children: posts
                      .map((post) => new Column(
                            children: <Widget>[
                              new Text(post.name!),
                            ],
                          ))
                      .toList());
            } else if (snapshot.hasError) {
              return CustomText(txt: "txt");
            }
            return new Center(
              child: new Column(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.all(50.0)),
                  new CircularProgressIndicator(),
                ],
              ),
            );

            // Center(
            //   child: Column(
            //     mainAxisAlignment: MainAxisAlignment.center,
            //     children: [
            //       textNormal(context,
            //           exampleData != null ? exampleData!.name! : "Tidak Ada Nama"),
            //       RawMaterialButton(
            //         onPressed: () {
            //           ExampleData.connectAPI("2").then((value) {
            //             setState(() {
            //               exampleData = value;
            //             });
            //           });
            //         },
            //         child: textNormal(context, "Get"),
            //       )
            //     ],
            //   ),
            // ),
          })
    ])));
  }
}
