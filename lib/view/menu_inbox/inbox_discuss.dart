import 'package:obatin/importPackage.dart';

class InboxDiscuss extends StatefulWidget {
  @override
  _InboxDiscussState createState() => _InboxDiscussState();
}

class _InboxDiscussState extends State<InboxDiscuss> {
  List listdata = [1];
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: ListView.builder(
            padding: EdgeInsets.all(0),
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: listdata.length,
            itemBuilder: (BuildContext context, int index) {
              return inboxCard(context, "Masker 3 Fly isi 50 pcs",
                  "Pesan Terakhir", "Info 2", true);
            }),
      ),
    );
  }
}
