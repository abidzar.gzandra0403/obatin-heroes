import 'package:obatin/importPackage.dart';

class InboxChat extends StatefulWidget {
  @override
  _InboxChatState createState() => _InboxChatState();
}

class _InboxChatState extends State<InboxChat> {
  List listdata = [1];
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: Column(
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox(
                  height: getWidth(context, 0.12),
                  width: getWidth(context, 0.6),
                  child: CustomTextField(
                    hint: Str().searchmessage,
                    prefixIcon: Icons.search,
                    styleId: 2,
                  )),
              CustomButton(
                width: 0.12,
                styleId: 4,
                shapeId: 4,
                isize: 0.06,
                onPressed: () {},
                prefixIcon: Icons.mark_chat_read_outlined,
              ),
              CustomButton(
                width: 0.12,
                styleId: 4,
                shapeId: 4,
                isize: 0.06,
                onPressed: () {},
                prefixIcon: Icons.delete_outline_outlined,
              )
            ],
          ),
          listview(listdata, 4)
        ],
      ),
    );
  }

  listview(List list, int callId, {int? itemlength}) {
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: ListView.builder(
                padding: EdgeInsets.all(0),
                shrinkWrap: true,
                physics: NeverScrollableScrollPhysics(),
                itemCount: itemlength == null
                    ? list.length
                    : list.length >= itemlength
                        ? itemlength
                        : list.length,
                itemBuilder: (BuildContext context, int index) {
                  return inboxChatCard(
                      context, "Dr. Abidzar", "Info 1", "Info 2", true);
                }),
          ),
        ],
      ),
    );
  }
}
