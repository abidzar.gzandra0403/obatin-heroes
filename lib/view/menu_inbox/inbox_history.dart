import 'package:obatin/importPackage.dart';

class InboxHistory extends StatefulWidget {
  @override
  _InboxHistoryState createState() => _InboxHistoryState();
}

class _InboxHistoryState extends State<InboxHistory> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: 2,
              itemBuilder: (context, index) =>
                  inboxReviewHistoryCard(context, "Dewi", "30.000", btnId: 2))),
    );
  }
}
