import 'package:obatin/importPackage.dart';

class ChatConversation extends StatefulWidget {
  @override
  _ChatConversationState createState() => _ChatConversationState();
}

class _ChatConversationState extends State<ChatConversation> {
  List listdata = [1, 2];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: inboxChatConversationAppBar(context),
      body: Column(
        children: [
          Expanded(
              child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: defaultPaddingContent(context)),
                  child: listview(listdata, 4))),
          Container(
            constraints: BoxConstraints(
                minHeight: getWidth(context, 0.2),
                maxHeight: getWidth(context, 0.5)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.04)),
            color: bgColor(),
            child: Row(children: [
              Expanded(
                  child: CustomTextField(
                hint: Str().writemessage,
                styleId: 2,
                maxlines: 7,
              )),
              setWSpacing(getWidth(context, 0.02)),
              SizedBox(
                  height: getWidth(context, 0.12),
                  width: getWidth(context, 0.12),
                  child: RawMaterialButton(
                    onPressed: () {},
                    fillColor: defaultcolor1,
                    highlightColor: defaultcolor2,
                    shape: CircleBorder(),
                    child: Icon(
                      Icons.send,
                      color: Colors.white,
                      size: getWidth(context, 0.05),
                    ),
                  ))
            ]),
          )
        ],
      ),
    );
  }

  listview(List list, int callId, {String? title, int? itemlength}) {
    return Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.02)),
      child: ListView.builder(
          padding: EdgeInsets.all(0),
          itemCount: itemlength == null
              ? list.length
              : list.length >= itemlength
                  ? itemlength
                  : list.length,
          itemBuilder: (BuildContext context, int index) {
            return chatBubble(context, list[index]);
          }),
    );
  }

  chatBubble(BuildContext context, int id) {
    return Align(
      alignment: id == 1 ? Alignment.centerRight : Alignment.centerLeft,
      child: Container(
        padding: EdgeInsets.all(getWidth(context, 0.03)),
        margin: EdgeInsets.only(bottom: getWidth(context, 0.01)),
        constraints: BoxConstraints(maxWidth: getWidth(context, 0.75)),
        decoration: BoxDecoration(
            color: id == 1 ? defaultcolor2 : Colors.transparent,
            border: id == 1 ? null : Border.all(width: 2, color: defaultcolor3),
            borderRadius: BorderRadius.only(
                topLeft:
                    Radius.circular(getWidth(context, id == 1 ? 0.06 : 0.01)),
                bottomLeft: Radius.circular(getWidth(context, 0.06)),
                topRight:
                    Radius.circular(getWidth(context, id == 1 ? 0.01 : 0.06)),
                bottomRight: Radius.circular(getWidth(context, 0.06)))),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            CustomText(
                txt: id == 1
                    ? "Siang dok, apakah hasil pemeriksaan kemarin sudah keluar hasilnya?"
                    : "Siang juga, sudah keluar hasilnya, silahkan dijemput bu",
                size: 0.04,
                color: id == 1 ? Colors.white : fontColor1()),
            CustomText(
                txt: "16:00",
                color: id == 1 ? Colors.grey.shade300 : fontColor1())
          ],
        ),
      ),
    );
  }
}
