import 'package:obatin/importPackage.dart';

inboxChatConversationAppBar(BuildContext context) {
  return AppBar(
    systemOverlayStyle: SystemUiOverlayStyle.light.copyWith(
      statusBarIconBrightness:
          themeID == 1 ? Brightness.dark : Brightness.light,
    ),
    automaticallyImplyLeading: false,
    toolbarHeight: getWidth(context, 0.2),
    backgroundColor: bgColor(),
    elevation: 0.0,
    title: Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.03)),
      child: Row(mainAxisAlignment: MainAxisAlignment.start, children: <Widget>[
        IconButton(
            icon: Icon(
              Icons.arrow_back_ios,
              color: fontColor1(),
              size: getWidth(context, 0.04),
            ),
            onPressed: () {
              Navigator.pop(context);
            }),
        CircleAvatar(
          radius: getWidth(context, 0.05),
          backgroundColor: defaultcolor3,
          child: Icon(Icons.person),
        ),
        setWSpacing(getWidth(context, 0.02)),
        Expanded(
          child: CustomText(
              txt: "dr. Abidzar", size: 0.04, color: fontColor1(), bold: true),
        ),
        SizedBox(
            child: RawMaterialButton(
          onPressed: () {},
          shape: CircleBorder(),
          child: Icon(
            Icons.call,
            size: getWidth(context, 0.07),
            color: btnBlueColor(),
          ),
        ))
      ]),
    ),
  );
}
