import 'package:obatin/importPackage.dart';

inboxChatCard(BuildContext context, String sendername, String lastchat,
    String lastactive, bool profilephotos) {
  return Padding(
    padding: EdgeInsets.only(bottom: getWidth(context, 0.015)),
    child: SizedBox(
      height: getWidth(context, 0.18),
      child: RawMaterialButton(
        onPressed: () {
          goToPage(context, page: ChatConversation());
        },
        highlightColor: btnHightlightColor(),
        elevation: 0,
        highlightElevation: 0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.all(8),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          radius: getWidth(context, 0.06),
                          backgroundColor: defaultcolor3,
                          child: Icon(Icons.person),
                        ),
                        setWSpacing(getWidth(context, 0.02)),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              txt: sendername,
                              bold: true,
                              color: fontColor1(),
                              size: 0.042,
                            ),
                            CustomText(
                                txt: lastchat,
                                size: 0.035,
                                color: Colors.grey.shade500),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        CustomText(txt: "19:00", color: fontColor1()),
                        CircleAvatar(
                          radius: getWidth(context, 0.03),
                          backgroundColor: Colors.red,
                          child: CustomText(
                              txt: "2", bold: true, color: Colors.white),
                        )
                      ],
                    )
                  ]),
            ),
            Divider(
              color: Colors.grey,
              height: getWidth(context, 0.001),
              indent: getWidth(context, 0.16),
            )
          ],
        ),
      ),
    ),
  );
}
