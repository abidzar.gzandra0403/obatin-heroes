import 'package:obatin/importPackage.dart';

inboxReviewCard(
    BuildContext context, String patientName, String komentar, String datetime,
    {String? rating}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.02)),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CircleAvatar(
          backgroundColor: Colors.blue.shade100,
          radius: getWidth(context, 0.06),
          child: Icon(
            Icons.person,
            color: Colors.white,
          ),
        ),
        setWSpacing(getWidth(context, 0.03)),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              txt: patientName,
              bold: true,
              size: 0.034,
            ),
            if (rating != null)
              Row(
                children: [
                  Icon(
                    Icons.star,
                    size: getWidth(context, 0.03),
                    color: yellow,
                  ),
                  setWSpacing(getWidth(context, 0.01)),
                  CustomText(
                    txt: rating,
                    size: 0.034,
                  ),
                ],
              ),
            CustomText(
              txt: datetime,
              color: Colors.grey,
              size: 0.034,
            ),
            CustomText(
              txt: komentar,
              size: 0.034,
            ),
          ],
        ),
      ],
    ),
  );
}
