import 'package:obatin/importPackage.dart';

inboxReviewHistoryCard(BuildContext context, String title, String price,
    {int? btnId, int? serviceId}) {
  IconData initIcon = Icons.person_outline_outlined;

  return Padding(
      padding: EdgeInsets.only(bottom: getWidth(context, 0.015)),
      child: RawMaterialButton(
        fillColor: btnColor(),
        elevation: 0,
        highlightElevation: 0,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(getWidth(context, 0.03))),
        padding: EdgeInsets.all(getWidth(context, 0.02)),
        onPressed: () {},
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(
                children: [
                  CircleAvatar(
                      radius: getWidth(context, 0.05),
                      backgroundColor: defaultcolor3,
                      child: Icon(
                        initIcon,
                        color: Colors.white,
                      )),
                  setWSpacing(getWidth(context, 0.02)),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        txt: title,
                        bold: true,
                        size: 0.038,
                      ),
                      if (serviceId != null)
                        CustomText(
                          txt: serviceTypeSelect(serviceId: serviceId),
                          bold: true,
                          size: 0.031,
                          color: defaultcolor1,
                        ),
                      CustomText(
                        txt: "Rp." + price,
                        size: 0.036,
                      )
                    ],
                  ),
                ],
              ),
              if (btnId == 1)
                CustomButton(
                  styleId: 3,
                  onPressed: () {
                    btnId == 1
                        ? CustomBottomDialog(context, txt: "On Working")
                        : CustomBottomDialog(context, txt: "On Working");
                  },
                  fsize: 0.032,
                  btnText: Str().seerating,
                )
            ]),
      ));
}

textwithicon(BuildContext context, String txt, {Widget? serviceIcon}) {
  return SizedBox(
    height: getWidth(context, 0.04),
    child: Row(
      children: [
        serviceIcon == null
            ? Icon(ObatinIconpack.clock,
                size: getWidth(context, 0.03), color: iconcolorclockgreen)
            : serviceIcon,
        setWSpacing(5),
        Text(
          txt,
          style: hnnormal(context,
              txtSize: getWidth(context, 0.03), txtColor: fontColor1()),
        )
      ],
    ),
  );
}
