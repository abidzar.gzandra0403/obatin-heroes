import 'package:obatin/importPackage.dart';

class InboxReview extends StatefulWidget {
  @override
  _InboxReviewState createState() => _InboxReviewState();
}

class _InboxReviewState extends State<InboxReview> {
  List nama = ["Anisa", "Rosa", "Bedul"];
  List review = ["Review1", "Review2", "Review3"];

  progressBar(double percent, String txt) {
    return Padding(
      padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
      child: LinearPercentIndicator(
        width: getWidth(context, 0.5),
        animation: true,
        lineHeight: 15.0,
        animationDuration: 2000,
        percent: percent,
        leading: CustomText(
          txt: txt,
          width: 0.05,
        ),
        linearStrokeCap: LinearStrokeCap.roundAll,
        progressColor: defaultcolor2,
        backgroundColor: borderBlueColor(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.01)),
      child: Column(
        children: [
          Container(
            margin: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            decoration: BoxDecoration(
                color: bgColorDarken(),
                border: Border.all(width: 1, color: borderBlueColor())),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(getWidth(context, 0.02)),
                  child: Column(
                    children: <Widget>[
                      CustomText(
                        txt: "4.8",
                        bold: true,
                        size: 0.1,
                        color: defaultcolor2,
                      ),
                      CustomText(txt: Str().from + " 5.0")
                    ],
                  ),
                ),
                Padding(
                    padding: EdgeInsets.all(15.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          progressBar(0.5, "5"),
                          progressBar(0.3, "4"),
                          progressBar(0.2, "3"),
                          progressBar(0.3, "2"),
                          progressBar(0.2, "1"),
                        ])),
              ],
            ),
          ),
          SingleChildScrollView(
            child: Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: defaultPaddingContent(context)),
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: nama.length,
                    itemBuilder: (context, index) => inboxReviewCard(
                        context, nama[index], review[index], "24-04-2021",
                        rating: "4.8"))),
          ),
        ],
      ),
    );
  }
}
