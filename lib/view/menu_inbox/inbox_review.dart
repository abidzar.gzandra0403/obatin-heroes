import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/menu_inbox/widget/inboxReviewHistoryCard.dart';

class InboxReview extends StatefulWidget {
  @override
  _InboxReviewState createState() => _InboxReviewState();
}

class _InboxReviewState extends State<InboxReview> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      body: SingleChildScrollView(
        child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: ListView.builder(
                shrinkWrap: true,
                itemCount: 2,
                itemBuilder: (context, index) => inboxReviewHistoryCard(
                    context, "Dewi", "60.000",
                    serviceId: 1, btnId: 1))),
      ),
    );
  }
}
