import 'package:obatin/importPackage.dart';

@immutable
class InboxMain extends StatefulWidget {
  @override
  _InboxMainState createState() => _InboxMainState();
}

class _InboxMainState extends State<InboxMain> {
  List listdata = [1, 2, 4, 1, 3];
  int currentPage = 0;
  List<String> btnTextList = [
    Str().chat,
    Str().discuss,
    Str().review,
    // Str().history,
  ];

  PageController pageController = new PageController();

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness:
              themeID == 1 ? Brightness.dark : Brightness.light),
      child: Scaffold(
          backgroundColor: bgColor(),
          appBar: CustomAppBar(context, Str().inbox, true),
          body: Column(
            children: [
              TopNavbarButton(
                  textBtnList: btnTextList,
                  pageController: pageController,
                  currentIndex: currentPage),
              Expanded(
                child: PageView(
                  controller: pageController,
                  onPageChanged: (index) {
                    setState(() {
                      currentPage = index;
                    });
                  },
                  children: [
                    InboxChat(),
                    InboxDiscuss(),
                    InboxReview(),
                    // InboxHistory(),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
