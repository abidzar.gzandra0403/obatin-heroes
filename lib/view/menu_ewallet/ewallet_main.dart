import 'package:obatin/importPackage.dart';

class EWalletMain extends StatefulWidget {
  @override
  _EWalletMainState createState() => _EWalletMainState();
}

class _EWalletMainState extends State<EWalletMain> {
  bool animated = false;
  DateTimeRange? myDateRange;
  GlobalKey<FormState> myFormKey = new GlobalKey();
  Timer? animateTime;
  @override
  void initState() {
    animateTime = Timer.periodic(Duration(milliseconds: 300), (_) {
      setState(() {
        animated = !animated;
        animateTime!.cancel();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: gopayBlueColor2(),
      appBar: CustomAppBar(
        context,
        "Dompet Digital",
        true,
        fontColor: Colors.white,
        brightness: Brightness.dark,
        backgroundColor: gopayBlueColor2(),
      ),
      body: Stack(
        children: [
          eWalletHeader(context, animated: animated),
          Container(
            height: getHeight(context, 1),
            margin: EdgeInsets.only(top: getWidth(context, 0.5)),
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context),
                vertical: getWidth(context, 0.05)),
            decoration: BoxDecoration(
                color: bgColor(),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(getWidth(context, 0.08)),
                    topRight: Radius.circular(getWidth(context, 0.08)))),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Form(
                              key: myFormKey,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  CustomText(
                                    txt: "Lihat Berdasarkan Tanggal",
                                    bold: true,
                                    color: Colors.grey,
                                  ),
                                  SizedBox(
                                    width: getWidth(context, 0.5),
                                    height: getWidth(context, 0.15),
                                    child: DateRangeField(
                                        saveText: "Konfirmasi",
                                        helpText: "Pilih Jangka Tanggal",
                                        cancelText: "Batal",
                                        confirmText: "Konfirmasi",
                                        errorFormatText: "Format Salah",
                                        errorInvalidText: "Diluar Jangka",
                                        errorInvalidRangeText: "Jangka Salah",
                                        fieldStartHintText: "Tanggal Mulai",
                                        fieldEndHintText: "Tanggal Akhir",
                                        fieldStartLabelText: "Tanggal Mulai",
                                        fieldEndLabelText: "Tanggal Akhir",
                                        firstDate: DateTime(1990),
                                        // initialValue: DateTimeRange(
                                        //     start: DateTime.now(),
                                        //     end: DateTime.now()),
                                        enabled: true,
                                        decoration: InputDecoration(
                                          contentPadding: EdgeInsets.symmetric(
                                              horizontal:
                                                  getWidth(context, 0.02),
                                              vertical: 0),
                                          border: OutlineInputBorder(
                                              borderSide: const BorderSide(
                                                  width: 0,
                                                  color: Colors.transparent),
                                              borderRadius:
                                                  BorderRadius.circular(
                                                      getWidth(context, 0))),
                                        ),
                                        validator: (value) {
                                          if (value!.start
                                              .isBefore(DateTime.now())) {
                                            return 'Pilih Tanggal Akhir';
                                          }
                                          return null;
                                        },
                                        onSaved: (value) {
                                          // setState(() {
                                          //   myDateRange = value!;
                                          // });
                                        }),
                                  ),
                                ],
                              ),
                            ),
                            _buttonSettings(
                              func: () => btnFunc(),
                              title: "Lihat Semua Transaksi",
                            ),
                            CustomExpansionTile(
                                title: "Pemasukan",
                                bold: true,
                                children: [
                                  _buttonSettings(
                                      func: () => btnFunc(),
                                      title: "Perolehan Poin"),
                                  _buttonSettings(
                                      func: () => btnFunc(),
                                      title: "Insentif Layanan"),
                                  _buttonSettings(
                                      func: () => btnFunc(),
                                      title: "Insentif Merujuk"),
                                  _buttonSettings(
                                      func: () => btnFunc(),
                                      title: "Insentif Resep"),
                                  _buttonSettings(
                                      func: () => btnFunc(), title: "Cashback"),
                                  _buttonSettings(
                                      func: () => btnFunc(),
                                      title: "Insentif Promo"),
                                ]),
                            CustomExpansionTile(
                                title: "Penarikan",
                                bold: true,
                                children: [
                                  _buttonSettings(
                                      func: () => btnFunc(), title: "Poin"),
                                  _buttonSettings(
                                      func: () => btnFunc(), title: "Dana"),
                                ]),
                            CustomExpansionTile(
                                title: "Saldo",
                                bold: true,
                                children: [
                                  _buttonSettings(
                                      func: () => btnFunc(), title: "Poin"),
                                  _buttonSettings(
                                      func: () => btnFunc(), title: "Dana"),
                                ]),
                            CustomText(
                              toppad: 0.02,
                              botpad: 0.02,
                              txt: "Transaksi Baru-baru ini",
                              bold: true,
                              color: Colors.grey,
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 4,
                              itemBuilder: (BuildContext context, int index) =>
                                  eWalletTransactionCard(
                                      context, "Top Up", "160.000",
                                      transactionType: 1),
                            ),
                            ListView(
                              shrinkWrap: true,
                              children: [],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  btnFunc() {
    return CustomBottomDialog(context, txt: "On Working");
  }

  _buttonSettings(
      {Widget? page,
      VoidCallback? func,
      @required String? title,
      IconData? icon}) {
    return CustomButton(
        styleId: 5,
        btnText: title!,
        bColor: btnColor(),
        onPressed: () {
          func != null ? func() : goToPage(context, page: page!);
        });
  }
}
