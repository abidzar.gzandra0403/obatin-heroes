import 'package:obatin/importPackage.dart';

eWalletHeader(BuildContext context, {@required bool? animated}) {
  iconButton(
    var svgName,
    String text, {
    @required Widget? gotopage,
  }) {
    return CustomButton(
      styleId: 6,
      width: 0.15,
      onPressed: () => goToPage(context, page: gotopage!),
      btnText: text,
      fColor: Colors.white,
      fsize: 0.03,
      verpad: 0,
      bColor: Colors.transparent,
      prefixWidget: CustomSVG(
        svgName: svgName,
        color: Colors.white,
        size: 0.06,
      ),
    );
  }

  return Container(
    color: gopayBlueColor2(),
    height: getWidth(context, 0.6),
    width: getWidth(context, 1),
    child: Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: Transform.translate(
            offset: Offset(0, -getWidth(context, 0.12)),
            child: CustomText(
              txt: "Rp. 455.000,-",
              color: Colors.white,
              bold: true,
              size: 0.09,
            ),
          ),
        ),
        Align(
          alignment: Alignment.center,
          child: Transform.translate(
            offset: Offset(0, -getWidth(context, 0.04)),
            child: CustomText(
              txt: "Poin " + "1500",
              color: Colors.white,
              bold: true,
              size: 0.045,
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Transform.translate(
              offset: Offset(getWidth(context, 0.05), getWidth(context, 0.12)),
              child: Container(
                width: getWidth(context, 0.2),
                padding: EdgeInsets.all(getWidth(context, 0.012)),
                decoration: BoxDecoration(
                    color: Colors.white70,
                    borderRadius:
                        BorderRadius.circular(getWidth(context, 0.03))),
                child: icon_gopay,
              )),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Transform.translate(
              offset: Offset(-getWidth(context, 0.1), getWidth(context, 0.2)),
              child: AnimatedContainer(
                duration: Duration(milliseconds: 1400),
                curve: Curves.ease,
                margin: EdgeInsets.only(top: animated! ? 0 : 100, right: 0),
                child: CircleAvatar(
                  radius: getWidth(context, 0.2),
                  backgroundColor: Colors.white12,
                ),
              )),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: Transform.translate(
            offset: Offset(getWidth(context, 0.35), getWidth(context, 0.08)),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 1400),
              curve: Curves.ease,
              margin: EdgeInsets.only(top: animated ? 0 : 100, right: 0),
              child: CircleAvatar(
                radius: getWidth(context, 0.53),
                backgroundColor: Colors.white12,
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: Transform.translate(
            offset: Offset(-getWidth(context, 0.03), getWidth(context, 0.13)),
            child: SizedBox(
              width: getWidth(context, 0.3),
              height: getWidth(context, 0.15),
              child: Row(
                children: [
                  iconButton(svg_gopaytransfer, "Bayar", gotopage: EmptyPage()),
                  iconButton(svg_gopaytopup, "Top Up", gotopage: EmptyPage()),
                ],
              ),
            ),
          ),
        )

        // Align(
        //   alignment: Alignment.topLeft,
        //   child: Transform.translate(
        //     offset: Offset(getWidth(context, 0.15), getWidth(context, 0)),
        //     child: CircleAvatar(
        //       radius: getWidth(context, 0.09),
        //       backgroundColor: Colors.white12,
        //     ),
        //   ),
        // )
      ],
    ),
  );
}
