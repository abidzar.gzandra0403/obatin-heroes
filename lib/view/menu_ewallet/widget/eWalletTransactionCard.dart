import 'package:obatin/importPackage.dart';

eWalletTransactionCard(BuildContext context, String title, String cost,
    {@required int? transactionType}) {
  return CustomButton(
    // width: 1,
    verpad: 0.01,
    vermargin: 0.02,
    styleId: 6,
    bColor: btnColor(),
    onPressed: () {},
    prefixWidget: Row(
      children: [
        Container(
          height: getWidth(context, 0.1),
          width: getWidth(context, 0.15),
          child: Center(
              child: CustomSVG(
            svgName: transactionType == 1 ? svg_gopaytopup : svg_gopaytransfer,
            color: defaultcolor2,
            size: 0.07,
          )),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(txt: title, bold: true),
              SizedBox(
                width: getWidth(context, 0.7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(
                      txt: "09:00",
                      prefixIcon: ObatinIconpack.clock,
                      iconColor: Colors.green,
                    ),
                    CustomText(
                      txt: "Rp." + cost + ",-",
                      bold: true,
                    )
                  ],
                ),
              ),
            ],
          ),
        )
      ],
    ),
  );
}
