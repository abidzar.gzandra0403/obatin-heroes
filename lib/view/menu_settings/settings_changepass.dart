import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/alltext.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/menu_settings/widget/settingsPasswordField.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';

TextEditingController settingspassTxtbox = new TextEditingController();
TextEditingController settingspassConfirmTxtbox = new TextEditingController();

class SettingsChangePass extends StatefulWidget {
  @override
  _SettingsChangePassState createState() => _SettingsChangePassState();
}

class _SettingsChangePassState extends State<SettingsChangePass> {
  handleBack() {
    CustomAlertDialog(context,
        title: Str().setting$cancelchangepassworddialog,
        txtButton1: Str().yes,
        txtButton2: Str().cancel, funcButton1: () {
      settingspassTxtbox.text = "";
      settingspassConfirmTxtbox.text = "";
      Navigator.pop(context);
    });
  }

  confirmFunc() {
    CustomAlertDialog(context,
        title: Str().setting$changepassworddialog,
        contentText: Str().setting$changepassworddialogdesc,
        txtButton1: Str().sure,
        txtButton2: Str().cancel, funcButton1: () {
      settingspassTxtbox.text = "";
      settingspassConfirmTxtbox.text = "";
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => handleBack(),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().setting$changepassword, true,
            customBackFunc: () => handleBack()),
        body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Stack(
              children: [
                Column(
                  children: [
                    CustomTextField(
                      hint: Str().oldpassword,
                    ),
                    settingsPasswordField(
                        context, Str().newpassword, settingspassTxtbox),
                    settingsPasswordField(context, Str().newpasswordconfirm,
                        settingspassConfirmTxtbox,
                        txtId: 2, targetTxtbox: settingspassTxtbox),
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomButton(
                      vermargin: 0.04,
                      height: 0.1,
                      styleId: 1,
                      onPressed: () => confirmFunc(),
                      btnText: Str().savechanges),
                )
              ],
            )),
      ),
    );
  }
}
