import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customDropDown.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';
import 'package:obatin/view/common_widget/passwordFieldValidator.dart';

class SettingsBankAccount extends StatefulWidget {
  @override
  _SettingsBankAccountState createState() => _SettingsBankAccountState();
}

class _SettingsBankAccountState extends State<SettingsBankAccount> {
  PageController pageController = new PageController();
  String btnText = Str().next;
  int currentIndex = 0;
  handleBack() {
    if (currentIndex == 0) {
      Navigator.pop(context);
    } else if (currentIndex == 1) {
      CustomAlertDialog(
        context,
        title: Str().setting$canceladdaccountdialog,
        contentText: Str().allchangenotbesave,
        txtButton1: Str().yes,
        txtButton2: Str().cancel,
        funcButton1: () => pageController.previousPage(
            duration: Duration(milliseconds: 500), curve: Curves.ease),
      );
    } else {
      btnText = Str().next;
      pageController.previousPage(
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }
  }

  @override
  Widget build(BuildContext context) {
    nextFunc() {
      if (currentIndex == 2) {
        CustomAlertDialog(
          context,
          title: Str().sent,
          contentIcon: Icons.check_circle_outline,
          funcDismiss: () => Navigator.pop(context),
          contentText: Str().setting$accountverification,
        );
      } else {
        pageController.nextPage(
            duration: Duration(milliseconds: 400), curve: Curves.ease);
      }
    }

    return WillPopScope(
      onWillPop: () => handleBack(),
      child: Scaffold(
          appBar: CustomAppBar(
              context,
              currentIndex == 0
                  ? Str().setting$bankaccount
                  : currentIndex == 1
                      ? Str().setting$addbankaccount
                      : Str().confirmation,
              true,
              customBackFunc: () => handleBack()),
          backgroundColor: bgColor(),
          body: Stack(
            children: [
              PageView(
                physics: NeverScrollableScrollPhysics(),
                onPageChanged: (index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                controller: pageController,
                children: [
                  page1(),
                  page2(),
                  page3(),
                ],
              ),
              if (currentIndex >= 1)
                CustomButton(
                  styleId: 1,
                  width: 0.9,
                  height: 0.1,
                  vermargin: 0.03,
                  btnText: btnText,
                  alignment: Alignment.bottomCenter,
                  onPressed: () {
                    setState(() {
                      btnText = Str().send;
                    });
                    nextFunc();
                  },
                )
            ],
          )),
    );
  }

  page1() {
    creditCard() {
      return Container(
        padding: EdgeInsets.all(getWidth(context, 0.04)),
        decoration: BoxDecoration(
            color: btnColor(),
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, offset: Offset(1, 1), blurRadius: 5)
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: defaultcolor4,
                  child: Icon(
                    Icons.credit_card,
                    color: Colors.white,
                  ),
                ),
                setWSpacing(getWidth(context, 0.03)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      txt: "BRI",
                      bold: true,
                    ),
                    CustomText(
                      txt: "Checked (" + Str().primary + ")",
                      color: defaultcolor2,
                    )
                  ],
                ),
              ],
            ),
            CustomText(
              txt: "****0092",
              size: 0.05,
            )
          ],
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.03)),
        child: Column(
          children: [
            creditCard(),
            CustomButton(
              vermargin: 0.03,
              // width: 0.15,
              styleId: 1,
              shapeId: 2,
              onPressed: () => pageController.nextPage(
                  duration: Duration(milliseconds: 500), curve: Curves.ease),
              btnText: Str().setting$addbankaccount,
              prefixIcon: Icons.add,
              isize: 0.07,
            )
          ],
        ),
      ),
    );
  }

  page2() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        child: Column(
          children: [
            CustomTextField(hint: Str().setting$accountfullname),
            CustomTextField(hint: Str().setting$accountnumber),
            CustomDropDown(hint: Str().setting$bankname)
          ],
        ),
      ),
    );
  }

  page3() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
                txt: Str().setting$accountfullname,
                bold: true,
                size: 0.035,
                botpad: 0.01),
            CustomText(txt: "Abidzar Ghifari Zandra", size: 0.04, botpad: 0.03),
            CustomText(
                txt: Str().setting$accountnumber,
                bold: true,
                size: 0.035,
                botpad: 0.01),
            CustomText(txt: "151011526122001", size: 0.04, botpad: 0.03),
            CustomText(
                txt: Str().setting$bankname,
                bold: true,
                size: 0.035,
                botpad: 0.01),
            CustomText(txt: "BRI", size: 0.04, botpad: 0.01),
          ],
        ),
      ),
    );
  }
}
