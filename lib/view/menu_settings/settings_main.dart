import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:obatin/main.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/alltext.dart';
import 'package:obatin/view/common/asset_images.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customSVG.dart';
import 'package:obatin/view/common_widget/customWidget/customSwitch.dart';
import 'package:obatin/view/emptyPage.dart';
import 'package:obatin/view/login_page/login_main.dart';
import 'package:obatin/view/menu_settings/settings_bankaccount.dart';
import 'package:obatin/view/menu_settings/settings_changedescription.dart';
import 'package:obatin/view/menu_settings/settings_changename.dart';
import 'package:obatin/view/menu_settings/settings_changepass.dart';
import 'package:obatin/view/menu_settings/settings_notification.dart';
import 'package:obatin/view/menu_settings/settings_service.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class SettingsMain extends StatefulWidget {
  @override
  _SettingsMainState createState() => _SettingsMainState();
}

class _SettingsMainState extends State<SettingsMain> {
  bool allowreview = false;
  settingsBtn(
      {@required String? title,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
        styleId: 5,
        verpad: 0.02,
        btnText: title,
        btnText2: info != null ? info : null,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        prefixWidget: CustomSVG(
          svgName: svgName,
          color: defaultcolor3,
          size: 0.06,
          hormargin: 0.02,
        ),
        onPressed: () {
          if (gotopage != null) {
            Navigator.push(context, SlideRightRoute(page: gotopage));
          } else {
            func!();
          }
        });
  }

  allowratingSwitchFunc() {
    setState(() {
      allowreview = !allowreview;
    });
  }

  alertDialog() {
    button(IconData icon, String txt) {
      return CustomButton(
        styleId: 3,
        vermargin: 0.02,
        height: 0.1,
        onPressed: () {},
        prefixIcon: icon,
        btnText: txt,
      );
    }

    return CustomAlertDialog(context,
        title: Str().setting$changeprofilephoto,
        contentWidget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            button(Icons.add_a_photo_outlined, Str().setting$opencamera),
            button(Icons.image_outlined, Str().setting$uploadphoto),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, Str().settings, true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                child: Center(
                  child: CircleAvatar(
                      radius: getWidth(context, 0.12),
                      child: RawMaterialButton(
                        shape: CircleBorder(),
                        onPressed: () => alertDialog(),
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(999),
                          child: Stack(
                            children: [
                              img_profilephotosexample,
                              Align(
                                alignment: Alignment.bottomCenter,
                                child: Container(
                                  height: getWidth(context, 0.08),
                                  alignment: Alignment.center,
                                  decoration:
                                      BoxDecoration(color: Colors.black45),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.edit,
                                        size: getWidth(context, 0.04),
                                      ),
                                      setWSpacing(getWidth(context, 0.01)),
                                      CustomText(
                                          txt: Str().edit,
                                          bold: true,
                                          color: Colors.white)
                                    ],
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: getWidth(context, 0.05)),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: defaultPaddingContent(context)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                                txt: Str().setting$practiceNservicetitle,
                                bold: true,
                                color: Colors.grey),
                            settingsBtn(
                                title: Str().setting$practiceNservice,
                                info: Str().setting$practiceNservicedesc,
                                svgName: svg_iconpracticesetting,
                                gotopage: SettingsService()),
                            settingsBtn(
                                title: Str().setting$titleNfullname,
                                info: Str().setting$titleNfullnamedesc,
                                svgName: svg_iconedittitle,
                                gotopage: SettingsChangeName()),
                            CustomText(
                                txt: Str().setting$userinfotitle,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            settingsBtn(
                                title: Str().setting$editdescription,
                                info: Str().setting$editdescriptiondesc,
                                svgName: svg_icondescription,
                                gotopage: SettingsChangeDescription()),
                            settingsBtn(
                                title: Str().setting$allowreviewNrating,
                                svgName: svg_iconreview,
                                func: () => allowratingSwitchFunc(),
                                suffixWidget: CustomSwitch(
                                    rightmargin: 0.03,
                                    onToggle: () => allowratingSwitchFunc(),
                                    value: allowreview)),
                            settingsBtn(
                                title: Str().setting$bankaccount,
                                info: Str().setting$bankaccountdesc,
                                svgName: svg_iconbankaccount,
                                gotopage: SettingsBankAccount()),
                            CustomText(
                                txt: Str().setting$accountsettingtitle,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            settingsBtn(
                                title: Str().setting$changepassword,
                                info: Str().setting$changepassworddesc,
                                svgName: svg_iconpassword,
                                gotopage: SettingsChangePass()),
                            settingsBtn(
                                title: Str().setting$notification,
                                info: Str().setting$notificationdesc,
                                svgName: svg_iconnotifsettings,
                                gotopage: SettingsNotification())
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
