import 'package:flutter/material.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';
import 'package:obatin/view/menu_settings/widget/settingsWorktime.dart';

class SettingsServiceEdit extends StatefulWidget {
  final String? noSip;
  final String? service;
  final int? editId;
  final int? serviceType;
  final String? allserviceTitle;
  SettingsServiceEdit(
      {this.service,
      @required this.editId,
      @required this.noSip,
      @required this.serviceType,
      this.allserviceTitle});
  @override
  _SettingsServiceEditState createState() => _SettingsServiceEditState();
}

class _SettingsServiceEditState extends State<SettingsServiceEdit> {
  String title = "";
  @override
  void initState() {
    super.initState();
    title = widget.allserviceTitle != null
        ? widget.allserviceTitle!
        : widget.serviceType == 1
            ? "Edit Layanan " + widget.service!
            : "Edit Layanan Sukarela " + widget.service!;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, "", true,
          rightWidget: CustomButton(
            styleId: 3,
            onPressed: () => Navigator.pop(context),
            btnText: "Simpan",
            suffixIcon: Icons.check_outlined,
          )),
      backgroundColor: bgColor(),
      body: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                txt: title,
                bold: true,
                align: TextAlign.center,
                size: 0.042,
                spacing: 1,
              ),
              CustomText(
                txt: "SIP. " + widget.noSip!,
                toppad: 0.08,
                botpad: 0.05,
                bold: true,
                size: 0.037,
              ),
              if (widget.editId == 0 || widget.editId == 1)
                if (widget.serviceType == 1)
                  CustomTextField(
                    hint: "Tarif / Jam",
                  ),
              if (widget.editId == 0 || widget.editId == 2)
                settingsWorktime(context),
            ],
          ),
        ),
      ),
    );
  }
}
