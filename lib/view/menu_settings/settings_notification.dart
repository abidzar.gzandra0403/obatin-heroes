import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customSwitch.dart';

class SettingsNotification extends StatefulWidget {
  @override
  _SettingsNotificationState createState() => _SettingsNotificationState();
}

class _SettingsNotificationState extends State<SettingsNotification> {
  bool notifObatin = false;
  bool notifPromo = false;
  bool notifUpdateFitur = false;
  switchFunc(int btnId) {
    setState(() {
      if (btnId == 1) {
        notifObatin = !notifObatin;
      } else if (btnId == 2) {
        notifPromo = !notifPromo;
      } else {
        notifUpdateFitur = !notifUpdateFitur;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().setting$notificationsetting, true),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
            horizontal: defaultPaddingContent(context),
            vertical: getWidth(context, 0.04)),
        child: Column(
          children: [
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(1),
                verpad: 0.02,
                btnText: Str().setting$obatinnotif,
                btnText2: Str().setting$obatinnotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(1), value: notifObatin)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(2),
                verpad: 0.02,
                btnText: Str().setting$obatinnotif,
                btnText2: Str().setting$obatinnotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(2), value: notifPromo)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(3),
                verpad: 0.02,
                btnText: Str().setting$updatenotif,
                btnText2: Str().setting$updatenotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(3), value: notifUpdateFitur))
          ],
        ),
      ),
    );
  }
}
