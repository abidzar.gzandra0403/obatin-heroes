import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';
import 'package:obatin/view/common_widget/passwordFieldValidator.dart';

class SettingsChangeName extends StatefulWidget {
  @override
  _SettingsChangeNameState createState() => _SettingsChangeNameState();
}

class _SettingsChangeNameState extends State<SettingsChangeName> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, Str().setting$changefullnametitle, true,
          rightWidget: CustomButton(
              styleId: 3,
              btnText: Str().save,
              suffixIcon: Icons.check_outlined,
              onPressed: () {
                CustomAlertDialog(context,
                    title: Str().saved,
                    contentIcon: Icons.check_circle_outline,
                    funcDismiss: () => Navigator.pop(context),
                    width: 0.2);
              })),
      backgroundColor: bgColor(),
      body: SingleChildScrollView(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
          child: Column(
            children: [
              CustomTextField(hint: Str().fullname),
              CustomTextField(hint: Str().prefixtitle),
              CustomTextField(hint: Str().suffixtitle),
            ],
          ),
        ),
      ),
    );
  }
}
