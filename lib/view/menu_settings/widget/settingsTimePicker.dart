import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/textstyle_manager.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

TextEditingController jamkerja_mulai_jam = new TextEditingController();
TextEditingController jamkerja_mulai_menit = new TextEditingController();
TextEditingController jamkerja_sampai_jam = new TextEditingController();
TextEditingController jamkerja_sampai_menit = new TextEditingController();

settingsTimePicker(BuildContext context, int funcId, {String? day}) {
  jamkerja_mulai_jam.text = "08";
  jamkerja_mulai_menit.text = "00";
  jamkerja_sampai_jam.text = "20";
  jamkerja_sampai_menit.text = "00";
  String inittitle =
      funcId == 0 ? "Setel Jam Kerja Untuk Semua Hari" : "Setel Jam Kerja";
  return showMaterialModalBottomSheet(
      expand: false,
      isDismissible: true,
      bounce: true,
      backgroundColor: Colors.transparent,
      context: context,
      builder: (builder) {
        return new Container(
          height: getWidth(context, 1),
          decoration: BoxDecoration(
              color: btnColor(),
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(getWidth(context, 0.06)),
                  topRight: Radius.circular(getWidth(context, 0.06)))),
          child: Padding(
            padding: EdgeInsets.all(getWidth(context, 0.1)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CustomText(
                    txt: day != null ? inittitle + " " + day : inittitle,
                    bold: true,
                    size: 0.04),
                setHSpacing(getWidth(context, 0.05)),
                CustomText(txt: "Mulai", size: 0.045),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    timecounter(context),
                    setWSpacing(getWidth(context, 0.05)),
                    timecounter(context),
                  ],
                ),
                setHSpacing(getWidth(context, 0.05)),
                CustomText(txt: "Sampai", size: 0.045),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    timecounter(context),
                    setWSpacing(getWidth(context, 0.05)),
                    timecounter(context),
                  ],
                ),
                setHSpacing(getWidth(context, 0.04)),
                RawMaterialButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  shape: CircleBorder(),
                  padding: EdgeInsets.all(getWidth(context, 0.04)),
                  fillColor: defaultcolor2,
                  child: Icon(
                    Icons.check,
                    color: Colors.white,
                    size: getWidth(context, 0.06),
                  ),
                )
              ],
            ),
          ),
        );
      });
}

timecounter(BuildContext context) {
  arrow(int btnId, int arrowicon) {
    return SizedBox(
      width: getWidth(context, 0.1),
      height: getWidth(context, 0.07),
      child: RawMaterialButton(
        onPressed: () {},
        shape: StadiumBorder(),
        fillColor: btnColor(),
        child: Icon(
          arrowicon == 1
              ? Icons.arrow_drop_up_rounded
              : Icons.arrow_drop_down_rounded,
          color: defaultcolor2,
          size: getWidth(context, 0.07),
        ),
      ),
    );
  }

  return Row(
    children: [
      Container(
        height: getWidth(context, 0.15),
        width: getWidth(context, 0.15),
        decoration: BoxDecoration(
            border: Border.all(width: 2, color: fontColor2()),
            borderRadius: BorderRadius.circular(getWidth(context, 0.03))),
        child: TextField(
          textAlign: TextAlign.center,
          maxLength: 2,
          style: hnnormal(context, txtSize: getWidth(context, 0.08)),
          decoration: InputDecoration(
            counterText: "",
            enabledBorder: InputBorder.none,
            focusedBorder: InputBorder.none,
            disabledBorder: InputBorder.none,
            errorBorder: InputBorder.none,
            border: InputBorder.none,
          ),
        ),
      ),
      setWSpacing(getWidth(context, 0.02)),
      Column(
        children: [
          arrow(1, 1),
          setHSpacing(getWidth(context, 0.01)),
          arrow(2, 2),
        ],
      )
    ],
  );
}
