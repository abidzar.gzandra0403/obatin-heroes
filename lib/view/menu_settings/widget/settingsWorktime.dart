import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/menu_settings/widget/settingsTimePicker.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

settingsWorktime(BuildContext context) {
  rowcontent(String txt, String time, int id) {
    bool value = false;
    return SizedBox(
      height: getWidth(context, 0.09),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              CustomText(
                txt: txt,
                width: 0.2,
                size: 0.038,
              ),
              CustomText(
                txt: time,
                size: 0.038,
              ),
            ],
          ),
          Row(
            children: [
              CustomButton(
                styleId: 4,
                shapeId: 4,
                onPressed: () =>
                    settingsTimePicker(context, id, day: "Hari " + txt),
                prefixIcon: Icons.mode_edit_rounded,
                isize: 0.05,
                iColor: btnBlueColor(),
                width: 0.15,
              ),
              FlutterSwitch(
                width: getWidth(context, 0.1),
                height: getWidth(context, 0.05),
                valueFontSize: 0,
                toggleSize: getWidth(context, 0.05),
                value: value,
                borderRadius: 30.0,
                padding: getWidth(context, 0.004),
                activeColor: defaultcolor1,
                onToggle: (val) {},
              )
            ],
          )
        ],
      ),
    );
  }

  return Container(
    width: getWidth(context, 1),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(
          txt: "Jam Kerja",
          bold: true,
          size: 0.035,
          color: defaultcolor5,
          toppad: 0.02,
        ),
        rowcontent("Senin", "10:00-17:00", 1),
        rowcontent("Selasa", "10:00-17:00", 2),
        rowcontent("Rabu", "10:00-17:00", 3),
        rowcontent("Kamis", "10:00-17:00", 4),
        rowcontent("Jumat", "10:00-17:00", 5),
        rowcontent("Sabtu", "10:00-17:00", 6),
        rowcontent("Minggu", "10:00-17:00", 7),
        CustomButton(
          styleId: 1,
          shapeId: 2,
          vermargin: 0.02,
          btnText: "Setel Jam Untuk Semua Hari",
          onPressed: () => settingsTimePicker(context, 0),
        )
      ],
    ),
  );
}
