import 'package:flutter/material.dart';
import 'package:obatin/controller/bloc/crud_bloc/crud_sip_bloc.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customBottomDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SettingsServiceSipForm extends StatefulWidget {
  final int? callId;
  final String? noSip;
  final int? dataid;
  final String? alamat;
  SettingsServiceSipForm(
      {@required this.callId, this.noSip, this.dataid, this.alamat});
  @override
  _SettingsServiceSipFormState createState() => _SettingsServiceSipFormState();
}

class _SettingsServiceSipFormState extends State<SettingsServiceSipForm> {
  TextEditingController txtcNomorSip = new TextEditingController();
  TextEditingController txtcAlamatSip = new TextEditingController();

  @override
  void initState() {
    txtcAlamatSip.text = widget.alamat != null ? widget.alamat! : "";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
          context,
          widget.callId == 1
              ? "Tambah Izin Praktik / Kerja"
              : "Edit " + widget.noSip!,
          true,
          rightWidget: CustomButton(
            styleId: 3,
            onPressed: () async {
              if (widget.callId == 1) {
                if (txtcNomorSip.text.isEmpty || txtcAlamatSip.text.isEmpty) {
                  CustomBottomDialog(context, txt: "Mohon Lengkapi Form");
                } else {
                  context.read<CRUDSipBloc>().add(AddSIP(context,
                      noSIP: txtcNomorSip.text, alamat: txtcAlamatSip.text));
                  CustomAlertDialog(context,
                      title: "Berhasil Disimpan",
                      contentIcon: Icons.check_circle_outline,
                      contentText:
                          "Kami akan memverifikasi data nya terlebih dahulu. Mohon ditunggu ya.",
                      funcDismiss: () => Navigator.pop(context));
                }
              } else {
                if (txtcAlamatSip.text.isEmpty) {
                  CustomBottomDialog(context, txt: "Mohon Lengkapi Form");
                } else {
                  context.read<CRUDSipBloc>().add(UpdateSIP(context,
                      dataid: widget.dataid, alamat: txtcAlamatSip.text));
                  CustomAlertDialog(context,
                      title: "Berhasil Diupdate",
                      contentIcon: Icons.check_circle_outline,
                      contentText:
                          "Kami akan memverifikasi data nya terlebih dahulu. Mohon ditunggu ya.",
                      funcDismiss: () => Navigator.pop(context));
                }
              }
            },
            btnText: widget.callId == 1 ? "Simpan" : "Update",
            suffixIcon: Icons.check_outlined,
          )),
      backgroundColor: bgColor(),
      body: Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (widget.callId == 1)
                CustomTextField(
                  hint: "Nomor SIP",
                  controller: txtcNomorSip,
                ),
              CustomTextField(
                hint: "Alamat SIP",
                controller: txtcAlamatSip,
                maxlines: 6,
                minHeight: 0.3,
              ),
              CustomText(
                  toppad: 0.02,
                  botpad: 0.02,
                  txt:
                      "Anda dapat menambahkan SIP/STR Maksimal 3, setiap satu SIP/STR anda dapat membuka satu praktik.\nPihak OBAT-in akan melakukan verifikasi terlebih dahulu maksimal 3 x 24 Jam (tidak termasuk hari libur).\nPastikan masa berlaku SIP/STR masih aktif, apabila masa berlaku nomor SIP/STR sudah kadaluarsa maka proses verifikasi gagal.\nPastikan nomor SIP/STR yang anda masukkan sudah benar agar proses verfikasi lebih cepat."),
            ],
          ),
        ),
      ),
    );
  }
}
