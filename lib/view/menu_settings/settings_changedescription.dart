import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customBottomDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customTextField.dart';
import 'package:obatin/view/common_widget/passwordFieldValidator.dart';

class SettingsChangeDescription extends StatefulWidget {
  @override
  _SettingsChangeDescriptionState createState() =>
      _SettingsChangeDescriptionState();
}

class _SettingsChangeDescriptionState extends State<SettingsChangeDescription> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(context, Str().setting$changedesc, true,
          rightWidget: CustomButton(
              styleId: 3,
              btnText: Str().save,
              suffixIcon: Icons.check_outlined,
              onPressed: () {
                CustomBottomDialog(context, txt: Str().saved);
                Navigator.pop(context);
              })),
      backgroundColor: bgColor(),
      body: SingleChildScrollView(
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
          child: Column(
            children: [
              CustomTextField(
                  hint: Str().setting$yourdescription,
                  maxlines: 20,
                  minHeight: 0.4),
            ],
          ),
        ),
      ),
    );
  }
}
