import 'package:expandable/expandable.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/crud_bloc/crud_sip_bloc.dart';
import 'package:obatin/models/postProceedData.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/asset_images.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/menu_settings/settings_servicesipform.dart';
import 'package:obatin/view/common_widget/servicePracticeCard.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class SettingsService extends StatefulWidget {
  @override
  _SettingsServiceState createState() => _SettingsServiceState();
}

class _SettingsServiceState extends State<SettingsService> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
          backgroundColor: bgColor(),
          appBar: CustomAppBar(context, "Pengaturan Layanan", true),
          body: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: SizedBox(
                  height: getWidth(context, 0.4),
                  child: img_settings,
                ),
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                      getWidth(context, 0.06),
                      getWidth(context, 0.4),
                      getWidth(context, 0.06),
                      getWidth(context, 0.06)),
                  child: Container(
                    color: bgColor(),
                    child: BlocBuilder<CRUDSipBloc, CRUDSipState>(
                      builder: (context, state) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            textstyle(Str().setting$practiceinfo),
                            if (postProceedData_listSIP.isEmpty)
                              CustomText(
                                  toppad: 0.02,
                                  botpad: 0.02,
                                  txt: Str().setting$addlicensenotes),
                            ListView.builder(
                              shrinkWrap: true,
                              itemCount: postProceedData_listSIP.length,
                              itemBuilder: (context, index) => infoPraktek(
                                "SIP. " +
                                    postProceedData_listSIP[index]["nosip"],
                                1,
                                true,
                                dataid: postProceedData_listSIP[index]
                                    ["dataid"],
                                spesialisasi: "Rawat Inap Penyakit Dalam",
                                alamat: postProceedData_listSIP[index]
                                    ["alamat"],
                              ),
                            ),
                            if (postProceedData_listSIP.isNotEmpty &&
                                    postProceedData_listSIP.length != 3 ||
                                postProceedData_listSIP.isEmpty)
                              addSipButton(),
                            textstyle(Str().setting$yourservice),
                            if (postProceedData_listSIP.isEmpty)
                              CustomText(
                                  toppad: 0.02,
                                  botpad: 0.02,
                                  txt:
                                      "Pengaturan layanan praktik anda akan muncul disini."),
                            ServicePracticeCard(
                              styleId: 1,
                            ),
                          ]),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  addSipButton() {
    return Center(
        child: CustomButton(
      styleId: 3,
      shapeId: 2,
      onPressed: () => Navigator.push(
          context, SlideRightRoute(page: SettingsServiceSipForm(callId: 1))),
      btnText: Str().setting$addlicense,
      prefixIcon: Icons.add,
      fColor: defaultcolor1,
    ));
  }

  infoPraktek(String nosip, int callId, bool onVerificated,
      {@required int? dataid, String? spesialisasi, String? alamat}) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.012)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          border: Border.all(width: 1, color: Color.fromARGB(50, 0, 0, 0))),
      padding: EdgeInsets.all(getWidth(context, 0.02)),
      child: ExpandablePanel(
          theme: ExpandableThemeData(iconColor: defaultcolor2),
          header: Row(
            children: [
              CustomText(
                  txt: nosip, size: 0.035, color: fontColor1(), bold: true),
              if (onVerificated == false)
                Padding(
                  padding: EdgeInsets.only(left: getWidth(context, 0.02)),
                  child: CustomText(
                      txt: Str().setting$verificationprocess,
                      bold: true,
                      size: 0.035,
                      color: defaultcolor1),
                ),
            ],
          ),
          collapsed: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (spesialisasi != null)
                CustomText(txt: spesialisasi, color: fontColor1()),
              if (alamat != null) CustomText(txt: alamat, color: fontColor1()),
            ],
          ),
          expanded: expanded1Content(nosip, spesialisasi, alamat, callId,
              dataid: dataid, onVerificated: onVerificated)),
    );
  }

  expanded1Content(
      String nosip, String? spesialisasi, String? alamat, int callId,
      {@required int? dataid, bool? onVerificated}) {
    row(int sipNum) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
                txt: sipNum == 1 ? Str().specialization : Str().address,
                size: 0.036,
                bold: true,
                color: fontColor1()),
            CustomText(
                txt: sipNum == 1 ? spesialisasi! : alamat!,
                size: 0.035,
                color: fontColor1()),
          ],
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (spesialisasi != null) row(1),
        if (alamat != null) row(2),
        if (onVerificated == null || onVerificated == true)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomButton(
                  styleId: 3,
                  shapeId: 2,
                  width: 0.55,
                  btnText: Str().setting$changeaddress,
                  onPressed: () => Navigator.push(
                      context,
                      SlideRightRoute(
                          page: SettingsServiceSipForm(
                        callId: 2,
                        noSip: nosip,
                        dataid: dataid,
                        alamat: alamat,
                      )))),
              CustomButton(
                  styleId: 3,
                  shapeId: 2,
                  width: 0.26,
                  bColor: Color.fromARGB(100, 255, 0, 0),
                  fColor: Colors.white,
                  prefixIcon: Icons.delete_outline_outlined,
                  btnText: Str().delete,
                  onPressed: () async {
                    CustomAlertDialog(context,
                        contentText: Str().setting$deleteconfirmdialog,
                        txtButton1: Str().delete,
                        txtButton2: Str().cancel,
                        funcButton1: () => context
                            .read<CRUDSipBloc>()
                            .add(DeleteSIP(context, dataid: dataid)));
                  }),
            ],
          ),
      ],
    );
  }

  textstyle(String txt) {
    return Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.05)),
      child: CustomText(
        txt: txt,
        bold: true,
        color: defaultcolor1,
        size: 0.037,
      ),
    );
  }
}
