import 'package:obatin/importPackage.dart';

class HomeSideMenu extends StatefulWidget {
  final bool? loadNewData;
  final String? idpengguna;
  final String? idnakes;
  HomeSideMenu({this.loadNewData, this.idpengguna, this.idnakes});
  @override
  _HomeSideMenuState createState() => _HomeSideMenuState();
}

class _HomeSideMenuState extends State<HomeSideMenu> {
  ScrollController scrollController = new ScrollController();
  bool showSideMenu = false;

  @override
  void initState() {
    super.initState();
    loadNewData();
  }

  loadNewData() {
    if (widget.loadNewData != null && widget.loadNewData == true) {
      // GET ADDRESS USER & SAVE LOCALLY ADDRESS DATA
      context.read<SipSikBloc>().add(GetSipSik(context,
          idpengguna: widget.idpengguna, idnakes: widget.idnakes));
    }
  }

  slideFunc() {
    setState(() {
      showSideMenu = !showSideMenu;
    });
    scrollController.animateTo(
        showSideMenu ? scrollController.position.maxScrollExtent : 0,
        duration: Duration(milliseconds: 500),
        curve: Curves.fastOutSlowIn);
  }

  logoutFunc() {
    CustomAlertDialog(context,
        title: "Log Out",
        contentText: "Lanjutkan Log Out?",
        txtButton1: "Log Out",
        txtButton2: "Batal", funcButton1: () {
      context.read<SQFLITEUserDataBloc>().add(DeleteUserData());
      context.read<SQFLITESipSikBloc>().add(DeleteSipSikData(context));
      goToPage(context, pushReplace: true, page: LoginMain());
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
        builder: (context, darkmodeState) => Scaffold(
                body: AnimatedContainer(
              duration: Duration(milliseconds: 300),
              decoration: BoxDecoration(
                color: themeID == 1 ? defaultcolor1 : defaultcolor1Darken,
              ),
              child: SingleChildScrollView(
                physics: NeverScrollableScrollPhysics(),
                controller: scrollController,
                scrollDirection: Axis.horizontal,
                child: Row(children: [
                  SizedBox(
                      width: getWidth(context, 1),
                      height: getHeight(context, 1),
                      child: Stack(
                        children: [
                          HomeMain(
                            sideButtonFunc: () => slideFunc(),
                          ),
                          if (showSideMenu == true)
                            GestureDetector(
                              onTap: () {
                                slideFunc();
                              },
                            )
                        ],
                      )),
                  Transform.translate(
                    offset: Offset(-2, 0),
                    child: AnimatedContainer(
                      duration: Duration(milliseconds: 300),
                      width: getWidth(context, 0.04),
                      height: getHeight(context, 1),
                      decoration: BoxDecoration(
                          color: bgColor(),
                          borderRadius: BorderRadius.only(
                              topRight: Radius.circular(getWidth(context, 0.1)),
                              bottomRight:
                                  Radius.circular(getWidth(context, 0.1)))),
                    ),
                  ),
                  SizedBox(
                      width: getWidth(context, 0.5),
                      height: getHeight(context, 1),
                      child: Stack(
                        children: [
                          Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                sideButton(
                                    func: () => context
                                        .read<SettingsSwitchBloc>()
                                        .add(SwitchThemeMode()),
                                    txt: themeID == 1
                                        ? Str().lightmode
                                        : Str().darkmode,
                                    iconColor: themeID == 1
                                        ? Colors.yellow
                                        : Colors.blue.shade200,
                                    icon: themeID == 1
                                        ? Icons.light_mode_outlined
                                        : Icons.dark_mode_outlined),
                                sideButton(
                                    func: () => context
                                        .read<SettingsSwitchBloc>()
                                        .add(SwitchLang()),
                                    txt: langID == 1 ? "Indonesia" : "English",
                                    iconColor: Colors.green,
                                    icon: Icons.language_outlined),
                                sideButton(
                                    func: () => logoutFunc(),
                                    txt: "Log Out",
                                    iconColor: Colors.red,
                                    icon: Icons.logout_outlined),
                              ],
                            ),
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: CustomText(
                              botpad: 0.02,
                              txt: "OBAT-in Heroes v0.0.1 - Beta",
                              color: Colors.white,
                              size: 0.03,
                            ),
                          ),
                        ],
                      )),
                ]),
              ),
            )));
  }

  sideButton(
      {@required VoidCallback? func,
      String? txt,
      IconData? icon,
      Color? iconColor}) {
    return CustomButton(
      mainAxisAlignment: MainAxisAlignment.start,
      bColor: Colors.transparent,
      height: 0.12,
      iconhorpad: 0.02,
      styleId: 1,
      iColor: iconColor != null ? iconColor : null,
      onPressed: () => func!(),
      btnText: txt!,
      prefixIcon: icon!,
    );
  }
}
