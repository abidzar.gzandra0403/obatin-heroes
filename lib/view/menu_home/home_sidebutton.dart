import 'dart:async';

import 'package:another_transformer_page_view/another_transformer_page_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/main.dart';
import 'package:obatin/models/system_models/settingData.dart';
import 'package:obatin/models/system_models/stringDatabase.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/login_page/login_main.dart';
import 'package:obatin/view/menu_home/home_main.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';

// IndexController bannerController = new IndexController();
// Timer? bannerTimer;

class HomeSideButton extends StatefulWidget {
  final double? xoffsetEnd;
  HomeSideButton({
    @required this.xoffsetEnd,
  });
  @override
  _HomeSideButtonState createState() => _HomeSideButtonState();
}

class _HomeSideButtonState extends State<HomeSideButton>
    with TickerProviderStateMixin {
  late AnimationController xOffsetAnim;
  late Animation<double> xOffsetTween;
  bool showSideMenu = false;

  xoffsetAnimate() {
    xOffsetAnim = new AnimationController(
      duration: Duration(milliseconds: 300),
      vsync: this,
    )..addListener(() => setState(() {}));
    xOffsetTween = Tween(begin: 0.0, end: widget.xoffsetEnd)
        .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
  }

  showSideBtnFunc() {
    if (showSideMenu == false) {
      xOffsetAnim.forward();
    } else {
      xOffsetAnim.reverse();
    }
    setState(() {
      showSideMenu = !showSideMenu;
    });
  }

  logoutFunc() {
    // bannerTimer!.cancel();
    Navigator.pushReplacement(context, SlideRightRoute(page: LoginMain()));
  }

  @override
  void initState() {
    super.initState();
    xoffsetAnimate();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
          body: Stack(
        children: [
          AnimatedContainer(
            duration: Duration(milliseconds: 300),
            color: themeID == 1 ? defaultcolor1 : defaultcolor1Darken,
          ),
          Align(
            alignment: Alignment.centerRight,
            child: GestureDetector(
              onTap: () => showSideBtnFunc(),
              child: Container(
                color: Colors.transparent,
                width: getWidth(context, 0.5),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    _sideButton(
                        func: () => context
                            .read<SettingsSwitchBloc>()
                            .add(SwitchThemeMode()),
                        txt: themeID == 1 ? Str().lightmode : Str().darkmode,
                        iconColor:
                            themeID == 1 ? Colors.yellow : Colors.blue.shade200,
                        icon: themeID == 1
                            ? Icons.light_mode_outlined
                            : Icons.dark_mode_outlined),
                    _sideButton(
                        func: () => context
                            .read<SettingsSwitchBloc>()
                            .add(SwitchLang()),
                        txt: langID == 1 ? "Indonesia" : "English",
                        iconColor: Colors.lightGreen,
                        icon: Icons.language_outlined),
                    _sideButton(
                        func: () => logoutFunc(),
                        txt: "Log Out",
                        iconColor: Colors.red,
                        icon: Icons.logout_outlined),
                  ],
                ),
              ),
            ),
          ),
          Container(
            width: getWidth(context, 1),
            child: Transform.translate(
              offset:
                  new Offset(xOffsetTween.value, 0.0), // info.width * -position
              child: AnimatedContainer(
                duration: Duration(milliseconds: 300),
                decoration: BoxDecoration(
                    color: bgColor(),
                    borderRadius: BorderRadius.circular(
                        showSideMenu ? getWidth(context, 0.05) : 0)),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(getWidth(context, 0.05)),
                  child: HomeMain(
                    sideButtonFunc: () => showSideBtnFunc(),
                  ),
                ),
              ),
            ),
          ),
          if (showSideMenu == true)
            Align(
              alignment: Alignment.centerLeft,
              child: GestureDetector(
                onTap: () => showSideBtnFunc(),
                child: Container(
                  color: Colors.transparent,
                  width: getWidth(context, 0.5),
                  height: getHeight(context, 1),
                ),
              ),
            ),
        ],
      )),
    );
  }

  _sideButton(
      {@required VoidCallback? func,
      String? txt,
      IconData? icon,
      Color? iconColor}) {
    return CustomButton(
      mainAxisAlignment: MainAxisAlignment.start,
      bColor: Colors.transparent,
      height: 0.12,
      iconhorpad: 0.02,
      styleId: 1,
      iColor: iconColor != null ? iconColor : null,
      onPressed: () => func!(),
      btnText: txt!,
      prefixIcon: icon!,
    );
  }
}
