import 'package:obatin/importPackage.dart';

homeAppBar(BuildContext context, {@required VoidCallback? moreFunc}) {
  loadName() {
    getUserDataName() async {
      if (postProceedData_userData.length < 1) {
        var userName = '';
        return userName;
      } else {
        var userName = await postProceedData_userData[0]['nama']!;
        return userName;
      }
    }

    return FutureBuilder(
        future: getUserDataName(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return CustomText(
              txt: Str().hi + ", " + snapshot.data,
              bold: true,
              size: 0.037,
            );
          } else {
            return CustomText(
              txt: Str().hi + ", . . .",
              bold: true,
              size: 0.037,
            );
          }
        });
  }

  return BlocBuilder<SQFLITEUserDataBloc, SQFLITEUserDataState>(
    builder: (context, darkmodeState) => Container(
      padding: EdgeInsets.only(
          left: getWidth(context, 0.05),
          right: getWidth(context, 0.03),
          top: getWidth(context, 0.1),
          bottom: getWidth(context, 0.02)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              goToPage(context, page: KenalinMain());
            },
            child: Row(
              children: [
                CircleAvatar(
                    backgroundColor: Colors.blue.shade50,
                    radius: getWidth(context, 0.045),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(
                        getWidth(context, 1),
                      ),
                      child: ImgAsset.profilePhotoExample,
                    )),
                Container(
                  width: getWidth(context, 0.5),
                  margin: EdgeInsets.only(left: getWidth(context, 0.02)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      loadName(),
                      CustomText(
                        txt: "Spesialis Jantung",
                        size: 0.034,
                        color: fontColor2(),
                      ),
                      Row(
                        children: [
                          Icon(
                            ObatinIconpack.star_rating,
                            color: Colors.yellow,
                            size: getWidth(context, 0.03),
                          ),
                          CustomText(
                            txt: "4.5",
                            leftpad: 0.01,
                            size: 0.028,
                            bold: true,
                            color: Colors.grey.shade400,
                          )
                        ],
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Row(
            children: [
              CustomButton(
                  width: 0.12,
                  styleId: 4,
                  shapeId: 4,
                  prefixIcon: Icons.notifications_none_outlined,
                  isize: 0.07,
                  iColor: defaultcolor2,
                  onPressed: () {
                    Navigator.push(
                        context, SlideRightRoute(page: NotificationMain()));
                  }),
              CustomButton(
                  width: 0.12,
                  styleId: 4,
                  shapeId: 4,
                  prefixIcon: Icons.menu,
                  isize: 0.07,
                  iColor: defaultcolor2,
                  onPressed: () => moreFunc!())
            ],
          )
        ],
      ),
    ),
  );
}
