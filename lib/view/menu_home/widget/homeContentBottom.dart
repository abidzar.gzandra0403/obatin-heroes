import 'package:obatin/importPackage.dart';

class HomeContentBottom extends StatefulWidget {
  const HomeContentBottom({Key? key}) : super(key: key);

  @override
  _HomeContentBottomState createState() => _HomeContentBottomState();
}

class _HomeContentBottomState extends State<HomeContentBottom> {
  @override
  void initState() {
    super.initState();
    getFromSharedPreferences();
  }

  String nullDesc = Str().nullDesc;
  getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    String initDesc = prefs.getString("deskripsi") != null
        ? prefs.getString("deskripsi")!
        : '';
    setState(() {
      homedeskripsi = initDesc;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SQFLITESipSikBloc, SQFLITESipSikState>(
      builder: (context, state) => Padding(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          children: [
            homedeskripsi.isEmpty
                ? GestureDetector(
                    onTap: () =>
                        goToPage(context, page: ProfileChangeDescription()),
                    child: _homeDescriptionCard(context, nullDesc),
                  )
                : _homeDescriptionCard(context, homedeskripsi),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    SizedBox(
                      width: getWidth(context,
                          postProceedData_listSIP.length < 2 ? 0.49 : 0.49),
                      height: getWidth(context, 0.42),
                      child: postProceedData_listSIP.isEmpty
                          ? addSipBtn(
                              context,
                              width: 0.55,
                            )
                          : Column(
                              children: [
                                SizedBox(
                                    height: getWidth(
                                        context,
                                        postProceedData_listSIP.length == 1
                                            ? 0.42
                                            : 0.28),
                                    child: listSIP()),
                                if (postProceedData_listSIP.length == 2)
                                  addSipBtn(context,
                                      width: 0.55,
                                      height: 0.12,
                                      axisDirection: Axis.horizontal),
                                if (postProceedData_listSIP.length == 3)
                                  _bottomButton(context,
                                      svgName: svg_iconsip3,
                                      btnId: 1,
                                      vermargin: 0.01,
                                      width: 0.55,
                                      axisDirection: Axis.horizontal,
                                      height: 0.12,
                                      title: Str().practice + " 3",
                                      color1: praktik3Orange1,
                                      color2: praktik3Orange2,
                                      callId: 2,
                                      sip: postProceedData_listSIP[2]["nosip"])
                              ],
                            ),
                    ),
                  ],
                ),
                _bottomButton(
                  context,
                  btnId: 2,
                  width: 0.36,
                  title: "SOBAT-in",
                  imgWidget: ImgAsset.imgSobatin,
                  color1: Color.fromARGB(255, 73, 166, 214),
                  color2: Color.fromARGB(255, 77, 129, 240),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  btnFunc(BuildContext context, {int? btnId, int? callId}) {
    if (btnId == 0) {
      Navigator.push(
          context,
          SlideRightRoute(
              page: ProfileServiceSipForm(
            callId: 1,
          )));
    } else if (btnId == 1) {
      CustomBottomSheet(context,
          content: ServicePracticeCard(
            showOnlySipId: callId,
            switchToggleOnly: true,
            styleId: 2,
          ));
    } else {
      CustomAlertDialog(context,
          contentText: "Anda Akan Dialihkan ke menu SOBAT-in",
          txtButton1: "Lanjut",
          txtButton2: "Batal",
          funcButton1: () => goToPage(context, page: SobatinHomeMain()));
    }
  }

  _bottomButton(BuildContext context,
      {@required String? title,
      @required int? btnId,
      String? svgName,
      @required Color? color1,
      @required Color? color2,
      Widget? imgWidget,
      Axis? axisDirection,
      double? iconSize,
      String? sip,
      int? callId,
      double? width,
      double? height,
      double? vermargin}) {
    return Container(
      margin: EdgeInsets.symmetric(
          vertical: getWidth(context, vermargin != null ? vermargin : 0)),
      width: getWidth(
          context,
          width != null
              ? width
              : postProceedData_listSIP.length < 2
                  ? 0.55
                  : 0.36),
      height: getWidth(context, height != null ? height : 0.4),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            color1!,
            color2!,
          ],
        ),
        borderRadius: BorderRadius.circular(getWidth(context, 0.025)),
      ),
      child: RawMaterialButton(
          onPressed: () => btnFunc(context, btnId: btnId, callId: callId),
          fillColor: Colors.transparent,
          highlightColor: Color.fromARGB(50, 0, 183, 255),
          elevation: 0,
          highlightElevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(getWidth(context, 0.05)),
          ),
          child: Center(
            child: ListView(
              padding: EdgeInsets.all(0),
              physics: NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              scrollDirection: axisDirection != null
                  ? axisDirection
                  : btnId == 1
                      ? (postProceedData_listSIP.length < 3
                          ? Axis.vertical
                          : Axis.vertical)
                      : Axis.vertical,
              children: [
                svgName != null
                    ? CustomSVG(
                        vermargin: 0.02,
                        svgName: svgName,
                        size: iconSize != null
                            ? iconSize
                            : btnId == 1
                                ? (postProceedData_listSIP.length < 3
                                    ? 0.12
                                    : 0.09)
                                : 0.15,
                        color: Colors.white,
                      )
                    : Container(
                        height: getWidth(context, btnId == 1 ? 0.1 : 0.18),
                        margin: EdgeInsets.only(
                            bottom: btnId == 1 ? 0 : getWidth(context, 0.02)),
                        child: imgWidget,
                      ),
                CustomText(
                  textAlign: TextAlign.center,
                  txt: title,
                  bold: true,
                  color: Colors.white,
                  size: btnId == 1 ? 0.03 : 0.033,
                )
              ],
            ),
          )),
    );
  }

  addSipBtn(BuildContext context,
      {@required double? width,
      Axis? axisDirection,
      double? iconSize,
      double? height}) {
    int listSip =
        postProceedData_listSIP != null ? postProceedData_listSIP.length : 0;
    return _bottomButton(context,
        axisDirection: axisDirection != null ? axisDirection : null,
        vermargin: 0.01,
        width: width,
        height: height != null ? height : 0.4,
        title: Str().addpracticelicense,
        btnId: 0,
        iconSize: iconSize,
        svgName: svg_iconpracticesetting,
        color1: listSip == 0
            ? praktik1Red1
            : listSip == 1
                ? praktik2Green1
                : praktik3Orange1,
        color2: listSip == 0
            ? praktik1Red2
            : listSip == 1
                ? praktik2Green2
                : praktik3Orange2);
  }

  listSIP() {
    return ListView.builder(
        scrollDirection: postProceedData_listSIP.length < 3
            ? Axis.horizontal
            : Axis.horizontal,
        padding: EdgeInsets.all(0),
        itemCount: postProceedData_listSIP.length != 3
            ? postProceedData_listSIP.length
            : 2,
        shrinkWrap: true,
        physics: NeverScrollableScrollPhysics(),
        itemBuilder: (context, index) {
          return Row(
            children: [
              Padding(
                  padding: EdgeInsets.only(
                      right: getWidth(context, index == 0 ? 0.03 : 0),
                      bottom: postProceedData_listSIP.length >= 2
                          ? getWidth(context, 0.01)
                          : 0),
                  child: _bottomButton(context,
                      btnId: 1,
                      vermargin: 0.01,
                      width: postProceedData_listSIP.length == 1 ? 0.27 : 0.23,
                      svgName: postProceedData_listSIP.length == 1
                          ? svg_iconsip
                          : (index == 0 ? svg_iconsip1 : svg_iconsip2),
                      title: postProceedData_listSIP.length == 1
                          ? Str().practice
                          : Str().practice + " " + (index + 1).toString(),
                      color1: index == 0 ? praktik1Red1 : praktik2Green1,
                      color2: index == 0 ? praktik1Red2 : praktik2Green2,
                      callId: index,
                      sip: postProceedData_listSIP[index]["nosip"])),
              if (postProceedData_listSIP.length < 2)
                addSipBtn(context, width: 0.19, iconSize: 0.1),
            ],
          );
        });
  }

  _homeDescriptionCard(BuildContext context, String txt2) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.only(
            bottom: getWidth(context, 0.02), top: getWidth(context, 0.01)),
        padding: EdgeInsets.all(getWidth(context, 0.04)),
        decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.bottomLeft,
              end: Alignment.topRight,
              colors: [descriptionBlue1, descriptionBlue2],
            ),
            // color: defaultcolor3,
            borderRadius: BorderRadius.circular(getWidth(context, 0.03))),
        child: txt2.isEmpty
            ? Center(
                child: CustomText(
                txt: "Tambahkan Deskripsi",
                prefixIcon: Icons.add,
                color: Colors.white,
                iconColor: Colors.white,
              ))
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      CustomText(
                        txt: Str().description,
                        bold: true,
                        color: Colors.white,
                      ),
                      CustomButton(
                        height: 0.06,
                        width: 0.1,
                        styleId: 4,
                        iColor: Colors.white,
                        isize: 0.04,
                        onPressed: () =>
                            goToPage(context, page: ProfileChangeDescription()),
                        prefixIcon: Icons.edit_outlined,
                      )
                    ],
                  ),
                  setHSpacing(getWidth(context, 0.01)),
                  CustomText(
                    txt: txt2,
                    size: 0.032,
                    color: Colors.white,
                  )
                ],
              ));
  }
}
