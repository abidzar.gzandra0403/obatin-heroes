import 'package:obatin/importPackage.dart';

class HomeContentMiddle extends StatefulWidget {
  HomeContentMiddle({Key? key}) : super(key: key);

  @override
  _HomeContentMiddleState createState() => _HomeContentMiddleState();
}

class _HomeContentMiddleState extends State<HomeContentMiddle> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        HomeBanner(),
        Container(
          width: getWidth(context, 0.4),
          height: getWidth(context, 0.72),
          padding: EdgeInsets.only(
              left: getWidth(context, 0.03), right: getWidth(context, 0.01)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _menuButton(context, Str().activity,
                  svgName: svg_iconactivity, page: ActivityMain()),
              _menuButton(context, Str().inbox,
                  svgName: svg_iconinbox, page: InboxMain()),
              _menuButton(context, Str().profile,
                  svgName: svg_iconprofile, page: ProfileMain()),
              _menuButton(context, Str().support,
                  svgName: svg_iconsupport, page: SupportMain()),
            ],
          ),
        )
      ],
    );
  }

  _menuButton(BuildContext context, String btnTitle,
      {Widget? page, VoidCallback? func, @required String? svgName}) {
    return CustomButton(
      prefixWidget: CustomSVG(
        svgName: svgName!,
        hormargin: 0.01,
        color: Color.fromARGB(190, 74, 111, 135),
      ),
      styleId: 3,
      shapeId: 2,
      height: 0.16,
      iconhorpad: 0.02,
      isize: 0.05,
      fColor: themeID == 1 ? Colors.black45 : defaultcolor3,
      bColor: Color.fromARGB(10, 62, 114, 148),
      mainAxisAlignment: MainAxisAlignment.start,
      onPressed: () {
        page != null ? goToPage(context, page: page) : func!();
      },
      btnText: btnTitle,
    );
  }
}
