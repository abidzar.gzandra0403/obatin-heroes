import 'package:obatin/importPackage.dart';

class HomeContentTop extends StatelessWidget {
  final String lorem =
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, state) => Column(
        children: [
          Padding(
            padding: EdgeInsets.symmetric(
                vertical: getWidth(context, 0.01),
                horizontal: defaultPaddingContent(context)),
            child: eWalletWidget(context),
          ),
        ],
      ),
    );
  }
}
