import 'package:obatin/importPackage.dart';

class HomeMain extends StatefulWidget {
  final VoidCallback? sideButtonFunc;
  HomeMain({@required this.sideButtonFunc});
  @override
  _HomeMainState createState() => _HomeMainState();
}

class _HomeMainState extends State<HomeMain> {
  ScrollController homeScroll = ScrollController();
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness:
              themeID == 2 ? Brightness.light : Brightness.dark),
      child: BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
          builder: (context, darkmodeState) => AnimatedContainer(
              duration: Duration(milliseconds: 300),
              height: getHeight(context, 1),
              color: bgColor(),
              child: _homepagedokter())),
    );
  }

  _homepagedokter() {
    return Stack(children: [
      Padding(
        padding: EdgeInsets.only(top: getWidth(context, 0.02)),
        child: Column(
          children: [
            homeAppBar(context, moreFunc: () => widget.sideButtonFunc!()),
            Expanded(
              child: SingleChildScrollView(
                controller: homeScroll,
                padding: EdgeInsets.only(
                  bottom: getWidth(context, 0.02),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    HomeContentTop(),
                    HomeContentMiddle(),
                    HomeContentBottom()
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ]);
  }
}
