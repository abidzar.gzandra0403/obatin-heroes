import 'package:obatin/importPackage.dart';

class EmptyPage extends StatefulWidget {
  @override
  _EmptyPageState createState() => _EmptyPageState();
}

class _EmptyPageState extends State<EmptyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      body: Center(
          child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.gavel_sharp,
            size: getWidth(context, 0.2),
            color: Colors.grey,
          ),
          CustomText(
              txt: "On Working", size: 0.04, color: Colors.grey, bold: true)
        ],
      )),
    );
  }
}
