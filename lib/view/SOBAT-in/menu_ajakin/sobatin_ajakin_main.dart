import 'package:obatin/importPackage.dart';

class SobatinAjakinMain extends StatefulWidget {
  @override
  _SobatinAjakinMainState createState() => _SobatinAjakinMainState();
}

class _SobatinAjakinMainState extends State<SobatinAjakinMain> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: CustomAppBar(context, "", true, backgroundColor: Colors.white),
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                child: ImgAsset.imgAjakinBanner,
              ),
              CustomButton(
                styleId: 1,
                width: 0.5,
                height: 0.1,
                onPressed: () {},
                btnText: "AJAK-in",
              )
            ],
          ),
        ));
  }
}
