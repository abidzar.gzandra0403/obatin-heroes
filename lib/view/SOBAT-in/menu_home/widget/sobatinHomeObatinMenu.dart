import 'package:obatin/importPackage.dart';

sobatinHomeObatinMenu(BuildContext context) {
  pageRoute({@required Widget? page}) {
    goToPage(context, page: page!);
  }

  obatinButton(String txt, String? svgName, int btnId) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topCenter,
          end: Alignment.bottomCenter,
          colors: [
            btnId == 1
                ? Color.fromARGB(255, 73, 166, 214)
                : Color.fromARGB(255, 139, 191, 79),
            btnId == 1
                ? Color.fromARGB(255, 77, 129, 240)
                : Color.fromARGB(255, 57, 190, 105),
          ],
        ),
        borderRadius: BorderRadius.circular(getWidth(context, 0.05)),
      ),
      child: RawMaterialButton(
          onPressed: () {
            pageRoute(page: btnId == 1 ? SobatinAjakinMain() : EmptyPage());
          },
          padding: EdgeInsets.all(getWidth(context, 0.02)),
          fillColor: Colors.transparent,
          highlightColor: Color.fromARGB(50, 0, 183, 255),
          elevation: 0,
          highlightElevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(getWidth(context, 0.05)),
          ),
          child: Align(
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                CustomSVG(
                  vermargin: 0.02,
                  svgName: svgName,
                  size: 0.15,
                  color: Colors.white,
                ),
                CustomText(
                  txt: txt,
                  bold: true,
                  color: Colors.white,
                  size: 0.028,
                )
              ],
            ),
          )),
    );
  }

  return Container(
      width: double.infinity,
      child: GridView.count(
        mainAxisSpacing: getWidth(context, 0.05),
        crossAxisSpacing: getWidth(context, 0.05),
        crossAxisCount: 2,
        childAspectRatio: 1,
        physics: NeverScrollableScrollPhysics(),
        shrinkWrap: true,
        children: [
          obatinButton("AJAK-in", svg_iconajakin, 1),
          obatinButton("SHARE-in", svg_iconsharein, 2),
        ],
      ));
}
