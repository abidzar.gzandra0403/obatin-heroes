import 'package:obatin/importPackage.dart';

sobatinHomepageHeader(BuildContext context) {
  return Container(
      width: getWidth(context, 1),
      // height: getWidth(context, 0.26),
      margin: EdgeInsets.only(top: getWidth(context, 0.1), bottom: 10),
      padding: EdgeInsets.only(top: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Text("dr. Abidzar Ghifari Zandra",
                    //     style: hnnormal(
                    //         txtSize: getWidth(context, 0.034),
                    //         txtWeight: FontWeight.w700,
                    //         spacing: 1)),
                    // Text("SIP. " + "17101152610001",
                    //     style: hnnormal(
                    //         txtSize: getWidth(context, 0.03),
                    //         txtWeight: FontWeight.w500,
                    //         spacing: 1)),
                    // SizedBox(height: 5),
                    Text(Str().welcome,
                        style: hnnormal(context,
                            txtSize: getWidth(context, 0.05),
                            txtWeight: FontWeight.w500,
                            txtColor: Colors.black54)),
                  ],
                ),
              ),
              // Container(
              //   width: getWidth(context, 0.1),
              //   height: getWidth(context, 0.1),
              //   decoration: BoxDecoration(
              //     shape: BoxShape.circle,
              //     border: Border.all(
              //       color: Colors.black,
              //       width: 0.50,
              //     ),
              //   ),
              //   child: Icon(Icons.person),
              // ),
            ],
          ),
          setHSpacing(
            getWidth(context, 0.03),
          ),
          Container(
              width: double.infinity,
              height: 40,
              padding: const EdgeInsets.all(0),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: lightgrey,
              ),
              child: Container(
                width: 100,
                child: TextField(
                  style: hnnormal(context, txtSize: 20),
                  decoration: InputDecoration(
                      contentPadding: EdgeInsets.all(5),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                      prefixIcon: Icon(
                        Icons.search,
                        color: Colors.black38,
                        size: getWidth(context, 0.045),
                      )),
                ),
              )),
        ],
      ));
}
