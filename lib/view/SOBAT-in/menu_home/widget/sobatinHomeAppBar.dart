import 'package:obatin/importPackage.dart';

sobatinHomeAppBar(BuildContext context, {@required String? userName}) {
  return Container(
    padding: EdgeInsets.only(
        left: getWidth(context, 0.05),
        right: getWidth(context, 0.03),
        top: getWidth(context, 0.1),
        bottom: getWidth(context, 0.02)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            CircleAvatar(
                backgroundColor: Colors.blue.shade50,
                radius: getWidth(context, 0.045),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                    getWidth(context, 1),
                  ),
                  child: ImgAsset.profilePhotoExample,
                )),
            Container(
              width: getWidth(context, 0.5),
              margin: EdgeInsets.only(left: getWidth(context, 0.02)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    txt: userName!,
                    bold: true,
                    size: 0.037,
                  ),
                  CustomText(
                    txt: Str().welcome,
                    size: 0.034,
                    color: fontColor2(),
                  ),
                ],
              ),
            ),
          ],
        ),
        Row(
          children: [
            CustomButton(
                width: 0.12,
                styleId: 4,
                shapeId: 4,
                prefixIcon: Icons.notifications_none_outlined,
                isize: 0.07,
                iColor: defaultcolor2,
                onPressed: () {
                  Navigator.push(
                      context, SlideRightRoute(page: NotificationMain()));
                }),
          ],
        )
      ],
    ),
  );
}
