// import 'dart:async';

// import 'package:another_transformer_page_view/another_transformer_page_view.dart';
// import 'package:flutter/material.dart';
// import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
// import 'package:flutter_bloc/flutter_bloc.dart';
// import 'package:obatin/main.dart';
// import 'package:obatin/view/SOBAT-in/menu_home/sobatin_home_main.dart';
// import 'package:obatin/view/common/colorpalette.dart';
// import 'package:obatin/view/common/defaultvalue.dart';
// import 'package:obatin/view/common/transitionanimation.dart';
// import 'package:obatin/view/login_page/login_main.dart';
// import 'package:obatin/view/menu_home/home_main.dart';
// import 'package:obatin/view/common_widget/customWidget/customButton.dart';

// IndexController bannerController = new IndexController();
// Timer? bannerTimer;

// class SobatinHomeSideButton extends StatefulWidget {
//   final double? xoffsetEnd;
//   SobatinHomeSideButton({
//     @required this.xoffsetEnd,
//   });
//   @override
//   _SobatinHomeSideButtonState createState() => _SobatinHomeSideButtonState();
// }

// class _SobatinHomeSideButtonState extends State<SobatinHomeSideButton>
//     with TickerProviderStateMixin {
//   late AnimationController xOffsetAnim;
//   late Animation<double> xOffsetTween;
//   bool showSideMenu = false;

//   xoffsetAnimate() {
//     xOffsetAnim = new AnimationController(
//       duration: Duration(milliseconds: 300),
//       vsync: this,
//     )..addListener(() => setState(() {}));
//     xOffsetTween = Tween(begin: 0.0, end: widget.xoffsetEnd)
//         .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
//   }

//   // scaleAnimate() {
//   //   scaleAnim = new AnimationController(
//   //     duration: Duration(milliseconds: 500),
//   //     vsync: this,
//   //   )..addListener(() => setState(() {}));
//   //   scaleTween = Tween(begin: 1.0, end: 0.8)
//   //       .animate(new CurvedAnimation(parent: xOffsetAnim, curve: Curves.ease));
//   // }

//   showSideBtnFunc() {
//     if (showSideMenu == false) {
//       xOffsetAnim.forward();
//       // scaleAnim.forward();
//     } else {
//       xOffsetAnim.reverse();
//       // scaleAnim.reverse();
//     }
//     setState(() {
//       showSideMenu = !showSideMenu;
//     });
//   }

//   void slideshowTimer() {
//     bannerTimer =
//         Timer.periodic(Duration(seconds: 5), (_) => bannerController.next());
//   }

//   logoutFunc() {
//     bannerTimer!.cancel();
//     Navigator.pushReplacement(context, SlideRightRoute(page: LoginMain()));
//   }

//   @override
//   void initState() {
//     super.initState();
//     xoffsetAnimate();
//     // scaleAnimate();
//     slideshowTimer();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
//       builder: (context, darkmodeState) => Scaffold(
//           body: Stack(
//         children: [
//           AnimatedContainer(
//             duration: Duration(milliseconds: 300),
//             color: nightmode ? defaultcolor1Darken : defaultcolor1,
//           ),
//           Align(
//             alignment: Alignment.centerRight,
//             child: GestureDetector(
//               onTap: () => showSideBtnFunc(),
//               child: Container(
//                 color: Colors.transparent,
//                 width: getWidth(context, 0.5),
//                 child: Column(
//                   mainAxisAlignment: MainAxisAlignment.center,
//                   children: [
//                     sideButton(
//                         func: () => context
//                             .read<SettingsSwitchBloc>()
//                             .add(SwitchThemeMode()),
//                         txt: nightmode ? "Mode Terang" : "Mode Gelap",
//                         iconColor:
//                             nightmode ? Colors.yellow : Colors.blue.shade200,
//                         icon: nightmode
//                             ? Icons.light_mode_outlined
//                             : Icons.dark_mode_outlined),
//                     sideButton(
//                         func: () => context
//                             .read<SettingsSwitchBloc>()
//                             .add(SwitchLang()),
//                         txt: indLang ? "Bahasa Indonesia" : "English Language",
//                         iconColor: Colors.green,
//                         icon: Icons.language_outlined),
//                     sideButton(
//                         func: () => logoutFunc(),
//                         txt: "Log Out",
//                         iconColor: Colors.red,
//                         icon: Icons.logout_outlined),
//                   ],
//                 ),
//               ),
//             ),
//           ),
//           Container(
//             width: getWidth(context, 1),
//             child: Transform.translate(
//               offset:
//                   new Offset(xOffsetTween.value, 0.0), // info.width * -position
//               child: AnimatedContainer(
//                 duration: Duration(milliseconds: 300),
//                 decoration: BoxDecoration(
//                     color: bgColor(),
//                     borderRadius: BorderRadius.circular(
//                         showSideMenu ? getWidth(context, 0.1) : 0)),
//                 child: ClipRRect(
//                   borderRadius: BorderRadius.circular(getWidth(context, 0.1)),
//                   child: SobatinHomeMain(
//                     sideButtonFunc: () => showSideBtnFunc(),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           if (showSideMenu == true)
//             Align(
//               alignment: Alignment.centerLeft,
//               child: GestureDetector(
//                 onTap: () => showSideBtnFunc(),
//                 child: Container(
//                   color: Colors.transparent,
//                   width: getWidth(context, 0.6),
//                   height: getHeight(context, 1),
//                 ),
//               ),
//             ),
//         ],
//       )),
//     );
//   }

//   sideButton(
//       {@required VoidCallback? func,
//       String? txt,
//       IconData? icon,
//       Color? iconColor}) {
//     return CustomButton(
//       mainAxisAlignment: MainAxisAlignment.start,
//       bColor: Colors.transparent,
//       height: 0.12,
//       iconhorpad: 0.02,
//       styleId: 1,
//       iColor: iconColor != null ? iconColor : null,
//       onPressed: () => func!(),
//       btnText: txt!,
//       prefixIcon: icon!,
//     );
//   }
// }
