import 'package:obatin/importPackage.dart';

class SobatinHomeMain extends StatefulWidget {
  @override
  _SobatinHomeMainState createState() => _SobatinHomeMainState();
}

class _SobatinHomeMainState extends State<SobatinHomeMain> {
  ScrollController homeScroll = ScrollController();
  IndexController bannerController = new IndexController();
  Timer? bannerTimer;
  void slideshowTimer() {
    bannerTimer =
        Timer.periodic(Duration(seconds: 5), (_) => bannerController.next());
  }

  @override
  void initState() {
    super.initState();
    slideshowTimer();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        body: homepagedokter(),
      ),
    );
  }

  homepagedokter() {
    String lorem =
        "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    OutlineInputBorder borderInput = OutlineInputBorder(
        borderSide: BorderSide(width: 2, color: Colors.white),
        borderRadius: BorderRadius.circular(getWidth(context, 0.1)));

    return Stack(children: [
      Padding(
        padding: EdgeInsets.only(top: getWidth(context, 0.02)),
        child: Column(
          children: [
            sobatinHomeAppBar(
              context,
              userName: postProceedData_userData[0]['nama'],
            ),
            Expanded(
              child: SingleChildScrollView(
                controller: homeScroll,
                padding: EdgeInsets.only(
                  bottom: getWidth(context, 0.02),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    setHSpacing(getWidth(context, 0.02)),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          vertical: getWidth(context, 0.01),
                          horizontal: defaultPaddingContent(context)),
                      child: eWalletWidget(context),
                    ),
                    Row(
                      children: [
                        HomeBanner(),
                        Container(
                          width: getWidth(context, 0.4),
                          height: getWidth(context, 0.72),
                          padding: EdgeInsets.only(
                            left: getWidth(context, 0.03),
                            right: getWidth(context, 0.01),
                          ),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              menuButton(Str().profile,
                                  icon: Icons.list_alt_outlined,
                                  page: SobatinProfilMain()),
                              menuButton(Str().activity,
                                  icon: Icons.chat_outlined,
                                  page: SobatinActivityMain()),
                              menuButton(Str().support,
                                  icon: Icons.help_outline,
                                  page: SobatinSupportMain()),
                            ],
                          ),
                        )
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                          horizontal: getWidth(context, 0.06),
                          vertical: getWidth(context, 0.02)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomText(
                            txt: Str().features,
                            bold: true,
                            leftpad: 0.02,
                          ),
                          sobatinHomeObatinMenu(context),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    ]);
  }

  menuButton(String btnTitle,
      {Widget? page, VoidCallback? func, @required IconData? icon}) {
    return CustomButton(
      prefixIcon: icon,
      styleId: 3,
      shapeId: 2,
      height: 0.22,
      iconhorpad: 0.02,
      isize: 0.05,
      fColor: themeID == 1 ? defaultcolor1 : defaultcolor3,
      bColor: Color.fromARGB(20, 0, 162, 255),
      mainAxisAlignment: MainAxisAlignment.start,
      onPressed: () {
        page != null ? goToPage(context, page: page) : func!();
      },
      btnText: btnTitle,
    );
  }
}
