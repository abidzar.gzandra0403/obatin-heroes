import 'package:obatin/importPackage.dart';

class SobatinActivityMain extends StatefulWidget {
  @override
  _SobatinActivityMainState createState() => _SobatinActivityMainState();
}

class _SobatinActivityMainState extends State<SobatinActivityMain> {
  bool allowreview = false;
  settingsBtn(
      {@required String? title,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
        styleId: 5,
        verpad: 0.02,
        btnText: title,
        btnText2: info != null ? info : null,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        horpad: 0.03,
        prefixWidget: CustomSVG(
          svgName: svgName,
          color: defaultcolor3,
          size: 0.06,
        ),
        onPressed: () {
          if (gotopage != null) {
            goToPage(context, page: gotopage);
          } else {
            func!();
          }
        });
  }

  ajakin_Btn(
      {@required String? title,
      @required int? callId,
      @required int? btnId,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
      styleId: 5,
      verpad: 0.02,
      btnText: title,
      btnText2: info != null ? info : null,
      suffixWidget: suffixWidget != null ? suffixWidget : null,
      horpad: 0.03,
      prefixWidget: CustomSVG(
        svgName: svgName,
        color: defaultcolor3,
        size: 0.06,
      ),
      onPressed: () => Navigator.push(
          context,
          SlideRightRoute(
              page: SobatinActivityContentList(
                  title: title,
                  callId: callId,
                  bottomWidget: bottomWidgetajakin(btnId: btnId)))),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, Str().activity$Title, true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.only(top: getWidth(context, 0.05)),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: defaultPaddingContent(context)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                                txt: Str().activity$TitleCashback,
                                bold: true,
                                color: Colors.grey),
                            settingsBtn(
                                title: Str().activity$ProductCashbackList,
                                info: Str().activity$ProductCashbackListDesc,
                                svgName: svg_listproduct,
                                gotopage: EmptyPage()),
                            settingsBtn(
                                title: Str().activity$serviceCashbackList,
                                info: Str().activity$serviceCashbackListDesc,
                                svgName: svg_iconedittitle,
                                gotopage: Cashback_list()),
                            CustomText(
                                txt: Str().activity$Ajakinlist,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            ajakin_Btn(
                                title: Str().activity$HealthProducts,
                                callId: 3,
                                svgName: svg_iconmedicalproduct,
                                info: Str().activity$HealthProductsDesc,
                                btnId: 1),
                            ajakin_Btn(
                                title: Str().activity$PlusDoctor,
                                info: Str().activity$HealthProductsDesc,
                                callId: 3,
                                svgName: svg_iconlistdoctor,
                                btnId: 2),
                            ajakin_Btn(
                                title: Str().activity$PlusNakes,
                                svgName: svg_iconnurse,
                                info: Str().activity$HealthProductsDesc,
                                callId: 3,
                                btnId: 3),
                            ajakin_Btn(
                                title: Str().activity$PlusFasyankes,
                                callId: 3,
                                svgName: svg_iconhospital,
                                info: Str().activity$HealthProductsDesc,
                                btnId: 4),
                            CustomText(
                                txt: Str().activity$Shareinlist,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            ajakin_Btn(
                                title: Str().beingserved,
                                info: Str().beingserveddesc,
                                svgName: svg_iconbeingserved,
                                callId: 3,
                                btnId: 1),
                            ajakin_Btn(
                                title: Str().newshare,
                                info: Str().newsharedesc,
                                svgName: svg_iconsharein,
                                callId: 2,
                                btnId: 1),
                            ajakin_Btn(
                                title: Str().waitingpayment,
                                info: Str().waitingscheduleDesc,
                                svgName: svg_iconwallet,
                                callId: 4,
                                btnId: 3),
                            ajakin_Btn(
                                title: Str().waitingschedule,
                                info: Str().waitingpaymentDesc,
                                svgName: svg_iconwaitingschedule,
                                callId: 4,
                                btnId: 2),
                            ajakin_Btn(
                                title: Str().reschedule,
                                info: Str().rescheduleDesc,
                                svgName: svg_iconreschedule,
                                callId: 4,
                                btnId: 4),
                            ajakin_Btn(
                                title: Str().ordercomplete,
                                info: Str().ordercompleteDesc,
                                svgName: svg_iconordercomplete,
                                callId: 4,
                                btnId: 5),
                            ajakin_Btn(
                                title: Str().ordercanceled,
                                info: Str().ordercanceledDesc,
                                svgName: svg_iconcancel,
                                callId: 4,
                                btnId: 8),
                            ajakin_Btn(
                                title: Str().ordercomplain,
                                info: Str().ordercomplainDesc,
                                svgName: svg_iconcompaint,
                                callId: 4,
                                btnId: 7),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bottomWidget({@required int? btnId}) {
    String txt = btnId == 4
        ? Str().changeschedule + ":" + " Selasa, 15 Juli 2021 10:00 WIB"
        : btnId == 5
            ? "INV/0023/TC/01052022"
            : btnId == 7
                ? Str().complaint + ": " + " Kamis, 01 Mei 2022 07:50 WIB"
                : btnId == 8
                    ? Str().cancellation + ": " + "Senin, 05 Mei 2022 12:20 WIB"
                    : btnId == 9
                        ? Str().referto +
                            " " +
                            "dr. Amal Sepanjang Hayat, Sp.BS"
                        : btnId == 10
                            ? Str().referfrom + " " + "dr. Ridho Ilahi, Sp.B"
                            : "";
    Color color = btnId == 4
        ? Colors.green
        : btnId == 5
            ? defaultcolor2
            : btnId == 7
                ? Colors.orange
                : btnId == 8
                    ? Colors.red
                    : btnId == 9
                        ? Colors.lightBlue
                        : btnId == 10
                            ? Colors.green
                            : defaultcolor3;
    return txt == ""
        ? SizedBox()
        : Container(
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.02),
                vertical: getWidth(context, 0.01)),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(
                  getWidth(context, 0.03),
                ),
                color: color),
            child: CustomText(
              txt: txt,
              size: 0.028,
              color: Colors.white,
            ),
          );
  }

  bottomWidgetajakin({@required int? btnId}) {
    String txt = btnId == 1
        ? "Jl. Setia Budi No 11, Jakarta Timur"
        : btnId == 2
            ? "STR.33.1.1.100.3.16.041458"
            : btnId == 3
                ? "STR. 33.1.1.100.3.16.041458"
                : btnId == 4
                    ? "Jl. Setia Budi No 11, Jakarta Timur"
                    : "";

    return txt == ""
        ? SizedBox()
        : Row(
            children: [
              rowText(
                  icon: btnId == 1
                      ? Icons.map
                      : btnId == 2
                          ? ObatinIconpack.identity
                          : btnId == 3
                              ? ObatinIconpack.identity
                              : Icons.map,
                  clockString: txt,
                  color: defaultcolor1),
            ],
          );
  }

  rowText({IconData? icon, String? clockString, Color? color}) {
    return SizedBox(
      height: getWidth(context, 0.040),
      child: Row(
        children: [
          Icon(icon, size: getWidth(context, 0.03), color: color),
          setWSpacing(5),
          if (clockString != null)
            CustomText(
              txt: clockString,
              size: 0.03,
            ),
        ],
      ),
    );
  }
}
