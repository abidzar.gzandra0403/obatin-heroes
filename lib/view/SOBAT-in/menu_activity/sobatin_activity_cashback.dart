import 'package:obatin/importPackage.dart';

class Cashback_list extends StatefulWidget {
  @override
  _Cashback_listState createState() => _Cashback_listState();
}

class _Cashback_listState extends State<Cashback_list> {
  bool animated = false;
  DateTimeRange? myDateRange;
  GlobalKey<FormState> myFormKey = new GlobalKey();
  Timer? animateTime;
  @override
  void initState() {
    animateTime = Timer.periodic(Duration(milliseconds: 300), (_) {
      setState(() {
        animated = !animated;
        animateTime!.cancel();
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: gopayBlueColor2(),
      appBar: CustomAppBar(
        context,
        "Cashback",
        true,
        fontColor: Colors.white,
        brightness: Brightness.dark,
        backgroundColor: gopayBlueColor2(),
      ),
      body: Stack(
        children: [
          cashbackHeader(context, animated: animated),
          Container(
            height: getHeight(context, 1),
            margin: EdgeInsets.only(top: getWidth(context, 0.3)),
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context),
                vertical: getWidth(context, 0.05)),
            decoration: BoxDecoration(
                color: bgColor(),
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(getWidth(context, 0.08)),
                    topRight: Radius.circular(getWidth(context, 0.08)))),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                              toppad: 0.02,
                              botpad: 0.02,
                              txt: Str().activity$cashbackdesc,
                              bold: true,
                              color: Colors.grey,
                            ),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              itemBuilder: (BuildContext context, int index) =>
                                  cashbackTransactionCard(
                                      context,
                                      "Dr. Budianto",
                                      "Layanan Visit Pasien ",
                                      "30.000",
                                      transactionType: 1),
                            ),
                            ListView(
                              shrinkWrap: true,
                              children: [],
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  btnFunc() {
    return CustomBottomDialog(context, txt: "On Working");
  }

  _buttonSettings(
      {Widget? page,
      VoidCallback? func,
      @required String? title,
      IconData? icon}) {
    return CustomButton(
        styleId: 5,
        btnText: title!,
        bColor: btnColor(),
        onPressed: () {
          func != null ? func() : goToPage(context, page: page!);
        });
  }
}
