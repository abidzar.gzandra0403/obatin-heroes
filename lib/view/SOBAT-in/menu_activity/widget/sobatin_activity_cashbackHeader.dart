import 'package:obatin/importPackage.dart';

cashbackHeader(BuildContext context, {@required bool? animated}) {
  return Container(
    color: gopayBlueColor2(),
    height: getWidth(context, 0.6),
    width: getWidth(context, 1),
    child: Stack(
      children: [
        Align(
          alignment: Alignment.center,
          child: Transform.translate(
            offset: Offset(0, -getWidth(context, 0.2)),
            child: CustomText(
              txt: "Rp. 455.000,-",
              color: Colors.white,
              bold: true,
              size: 0.09,
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Transform.translate(
              offset: Offset(-getWidth(context, 0.15), getWidth(context, 0)),
              child: AnimatedContainer(
                duration: Duration(milliseconds: 1400),
                curve: Curves.ease,
                margin: EdgeInsets.only(top: animated! ? 0 : 10, right: 0),
                child: CircleAvatar(
                  radius: getWidth(context, 0.1),
                  backgroundColor: Colors.white12,
                ),
              )),
        ),
        Align(
          alignment: Alignment.centerRight,
          child: Transform.translate(
            offset: Offset(getWidth(context, 0.13), getWidth(context, 0)),
            child: AnimatedContainer(
              duration: Duration(milliseconds: 1400),
              curve: Curves.ease,
              margin: EdgeInsets.only(top: animated ? 50 : 50, right: 0),
              child: CircleAvatar(
                radius: getWidth(context, 0.23),
                backgroundColor: Colors.white12,
              ),
            ),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: Transform.translate(
            offset: Offset(getWidth(context, 0.15), getWidth(context, 0)),
            child: CircleAvatar(
              radius: getWidth(context, 0.09),
              backgroundColor: Colors.white12,
            ),
          ),
        )
      ],
    ),
  );
}
