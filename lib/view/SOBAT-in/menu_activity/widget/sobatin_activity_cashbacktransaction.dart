import 'package:obatin/importPackage.dart';

cashbackTransactionCard(
    BuildContext context, String dokter, String title, String cost,
    {@required int? transactionType}) {
  return CustomButton(
    verpad: 0.01,
    vermargin: 0.02,
    styleId: 6,
    bColor: btnColor(),
    onPressed: () {},
    prefixWidget: Row(
      children: [
        Container(
          height: getWidth(context, 0.1),
          width: getWidth(context, 0.15),
          child: Center(
              child: CustomSVG(
            svgName: svg_gopaytopup,
            color: defaultcolor2,
            size: 0.07,
          )),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(txt: "Dari " + dokter, bold: true),
              SizedBox(
                height: getWidth(context, 0.01),
              ),
              SizedBox(
                width: getWidth(context, 0.7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    CustomText(txt: title, bold: false),
                    CustomText(
                      txt: "Rp." + cost + ",-",
                      bold: true,
                    )
                  ],
                ),
              ),
              SizedBox(
                height: getWidth(context, 0.01),
              ),
              CustomText(
                txt: "20 September 2021",
              ),
            ],
          ),
        )
      ],
    ),
  );
}
