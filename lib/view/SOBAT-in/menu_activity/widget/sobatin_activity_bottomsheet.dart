import 'package:obatin/importPackage.dart';

sobatinActivityBottomSheet(BuildContext context,
    {@required String? title,
    @required int? callId,
    @required String? name,
    @required String? order,
    @required int? serviceId,
    String? noreg}) {
  return CustomBottomSheet(
    context,
    content: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Center(
          child: Container(
            height: getWidth(context, 0.02),
            width: 40,
            margin: EdgeInsets.only(bottom: 10),
            decoration: BoxDecoration(
                color: Colors.black12, borderRadius: BorderRadius.circular(50)),
          ),
        ),
        Center(child: CustomText(txt: title, bold: true, size: 0.04)),
        setHSpacing(getWidth(context, 0.04)),
        Container(
          // height: getWidth(context, 0.5),
          child: Row(
            children: [
              CircleAvatar(
                child: Icon(Icons.person, color: Colors.white),
                radius: getWidth(context, 0.12),
                backgroundColor: defaultcolor3,
              ),
              Container(
                width: getWidth(context, 0.52),
                padding: EdgeInsets.only(left: getWidth(context, 0.025)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      txt: name,
                      size: 0.05,
                      bold: true,
                    ),
                    // initialServiceRequest(context, serviceId!),

                    customerInfo(context, ObatinIconpack.identity,
                        "1502110404990001", iconcoloridentity),
                    Divider(
                      height: getWidth(
                        context,
                        0.03,
                      ),
                      color: Colors.black,
                    ),
                    customerInfo(context, ObatinIconpack.gender, "Pria",
                        iconcolorgenderred),
                    customerInfo(context, ObatinIconpack.ages, "25 Tahun",
                        iconcolorageyellow),
                  ],
                ),
              ),
            ],
          ),
        ),
        setHSpacing(getWidth(context, 0.03)),
      ],
    ),
  );
}

customerInfo(BuildContext context, IconData icon, String text, Color color) {
  return SizedBox(
    height: getWidth(context, 0.043),
    child: Row(
      children: [
        SizedBox(
          width: getWidth(context, 0.03),
          child: Icon(
            icon,
            size: getWidth(context, 0.032),
            color: color,
          ),
        ),
        setWSpacing(10),
        CustomText(
          leftpad: 0.01,
          txt: text,
          size: 0.03,
        )
      ],
    ),
  );
}

detailinformationCustomer(
    BuildContext context, String txt1, String txt2, bool detailIcon,
    {IconData? icon, String? icontxt}) {
  return Padding(
    padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        CustomText(txt: txt1, size: 0.035, bold: true),
        CustomText(txt: txt2, size: 0.035),
        if (detailIcon == true)
          customerInfo(context, ObatinIconpack.distancepoint,
              "2.4 Km dari lokasi anda", iconcolordistance)
      ],
    ),
  );
}
