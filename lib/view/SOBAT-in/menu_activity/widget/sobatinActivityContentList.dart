import 'package:obatin/importPackage.dart';

contentList(
  BuildContext context, {
  @required String? title,
  @required int? callId,
  @required int? serviceId,
  String? noreg,
  String? patientName,
  String? clock,
  Widget? bottomWidget,
}) {
  rowText({IconData? icon, String? clockString, Color? color}) {
    return SizedBox(
      height: getWidth(context, 0.04),
      child: Row(
        children: [
          Icon(icon, size: getWidth(context, 0.03), color: color),
          setWSpacing(5),
          if (clockString != null)
            CustomText(
              txt: clockString,
              size: 0.03,
            ),
        ],
      ),
    );
  }

  return Padding(
    padding: const EdgeInsets.only(bottom: 10),
    child: RawMaterialButton(
      onPressed: () {
        sobatinActivityBottomSheet(context,
            name: patientName,
            title: title,
            callId: callId,
            noreg: noreg,
            order: clock,
            serviceId: serviceId);
      },
      fillColor: btnColor(),
      highlightColor: Colors.blue.shade50,
      elevation: 0,
      highlightElevation: 0,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
          side: BorderSide(width: 1, color: Color.fromARGB(50, 0, 0, 0))),
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(getWidth(context, 0.02)),
            child: Row(
              children: [
                CircleAvatar(
                  backgroundColor: btnBlueColor(),
                  radius: getWidth(context, 0.08),
                  child: Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                ),
                Expanded(
                    child: Padding(
                  padding: const EdgeInsets.all(8),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                          txt: patientName != null ? patientName : "",
                          bold: true,
                          size: 0.042),
                      if (noreg != null)
                        CustomText(
                          txt: "No Reg. " + noreg,
                          size: 0.03,
                        ),
                      bottomWidget != null ? bottomWidget : SizedBox(),
                    ],
                  ),
                )),
              ],
            ),
          )
        ],
      ),
    ),
  );
}
