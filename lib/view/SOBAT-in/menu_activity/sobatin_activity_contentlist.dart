import 'package:obatin/importPackage.dart';

class SobatinActivityContentList extends StatefulWidget {
  final String? title;
  final int? callId;
  final Widget? bottomWidget;
  SobatinActivityContentList(
      {@required this.title, @required this.callId, this.bottomWidget});
  @override
  _SobatinActivityContentListState createState() =>
      _SobatinActivityContentListState();
}

class _SobatinActivityContentListState
    extends State<SobatinActivityContentList> {
  List listdata = [
    "Abidzar",
    "Wahyu",
    "Anam",
    "Wahyu",
  ];

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, widget.title!, true),
        body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              children: [
                Container(
                  height: getWidth(context, 0.1),
                  margin: EdgeInsets.only(bottom: getWidth(context, 0.08)),
                  child: Row(
                    children: [
                      Expanded(
                        child: SizedBox(
                            height: getWidth(context, 0.1),
                            child: textFieldStyle1(context, Str().searchpartner,
                                icons: Icons.search)),
                      ),
                      setWSpacing(getWidth(context, 0.02)),
                      CustomButton(
                          styleId: 1,
                          height: 0.1,
                          shapeId: 2,
                          bold: true,
                          btnText: "Filter",
                          onPressed: () {})
                    ],
                  ),
                ),
                Expanded(child: list(listdata)),
              ],
            )),
      ),
    );
  }

  list(List list) {
    return Container(
      // height: getWidth(context, 0.2 * list.length),
      // color: Colors.amber,
      child: ListView.builder(
          padding: EdgeInsets.all(0),
          // physics: NeverScrollableScrollPhysics(),
          itemCount: list.length,
          itemBuilder: (BuildContext context, int index) {
            return contentList(context,
                title: widget.title,
                callId: widget.callId,
                patientName: list[index],
                serviceId: 1,
                bottomWidget: widget.bottomWidget);
          }),
    );
  }
}
