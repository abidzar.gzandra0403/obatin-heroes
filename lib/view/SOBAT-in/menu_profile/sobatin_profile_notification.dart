import 'package:obatin/importPackage.dart';

class SobatinProfileNotification extends StatefulWidget {
  @override
  _SobatinProfileNotificationState createState() =>
      _SobatinProfileNotificationState();
}

class _SobatinProfileNotificationState
    extends State<SobatinProfileNotification> {
  bool notifObatin = false;
  bool notifPromo = false;
  bool notifUpdateFitur = false;
  switchFunc(int btnId) {
    setState(() {
      if (btnId == 1) {
        notifObatin = !notifObatin;
      } else if (btnId == 2) {
        notifPromo = !notifPromo;
      } else {
        notifUpdateFitur = !notifUpdateFitur;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().setting$notification, true),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
            horizontal: defaultPaddingContent(context),
            vertical: getWidth(context, 0.04)),
        child: Column(
          children: [
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(1),
                verpad: 0.02,
                btnText: Str().setting$obatinnotif,
                btnText2: Str().setting$obatinnotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(1), value: notifObatin)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(2),
                verpad: 0.02,
                btnText: Str().setting$promonotif,
                btnText2: Str().profile$notivpromo,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(2), value: notifPromo)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(3),
                verpad: 0.02,
                btnText: Str().profil$FeatureUpdateNotification,
                btnText2: Str().profile$FeatureUpdateNotivicationDesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(3), value: notifUpdateFitur))
          ],
        ),
      ),
    );
  }
}
