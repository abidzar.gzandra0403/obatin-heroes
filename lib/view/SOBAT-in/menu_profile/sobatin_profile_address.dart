import 'package:obatin/importPackage.dart';

class SobatinProfileAddress extends StatefulWidget {
  @override
  _SobatinProfileAddressState createState() => _SobatinProfileAddressState();
}

class _SobatinProfileAddressState extends State<SobatinProfileAddress> {
  btnFunc(int btnId) {
    if (btnId == 1) {
      goToPage(context, page: EmptyPage());
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().profile$Address, true),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              children: [
                SizedBox(
                  width: getWidth(context, 1),
                  height: getWidth(context, 0.4),
                  child: ImgAsset.imgSettings,
                ),
                setHSpacing(getWidth(context, 0.02)),
                ListView(
                  shrinkWrap: true,
                  children: [
                    sobatinprofileAddressCard(context, "Ahmad Khoirul Anam",
                        "Ds. Kertomulyo RT 5 RW 1, Kec. Margoyoso Kab. Pati, Jawa tengah, 59154",
                        primary: true),
                    sobatinprofileAddressCard(context, "Ahmad Khoirul Anam",
                        "Ds. Kertomulyo RT 5 RW 1, Kec. Margoyoso Kab. Pati, Jawa tengah, 59154"),
                  ],
                ),
                CustomButton(
                  styleId: 3,
                  shapeId: 2,
                  // height: 0.1,
                  // vermargin: 0.02,
                  // width: 0.1,
                  onPressed: () => Navigator.push(
                      context,
                      SlideRightRoute(
                          page: SobatinProfileAddressAdd(callID: 1))),
                  prefixIcon: Icons.add_outlined,
                  btnText: "Tambah Alamat",
                  isize: 0.05,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
