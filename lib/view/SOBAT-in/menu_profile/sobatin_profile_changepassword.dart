import 'package:obatin/importPackage.dart';

class SobatinProfileChangePassword extends StatefulWidget {
  @override
  _SobatinProfileChangePasswordState createState() =>
      _SobatinProfileChangePasswordState();
}

class _SobatinProfileChangePasswordState
    extends State<SobatinProfileChangePassword> {
  TextEditingController txtBoxPassword = new TextEditingController();
  TextEditingController txtBoxConfirmPassword = new TextEditingController();
  handleBack() {
    CustomAlertDialog(context,
        title: Str().setting$cancelchangepassworddialog,
        txtButton1: Str().yes,
        txtButton2: Str().cancel, funcButton1: () {
      txtBoxPassword.text = "";
      txtBoxConfirmPassword.text = "";
      Navigator.pop(context);
    });
  }

  confirmFunc() {
    CustomAlertDialog(context,
        title: Str().setting$changepassword,
        contentText: Str().setting$changepassworddialogdesc,
        txtButton1: Str().sure,
        txtButton2: Str().cancel, funcButton1: () {
      txtBoxPassword.text = "";
      txtBoxConfirmPassword.text = "";
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => handleBack(),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().setting$changepassword, true,
            customBackFunc: () => handleBack()),
        body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Stack(
              children: [
                Column(
                  children: [
                    CustomTextField(
                      hint: Str().profile$oldpassword,
                    ),
                    passwordFieldValidator(
                        context, Str().newpassword, txtBoxPassword),
                    passwordFieldValidator(context, Str().newpasswordconfirm,
                        txtBoxConfirmPassword,
                        txtId: 2, targetTxtbox: txtBoxPassword)
                  ],
                ),
                Align(
                  alignment: Alignment.bottomCenter,
                  child: CustomButton(
                    vermargin: 0.04,
                    height: 0.1,
                    styleId: 1,
                    onPressed: () => confirmFunc(),
                    btnText: Str().savechanges,
                  ),
                )
              ],
            )),
      ),
    );
  }
}
