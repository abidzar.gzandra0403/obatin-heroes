import 'package:obatin/importPackage.dart';

class SobatinProfileSharein extends StatefulWidget {
  @override
  _SobatinProfileShareinState createState() => _SobatinProfileShareinState();
}

class _SobatinProfileShareinState extends State<SobatinProfileSharein> {
  String btnText = "SHARE-in";
  String btnText2 = "Ajak-in";
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, "SHARE-in", true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Column(
                  children: [
                    tile(
                        title: Str().profile$sharecommission,
                        // subtitle: Str().profile$passiveincomesub,
                        expanded: true,
                        content: [
                          rowText(Str().profile$sharedesc1),
                          rowText(Str().profile$sharedesc2),
                          rowText(Str().profile$sharedesc3),
                          rowText(Str().profile$sharedesc4),
                          rowText(Str().profile$sharedesc5),
                          btn(btnText)
                        ]),
                    tile(
                        title: Str().profile$charity,
                        expanded: true,
                        content: [
                          rowText(Str().profile$sharecharitydesc1),
                          rowText(Str().profile$sharecharitydesc2),
                          rowText(Str().profile$sharecharitydesc3),
                          rowText(Str().profile$sharecharitydesc4),
                          rowText(Str().profile$sharecharitydesc5),
                          btn(btnText),
                          rowText(Str().profile$sharecharitydesc6),
                          btn(btnText2)
                        ]),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  tile({
    @required String? title,
    @required bool? expanded,
    @required List<Widget>? content,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          initiallyExpanded: expanded!,
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.01), 0,
              getWidth(context, 0.01), getWidth(context, 0.02)),
          children: content!,
        ),
      ),
    );
  }

  btn(String btnText) {
    return Padding(
      padding: const EdgeInsets.only(left: 40, right: 40),
      child: CustomButton(
        styleId: 1,
        width: 0.9,
        height: 0.1,
        vermargin: 0.03,
        btnText: btnText,
        alignment: Alignment.bottomCenter,
        onPressed: () {
          setState(() {
            btnText = "Kirim";
          });
          // nextFunc();
        },
      ),
    );
  }

  rowText(String txt2) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, bottom: 5),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: "•",
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }
}
