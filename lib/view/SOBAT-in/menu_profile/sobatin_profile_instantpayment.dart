import 'package:obatin/importPackage.dart';

class SobatinProfileInstantPayment extends StatefulWidget {
  @override
  _SobatinProfileInstantPaymentState createState() =>
      _SobatinProfileInstantPaymentState();
}

class _SobatinProfileInstantPaymentState
    extends State<SobatinProfileInstantPayment> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, "Pembayaran Instan", true),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          children: [
            CustomExpansionTile(title: "Transfer Bank", children: [
              CustomButton(
                styleId: 5,
                onPressed: () {},
                btnText: "BRI",
              ),
              CustomButton(
                styleId: 5,
                onPressed: () {},
                btnText: "BNI",
              )
            ]),
            CustomExpansionTile(
                title: "Kartu Kredit / Debit Online",
                children: [
                  CustomButton(
                    styleId: 5,
                    onPressed: () {},
                    btnText: "Kartu Kredit BRI",
                  ),
                  CustomButton(
                    styleId: 5,
                    onPressed: () {},
                    btnText: "Kartu Kredit Mandiri",
                  )
                ]),
            CustomExpansionTile(title: "BCA OneKlik", children: [
              CustomButton(
                styleId: 5,
                onPressed: () {},
                btnText: "Kartu BCA OneKlik",
              ),
            ]),
            CustomButton(
              styleId: 5,
              vermargin: 0.015,
              height: 0.135,
              bColor: btnColor(),
              onPressed: () {},
              btnText: "Alfamart",
            ),
            CustomButton(
              styleId: 5,
              vermargin: 0.015,
              height: 0.135,
              bColor: btnColor(),
              onPressed: () {},
              btnText: "Indomaret",
            ),
          ],
        ),
      ),
    );
  }
}
