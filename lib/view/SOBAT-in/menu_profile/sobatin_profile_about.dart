import 'package:obatin/importPackage.dart';

class SobatinProfileAbout extends StatefulWidget {
  @override
  _SobatinProfileAboutState createState() => _SobatinProfileAboutState();
}

class _SobatinProfileAboutState extends State<SobatinProfileAbout> {
  String btnText = "AJAK-in";
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, Str().profile$sobatadvantages, true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Column(
                  children: [
                    tile(
                        title: Str().profile$passiveincome,
                        subtitle: Str().profile$passiveincomesub,
                        expanded: true,
                        content: [
                          rowText(Str().profile$passiveincomedesc1),
                          rowText(Str().profile$passiveincomedesc2),
                          rowText(Str().profile$passiveincomedesc4),
                          rowText(Str().profile$passiveincomedesc4),
                          btn(btnText)
                        ]),
                    tile(
                        title: Str().profile$activeincome,
                        subtitle: Str().profile$activeincomesub,
                        expanded: false,
                        content: [
                          rowText(Str().profile$activeincomedesc1),
                          rowText(Str().profile$activeincomedesc2),
                          rowText(Str().profile$activeincomedesc3),
                          rowText(Str().profile$activeincomedesc4),
                          rowText(Str().profile$activeincomedesc5),
                        ]),
                    tile(
                        title: Str().profile$cashback,
                        subtitle: Str().profile$cashbacksub,
                        expanded: false,
                        content: [
                          rowText(Str().profile$cashbacksubdesc1),
                          rowText(Str().profile$cashbacksubdesc2),
                          rowText(Str().profile$cashbacksubdesc3),
                          rowText(Str().profile$cashbacksubdesc4),
                          rowText(Str().profile$cashbacksubdesc5),
                        ]),
                    tile(
                        title: Str().profile$charity,
                        subtitle: Str().profile$charitysub,
                        expanded: false,
                        content: [
                          rowText(Str().profile$charitydesc1),
                          rowText(Str().profile$charitydesc2),
                          rowText(Str().profile$charitydesc3),
                          rowText(Str().profile$charitydesc4),
                          rowText(Str().profile$charitydesc5),
                          rowText(Str().profile$charitydesc6),
                        ])
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  tile({
    @required String? title,
    @required String? subtitle,
    @required bool? expanded,
    @required List<Widget>? content,
  }) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          initiallyExpanded: expanded!,
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CustomText(
                txt: subtitle, bold: false, size: 0.038, color: fontColor1()),
          ),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: content!,
        ),
      ),
    );
  }

  btn(String btnText) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: CustomButton(
        styleId: 1,
        width: 0.9,
        height: 0.1,
        vermargin: 0.03,
        btnText: btnText,
        alignment: Alignment.bottomCenter,
        onPressed: () {
          setState(() {
            btnText = "Kirim";
          });
          // nextFunc();
        },
      ),
    );
  }

  rowText(String txt2) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, bottom: 5),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: "•",
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }
}
