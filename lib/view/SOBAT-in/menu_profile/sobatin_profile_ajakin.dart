import 'package:obatin/importPackage.dart';

class SobatinProfileAjakin extends StatefulWidget {
  @override
  _SobatinProfileAjakinState createState() => _SobatinProfileAjakinState();
}

class _SobatinProfileAjakinState extends State<SobatinProfileAjakin> {
  String btnText = "AJAK-in";
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, "AJAK-in", true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: getWidth(context, 0.12)),
                child: CustomText(
                    txt: Str().profile$ajakinsub,
                    bold: false,
                    size: 0.038,
                    color: fontColor1()),
              ),
              tile2(
                "Active Income",
              ),
            ],
          ),
        ),
      ),
    );
  }

  tile2(
    String title,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                rowText(Str().profile$ajakindesc1, "1"),
                rowText(Str().profile$ajakindesc2, "2"),
                rowText(Str().profile$ajakindesc3, "3"),
                rowText(Str().profile$ajakindesc4, "4"),
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: CustomButton(
                    styleId: 1,
                    width: 0.9,
                    height: 0.1,
                    vermargin: 0.03,
                    btnText: btnText,
                    alignment: Alignment.bottomCenter,
                    onPressed: () {},
                  ),
                )
              ],
            ),
          )),
    );
  }

  rowText(String txt2, String txt3) {
    return Padding(
      padding: const EdgeInsets.only(left: 60, right: 40, top: 10),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: txt3 + ".",
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }
}
