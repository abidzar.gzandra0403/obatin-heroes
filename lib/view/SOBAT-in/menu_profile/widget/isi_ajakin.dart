import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class isi_ajakin extends StatefulWidget {
  @override
  _isi_ajakinState createState() => _isi_ajakinState();
}

class _isi_ajakinState extends State<isi_ajakin> {
  String btnText = "AJAK-in";
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, "AJAK-in", true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: defaultPaddingContent(context)),
                child: CustomText(
                    txt:
                        "Ajak-ajak yang lain yuk buat bergabung, komisi nya lumayan loh",
                    bold: false,
                    size: 0.038,
                    color: fontColor1()),
              ),
              tile2("Active Income",
                  "Makin banyak pennghasilan tambahan dengan sedikit bekerja "),
            ],
          ),
        ),
      ),
    );
  }

  tile2(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: Container(
            child: Column(
              children: [
                rowText(
                    "Penghasilan tambahan (passive income) selama satu bulan penuh dari hasil AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes bergabung di OBAT-in",
                    "1"),
                rowText(
                    "Passive income berupa komisi sebesar 1% selama sebulan penuh dari setiap transaksi pihak yang berhasil di AJAK-in",
                    "2"),
                rowText(
                    "Sekali berhasil AJAK-in, untungnya sebulan penuh tanpa ngapa-ngapain lagi",
                    "3"),
                rowText(
                    "Yuk banyak-banyak AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter dan/atau Nakes dan/atau Fasyankes bergabung di OBAT-in.",
                    "4"),
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: CustomButton(
                    styleId: 1,
                    width: 0.9,
                    height: 0.1,
                    vermargin: 0.03,
                    btnText: btnText,
                    alignment: Alignment.bottomCenter,
                    onPressed: () {},
                  ),
                )
              ],
            ),
          )),
    );
  }

  rowText(String txt2, String txt3) {
    return Padding(
      padding: const EdgeInsets.only(left: 50, right: 50, top: 10),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: txt3,
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }
}
