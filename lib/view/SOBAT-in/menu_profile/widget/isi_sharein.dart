import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class isi_share extends StatefulWidget {
  @override
  _isi_shareState createState() => _isi_shareState();
}

class _isi_shareState extends State<isi_share> {
  String btnText = "SHARE-in";
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, "SHARE-in", true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: defaultPaddingContent(context)),
                child: CustomText(
                    txt: "Bisa dapat komisi, bisa juga untuk charity (amal)",
                    bold: false,
                    size: 0.038,
                    color: fontColor1()),
              ),
              SizedBox(
                height: getWidth(context, 0.01),
              ),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: defaultPaddingContent(context)),
                child: CustomText(txt: "KOMISI"),
              ),
              tile2("Active Income",
                  "Makin banyak pennghasilan tambahan dengan sedikit bekerja "),
              Padding(
                padding: EdgeInsets.symmetric(
                    horizontal: defaultPaddingContent(context)),
                child: CustomText(txt: "AMAL"),
              ),
              tile3("Active Income",
                  "Makin banyak pennghasilan tambahan dengan sedikit bekerja "),
            ],
          ),
        ),
      ),
    );
  }

  tile2(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: Container(
            child: Column(
              children: [
                rowText(
                    "Penghasilan tambahan (active income) dari hasil SHARE-in produk dan atau jasa Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes OBAT-in",
                    "1"),
                rowText(
                    "Active income berupa komisi 2% dari harga jual produk di OBAT-in yang berhasil dibeli melalui link yang SOBAT-in SHARE-in",
                    "2"),
                rowText(
                    "Active income berupa komisi 5% dari tarif jasa Dokter/Nakes/Fasyankes di OBAT-in yang berhasil “dibeli” melalui link yang SOBAT-in SHARE-in",
                    "3"),
                rowText(
                    "SHARE-in Produk dan/atau Jasa di OBAT-in ke WA atau sosmed mu sebanyak-banyaknya",
                    "4"),
                rowText(
                    "Makin sering SHARE-in, makin banyak peluang dapat tambahan penghasilan",
                    "5"),
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: CustomButton(
                    styleId: 1,
                    width: 0.9,
                    height: 0.1,
                    vermargin: 0.03,
                    btnText: btnText,
                    alignment: Alignment.bottomCenter,
                    onPressed: () {},
                  ),
                )
              ],
            ),
          )),
    );
  }

  tile3(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      child: Theme(
          data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
          child: Container(
            child: Column(
              children: [
                rowText(
                    "Membantu banyak orang yang tidak mampu disekitar SOBAT-in untuk berobat kepada Dokter/Nakes/Fasyankes melalui Layanan Sukarela (gratis tanpa dipungut biaya). SOBAT-in dapat memberitahu dan atau mendaftarkan pasien tersebut melalui akun SOBAT-in",
                    "1"),
                rowText(
                    "OBAT-in mempunyai Jenis Layanan Sukarela, Dokter/Nakes/ Fasyankes dapat mengaktifkan layanan ini",
                    "2"),
                rowText(
                    "SOBAT-in, SHARE-in Jenis layanan tersebut ke orang yang membutuhkan",
                    "3"),
                rowText(
                    "OBAT-in percaya diluar sana masih banyak Dokter/Nakes/ Fasyankes baik yang ingin beramal",
                    "4"),
                rowText(
                    "OBAT-in juga percaya, SOBAT-in mau membantu SHARE-in agar membentuk lingkaran amal yang tidak terputus",
                    "5"),
                rowText(
                    "AJAK-in lebih banyak Dokter/Nakes/ Fasyankes untuk bergabung, agar makin banyak Dokter/Nakes/ Fasyankes yang mengaktifkan Jenis Layanan Sukarela.",
                    "6"),
                Padding(
                  padding: const EdgeInsets.only(left: 50, right: 50),
                  child: CustomButton(
                    styleId: 1,
                    width: 0.9,
                    height: 0.1,
                    vermargin: 0.03,
                    btnText: btnText,
                    alignment: Alignment.bottomCenter,
                    onPressed: () {
                      // nextFunc();
                    },
                  ),
                )
              ],
            ),
          )),
    );
  }

  rowText(String txt2, String txt3) {
    return Padding(
      padding: const EdgeInsets.only(left: 50, right: 50, top: 10),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: txt3,
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }
}
