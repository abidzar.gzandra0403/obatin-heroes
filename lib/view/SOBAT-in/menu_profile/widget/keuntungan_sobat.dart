import 'dart:async';

import 'package:flutter/material.dart';
import 'package:obatin/models/system_models/serviceBool.dart';
import 'package:obatin/view/SOBAT-in/menu_home/sobatin_home_main.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

import 'package:obatin/view/menu_settings/settings_service.dart';

class keuntungan_sobat extends StatefulWidget {
  @override
  _keuntungan_sobatState createState() => _keuntungan_sobatState();
}

class _keuntungan_sobatState extends State<keuntungan_sobat> {
  String btnText = "AJAK-in";
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 25, right: 25),
      child: Column(
        children: [
          tile(
              "Passive Income",
              "Dapatkan penghasilan tambahan selama sebulan penuh tanpa bekerja",
              "Penghasilan tambahan (passive income) selama satu bulan penuh dari hasil AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes bergabung di OBAT-in",
              "Passive income berupa komisi sebesar 1% selama sebulan penuh dari setiap transaksi pihak yang berhasil di AJAK-in",
              "Sekali berhasil AJAK-in, untungnya sebulan penuh tanpa ngapa-ngapain lagi",
              "Yuk banyak-banyak AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter dan/atau Nakes dan/atau Fasyankes bergabung di OBAT-in"),
          tile2("Active Income",
              "Makin banyak pennghasilan tambahan dengan sedikit bekerja "),
          tile3("Cashback",
              "Pasti dapat cashback dari belanja produk dan jasa OBAT-in"),
          tile2("Charity", "Lakukan amal dengan mudah tanpa keluar uang")
        ],
      ),
    );
  }

  tile(
    String title,
    String subtitle,
    String text1,
    String text2,
    String text3,
    String text4,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          onExpansionChanged: (isExpanded) {
            Timer _timer;
            _timer = new Timer(const Duration(milliseconds: 300), () {
              if (isExpanded) {
                homeScroll.animateTo(homeScroll.position.maxScrollExtent,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut);
              }
            });
          },
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CustomText(
                txt: subtitle, bold: false, size: 0.038, color: fontColor1()),
          ),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20),
              child: Column(
                children: [
                  CustomText(
                      txt: text1,
                      bold: false,
                      size: 0.038,
                      color: fontColor1()),
                  CustomText(
                      txt: text2,
                      bold: false,
                      size: 0.038,
                      color: fontColor1()),
                  CustomText(
                      txt: text3,
                      bold: false,
                      size: 0.038,
                      color: fontColor1()),
                  CustomText(
                      txt: text4,
                      bold: false,
                      size: 0.038,
                      color: fontColor1()),
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: CustomButton(
                      styleId: 1,
                      width: 0.9,
                      height: 0.1,
                      vermargin: 0.03,
                      btnText: btnText,
                      alignment: Alignment.bottomCenter,
                      onPressed: () {
                        setState(() {
                          btnText = "Kirim";
                        });
                        // nextFunc();
                      },
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  tile2(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          onExpansionChanged: (isExpanded) {
            Timer _timer;
            _timer = new Timer(const Duration(milliseconds: 300), () {
              if (isExpanded) {
                homeScroll.animateTo(homeScroll.position.maxScrollExtent,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut);
              }
            });
          },
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CustomText(
                txt: subtitle, bold: false, size: 0.038, color: fontColor1()),
          ),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: [
            rowText(
                "Penghasilan tambahan (active income) dari hasil SHARE-in produk dan atau jasa Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes OBAT-in"),
            rowText(
                "Active income berupa komisi 2% dari harga jual produk di OBAT-in yang berhasil dibeli melalui link yang SOBAT-in SHARE-in"),
            rowText(
                "Active income berupa komisi 5% dari tarif jasa Dokter/Nakes/Fasyankes di OBAT-in yang berhasil “dibeli” melalui link yang SOBAT-in SHARE-in"),
            rowText(
                "SHARE-in Produk dan/atau Jasa di OBAT-in ke WA atau sosmed mu sebanyak-banyaknya"),
            rowText(
                "Makin sering SHARE-in, makin banyak peluang dapat tambahan penghasilan."),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: CustomButton(
                styleId: 1,
                width: 0.9,
                height: 0.1,
                vermargin: 0.03,
                btnText: btnText,
                alignment: Alignment.bottomCenter,
                onPressed: () {
                  setState(() {
                    btnText = "Kirim";
                  });
                  // nextFunc();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  tile3(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          onExpansionChanged: (isExpanded) {
            Timer _timer;
            _timer = new Timer(const Duration(milliseconds: 300), () {
              if (isExpanded) {
                homeScroll.animateTo(homeScroll.position.maxScrollExtent,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut);
              }
            });
          },
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CustomText(
                txt: subtitle, bold: false, size: 0.038, color: fontColor1()),
          ),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: [
            rowText(
                "Pasti dapat cashback dari setiap pembelian produk dan/atau jasa di OBAT-in untuk diri sendiri maupun orang lain melalui akun SOBAT-in"),
            rowText(
                "Cashback 2% dari harga jual produk Mitra Plus yang dibeli di OBAT-in"),
            rowText(
                "Cashback 5% dari tarif  jasa Dokter/Nakes/Fasyankes di OBAT-in, kecuali Layanan Sukarela"),
            rowText(
                "Cashback di dapatkan setelah barang diterima dan/atau jasa telah selesai dilayani tanpa komplain"),
            rowText(
                "Mari belanja produk dan/atau gunakan jasa Dokter/Nakes/Fasyankes di aplikasi OBAT-in dan dapatkan cashback nya"),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: CustomButton(
                styleId: 1,
                width: 0.9,
                height: 0.1,
                vermargin: 0.03,
                btnText: btnText,
                alignment: Alignment.bottomCenter,
                onPressed: () {
                  setState(() {
                    btnText = "Kirim";
                  });
                  // nextFunc();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  tile4(
    String title,
    String subtitle,
  ) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.015)),
      decoration: BoxDecoration(
          color: btnColor(),
          borderRadius: BorderRadius.circular(10),
          boxShadow: [
            BoxShadow(
                blurRadius: 2, color: Colors.black12, offset: Offset(1, 1))
          ]),
      child: Theme(
        data: Theme.of(context).copyWith(dividerColor: Colors.transparent),
        child: ExpansionTile(
          trailing: Icon(
            Icons.arrow_drop_down,
            color: defaultcolor2,
          ),
          onExpansionChanged: (isExpanded) {
            Timer _timer;
            _timer = new Timer(const Duration(milliseconds: 300), () {
              if (isExpanded) {
                homeScroll.animateTo(homeScroll.position.maxScrollExtent,
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut);
              }
            });
          },
          expandedCrossAxisAlignment: CrossAxisAlignment.start,
          maintainState: true,
          title: CustomText(
              txt: title, bold: true, size: 0.038, color: fontColor1()),
          subtitle: Padding(
            padding: const EdgeInsets.only(top: 10),
            child: CustomText(
                txt: subtitle, bold: false, size: 0.038, color: fontColor1()),
          ),
          expandedAlignment: Alignment.centerLeft,
          childrenPadding: EdgeInsets.fromLTRB(getWidth(context, 0.02), 0,
              getWidth(context, 0.02), getWidth(context, 0.02)),
          children: [
            rowText(
                "Membantu banyak orang yang tidak mampu disekitar SOBAT-in untuk berobat kepada Dokter/Nakes/Fasyankes melalui Layanan Sukarela (gratis tanpa dipungut biaya). SOBAT-in dapat memberitahu dan atau mendaftarkan pasien tersebut melalui akun SOBAT-in"),
            rowText("OBAT-in mempunyai Jenis Layanan Sukarela"),
            rowText(
                "SOBAT-in, SHARE-in Jenis layanan tersebut ke orang yang membutuhkan"),
            rowText(
                "OBAT-in percaya diluar sana masih banyak Dokter/Nakes/ Fasyankes baik yang ingin beramal"),
            rowText(
                "OBAT-in juga percaya, SOBAT-in mau membantu SHARE-in agar membentuk lingkaran amal yang tidak terputus"),
            rowText(
                "AJAK-in lebih banyak Dokter/Nakes/ Fasyankes untuk bergabung, agar makin banyak Dokter/Nakes/ Fasyankes yang mengaktifkan Jenis Layanan Sukarela"),
            Padding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              child: CustomButton(
                styleId: 1,
                width: 0.9,
                height: 0.1,
                vermargin: 0.03,
                btnText: btnText,
                alignment: Alignment.bottomCenter,
                onPressed: () {
                  setState(() {
                    btnText = "Kirim";
                  });
                  // nextFunc();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  rowText(String txt2) {
    return Padding(
      padding: const EdgeInsets.only(left: 30),
      child: Row(
        children: [
          SizedBox(
            width: getWidth(context, 0.05),
            child: CustomText(
              txt: "•",
            ),
          ),
          SizedBox(
            width: getWidth(context, 0.7),
            child: CustomText(
              txt: txt2,
            ),
          ),
        ],
      ),
    );
  }

  contentTile(String txt1, bool boolid, {bool? sukarela}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(
          getWidth(context, 0.02),
          getWidth(context, 0.02),
          getWidth(context, sukarela != null ? 0 : 0.02),
          getWidth(context, 0.02)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Row(
            children: [
              Padding(
                padding: EdgeInsets.only(right: getWidth(context, 0.02)),
                child: Icon(
                  Icons.circle,
                  color: Colors.grey.shade400,
                  size: getWidth(context, 0.02),
                ),
              ),
              CustomText(
                  txt: txt1, bold: false, size: 0.038, color: fontColor1()),
            ],
          ),
        ],
      ),
    );
  }

  content(String txt1, String txt2) {
    return Padding(
      padding: EdgeInsets.all(
        getWidth(context, 0.02),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(txt: txt1, bold: true, size: 0.036),
          CustomText(txt: txt2, size: 0.036)
        ],
      ),
    );
  }

  contentOpenTime(String txt1,
      {String? txt2,
      String? txt3,
      String? txt4,
      String? txt5,
      String? txt6,
      String? txt7,
      String? txt8}) {
    return Padding(
      padding: EdgeInsets.all(
        getWidth(context, 0.02),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(txt: txt1, bold: true, size: 0.036),
          if (txt2 != null) CustomText(txt: "Senin, " + txt2, size: 0.036),
          if (txt3 != null) CustomText(txt: "Selasa, " + txt3, size: 0.036),
          if (txt4 != null) CustomText(txt: "Rabu, " + txt4, size: 0.036),
          if (txt5 != null) CustomText(txt: "Kamis, " + txt5, size: 0.036),
          if (txt6 != null) CustomText(txt: "Jumat, " + txt6, size: 0.036),
          if (txt7 != null) CustomText(txt: "Sabtu, " + txt7, size: 0.036),
          if (txt8 != null) CustomText(txt: "Minggu, " + txt8, size: 0.036),
        ],
      ),
    );
  }
}
