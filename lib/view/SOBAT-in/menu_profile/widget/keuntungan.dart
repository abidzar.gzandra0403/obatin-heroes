import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/servicebool_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:obatin/view/SOBAT-in/menu_profile/widget/keuntungan_sobat.dart';
import 'package:obatin/view/common/asset_images.dart';
import 'package:obatin/view/common/colorpalette.dart';
import 'package:obatin/view/common/defaultvalue.dart';
import 'package:obatin/view/common/transitionanimation.dart';
import 'package:obatin/view/common_widget/customWidget/customAppBar.dart';
import 'package:obatin/view/common_widget/customWidget/customSVG.dart';
import 'package:obatin/view/common_widget/customWidget/customAlertDialog.dart';
import 'package:obatin/view/common_widget/customWidget/customButton.dart';
import 'package:obatin/view/common_widget/customWidget/customText.dart';

class keuntungan extends StatefulWidget {
  @override
  _keuntunganState createState() => _keuntunganState();
}

class _keuntunganState extends State<keuntungan> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, "Keuntungan SOBAT-in", true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              BlocBuilder<ServiceBoolBloc, ServiceBoolState>(
                builder: (context, state) => keuntungan_sobat(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

profil(
  BuildContext context, {
  @required String? nama,
}) {
  return Container(
    padding: EdgeInsets.only(
        left: getWidth(context, 0.06),
        right: getWidth(context, 0.06),
        bottom: getWidth(context, 0.02)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Row(
          children: [
            CircleAvatar(
                backgroundColor: Colors.blue.shade50,
                radius: getWidth(context, 0.08),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(
                    getWidth(context, 1),
                  ),
                  child: img_profilephotosexample,
                )),
            Container(
              width: getWidth(context, 0.5),
              margin: EdgeInsets.only(left: getWidth(context, 0.02)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CustomText(
                    txt: nama,
                    bold: true,
                    size: 0.045,
                  ),
                  CustomText(
                    txt: "01 Januari 1999",
                    size: 0.034,
                    color: fontColor2(),
                  ),
                  CustomText(
                    txt: "abidzar@gmail.com",
                    size: 0.034,
                    color: fontColor2(),
                  ),
                  CustomText(
                    txt: "081805822676",
                    size: 0.034,
                    color: fontColor2(),
                  ),
                ],
              ),
            ),
          ],
        ),
        Row(
          children: [
            CustomButton(
                width: 0.12,
                styleId: 4,
                shapeId: 4,
                prefixIcon: Icons.edit,
                isize: 0.07,
                iColor: defaultcolor2,
                onPressed: () {
                  // Navigator.push(
                  //     context, SlideRightRoute(page: NotificationMain()));
                }),
          ],
        )
      ],
    ),
  );
}
