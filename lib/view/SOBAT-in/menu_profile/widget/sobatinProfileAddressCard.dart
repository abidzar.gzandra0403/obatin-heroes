import 'package:obatin/importPackage.dart';

sobatinprofileAddressCard(
    BuildContext context, String patientName, String address,
    {bool? primary}) {
  return CustomButton(
    styleId: 6,
    horpad: 0.02,
    vermargin: 0.02,
    bColor: btnColor(),
    onPressed: () => Navigator.push(
        context, SlideRightRoute(page: SobatinProfileAddressAdd(callID: 2))),
    prefixWidget: Row(
      children: [
        Container(
          child: Icon(
            Icons.maps_home_work,
            size: getWidth(context, 0.07),
            color: defaultcolor3,
          ),
        ),
        Expanded(
            child: Padding(
          padding: const EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                txt: patientName,
                bold: true,
              ),
              CustomText(txt: address),
              if (primary == true && primary != null)
                CustomText(
                  txt: "Utama",
                  toppad: 0.02,
                  bold: true,
                  color: defaultcolor2,
                  size: 0.04,
                )
            ],
          ),
        )),
        Icon(
          Icons.edit_outlined,
          color: fontColor2(),
        )
      ],
    ),
  );
}
