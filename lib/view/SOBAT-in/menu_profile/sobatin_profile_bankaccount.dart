import 'package:obatin/importPackage.dart';

class SobatinProfileBankAccount extends StatefulWidget {
  @override
  _SobatinProfileBankAccountState createState() =>
      _SobatinProfileBankAccountState();
}

class _SobatinProfileBankAccountState extends State<SobatinProfileBankAccount> {
  PageController pageController = new PageController();
  TextEditingController txtcNamaPemilik = new TextEditingController();
  TextEditingController txtcNorek = new TextEditingController();
  TextEditingController txtcBank = new TextEditingController();
  String btnText = Str().next;
  int currentIndex = 0;
  handleBack() {
    if (currentIndex == 0) {
      Navigator.pop(context);
    } else if (currentIndex == 1) {
      CustomAlertDialog(
        context,
        title: "Batal Tambah Rekening?",
        contentText: "Semua perubahan tidak akan disimpan.",
        txtButton1: "Ya",
        txtButton2: "Batal",
        funcButton1: () => pageController.previousPage(
            duration: Duration(milliseconds: 500), curve: Curves.ease),
      );
    } else {
      btnText = Str().next;
      pageController.previousPage(
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }
  }

  @override
  Widget build(BuildContext context) {
    nextFunc() {
      if (currentIndex == 2) {
        CustomAlertDialog(
          context,
          title: "Terkirim",
          contentIcon: Icons.check_circle_outline,
          funcDismiss: () => Navigator.pop(context),
          contentText:
              "Nomor rekening akan kami verifikasi. Mohon menunggu ya.",
        );
      } else {
        if (txtcNamaPemilik.text.isEmpty || txtcNorek.text.isEmpty) {
          CustomBottomDialog(context,
              txt: "Mohon Lengkapi Data", suffixIcon: Icons.info_outline);
        } else {
          pageController.nextPage(
              duration: Duration(milliseconds: 400), curve: Curves.ease);
        }
      }
    }

    return WillPopScope(
      onWillPop: () => handleBack(),
      child: Scaffold(
          appBar: CustomAppBar(
              context,
              currentIndex == 0
                  ? Str().setting$bankaccount
                  : currentIndex == 1
                      ? Str().setting$addbankaccount
                      : "Konfirmasi",
              true,
              customBackFunc: () => handleBack()),
          backgroundColor: bgColor(),
          body: Stack(
            children: [
              PageView(
                onPageChanged: (index) {
                  setState(() {
                    currentIndex = index;
                  });
                },
                controller: pageController,
                children: [
                  page1(),
                  page2(),
                  page3(),
                ],
              ),
              if (currentIndex >= 1)
                CustomButton(
                  styleId: 1,
                  width: 0.9,
                  height: 0.1,
                  vermargin: 0.03,
                  btnText: btnText,
                  alignment: Alignment.bottomCenter,
                  onPressed: () {
                    setState(() {
                      btnText = "Kirim";
                    });
                    nextFunc();
                  },
                )
            ],
          )),
    );
  }

  page1() {
    creditCard() {
      return Container(
        padding: EdgeInsets.all(getWidth(context, 0.04)),
        decoration: BoxDecoration(
            color: btnColor(),
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                  color: Colors.black26, offset: Offset(1, 1), blurRadius: 5)
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                CircleAvatar(
                  backgroundColor: defaultcolor4,
                  child: Icon(
                    Icons.credit_card,
                    color: Colors.white,
                  ),
                ),
                setWSpacing(getWidth(context, 0.03)),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      txt: "BRI",
                      bold: true,
                    ),
                    CustomText(
                      txt: "Checked (UTAMA)",
                      color: defaultcolor2,
                    )
                  ],
                ),
              ],
            ),
            CustomText(
              txt: "****0092",
              size: 0.05,
            )
          ],
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.03)),
        child: Column(
          children: [
            creditCard(),
            CustomButton(
              vermargin: 0.03,
              // width: 0.15,
              styleId: 1,
              shapeId: 2,
              onPressed: () => pageController.nextPage(
                  duration: Duration(milliseconds: 500), curve: Curves.ease),
              btnText: Str().setting$addbankaccount,
              prefixIcon: Icons.add,
              isize: 0.07,
            )
          ],
        ),
      ),
    );
  }

  page2() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        child: Column(
          children: [
            CustomTextField(
              hint: Str().setting$accountfullname,
              controller: txtcNamaPemilik,
            ),
            CustomTextField(
              hint: "No. Rekening",
              controller: txtcNorek,
            ),
            CustomDynamicDropDown(
              context,
              hint: "Nama Bank",
              contentWidget: SizedBox(),
              dialogTitle: "",
              showIfCondition: false,
            )
          ],
        ),
      ),
    );
  }

  page3() {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
              toppad: 0.02,
              botpad: 0.04,
              txt: "Mohon pastikan tidak ada data yang salah.",
              size: 0.038,
            ),
            CustomText(
                txt: "Nama Lengkap Pemilik Rekening",
                bold: true,
                size: 0.035,
                botpad: 0.01),
            CustomText(txt: "Abidzar Ghifari Zandra", size: 0.04, botpad: 0.03),
            CustomText(
                txt: "No. Rekening", bold: true, size: 0.035, botpad: 0.01),
            CustomText(txt: "151011526122001", size: 0.04, botpad: 0.03),
            CustomText(txt: "Nama Bank", bold: true, size: 0.035, botpad: 0.01),
            CustomText(txt: "BRI", size: 0.04, botpad: 0.01),
          ],
        ),
      ),
    );
  }
}
