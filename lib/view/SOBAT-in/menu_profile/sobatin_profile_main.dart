import 'package:obatin/importPackage.dart';

class SobatinProfilMain extends StatefulWidget {
  @override
  _SobatinProfilMainState createState() => _SobatinProfilMainState();
}

class _SobatinProfilMainState extends State<SobatinProfilMain> {
  bool allowreview = false;
  settingsBtn(
      {@required String? title,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
        styleId: 5,
        verpad: 0.02,
        btnText: title,
        btnText2: info != null ? info : null,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        horpad: 0.03,
        prefixWidget: CustomSVG(
          svgName: svgName,
          color: defaultcolor3,
          size: 0.06,
        ),
        onPressed: () {
          if (gotopage != null) {
            goToPage(context, page: gotopage);
          } else {
            func!();
          }
        });
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, Str().profile, true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              profil(context, nama: "Dr. Paidi"),
              Container(
                margin: EdgeInsets.only(top: getWidth(context, 0.05)),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: defaultPaddingContent(context)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                                txt: Str().profile$aboutsobatin,
                                bold: true,
                                color: Colors.grey),
                            settingsBtn(
                                title: Str().profile$Advantagessobatin,
                                info: Str().profile$TitleAdvantagessobatin,
                                svgName: svg_iconpracticesetting,
                                gotopage: SobatinProfileAbout()),
                            settingsBtn(
                                title: "AJAK-in",
                                info: Str().profile$TitleAjakin,
                                svgName: svg_iconedittitle,
                                gotopage: SobatinProfileAjakin()),
                            settingsBtn(
                                title: "SHARE-in",
                                info: Str().profile$TitleSharein,
                                svgName: svg_iconedittitle,
                                gotopage: SobatinProfileSharein()),
                            CustomText(
                                txt: Str().profile$Accountsettings,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            settingsBtn(
                                title: Str().profile$AddressList,
                                info: Str().profile$TitleAddressList,
                                svgName: svg_icondescription,
                                gotopage: SobatinProfileAddress()),
                            settingsBtn(
                                title: Str().setting$bankaccount,
                                info: Str().setting$bankaccountdesc,
                                svgName: svg_iconbankaccount,
                                gotopage: SobatinProfileBankAccount()),
                            settingsBtn(
                                title: Str().profile$InstantPayment,
                                info: Str().profile$InstantPaymentDesc,
                                svgName: svg_iconbankaccount,
                                gotopage: SobatinProfileInstantPayment()),
                            settingsBtn(
                                title: Str().profile$AccountSecurity,
                                info: Str().profile$AccountSecurityDesc,
                                svgName: svg_iconprivacypolicy,
                                gotopage: SobatinProfileAccountSecurity()),
                            settingsBtn(
                                title: Str().notification,
                                info: Str().notificationDesc,
                                svgName: svg_iconnotifsettings,
                                gotopage: SobatinProfileNotification()),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

profil(
  BuildContext context, {
  @required String? nama,
}) {
  return Container(
    padding: EdgeInsets.only(
        left: getWidth(context, 0.06),
        right: getWidth(context, 0.06),
        bottom: getWidth(context, 0.02)),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        CircleAvatar(
            backgroundColor: Colors.blue.shade50,
            radius: getWidth(context, 0.08),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(
                getWidth(context, 1),
              ),
              child: ImgAsset.profilePhotoExample,
            )),
        Container(
          width: getWidth(context, 0.5),
          margin: EdgeInsets.only(left: getWidth(context, 0.02)),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              CustomText(
                txt: postProceedData_userData[0]['nama'],
                bold: true,
                size: 0.045,
              ),
              CustomText(
                txt: postProceedData_userData[0]["tanggal_lahir"]
                    .toString()
                    .substring(0, 10),
                size: 0.034,
                color: fontColor2(),
              ),
              CustomText(
                txt: postProceedData_userData[0]['email'],
                size: 0.034,
                color: fontColor2(),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}
