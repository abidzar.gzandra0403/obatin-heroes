import 'package:obatin/importPackage.dart';

class SobatinProfileAccountSecurity extends StatefulWidget {
  @override
  _SobatinProfileAccountSecurityState createState() =>
      _SobatinProfileAccountSecurityState();
}

class _SobatinProfileAccountSecurityState
    extends State<SobatinProfileAccountSecurity> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().profile$AccountSecurity, true),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
            horizontal: defaultPaddingContent(context),
            vertical: getWidth(context, 0.04)),
        child: Column(
          children: [
            CustomButton(
              styleId: 5,
              onPressed: () =>
                  goToPage(context, page: SobatinProfileChangePassword()),
              verpad: 0.02,
              btnText: Str().setting$changepassword,
              btnText2: Str().profile$changepasswordyouraccoun,
            ),
            CustomButton(
              styleId: 5,
              onPressed: () => CustomBottomDialog(context, txt: "On Working"),
              verpad: 0.02,
              btnText: "Setel PIN",
              btnText2: Str().profile$setpindesc,
            )
          ],
        ),
      ),
    );
  }
}
