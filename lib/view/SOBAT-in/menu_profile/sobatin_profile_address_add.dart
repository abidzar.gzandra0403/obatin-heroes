import 'package:obatin/importPackage.dart';

class SobatinProfileAddressAdd extends StatefulWidget {
  final int? callID;
  SobatinProfileAddressAdd({@required this.callID});

  @override
  _SobatinProfileAddressAddState createState() =>
      _SobatinProfileAddressAddState();
}

class _SobatinProfileAddressAddState extends State<SobatinProfileAddressAdd> {
  TextEditingController txtcAlamatLengkap = new TextEditingController();
  TextEditingController txtcKecamatan = new TextEditingController();
  TextEditingController txtcKabupaten = new TextEditingController();
  TextEditingController txtcDetailSpesifik = new TextEditingController();
  bool? primaryAddress = false;
  toggleSwitchFunc() {
    setState(() {
      primaryAddress = !primaryAddress!;
    });
  }

  buttonFunc() async {
    if (txtcAlamatLengkap.text.isEmpty ||
        txtcKecamatan.text.isEmpty ||
        txtcKabupaten.text.isEmpty ||
        txtcDetailSpesifik.text.isEmpty) {
      CustomBottomDialog(context,
          txt: "Mohon lengkapi data", prefixIcon: Icons.info_outline);
    } else {
      CustomBottomDialog(context,
          txt: widget.callID == 1 ? "Berhasil Disimpan" : "Berhasil Diupdate",
          suffixIcon: Icons.check_rounded);
      Navigator.pop(context);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(
          context, widget.callID == 1 ? "Tambah Alamat" : "Edit", true),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          children: [
            CustomTextField(
              hint: Str().profile$completeaddress,
              controller: txtcAlamatLengkap,
              maxlines: 5,
            ),
            CustomTextField(
              hint: "Kecamatan",
              controller: txtcKecamatan,
            ),
            CustomTextField(
              hint: "Kabupaten",
              controller: txtcKabupaten,
            ),
            CustomTextField(
              hint: "Detail Spesifik",
              controller: txtcDetailSpesifik,
            ),
            CustomButton(
              styleId: 5,
              bold: true,
              onPressed: () => toggleSwitchFunc(),
              btnText: Str().profile$MakeMainAddress,
              suffixWidget: CustomSwitch(
                  onToggle: () => toggleSwitchFunc(), value: primaryAddress),
            ),
            CustomButton(
              vermargin: 0.05,
              styleId: 1,
              onPressed: () => buttonFunc(),
              btnText: widget.callID == 1 ? Str().save : "Update",
            )
          ],
        ),
      ),
    );
  }
}
