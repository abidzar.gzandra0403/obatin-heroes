import 'package:obatin/importPackage.dart';

class SobatinSupportFAQAnswer extends StatefulWidget {
  final String? title;
  SobatinSupportFAQAnswer({@required this.title});
  @override
  _SobatinSupportFAQAnswerState createState() =>
      _SobatinSupportFAQAnswerState();
}

class _SobatinSupportFAQAnswerState extends State<SobatinSupportFAQAnswer> {
  btnFunc(int btnId) async {
    if (btnId == 1) {
      // const number = '08592119XXXX'; //set the number here
      // bool? res = await FlutterPhoneDirectCaller.callNumber(number);
    } else if (btnId == 2) {
    } else if (btnId == 3) {}
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, "FAQ", true),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.title!,
                        style: hnnormal(context,
                            txtWeight: FontWeight.bold,
                            txtSize: getWidth(context, 0.045)),
                      ),
                      SizedBox(
                        height: getWidth(context, 0.05),
                      ),
                      Row(
                        children: [
                          new Icon(
                            Icons.alarm,
                            size: 15.0,
                            color: Colors.grey,
                          ),
                          Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: CustomText(
                                txt: "09 Nov 2021",
                                bold: false,
                                size: 0.035,
                              )),
                        ],
                      ),
                      CustomText(
                        txt: loremIpsum,
                        toppad: 0.05,
                        botpad: 0.05,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(
                                txt: "Pertanyaanmu belum terjawab?",
                                bold: true,
                                size: 0.045,
                              ),
                              CustomText(
                                txt: "Mister'O siap bantu lebih lanjut : ",
                                bold: true,
                                size: 0.035,
                              )
                            ],
                          ),
                          CustomButton(
                            styleId: 1,
                            onPressed: () => Navigator.push(
                                context,
                                SlideRightRoute(
                                    page: SobatinSupportComplains())),
                            btnText: "Tulis Kendala",
                          )
                        ],
                      ),
                    ],
                  ),
                ),

                // configbutton(context), OPTIONAL
              ],
            ),
          ),
        ),
      ),
    );
  }

  bulet(
    String txt,
  ) {
    return SizedBox(
      child: RawMaterialButton(
        onPressed: () {},
        child: Row(
          children: [
            new Text(
              "• ",
              style: hnnormal(context, txtSize: getWidth(context, 0.045)),
            ),
            Expanded(
              child: Text(
                txt,
                style: hnnormal(context, txtSize: getWidth(context, 0.035)),
              ),
            ),
          ],
        ),
      ),
    );
  }

  buttonakun(String txt, int btnId) {
    return Padding(
      padding: EdgeInsets.only(bottom: getWidth(context, 0.05)),
      child: Container(
        height: 70,
        width: 85,
        child: RawMaterialButton(
          onPressed: () {
            btnFunc(btnId);
          },
          padding: EdgeInsets.all(
            getWidth(context, 0.01),
          ),
          fillColor: Colors.blue,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                txt,
                textAlign: TextAlign.center,
                style: hnnormal(context,
                    txtSize: getWidth(context, 0.035), txtColor: Colors.white),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
