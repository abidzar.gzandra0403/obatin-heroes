import 'package:obatin/importPackage.dart';

class SobatinSupportHelpCenter extends StatefulWidget {
  @override
  _SobatinSupportHelpCenterState createState() =>
      _SobatinSupportHelpCenterState();
}

class _SobatinSupportHelpCenterState extends State<SobatinSupportHelpCenter> {
  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, Str().support$supportcenterSobat, true),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                setHSpacing(getWidth(context, 0.05)),
                CustomText(
                    txt: Str().support$welcometojelasin +
                        "\n" +
                        Str().support$introductionmistero),
                CustomText(
                  toppad: 0.05,
                  bold: true,
                  txt: Str().support$anyhelpmistero,
                ),
                CustomTextField(
                  hint: Str().support$searchtopic,
                  styleId: 2,
                  prefixIcon: Icons.search,
                ),
                CustomText(
                  toppad: 0.05,
                  botpad: 0.02,
                  bold: true,
                  txt: Str().support$selecttopicmistero,
                ),
                GridView.count(
                  physics: NeverScrollableScrollPhysics(),
                  crossAxisCount: 4,
                  shrinkWrap: true,
                  childAspectRatio: 1.4,
                  crossAxisSpacing: getWidth(context, 0.02),
                  mainAxisSpacing: getWidth(context, 0.02),
                  children: [
                    buttonTopic(Str().profile$account),
                    buttonTopic(Str().profile$order),
                    buttonTopic(Str().profile$payment),
                    buttonTopic(Str().profile$Delivery),
                    buttonTopic(Str().support$complain),
                    buttonTopic(Str().profile$Promotion),
                    buttonTopic("Menu OBAT-in"),
                    buttonTopic(Str().support$termNcondition),
                  ],
                ),
                CustomText(
                  toppad: 0.05,
                  botpad: 0.02,
                  bold: true,
                  txt: Str().support$frequentquestion,
                ),
                ListView(
                  physics: NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  children: [
                    buttonFaq("Bagaimana cara kerja aplikasi?"),
                    buttonFaq("Gabung Mitra Obatin?"),
                    buttonFaq("Cara Pembayaran?"),
                    buttonFaq("Apa itu layanan unggulan dan spesialisasi?"),
                    buttonFaq("Bagaimana mekanisme layanan Visit Patient?"),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  buttonTopic(String txt) {
    return CustomButton(
      horpad: 0.01,
      styleId: 6,
      onPressed: () {},
      btnText: txt,
      fColor: Colors.white,
    );
  }

  buttonFaq(String txt) {
    return CustomButton(
      styleId: 5,
      onPressed: () => Navigator.push(
          context, SlideRightRoute(page: SobatinSupportFAQAnswer(title: txt))),
      btnText: txt,
    );
  }
}
