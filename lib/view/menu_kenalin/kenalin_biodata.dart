import 'package:obatin/importPackage.dart';

class KenalinBiodata extends StatefulWidget {
  final String? title;
  final int? callId;
  KenalinBiodata({@required this.title, @required this.callId});
  @override
  _KenalinBiodataState createState() => _KenalinBiodataState();
}

class _KenalinBiodataState extends State<KenalinBiodata> {
  TextEditingController txtcNama = new TextEditingController();
  TextEditingController txtctempatlahir = new TextEditingController();
  TextEditingController txtctanggallahir = new TextEditingController();
  TextEditingController txtcemail = new TextEditingController();
  TextEditingController txtcnik = new TextEditingController();

  @override
  void initState() {
    super.initState();
    initValue();
  }

  initValue() {
    txtcnik.text = postProceedData_userData[0]["nik"];
    txtcemail.text = postProceedData_userData[0]["email"];
    txtcNama.text = postProceedData_userData[0]["nama"];
    txtctempatlahir.text = postProceedData_userData[0]["tempat_lahir"];
    txtctanggallahir.text = postProceedData_userData[0]["tanggal_lahir"]
        .toString()
        .substring(0, 10);
  }

  buttonFunc() async {
    context.read<UserBloc>().add(EditBiodataUser(
          context,
          nama: txtcNama.text,
          tempatlahir: txtctempatlahir.text,
          tanggallahir: txtctanggallahir.text,
          email: txtcemail.text,
          nik: txtcnik.text,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, widget.title!, true,
          rightWidget: CustomButton(
            styleId: 3,
            onPressed: () => buttonFunc(),
            btnText: "Update",
          )),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: widget.callId == 2
            ? CustomTextField(
                hint: "Email",
                controller: txtcemail,
                maxlines: 5,
              )
            : widget.callId == 3
                ? CustomTextField(
                    hint: "No. NIK",
                    controller: txtcnik,
                    maxlines: 5,
                  )
                : Column(
                    children: [
                      CustomTextField(
                        hint: "Nama",
                        controller: txtcNama,
                        maxlines: 5,
                      ),
                      CustomTextField(
                        hint: "Tempat Lahir",
                        controller: txtctempatlahir,
                      ),
                      // CustomTextField(
                      //   hint: "Tanggal Lahir",
                      //   controller: txtctanggallahir,
                      // ),
                      CustomDateField(
                          textEditingController: txtctanggallahir,
                          hint: "Tanggal Lahir"),
                    ],
                  ),
      ),
    );
  }
}
