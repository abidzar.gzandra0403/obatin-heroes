import 'package:obatin/importPackage.dart';

class KenalinMain extends StatefulWidget {
  @override
  _KenalinMainState createState() => _KenalinMainState();
}

class _KenalinMainState extends State<KenalinMain> {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SQFLITEUserDataBloc, SQFLITEUserDataState>(
      builder: (context, state) => Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, "KENAL-in", true),
        body: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              children: [
                kenalinPhotoProfile(context),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        toppad: 0.04,
                        botpad: 0.02,
                        txt: Str().kenalin$myaccount,
                        bold: true,
                        color: Colors.grey,
                      ),
                      Container(
                        width: getWidth(context, 1),
                        padding: EdgeInsets.all(getWidth(context, 0.04)),
                        decoration: BoxDecoration(
                          color: bgColorDarken(),
                          border:
                              Border.all(width: 1, color: borderBlueColor()),
                          borderRadius:
                              BorderRadius.circular(getWidth(context, 0.03)),
                        ),
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  rowContentBiodata("Nama",
                                      postProceedData_userData[0]["nama"]),
                                  rowContentBiodata(
                                      "Tempat Lahir",
                                      postProceedData_userData[0]
                                          ["tempat_lahir"]),
                                  rowContentBiodata(
                                      "Tanggal Lahir",
                                      postProceedData_userData[0]
                                              ["tanggal_lahir"]
                                          .toString()
                                          .substring(0, 10)),
                                ],
                              ),
                              CustomButton(
                                width: 0.1,
                                styleId: 3,
                                shapeId: 4,
                                onPressed: () => goToPage(context,
                                    page: KenalinBiodata(
                                        title: "Update Biodata", callId: 1)),
                                prefixIcon: Icons.edit,
                              ),
                            ]),
                      ),
                      setHSpacing(getWidth(context, 0.04)),
                      buttonKenalin(
                          title: postProceedData_userData[0]["email"],
                          icon: Icons.email_outlined,
                          onPress: () => goToPage(context,
                              page: KenalinBiodata(
                                  title: "Ubah Email", callId: 2)),
                          suffixWidget: CustomText(
                            txt: "Ubah Email",
                            color: defaultcolor2,
                            rightpad: 0.01,
                          )),
                      buttonKenalin(
                        title: "Nomor NIK",
                        icon: Icons.perm_identity_outlined,
                        onPress: () => goToPage(context,
                            page:
                                KenalinBiodata(title: "Nomor NIK", callId: 3)),
                      ),
                      buttonKenalin(
                          title: "Ubah Password",
                          icon: Icons.password_outlined,
                          onPress: () =>
                              goToPage(context, page: KenalinChangePass())),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  rowContentBiodata(String txt1, String txt2) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.005)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          CustomText(
            txt: txt1,
            size: 0.036,
            width: 0.3,
            bold: true,
            color: defaultcolor2,
          ),
          CustomText(
            width: 0.6,
            botpad: 0.005,
            txt: txt2,
            size: 0.036,
          )
        ],
      ),
    );
  }

  buttonKenalin(
      {Widget? gotoPage,
      @required String? title,
      @required IconData? icon,
      Widget? suffixWidget,
      VoidCallback? onPress}) {
    return CustomButton(
        styleId: 5,
        btnText: title!,
        prefixIcon: icon!,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        onPressed: () {
          if (gotoPage != null)
            goToPage(context, page: gotoPage);
          else if (onPress != null) {
            onPress();
          }
        });
  }
}
