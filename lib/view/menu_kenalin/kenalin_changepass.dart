import 'package:obatin/importPackage.dart';

class KenalinChangePass extends StatefulWidget {
  @override
  _KenalinChangePassState createState() => _KenalinChangePassState();
}

class _KenalinChangePassState extends State<KenalinChangePass> {
  TextEditingController settingspassTxtbox = new TextEditingController();
  TextEditingController settingspassConfirmTxtbox = new TextEditingController();
  handleBack() {
    CustomAlertDialog(context,
        title: Str().setting$cancelchangepassworddialog,
        txtButton1: Str().yes,
        txtButton2: Str().cancel, funcButton1: () {
      settingspassTxtbox.text = "";
      settingspassConfirmTxtbox.text = "";
      Navigator.pop(context);
    });
  }

  confirmFunc() {
    CustomAlertDialog(context,
        title: Str().setting$changepassworddialog,
        contentText: Str().setting$changepassworddialogdesc,
        txtButton1: Str().sure,
        txtButton2: Str().cancel, funcButton1: () {
      settingspassTxtbox.text = "";
      settingspassConfirmTxtbox.text = "";
      Navigator.pop(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () => handleBack(),
      child: Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(
          context,
          Str().setting$changepassword,
          true,
          customBackFunc: () => handleBack(),
          rightWidget: CustomButton(
              styleId: 3,
              onPressed: () => confirmFunc(),
              btnText: Str().savechanges),
        ),
        body: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: defaultPaddingContent(context)),
            child: Column(
              children: [
                CustomTextField(
                  hint: Str().oldpassword,
                ),
                passwordFieldValidator(
                    context, Str().newpassword, settingspassTxtbox),
                passwordFieldValidator(context, Str().newpasswordconfirm,
                    settingspassConfirmTxtbox,
                    txtId: 2, targetTxtbox: settingspassTxtbox),
              ],
            )),
      ),
    );
  }
}
