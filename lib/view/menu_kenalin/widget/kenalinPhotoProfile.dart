import 'package:obatin/importPackage.dart';

kenalinPhotoProfile(BuildContext context) {
  changePhotoDialog() {
    button(IconData icon, String txt) {
      return CustomButton(
        styleId: 3,
        vermargin: 0.02,
        height: 0.1,
        onPressed: () {},
        prefixIcon: icon,
        btnText: txt,
      );
    }

    return CustomAlertDialog(context,
        title: "Ubah Foto Profil",
        contentWidget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            button(Icons.add_a_photo_outlined, "Buka Kamera"),
            button(Icons.image_outlined, "Unggah Foto"),
          ],
        ));
  }

  return Container(
    child: Center(
      child: CircleAvatar(
          radius: getWidth(context, 0.12),
          child: RawMaterialButton(
            shape: CircleBorder(),
            onPressed: () => changePhotoDialog(),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(999),
              child: Stack(
                children: [
                  ImgAsset.profilePhotoExample,
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      height: getWidth(context, 0.08),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(color: Colors.black45),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(
                            Icons.edit,
                            size: getWidth(context, 0.04),
                          ),
                          setWSpacing(getWidth(context, 0.01)),
                          CustomText(
                              txt: Str().edit, bold: true, color: Colors.white)
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )),
    ),
  );
}
