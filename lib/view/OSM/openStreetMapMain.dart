import 'package:obatin/importPackage.dart';

class OpenStreetMapMain extends StatefulWidget {
  final Function(
      String address,
      String province,
      String city,
      String kecamatan,
      String kelurahan,
      String postalCode,
      String longitude,
      String latitude)? onGetLocation;
  OpenStreetMapMain({this.onGetLocation});
  @override
  _OpenStreetMapMainState createState() => _OpenStreetMapMainState();
}

class _OpenStreetMapMainState extends State<OpenStreetMapMain> {
  MapController mapController = MapController();
  GeoPoint? geoPoint;
  bool finishloading = false;

  initLocation() async {
    mapController = MapController(
      initMapWithUserPosition: true,
    );
  }

  @override
  void initState() {
    super.initState();
    initLocation();
  }

  @override
  void dispose() {
    super.dispose();
    mapController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: bgColor(),
        appBar: CustomAppBar(context, "Pilih Lokasi", true),
        body: Stack(
          children: [
            OSMFlutter(
              controller: mapController,
              trackMyPosition: false,
              initZoom: 17,
              minZoomLevel: 8,
              maxZoomLevel: 19,
              stepZoom: 1.0,
              isPicker: true,
              showContributorBadgeForOSM: true,
              onMapIsReady: (bool ready) {
                finishloading = ready;
              },
              mapIsLoading: Container(
                color: bgColor(),
                child: Transform.translate(
                  offset: Offset(0, -getWidth(context, 0.1)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.location_on,
                        color: defaultcolor2,
                        size: getWidth(context, 0.3),
                      ),
                      SpinKitCircle(
                        color: defaultcolor2,
                        size: getWidth(context, 0.1),
                      )
                    ],
                  ),
                ),
              ),
              userLocationMarker: UserLocationMaker(
                personMarker: MarkerIcon(
                  icon: Icon(
                    Icons.location_history_rounded,
                    color: Colors.red,
                    size: 48,
                  ),
                ),
                directionArrowMarker: MarkerIcon(
                  icon: Icon(
                    Icons.double_arrow,
                    size: 48,
                  ),
                ),
              ),
              road: Road(
                startIcon: MarkerIcon(
                  icon: Icon(
                    Icons.person,
                    size: 64,
                    color: Colors.brown,
                  ),
                ),
                roadColor: Colors.yellowAccent,
              ),
              markerOption: MarkerOption(
                  defaultMarker: MarkerIcon(
                icon: Icon(
                  Icons.person_pin_circle,
                  color: Colors.blue,
                  size: 56,
                ),
              )),
            ),
            CustomButton(
              alignment: Alignment.bottomCenter,
              styleId: 1,
              width: 0.5,
              vermargin: 0.08,
              onPressed: () async {
                if (finishloading) {
                  geoPoint = await mapController
                      .getCurrentPositionAdvancedPositionPicker();
                  double longitude = geoPoint!.longitude;
                  double latitude = geoPoint!.latitude;
                  List<Placemark> address =
                      await placemarkFromCoordinates(latitude, longitude);
                  await widget.onGetLocation!(
                    address[0].street!,
                    address[0].administrativeArea!,
                    address[0].subAdministrativeArea!,
                    address[0].locality!,
                    address[0].subLocality!,
                    address[0].postalCode!,
                    longitude.toString(),
                    latitude.toString(),
                  );
                  Navigator.pop(context);
                }
              },
              btnText: "Pilih Lokasi",
            ),
          ],
        ));
  }
}
