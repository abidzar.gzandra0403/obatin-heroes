import 'package:obatin/importPackage.dart';

class RegisterEmailForm extends StatefulWidget {
  final int? callID;
  final TextEditingController? txtboxEmail;
  final TextEditingController? txtboxPassword;
  final TextEditingController? txtboxPConfirm;
  RegisterEmailForm({
    @required this.callID,
    @required this.txtboxEmail,
    @required this.txtboxPassword,
    this.txtboxPConfirm,
  });

  @override
  _RegisterEmailFormState createState() => _RegisterEmailFormState();
}

class _RegisterEmailFormState extends State<RegisterEmailForm> {
  bool isEmailValid = false;
  bool secureText = true;
  showHide() {
    setState(() {
      secureText = !secureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Center(
        child: Container(
          width: getWidth(context, 1),
          padding: EdgeInsets.symmetric(
              horizontal: getWidth(context, 0.1),
              vertical: getWidth(context, 0.05)),
          child: Column(
            children: <Widget>[
              CustomText(
                  txt: widget.callID == 1
                      ? "Buat Akun HEROES"
                      : "Login Akun OBAT-in",
                  bold: true,
                  size: 0.05,
                  spacing: 1),
              setHSpacing(getWidth(context, 0.06)),
              CustomTextField(
                hint: "Email",
                suffixIcon: widget.txtboxEmail!.text.isEmpty
                    ? null
                    : isEmailValid
                        ? Icons.check_circle_outline
                        : Icons.block_outlined,
                suffixIconColor:
                    isEmailValid ? Colors.green : Colors.red.shade300,
                onChanged: () => {
                  setState(() {
                    isEmailValid = EmailValidator.validate(
                        widget.txtboxEmail!.text.isEmpty
                            ? ""
                            : widget.txtboxEmail!.text);
                  })
                },
                keyboardtype: TextInputType.emailAddress,
                controller: widget.txtboxEmail!,
              ),
              widget.callID == 1
                  ? Column(
                      children: [
                        passwordFieldValidator(
                            context, Str().password, widget.txtboxPassword!),
                        passwordFieldValidator(context, Str().passwordconfirm,
                            widget.txtboxPConfirm!,
                            txtId: 2, targetTxtbox: widget.txtboxPassword!),
                      ],
                    )
                  : PasswordField(
                      hint: Str().password,
                      controller: widget.txtboxPassword,
                      secureText: secureText,
                      onIconPress: () {
                        showHide();
                      })
            ],
          ),
        ),
      ),
    );
  }
}
