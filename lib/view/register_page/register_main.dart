import 'package:obatin/importPackage.dart';
import 'package:http/http.dart' as http;

class RegisterMain extends StatefulWidget {
  final bool? createNEW;
  RegisterMain({@required this.createNEW});
  @override
  _RegisterMainState createState() => _RegisterMainState();
}

class _RegisterMainState extends State<RegisterMain> {
  TextEditingController txtboxEmail = new TextEditingController();
  TextEditingController txtboxPassword = new TextEditingController();
  TextEditingController txtboxPConfirm = new TextEditingController();
  TextEditingController txtboxNamaLengkap = new TextEditingController();
  TextEditingController txtboxTanggalLahir = new TextEditingController();
  TextEditingController txtboxTempatLahir = new TextEditingController();
  TextEditingController txtboxNoSTR = new TextEditingController();
  TextEditingController txtboxBerlakuDari = new TextEditingController();
  TextEditingController txtboxBerlakuSampai = new TextEditingController();
  bool createNEWInit = false;
  int userType = 0;
  var buttonText = "Next";
  bool fullscreen = false;

  int indexNow = 1;
  final controller = new PageController(initialPage: 0, keepPage: true);
  List<String> radioValue = ["Ya", "Tidak"];
  String idmitralevel = '';
  String radioInitAddressNow = "Ya";
  String radioInitGelarDpn = "Ya";
  String radioInitGelarBlk = "Ya";
  bool showNextButton = false;
  String btnNextText = "Selanjutnya";

  @override
  void initState() {
    super.initState();
    createNEWInit = widget.createNEW != null ? widget.createNEW! : false;
    print("Buat Akun Baru :" + createNEWInit.toString());
  }

  void pageChanged(int index) {
    setState(() {
      indexNow = index + 1;
    });
  }

  checkifalreadyUser(
      {@required bool? createNEW,
      VoidCallback? functionIfHeroesAlreadyAsUser}) {
    print(txtboxEmail.text);
    context.read<UserBloc>().add(CheckIfAlreadyUser(context,
        createNEW: createNEW!,
        pageController: controller,
        email: txtboxEmail.text,
        password: txtboxPassword.text,
        functionIfHeroesAlreadyAsUser: functionIfHeroesAlreadyAsUser != null
            ? functionIfHeroesAlreadyAsUser
            : null));
  }

  sendNverifyData({@required bool? createNEW}) {
    print(txtboxEmail.text);
    context.read<UserBloc>().add(RegisterUserHeroes(
          context,
          email: txtboxEmail.text,
          password: createNEWInit ? txtboxPassword.text : "0",
          passwordconfirm: createNEWInit ? txtboxPConfirm.text : "0",
          namalengkap: createNEWInit ? txtboxNamaLengkap.text : "0",
          tmptlahir: createNEWInit ? txtboxTempatLahir.text : "0",
          tglahir: createNEWInit ? txtboxTanggalLahir.text : "2000-01-01",
          nostr: txtboxNoSTR.text,
          berlakudari: txtboxBerlakuDari.text,
          berlakusampai: txtboxBerlakuSampai.text,
          idmitralevel: idmitralevel,
        ));
  }

  getMitraLevel() async {
    var apiUrl = Uri.parse("http://103.226.139.253:32258/v1/levelmitra/get");
    try {
      var result = await http.get(
        apiUrl,
        headers: {"Accept": "application/json"},
      );
      var jsonObject = await json.decode(result.body);
      if (jsonObject["code"] == 1) {
        print("error");
      } else if (jsonObject["code"] == 0) {
        idmitralevel = jsonObject['data']['items'][0]['Id_level'];
        print(idmitralevel);
      }
    } catch (e) {
      print("Sambungan Gagal" + "\nKeterangan :\n" + e.toString());
    }
  }

  nextButton() {
    if (indexNow == 2) {
      createNEWInit == true
          ? checkifalreadyUser(
              createNEW: true,
              functionIfHeroesAlreadyAsUser: () {
                setState(() {
                  createNEWInit = false;
                  txtboxPassword.text = '';
                  txtboxPConfirm.text = '';
                });
              })
          : checkifalreadyUser(createNEW: false);
    } else if (indexNow == 3) {
      sendNverifyData(createNEW: createNEWInit);
    } else {
      controller.animateToPage(indexNow,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }
  }

  Future handleBack() async {
    return CustomAlertDialog(context,
        title: Str().cancel,
        contentText: Str().allchangenotbesave,
        txtButton1: Str().yes,
        txtButton2: Str().cancel, funcButton1: () {
      Navigator.of(context).pop();
      goToPage(context, pushReplace: true, page: LoginMain());
    });
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness:
                themeID == 1 ? Brightness.dark : Brightness.light),
        child: Scaffold(
          backgroundColor: bgColor(),
          body: pageviewItem(context),
        ));
  }

  pageviewItem(BuildContext context) {
    return Stack(children: [
      obatinHeader(context, Str().register),
      AnimatedPadding(
        padding: EdgeInsets.only(top: getWidth(context, fullscreen ? 0 : 0.6)),
        duration: Duration(milliseconds: 600),
        curve: Curves.decelerate,
        child: AnimatedContainer(
          // height: getWidth(context, 1),
          duration: Duration(milliseconds: 600),
          width: getWidth(context, 1),
          padding: EdgeInsets.only(top: getWidth(context, 0.1)),
          decoration: BoxDecoration(
              color: bgColor(),
              borderRadius: BorderRadius.only(
                  topLeft:
                      Radius.circular(getWidth(context, fullscreen ? 0 : 0.1)),
                  topRight: Radius.circular(
                      getWidth(context, fullscreen ? 0 : 0.1)))),
          child: Column(
            children: [
              Expanded(
                child: PageView(
                  physics: NeverScrollableScrollPhysics(),
                  controller: controller,
                  onPageChanged: (index) {
                    pageChanged(index);
                  },
                  children: <Widget>[
                    nakesSelect(),
                    createNEWInit == false
                        ? RegisterEmailForm(
                            callID: 2,
                            txtboxEmail: txtboxEmail,
                            txtboxPassword: txtboxPassword,
                          )
                        : RegisterEmailForm(
                            callID: 1,
                            txtboxEmail: txtboxEmail,
                            txtboxPassword: txtboxPassword,
                            txtboxPConfirm: txtboxPConfirm,
                          ),
                    RegisterBiodata(
                        callID: createNEWInit == true ? 1 : 2,
                        userType: userType,
                        txtboxEmail: txtboxEmail,
                        txtboxPassword: txtboxPassword,
                        txtboxPConfirm: txtboxPConfirm,
                        txtboxNamaLengkap: txtboxNamaLengkap,
                        txtboxTanggalLahir: txtboxTanggalLahir,
                        txtboxTempatLahir: txtboxTempatLahir,
                        txtboxNoSTR: txtboxNoSTR,
                        txtboxBerlakuDari: txtboxBerlakuDari,
                        txtboxBerlakuSampai: txtboxBerlakuSampai),
                    RegisterDocumentUpload(
                        controller: controller, userType: userType),
                    RegisterOTPVerification(onWillPop: () {
                      setState(() {
                        btnNextText = "Selanjutnya";
                      });
                      controller.animateToPage(3,
                          duration: Duration(milliseconds: 500),
                          curve: Curves.ease);
                    })
                  ],
                ),
              ),
              if (showNextButton == true)
                CustomButton(
                    vermargin: 0.04,
                    styleId: 1,
                    width: 0.9,
                    height: 0.1,
                    btnText: btnNextText,
                    onPressed: () {
                      setState(() {
                        if (indexNow == 4) {
                          btnNextText = "Verifikasi";
                          nextButton();
                        } else if (indexNow == 5) {
                          registerInputOTPDialog(context);
                        } else {
                          nextButton();
                        }
                      });
                    })
            ],
          ),
        ),
      ),
    ]);
  }

  nakesSelect() {
    button(String title, int btnId) {
      return Padding(
        padding: EdgeInsets.only(top: getWidth(context, 0.04)),
        child: RawMaterialButton(
          onPressed: () {
            if (btnId == 3) {
              CustomBottomDialog(context, txt: "Segera Datang");
            } else {
              setState(() {
                fullscreen = true;
                showNextButton = true;
                userType = btnId;
              });
              getMitraLevel();
              nextButton();
            }
          },
          fillColor: Colors.transparent,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            height: getWidth(context, 0.27),
            width: getWidth(context, 0.6),
            child: Stack(
              children: [
                SizedBox(
                  height: getWidth(context, 0.27),
                  width: getWidth(context, 0.6),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FittedBox(
                      fit: BoxFit.cover,
                      child: btnId == 1
                          ? ImgAsset.imgRegisterdoctor
                          : btnId == 2
                              ? ImgAsset.imgRegisternakes
                              : ImgAsset.imgRegisterpharmacy,
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(getWidth(context, 0.02)),
                  child: Align(
                    alignment: Alignment.bottomLeft,
                    child: CustomText(
                        txt: title,
                        bold: true,
                        size: 0.04,
                        color: Colors.black54),
                  ),
                ),
                if (btnId == 3)
                  Container(
                    padding: EdgeInsets.all(getWidth(context, 0.02)),
                    margin: EdgeInsets.all(getWidth(context, 0.02)),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: defaultcolor2),
                    child: CustomText(
                      txt: "Coming Soon",
                      bold: true,
                      color: Colors.white,
                    ),
                  ),
              ],
            ),
          ),
        ),
      );
    }

    return Center(
      child: Container(
        width: getWidth(context, 1),
        child: Column(
          children: <Widget>[
            CustomText(txt: "Daftar Sebagai", bold: true, size: 0.038),
            button("Dokter", 1),
            button("Tenaga Kesehatan", 2),
            button("Penjual Produk Kesehatan", 3),
          ],
        ),
      ),
    );
  }
}
