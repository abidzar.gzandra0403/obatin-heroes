import 'package:obatin/importPackage.dart';

class RegisterOTPVerification extends StatelessWidget {
  final VoidCallback? onWillPop;
  const RegisterOTPVerification({@required this.onWillPop});

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        onWillPop!();
        return false;
      },
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            width: getWidth(context, 1),
            height: getWidth(context, 1),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.1),
                vertical: getWidth(context, 0.05)),
            child: Column(
              children: <Widget>[
                CustomText(
                    txt: "Nomor Telepon", bold: true, size: 0.05, spacing: 1),
                setHSpacing(getWidth(context, 0.06)),
                Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: getWidth(context, 0.1)),
                    child: CustomTextField(
                        hint: "Nomor Telepon",
                        keyboardtype: TextInputType.number)),
                Padding(
                  padding: EdgeInsets.only(top: getWidth(context, 0.03)),
                  child: Text(
                    Str().phonecheck,
                    style: hnnormal(context,
                        txtSize: getWidth(context, 0.04),
                        txtColor: fontColor1()),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
