import 'package:obatin/importPackage.dart';

registerInputOTPDialog(BuildContext context) {
  final textToken = new TextEditingController();
  final registerToken1 = new TextEditingController();
  final registerToken2 = new TextEditingController();
  final registerToken3 = new TextEditingController();
  final registerToken4 = new TextEditingController();
  dismiss() {
    registerToken1.text = '';
    registerToken2.text = '';
    registerToken3.text = '';
    registerToken4.text = '';
  }

  fillToken() {
    if (textToken.text.length == 0) {
      registerToken1.text = '';
    } else if (textToken.text.length > 0 && textToken.text.length <= 1) {
      registerToken1.text = textToken.text.substring(0, 1);
      registerToken2.text = '';
    } else if (textToken.text.length <= 2) {
      registerToken2.text = textToken.text.substring(1, 2);
      registerToken3.text = '';
    } else if (textToken.text.length <= 3) {
      registerToken3.text = textToken.text.substring(2, 3);
      registerToken4.text = '';
    } else if (textToken.text.length <= 4) {
      registerToken4.text = textToken.text.substring(3, 4);
    }
  }

  tokenInputBox(TextEditingController controller) {
    return Container(
        height: getWidth(context, 0.16),
        width: getWidth(context, 0.13),
        decoration: BoxDecoration(
            color: btnColor(),
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  blurRadius: getWidth(context, 0.006),
                  color: Colors.black26,
                  offset: Offset(3, 3))
            ]),
        child: Center(
          child: TextField(
            controller: controller,
            readOnly: true,
            style: hnnormal(context,
                txtSize: getWidth(context, 0.12), txtColor: fontColor1()),
            textAlign: TextAlign.center,
            maxLength: 1,
            maxLengthEnforcement: MaxLengthEnforcement.enforced,
            enableInteractiveSelection: false,
            showCursor: false,
            keyboardType: TextInputType.number,
            inputFormatters: <TextInputFormatter>[
              FilteringTextInputFormatter.digitsOnly
            ],
            decoration: InputDecoration(
              counterText: '',
              border: InputBorder.none,
              focusedBorder: InputBorder.none,
              enabledBorder: InputBorder.none,
              errorBorder: InputBorder.none,
              disabledBorder: InputBorder.none,
            ),
          ),
        ));
  }

  return CustomAlertDialog(
    context,
    funcDismiss: () => countDownTimerState!.cancel(),
    customBody: Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Container(
          height: getWidth(context, 0.5),
          width: getWidth(context, 0.5),
          child: ImgAsset.imgPhoneverification,
        ),
        Center(
          child: Text(
            Str().enterotp,
            style: hnnormal(context,
                txtSize: getWidth(context, 0.04), txtColor: fontColor1()),
          ),
        ),
        Stack(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                tokenInputBox(registerToken1),
                tokenInputBox(registerToken2),
                tokenInputBox(registerToken3),
                tokenInputBox(registerToken4),
              ],
            ),
            Visibility(
                visible: true,
                child: TextField(
                  maxLength: 4,
                  showCursor: false,
                  style: TextStyle(fontSize: 1),
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    FilteringTextInputFormatter.digitsOnly
                  ],
                  decoration: InputDecoration(
                    counterText: '',
                    border: InputBorder.none,
                    focusedBorder: InputBorder.none,
                    enabledBorder: InputBorder.none,
                    errorBorder: InputBorder.none,
                    disabledBorder: InputBorder.none,
                  ),
                  autofocus: true,
                  controller: textToken,
                  onChanged: (_) => fillToken(),
                )),
          ],
        ),
        Container(
          margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.02)),
          height: getWidth(context, 0.1),
          child: Row(
            children: [
              CustomText(txt: "Kirim Ulang Kode OTP?"),
              setWSpacing(getWidth(context, 0.02)),
              CustomCountDown(
                onEndCount: () {},
                seconds: 10,
                widgetAfterCount: CustomButton(
                  height: 0.07,
                  styleId: 3,
                  onPressed: () {},
                  btnText: "Kirim Ulang",
                ),
              ),
            ],
          ),
        ),
        CustomButton(
          styleId: 1,
          width: 0.3,
          onPressed: () {
            Navigator.pop(context);
            Navigator.pop(context);
            countDownTimerState!.cancel();
            CustomAlertDialog(
              context,
              title: "Yeay pendaftaran berhasil terkirim",
              contentIcon: Icons.check_circle_outline,
              contentText:
                  "Pihak OBAT-in akan memverifikasi akun kamu dalam 1 x 24 Jam. Mohon menunggu ya.",
            );
          },
          btnText: "Kirim",
        )
      ],
    ),
  );
}
