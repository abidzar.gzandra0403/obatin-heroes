import 'package:obatin/importPackage.dart';

class RegisterDocumentUpload extends StatefulWidget {
  final PageController? controller;
  final int? userType;
  RegisterDocumentUpload({@required this.controller, @required this.userType});

  @override
  _RegisterDocumentUploadState createState() => _RegisterDocumentUploadState();
}

class _RegisterDocumentUploadState extends State<RegisterDocumentUpload> {
  @override
  Widget build(BuildContext context) {
    return widget.userType == 3
        ? documentUploadPenjual()
        : documentUploadDokterNakes();
  }

  documentUploadDokterNakes() {
    return WillPopScope(
      onWillPop: () async {
        widget.controller!.animateToPage(2,
            duration: Duration(milliseconds: 500), curve: Curves.ease);

        return false;
      },
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            width: getWidth(context, 1),
            height: getWidth(context, 1),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.1),
                vertical: getWidth(context, 0.05)),
            child: Column(
              children: <Widget>[
                CustomText(
                    txt: "Unggah Dokumen", bold: true, size: 0.05, spacing: 1),
                setHSpacing(getWidth(context, 0.06)),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.add_a_photo_outlined,
                  btnText: "Foto Kartu Identitas",
                ),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.portrait_outlined,
                  btnText: "Selfie Dengan Kartu Identitas",
                ),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.upload_file_outlined,
                  btnText: "Dokumen Surat Izin Praktik",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  documentUploadPenjual() {
    return WillPopScope(
      onWillPop: () async {
        widget.controller!.animateToPage(2,
            duration: Duration(milliseconds: 500), curve: Curves.ease);

        return false;
      },
      child: Center(
        child: SingleChildScrollView(
          child: Container(
            width: getWidth(context, 1),
            height: getWidth(context, 1),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.1),
                vertical: getWidth(context, 0.05)),
            child: Column(
              children: <Widget>[
                CustomText(
                    txt: "Unggah Dokumen", bold: true, size: 0.05, spacing: 1),
                setHSpacing(getWidth(context, 0.06)),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.add_a_photo_outlined,
                  btnText: "Foto Kartu Identitas",
                ),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.portrait_outlined,
                  btnText: "Selfie Dengan Kartu Identitas",
                ),
                CustomButton(
                  height: 0.1,
                  vermargin: 0.02,
                  styleId: 3,
                  onPressed: () {},
                  prefixIcon: Icons.upload_file_outlined,
                  btnText: "Dokumen Surat Izin Operasional",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
