import 'package:intl/intl.dart';
import 'package:obatin/importPackage.dart';

class RegisterBiodata extends StatefulWidget {
  final int? callID;
  final int? userType;
  final TextEditingController? txtboxEmail;
  final TextEditingController? txtboxPassword;
  final TextEditingController? txtboxPConfirm;
  final TextEditingController? txtboxNamaLengkap;
  final TextEditingController? txtboxTanggalLahir;
  final TextEditingController? txtboxTempatLahir;
  final TextEditingController? txtboxNoSTR;
  final TextEditingController? txtboxBerlakuDari;
  final TextEditingController? txtboxBerlakuSampai;
  RegisterBiodata(
      {@required this.callID,
      @required this.userType,
      @required this.txtboxEmail,
      @required this.txtboxPassword,
      @required this.txtboxPConfirm,
      @required this.txtboxNamaLengkap,
      @required this.txtboxTanggalLahir,
      @required this.txtboxTempatLahir,
      @required this.txtboxNoSTR,
      @required this.txtboxBerlakuDari,
      @required this.txtboxBerlakuSampai});

  @override
  _RegisterBiodataState createState() => _RegisterBiodataState();
}

class _RegisterBiodataState extends State<RegisterBiodata> {
  bool isEmailValid = false;
  var buttonText = "Next";
  bool fullscreen = false;
  int indexNow = 1;
  final controller = new PageController(initialPage: 0, keepPage: true);
  List<String> radioValue = ["Ya", "Tidak"];
  String radioInitAddressNow = "Ya";
  String radioInitGelarDpn = "Tidak";
  String radioInitGelarBlk = "Tidak";
  bool showGelarDpn = false;
  bool showGelarBlk = false;
  bool showAddressNow = false;
  bool showNextButton = false;
  String btnNextText = "Selanjutnya";
  void pageChanged(int index) {
    setState(() {
      indexNow = index + 1;
    });
  }

  nextButton() {
    controller.animateToPage(indexNow,
        duration: Duration(milliseconds: 500), curve: Curves.ease);
  }

  Future handleBack() async {
    return CustomAlertDialog(context,
        title: Str().cancel,
        contentText: Str().allchangenotbesave,
        txtButton1: Str().yes,
        txtButton2: Str().cancel, funcButton1: () {
      Navigator.of(context).pop();
      goToPage(context, pushReplace: true, page: LoginMain());
    });
  }

  @override
  Widget build(BuildContext context) {
    return widget.userType == 3
        ? biodataFormPenjual()
        : biodataFormDokterNakes();
  }

  biodataFormDokterNakes() {
    return WillPopScope(
      onWillPop: () async {
        handleBack();
        return false;
      },
      child: SingleChildScrollView(
        child: Center(
          child: Container(
            width: getWidth(context, 1),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.1),
                vertical: getWidth(context, 0.05)),
            child: Column(
              children: <Widget>[
                CustomText(
                    txt: widget.callID == 1
                        ? "Buat Akun HEROES Baru"
                        : "Lengkapi Data HEROES mu",
                    bold: true,
                    size: 0.05,
                    spacing: 1),
                setHSpacing(getWidth(context, 0.06)),
                if (widget.callID == 1)
                  Column(
                    children: [
                      CustomTextField(
                        hint: "Email",
                        suffixIcon: widget.txtboxEmail!.text.isEmpty
                            ? null
                            : isEmailValid
                                ? Icons.check_circle_outline
                                : Icons.block_outlined,
                        suffixIconColor:
                            isEmailValid ? Colors.green : Colors.red.shade300,
                        onChanged: () => {
                          setState(() {
                            isEmailValid = EmailValidator.validate(
                                widget.txtboxEmail!.text.isEmpty
                                    ? ""
                                    : widget.txtboxEmail!.text);
                          })
                        },
                        keyboardtype: TextInputType.emailAddress,
                        controller: widget.txtboxEmail,
                      ),
                      passwordFieldValidator(
                          context, Str().password, widget.txtboxPassword!),
                      passwordFieldValidator(context, Str().passwordconfirm,
                          widget.txtboxPConfirm!,
                          txtId: 2, targetTxtbox: widget.txtboxPassword!),
                      setHSpacing(getWidth(context, 0.04)),
                      Divider(),
                      setHSpacing(getWidth(context, 0.04)),
                    ],
                  ),
                if (widget.callID == 1)
                  CustomTextField(
                    hint: "Nama Lengkap",
                    controller: widget.txtboxNamaLengkap,
                  ),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
                  child: Row(
                    children: [
                      CustomText(width: 0.35, txt: "Gelar Depan?"),
                      RadioGroup<String>.builder(
                        groupValue: radioInitGelarDpn,
                        direction: Axis.horizontal,
                        onChanged: (value) => setState(() {
                          radioInitGelarDpn = value!;
                          if (value == "Ya") {
                            showGelarDpn = true;
                          } else {
                            showGelarDpn = false;
                          }
                        }),
                        items: radioValue,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                    ],
                  ),
                ),
                if (showGelarDpn == true) CustomTextField(hint: "Gelar Depan"),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
                  child: Row(
                    children: [
                      CustomText(width: 0.35, txt: "Gelar Belakang?"),
                      RadioGroup<String>.builder(
                        groupValue: radioInitGelarBlk,
                        direction: Axis.horizontal,
                        onChanged: (value) => setState(() {
                          radioInitGelarBlk = value!;
                          if (value == "Ya") {
                            showGelarBlk = true;
                          } else {
                            showGelarBlk = false;
                          }
                        }),
                        items: radioValue,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                    ],
                  ),
                ),
                if (showGelarBlk == true)
                  CustomTextField(hint: "Gelar Belakang"),
                if (widget.callID == 1)
                  Column(
                    children: [
                      CustomTextField(
                        hint: "Tanggal Lahir",
                        suffixIcon: Icons.date_range_outlined,
                        controller: widget.txtboxTanggalLahir,
                        readonly: true,
                        onTap: () {
                          DatePicker.showDatePicker(context,
                              showTitleActions: true,
                              minTime: DateTime(1940, 1, 1),
                              maxTime: DateTime.now(), onConfirm: (date) {
                            widget.txtboxTanggalLahir!.text =
                                DateFormat('yyyy-MM-dd').format(date);
                          },
                              theme: DatePickerTheme(
                                  containerHeight: getWidth(context, 0.7),
                                  itemHeight: getWidth(context, 0.2),
                                  headerColor: bgColor(),
                                  backgroundColor: bgColor(),
                                  itemStyle: TextStyle(
                                      color: fontColor1(),
                                      fontSize: getWidth(context, 0.036)),
                                  doneStyle: TextStyle(
                                      color: defaultcolor1,
                                      fontSize: getWidth(context, 0.038)),
                                  cancelStyle: TextStyle(
                                      color: Colors.red,
                                      fontSize: getWidth(context, 0.038))),
                              currentTime: DateTime.now()
                                  .subtract(new Duration(days: 5475)),
                              locale: LocaleType.id);
                        },
                      ),
                      CustomTextField(
                        hint: "Tempat Lahir",
                        controller: widget.txtboxTempatLahir,
                      ),
                    ],
                  ),
                Align(
                  alignment: Alignment.centerLeft,
                  child: CustomText(
                    botpad: 0.03,
                    toppad: 0.08,
                    txt: "Surat Tanda Registrasi",
                    bold: true,
                    size: 0.035,
                  ),
                ),
                CustomTextField(
                  hint: "No STR",
                  controller: widget.txtboxNoSTR,
                ),
                CustomTextField(
                  hint: "Berlaku Dari",
                  controller: widget.txtboxBerlakuDari,
                ),
                CustomTextField(
                    hint: "Berlaku Sampai",
                    controller: widget.txtboxBerlakuSampai),
                CustomText(
                  botpad: 0.02,
                  toppad: 0.04,
                  txt: "Mohon untuk melengkapi data diatas",
                  color: Colors.red,
                  size: 0.035,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  biodataFormPenjual() {
    return WillPopScope(
      onWillPop: () async {
        handleBack();
        return false;
      },
      child: SingleChildScrollView(
        child: Center(
          child: Container(
            width: getWidth(context, 1),
            padding: EdgeInsets.symmetric(
                horizontal: getWidth(context, 0.1),
                vertical: getWidth(context, 0.05)),
            child: Column(
              children: <Widget>[
                CustomText(
                    txt: "Lengkapi Data", bold: true, size: 0.05, spacing: 1),
                setHSpacing(getWidth(context, 0.06)),
                CustomTextField(hint: "Nama Lengkap Penanggung Jawab"),
                CustomDynamicDropDown(
                  context,
                  hint: "Jenis Kartu Identitas Penanggung Jawab",
                  showIfCondition: true,
                  contentWidget: SizedBox(),
                  dialogTitle: "Title",
                ),
                CustomTextField(hint: "Nomor Kartu Identitas Penanggung Jawab"),
                CustomTextField(hint: "Alamat Sesuai Kartu Identitas"),
                Padding(
                  padding:
                      EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
                  child: Row(
                    children: [
                      CustomText(
                          width: 0.4,
                          txt: "Alamat Sekarang Sama dengan Kartu Identitas?"),
                      RadioGroup<String>.builder(
                        groupValue: radioInitAddressNow,
                        direction: Axis.horizontal,
                        onChanged: (value) => setState(() {
                          radioInitAddressNow = value!;
                          if (value == "Ya") {
                            showAddressNow = false;
                          } else {
                            showAddressNow = true;
                          }
                        }),
                        items: radioValue,
                        itemBuilder: (item) => RadioButtonBuilder(
                          item,
                        ),
                      ),
                    ],
                  ),
                ),
                if (showAddressNow == true)
                  CustomTextField(hint: "Alamat Sekarang"),
                CustomTextField(hint: "Nama MITRA (Apotek/Toko/Kantor)"),
                CustomTextField(hint: "Alamat MITRA"),
                CustomTextField(hint: "Nomor Telepon MITRA"),
                CustomTextField(hint: "Email MITRA"),
                Row(
                  children: [
                    CustomDynamicDropDown(
                      context,
                      hint: "Bank",
                      width: 0.18,
                      showIfCondition: true,
                      contentWidget: SizedBox(),
                      dialogTitle: "Title",
                    ),
                    setWSpacing(getWidth(context, 0.04)),
                    CustomTextField(
                      hint: "Nomor Rekening MITRA",
                      width: 0.58,
                    ),
                  ],
                ),
                CustomDynamicDropDown(
                  context,
                  hint: "Jenis MITRA",
                  showIfCondition: true,
                  contentWidget: SizedBox(),
                  dialogTitle: "Title",
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
