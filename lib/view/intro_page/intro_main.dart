import 'package:obatin/importPackage.dart';

class IntroMain extends StatefulWidget {
  @override
  _IntroMainState createState() => _IntroMainState();
}

class _IntroMainState extends State<IntroMain> {
  bool finishBtn = false;
  bool showSkipBtn = true;
  int indexNow = 1;
  final controller = new PageController(initialPage: 0, keepPage: true);
  @override
  void initState() {
    super.initState();
  }

  void pageChanged(int index) {
    setState(() {
      indexNow = index + 1;
      if (indexNow == 3) {
        finishBtn = true;
        showSkipBtn = false;
      } else {
        finishBtn = false;
        showSkipBtn = true;
      }
    });
  }

  nextFunc() {
    if (indexNow == 3) {
      goToPage(context, pushReplace: true, page: LoginMain());
    } else {
      controller.animateToPage(indexNow,
          duration: Duration(milliseconds: 500), curve: Curves.ease);
    }
  }

  skipFunc() {
    goToPage(context, pushReplace: true, page: LoginMain());
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) =>
          AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle(
            statusBarColor: Colors.transparent,
            statusBarIconBrightness:
                themeID == 1 ? Brightness.dark : Brightness.light),
        child: Scaffold(
            body: Stack(
          children: <Widget>[
            AnimatedContainer(
              duration: Duration(milliseconds: 500),
              curve: Curves.fastOutSlowIn,
              color: bgColor(),
              child: PageView(
                controller: controller,
                onPageChanged: (index) {
                  pageChanged(index);
                },
                children: [
                  introSlide(context, ImgAsset.imgIntro1, Str().intro1title,
                      Str().intro1desc),
                  introSlide(context, ImgAsset.imgIntro2, Str().intro2title,
                      Str().intro2desc),
                  introSlide(context, ImgAsset.imgIntro3, Str().intro3title,
                      Str().intro3desc)
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      SmoothPageIndicator(
                        controller: controller,
                        count: 3,
                        effect: ExpandingDotsEffect(
                            dotColor: Colors.blue.shade50,
                            activeDotColor: accentColor),
                      ),
                      CustomButton(
                          styleId: 1,
                          btnText: finishBtn ? Str().finish : Str().next,
                          onPressed: () => nextFunc()),
                      showSkipBtn
                          ? CustomButton(
                              styleId: 4,
                              btnText: Str().skip,
                              height: 0.11,
                              onPressed: () => skipFunc())
                          : SizedBox(
                              height: getWidth(context, 0.11),
                            ),
                    ],
                  )),
            ),
            SwitchLanguageButton(),
            SwitchThemeButton(),
          ],
        )),
      ),
    );
  }

  introSlide(BuildContext context, var imgName, String text1, String text2) {
    return Column(
      children: <Widget>[
        introHeader(context, imgName),
        Center(
          child: Container(
            width: getWidth(context, 0.5),
            child: ImgAsset.imgObatin,
          ),
        ),
        SizedBox(
          height: getWidth(context, 0.04),
        ),
        CustomText(
          width: 0.9,
          textAlign: TextAlign.center,
          txt: text1,
          bold: true,
          color: defaultcolor1,
          size: 0.036,
        ),
        setHSpacing(getWidth(context, 0.02)),
        CustomText(
          width: 0.8,
          textAlign: TextAlign.center,
          txt: text2,
          size: 0.035,
        ),
      ],
    );
  }
}
