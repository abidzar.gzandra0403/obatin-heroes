import 'package:obatin/importPackage.dart';

introHeader(BuildContext context, Widget imgName) {
  return Container(
      width: getHeight(context, 1),
      height: getHeight(context, 0.55),
      child: FittedBox(fit: BoxFit.fitWidth, child: imgName));

  //if use clippath
  // ClipPath(
  //   clipper: CurveClipper1(),
  //   child: Container(
  //       width: getHeight(context, 1),
  //       height: getHeight(context, 0.55),
  //       child: FittedBox(fit: BoxFit.cover, child: setImg(imgName))),
  // );
}
