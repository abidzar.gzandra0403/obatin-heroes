import 'package:obatin/controller/bloc/service_bloc/deskripsi_nakes_blog.dart';
import 'package:obatin/importPackage.dart';

String homedeskripsi = "";

class ProfileChangeDescription extends StatefulWidget {
  @override
  _ProfileChangeDescriptionState createState() =>
      _ProfileChangeDescriptionState();
}

class _ProfileChangeDescriptionState extends State<ProfileChangeDescription> {
  // init state ini dipanggil pertama kali oleh flutter
  TextEditingController deskripsiController = TextEditingController();
  String deskripsi = "";
  @override
  initState() {
    super.initState();
    // baca Shared Preferences
    getFromSharedPreferences();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
        builder: (context, darkmodeState) => Scaffold(
              backgroundColor: bgColor(),
              appBar: CustomAppBar(
                context,
                Str().setting$changedesc,
                true,
                rightWidget: CustomButton(
                    styleId: 3,
                    btnText: Str().save,
                    suffixIcon: Icons.check_outlined,
                    onPressed: () {
                      if (deskripsiController.text.isEmpty) {
                        CustomBottomDialog(context, txt: "Mohon Lengkapi Form");
                      }

                      setIntoSharedPreferences();
                      context.read<DeskripsiBloc>().add(
                            CreateDeskripsi(
                              context,
                              deskripsi: deskripsiController.text,
                            ),
                          );
                      getFromSharedPreferences();

                      Navigator.pop(context);
                      CustomBottomDialog(context, txt: Str().saved);
                    }),
              ),
              body: SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                      horizontal: defaultPaddingContent(context)),
                  child: Column(
                    children: [
                      CustomTextField(
                        hint: Str().setting$yourdescription,
                        controller: deskripsiController,
                        maxlines: 20,
                        maxLength: 150,
                      ),
                      Padding(padding: EdgeInsets.only(top: 4)),
                      CustomText(
                          textAlign: TextAlign.start,
                          txt: Str().setting$descriptioninfo)
                    ],
                  ),
                ),
              ),
            ));
  }

  // method ini berfungsi untuk memasukkan data ke dalam SharedPreferences
  void setIntoSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    await prefs.setString("deskripsi", deskripsiController.text);
  }

  // Method ini berfungsi untuk mengambil data deskripsi dan Password dari SharedPreferences
  // kemudian dimasukkan ke variable deskripsi dan password
  void getFromSharedPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String initDesc = prefs.getString("deskripsi") != null
        ? prefs.getString("deskripsi")!
        : '';

    setState(() {
      deskripsiController.text = initDesc;
      deskripsi = initDesc;
      homedeskripsi = initDesc;
    });
  }
}
