import 'package:obatin/importPackage.dart';

profileUserInfo(
  BuildContext context,
) {
  return BlocBuilder<SQFLITEUserDataBloc, SQFLITEUserDataState>(
    builder: (context, state) => GestureDetector(
      onTap: () {
        goToPage(context, page: KenalinMain());
      },
      child: Container(
        color: bgColor(),
        padding: EdgeInsets.only(
            bottom: getWidth(context, 0.05),
            left: getWidth(context, 0.1),
            right: getWidth(context, 0.1)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                CircleAvatar(
                  radius: getWidth(context, 0.07),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(999),
                    child: ImgAsset.profilePhotoExample,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(left: getWidth(context, 0.02)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        width: 0.5,
                        txt: postProceedData_userData[0]['nama'],
                        bold: true,
                        size: 0.045,
                        color: defaultcolor1,
                      ),
                      CustomText(
                          width: 0.5,
                          txt: postProceedData_userData[0]['email'],
                          size: 0.035),
                    ],
                  ),
                ),
              ],
            ),
            Icon(
              Icons.open_in_new_outlined,
              color: fontColor1(),
            )
          ],
        ),
      ),
    ),
  );
}
