import 'package:obatin/importPackage.dart';

profilePasswordField(
  BuildContext context,
  String hint,
  TextEditingController txtController, {
  TextEditingController? targetTxtbox,
  int txtId = 1,
  bool? eyeIcon,
}) {
  fieldStyle(BuildContext context,
      {int? maxlines,
      bool? readonly,
      bool? obsecure,
      TextInputType? inputtype,
      TextInputAction? inputAction}) {
    return TextField(
      keyboardType: inputtype != null ? inputtype : TextInputType.text,
      obscureText: obsecure != null ? obsecure : false,
      readOnly: readonly != null ? readonly : false,
      minLines: 1,
      maxLines: maxlines != null ? maxlines : 1,
      textInputAction: inputAction != null ? inputAction : TextInputAction.done,
      style: hnnormal(context, txtSize: getWidth(context, 0.04)),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(getWidth(context, 0.03)),
        border: InputBorder.none,
        focusedBorder: InputBorder.none,
        enabledBorder: InputBorder.none,
        errorBorder: InputBorder.none,
        disabledBorder: InputBorder.none,
      ),
    );
  }

  return BlocBuilder<PasswordCheckerBloc, PasswordCheckerState>(
    builder: (context, state) => Container(
      margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
      child: Stack(
        children: [
          Container(
            constraints: BoxConstraints(minHeight: getWidth(context, 0.12)),
            margin: EdgeInsets.only(top: getWidth(context, 0.015)),
            decoration: BoxDecoration(
                border: Border.all(
                    width: getWidth(context, 0.006), color: borderColor()),
                borderRadius: BorderRadius.circular(10)),
            child: TextField(
              controller: txtController,
              obscureText: true,
              onChanged: (text) {
                context.read<PasswordCheckerBloc>().add(Check(
                    txtboxPassword: txtController.text,
                    txtboxPasswordConfirm: targetTxtbox!.text));
              },
              style: hnnormal(context,
                  txtSize: getWidth(context, 0.04),
                  txtColor: (txtId == 2 && state.warning == true)
                      ? Colors.red
                      : Colors.black),
              decoration: InputDecoration(
                border: InputBorder.none,
                focusedBorder: InputBorder.none,
                enabledBorder: InputBorder.none,
                errorBorder: InputBorder.none,
                disabledBorder: InputBorder.none,
                contentPadding: EdgeInsets.all(getWidth(context, 0.03)),
                hintStyle: TextStyle(
                    letterSpacing: 0,
                    color: (txtId == 2 && state.warning == true)
                        ? Colors.red
                        : Colors.black),
                suffixIcon: (txtId == 2 && state.warning == true)
                    ? Container(
                        width: getWidth(context, 0.25),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              Str().notmatch,
                              style: hnnormal(context, txtColor: Colors.red),
                            ),
                            Icon(
                              Icons.warning_amber_rounded,
                              color: Colors.red,
                            )
                          ],
                        ),
                      )
                    : null,
              ),
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: getWidth(context, 0.02)),
            padding: EdgeInsets.symmetric(horizontal: getWidth(context, 0.01)),
            decoration: BoxDecoration(color: bgColor()),
            child: CustomText(txt: hint, color: defaultcolor5, bold: true),
          ),
        ],
      ),
    ),
  );
}
