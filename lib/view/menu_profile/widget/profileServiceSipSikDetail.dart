import 'package:obatin/importPackage.dart';

profileServiceSipSikDetail(
    BuildContext context, String nosip, int callId, bool onVerificated,
    {@required String? idSip,
    @required int? dataid,
    String? spesialisasi,
    String? alamat,
    String? berlakusampai,
    String? berlakudari,
    String? propinsiberlaku,
    String? kotaberlaku}) {
  expanded1Content(
      String? idSip,
      String? nosip,
      String? spesialisasi,
      String? alamat,
      String? berlakusampai,
      String? berlakudari,
      String? propinsiberlaku,
      String? kotaberlaku,
      int callId,
      {@required int? dataid,
      bool? onVerificated}) {
    row(int sipNum) {
      return Padding(
        padding: EdgeInsets.symmetric(vertical: getWidth(context, 0.01)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CustomText(
                txt: sipNum == 1 ? Str().specialization : Str().valoduntil,
                size: 0.036,
                bold: true,
                color: fontColor1()),
            CustomText(
                txt: sipNum == 1
                    ? spesialisasi!
                    : berlakusampai.toString().substring(0, 10),
                size: 0.035,
                color: fontColor1()),
          ],
        ),
      );
    }

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        if (spesialisasi != null) row(1),
        if (alamat != null) row(2),
        if (onVerificated == null || onVerificated == true)
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomButton(
                styleId: 3,
                shapeId: 2,
                width: 0.55,
                btnText: Str().edit,
                onPressed: () => goToPage(context,
                    page: ProfileServiceSipEditForm(
                      idSip: idSip,
                      dataid: dataid,
                      title: "Update SIP",
                      callId: 1,
                      nosip: nosip,
                      berlakudari: berlakudari,
                      berlakusampai: berlakusampai,
                      propinsiberlaku: propinsiberlaku,
                      kotaberlaku: kotaberlaku,
                    )),
              ),
              CustomButton(
                  styleId: 3,
                  shapeId: 2,
                  width: 0.26,
                  bColor: Color.fromARGB(100, 255, 0, 0),
                  fColor: Colors.white,
                  prefixIcon: Icons.delete_outline_outlined,
                  btnText: Str().delete,
                  onPressed: () async {
                    await CustomAlertDialog(context,
                        contentText: Str().setting$deleteconfirmdialog,
                        txtButton1: Str().delete,
                        txtButton2: Str().cancel,
                        funcButton1: () => context.read<SipSikBloc>().add(
                            DeleteSipSik(context,
                                idSip: idSip, dataId: dataid)));
                  }),
            ],
          ),
      ],
    );
  }

  return Container(
    margin: EdgeInsets.symmetric(vertical: getWidth(context, 0.012)),
    decoration: BoxDecoration(
        color: bgColorDarken(),
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
            width: getWidth(context, 0.005), color: borderBlueColor())),
    padding: EdgeInsets.all(getWidth(context, 0.02)),
    child: ExpandablePanel(
        theme: ExpandableThemeData(iconColor: defaultcolor2),
        header: Row(
          children: [
            CustomText(
                txt: "No.SIP " + nosip,
                size: 0.035,
                color: fontColor1(),
                bold: true),
            if (onVerificated == false)
              Padding(
                padding: EdgeInsets.only(left: getWidth(context, 0.02)),
                child: CustomText(
                    txt: Str().setting$verificationprocess,
                    bold: true,
                    size: 0.035,
                    color: defaultcolor1),
              ),
          ],
        ),
        collapsed: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            if (spesialisasi != null)
              CustomText(txt: spesialisasi, color: fontColor1()),
            if (berlakusampai != null)
              CustomText(txt: berlakusampai, color: fontColor1()),
          ],
        ),
        expanded: expanded1Content(idSip, nosip, spesialisasi, alamat,
            berlakusampai, berlakudari, propinsiberlaku, kotaberlaku, callId,
            dataid: dataid, onVerificated: onVerificated)),
  );
}
