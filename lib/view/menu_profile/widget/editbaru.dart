// import 'package:obatin/importPackage.dart';

// // ignore: must_be_immutable
// class ProfileAddressAddEdit extends StatefulWidget {
//   final int? callID;
//   final String? alamat;
//   final String? alias;
//   final String? poi;
//   final String? kabupaten;
//   final String? provinsi;
//   final String? idAddress;
//   final String? long;
//   final String? lat;
//   final String? nosip;
//   ProfileAddressAddEdit(
//       {this.alamat,
//       this.callID,
//       this.alias,
//       this.kabupaten,
//       this.provinsi,
//       this.poi,
//       this.idAddress,
//       this.long,
//       this.lat,
//       this.nosip});

//   @override
//   _ProfileAddressAddEditState createState() => _ProfileAddressAddEditState();
// }

// class _ProfileAddressAddEditState extends State<ProfileAddressAddEdit> {
//   TextEditingController txtcAlamat = new TextEditingController();
//   TextEditingController txtcPoi = new TextEditingController();
//   TextEditingController txtcnosip = new TextEditingController();
//   String? provinsi;
//   String? kota;
//   String? long;
//   String? lat;
//   String? markAs;
//   bool markAsHome = false;
//   bool markAsOffice = false;
//   String? buttonText;
//   @override
//   void initState() {
//     super.initState();
//     initValue();
//   }

//   initValue() {
//     buttonText = widget.callID == 1 ? Str().save : "Update";
//     if (widget.callID != null && widget.callID == 2) {
//       txtcAlamat.text = widget.alamat!;
//       txtcPoi.text = widget.poi!;
//       txtcnosip.text = postProceedData_listSIP[0]["nosip"];
//       kota = widget.kabupaten!;
//       provinsi = widget.provinsi!;
//       long = widget.long!;
//       lat = widget.lat!;
//       widget.alias == "Rumah" ? markAsHome = true : markAsOffice = true;
//       print(kota! + ", " + provinsi! + ", " + long! + ", " + lat!);
//     }
//   }

//   saveupdateFunc() async {
//     if (txtcAlamat.text.isEmpty ||
//         (markAsHome == false && markAsOffice == false)) {
//       CustomBottomDialog(context,
//           txt: Str().pleaseinputform, prefixIcon: Icons.info_outline);
//     } else {
//       String alias = markAsHome ? "Rumah" : "Kantor";
//       if (widget.callID == 1) {
//         // context.read<AddressBloc>().add(CreateAddress(context,
//         //     defaultAddress: false,
//         //     alias: alias,
//         //     alamat: txtcAlamat.text,
//         //     kota: kota,
//         //     provinsi: provinsi,
//         //     poi: txtcPoi.text,
//         //     latitude: lat,
//         //     longitude: long));
//       } else {
//         // context.read<AddressBloc>().add(UpdateAddress(context,
//         //     idAddress: widget.idAddress,
//         //     defaultAddress: false,
//         //     alias: alias,
//         //     alamat: txtcAlamat.text,
//         //     kota: kota,
//         //     provinsi: provinsi,
//         //     poi: txtcPoi.text,
//         //     latitude: lat,
//         //     longitude: long));
//       }
//     }
//   }

//   deleteFunc() async {
//     CustomAlertDialog(context,
//         contentText: "Yakin ingin menghapus alamat?",
//         txtButton1: "Hapus",
//         txtButton2: "Batal", funcButton1: () {
//       // context
//       //     .read<AddressBloc>()
//       //     .add(DeleteAddress(context, idAddress: widget.idAddress));
//     });
//   }

//   saveButton() {
//     return Row(
//       children: [
//         if (widget.callID == 2)
//           CustomButton(
//             styleId: 3,
//             onPressed: () => saveupdateFunc(),
//             btnText: buttonText,
//             prefixIcon: Icons.check,
//           ),
//       ],
//     );
//   }

//   markAsButton(int? btnId) {
//     String title = btnId == 1 ? "Rumah" : "Kantor";

//     return CustomButton(
//         styleId: (btnId == 1 && markAsHome == true ||
//                 btnId == 2 && markAsOffice == true)
//             ? 1
//             : 3,
//         shapeId: 2,
//         hormargin: 0.01,
//         onPressed: () {
//           if (btnId == 1) {
//             markAs = title;
//             setState(() {
//               markAsHome = true;
//               markAsOffice = false;
//             });
//           } else {
//             markAs = title;
//             setState(() {
//               markAsHome = false;
//               markAsOffice = true;
//             });
//           }
//         },
//         btnText: title);
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: bgColor(),
//       appBar: CustomAppBar(context, "Alamat", true, rightWidget: saveButton()),
//       body: SingleChildScrollView(
//         padding:
//             EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: [
//             // CustomButton(
//             //   styleId: 3,
//             //   shapeId: 2,
//             //   onPressed: () => goToPage(context, page: OpenStreetMapMain(
//             //     onGetLocation: (address, province, city, kecamatan, kelurahan,
//             //         postalCode, longitude, latitude) {
//             //       // print(address + " " + province);
//             //       setState(() {
//             //         kota = city;
//             //         provinsi = province;
//             //         long = longitude;
//             //         lat = latitude;

//             //         txtcAlamat.text = address +
//             //             ', ' +
//             //             kelurahan +
//             //             ', ' +
//             //             kecamatan +
//             //             ', ' +
//             //             city +
//             //             ', ' +
//             //             province +
//             //             ', ' +
//             //             postalCode;
//             //       });
//             //     },
//             //   )),
//             //   btnText: "Pilih Lokasi",
//             //   prefixIcon: Icons.location_pin,
//             // ),
//             // CustomTextField(
//             //   hint: "Alamat",
//             //   hint2: "Pilih Lokasi...",
//             //   readonly: true,
//             //   controller: txtcAlamat,
//             //   minHeight: 0.3,
//             //   maxlines: 5,
//             // ),
//             CustomTextField(
//               hint: "No SIP",
//               controller: txtcnosip,
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
