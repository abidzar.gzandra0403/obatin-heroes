import 'package:obatin/importPackage.dart';

class ProfileService extends StatefulWidget {
  @override
  _ProfileServiceState createState() => _ProfileServiceState();
}

class _ProfileServiceState extends State<ProfileService> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: Scaffold(
          backgroundColor: bgColor(),
          appBar: CustomAppBar(context, Str().setting$service, true),
          body: Stack(
            children: [
              Align(
                alignment: Alignment.topCenter,
                child: SizedBox(
                  height: getWidth(context, 0.4),
                  child: ImgAsset.imgSettings,
                ),
              ),
              SingleChildScrollView(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(
                      getWidth(context, 0.06),
                      getWidth(context, 0.4),
                      getWidth(context, 0.06),
                      getWidth(context, 0.06)),
                  child: Container(
                    color: bgColor(),
                    child: BlocBuilder<SQFLITESipSikBloc, SQFLITESipSikState>(
                      builder: (context, state) => Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            textstyle(Str().setting$licensenumber),
                            if (postProceedData_listSIP.isEmpty)
                              CustomText(
                                  toppad: 0.02,
                                  botpad: 0.02,
                                  txt: Str().setting$addlicensenotes),
                            ListView.builder(
                              shrinkWrap: true,
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: postProceedData_listSIP.length,
                              itemBuilder: (context, index) => profileServiceSipSikDetail(
                                  context, postProceedData_listSIP[index]["nosip"], 1, true,
                                  idSip: postProceedData_listSIP[index]
                                      ["idSip"],
                                  dataid: postProceedData_listSIP[index]
                                      ["dataid"],
                                  spesialisasi: "Rawat Inap Penyakit Dalam",
                                  alamat: postProceedData_listSIP[index]
                                      ["kotaberlaku"],
                                  berlakusampai: postProceedData_listSIP[index]
                                          ["berlakusampai"]
                                      .toString()
                                      .substring(0, 10),
                                  berlakudari: postProceedData_listSIP[index]
                                          ["berlakudari"]
                                      .toString()
                                      .substring(0, 10),
                                  propinsiberlaku:
                                      postProceedData_listSIP[index]
                                          ["propinsiberlaku"],
                                  kotaberlaku: postProceedData_listSIP[index]
                                      ["kotaberlaku"]),
                            ),
                            if (postProceedData_listSIP.isNotEmpty &&
                                    postProceedData_listSIP.length != 3 ||
                                postProceedData_listSIP.isEmpty)
                              addSipButton(),
                            textstyle(Str().setting$yourservice),
                            if (postProceedData_listSIP.isEmpty)
                              CustomText(
                                  toppad: 0.02,
                                  botpad: 0.02,
                                  txt:
                                      "Pengaturan layanan praktik anda akan muncul disini."),
                            ServicePracticeCard(
                              styleId: 1,
                            ),
                          ]),
                    ),
                  ),
                ),
              ),
            ],
          )),
    );
  }

  addSipButton() {
    return Center(
        child: CustomButton(
      styleId: 3,
      shapeId: 2,
      onPressed: () => Navigator.push(
          context, SlideRightRoute(page: ProfileServiceSipForm(callId: 1))),
      btnText: Str().setting$addlicense,
      prefixIcon: Icons.add,
      fColor: defaultcolor1,
    ));
  }

  textstyle(String txt) {
    return Padding(
      padding: EdgeInsets.only(top: getWidth(context, 0.05)),
      child: CustomText(
        txt: txt,
        bold: true,
        color: defaultcolor1,
        size: 0.037,
      ),
    );
  }
}
