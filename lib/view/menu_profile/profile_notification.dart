import 'package:obatin/importPackage.dart';

class ProfileNotification extends StatefulWidget {
  @override
  _ProfileNotificationState createState() => _ProfileNotificationState();
}

class _ProfileNotificationState extends State<ProfileNotification> {
  bool notifObatin = false;
  bool notifPromo = false;
  bool notifUpdateFitur = false;
  switchFunc(int btnId) {
    setState(() {
      if (btnId == 1) {
        notifObatin = !notifObatin;
      } else if (btnId == 2) {
        notifPromo = !notifPromo;
      } else {
        notifUpdateFitur = !notifUpdateFitur;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().setting$notificationsetting, true),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(
            horizontal: defaultPaddingContent(context),
            vertical: getWidth(context, 0.04)),
        child: Column(
          children: [
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(1),
                verpad: 0.02,
                btnText: Str().setting$obatinnotif,
                btnText2: Str().setting$obatinnotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(1), value: notifObatin)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(2),
                verpad: 0.02,
                btnText: Str().setting$obatinnotif,
                btnText2: Str().setting$obatinnotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(2), value: notifPromo)),
            CustomButton(
                styleId: 5,
                onPressed: () => switchFunc(3),
                verpad: 0.02,
                btnText: Str().setting$updatenotif,
                btnText2: Str().setting$updatenotifdesc,
                suffixWidget: CustomSwitch(
                    onToggle: () => switchFunc(3), value: notifUpdateFitur))
          ],
        ),
      ),
    );
  }
}
