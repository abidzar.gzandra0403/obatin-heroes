import 'package:obatin/importPackage.dart';

class ProfileServiceSipEditForm extends StatefulWidget {
  final String? idSip;
  final int? dataid;
  final String? title;
  final String? nosip;
  final String? berlakudari;
  final String? berlakusampai;
  final String? propinsiberlaku;
  final String? kotaberlaku;
  final int? callId;
  ProfileServiceSipEditForm(
      {@required this.idSip,
      @required this.dataid,
      @required this.nosip,
      @required this.berlakudari,
      @required this.berlakusampai,
      @required this.propinsiberlaku,
      @required this.kotaberlaku,
      @required this.title,
      @required this.callId});
  @override
  _ProfileServiceSipEditFormState createState() =>
      _ProfileServiceSipEditFormState();
}

class _ProfileServiceSipEditFormState extends State<ProfileServiceSipEditForm> {
  TextEditingController txtcnosip = new TextEditingController();
  TextEditingController txtcberlakudari = new TextEditingController();
  TextEditingController txtcberlakusampai = new TextEditingController();
  TextEditingController txtckotaberlaku = new TextEditingController();
  TextEditingController txtcpropinsiberlaku = new TextEditingController();
  List provinsiValue = ['', ''];
  List kotaValue = ['', ''];
  List kecamatanValue = ['', ''];
  List kelurahanValue = ['', ''];
  bool kotaReadonly = true;

  @override
  void initState() {
    super.initState();
    initValue();
  }

  initValue() {
    txtcnosip.text = widget.nosip!;
    txtcberlakudari.text = widget.berlakudari!;
    txtcberlakusampai.text = widget.berlakusampai!;
    provinsiValue[1] = widget.propinsiberlaku!;
    kotaValue[1] = widget.kotaberlaku!;
  }

  buttonFunc() async {
    context.read<SipSikBloc>().add(UpdateSipSik(context,
        dataid: widget.dataid,
        idSip: widget.idSip,
        nosip: txtcnosip.text,
        berlakudari: txtcberlakudari.text,
        berlakusampai: txtcberlakusampai.text,
        kotaberlaku: kotaValue[1],
        propinsiberlaku: provinsiValue[1]));
  }

  listViewContent({@required int? valueID, String? id}) {
    bool finishLoading = false;
    return BlocBuilder<RegionSelectorBloc, RegionSelectorState>(
      builder: (context, state) {
        loadingData() async {
          List load = valueID == 1
              ? state.provinsiList
              : valueID == 2
                  ? state.kabupatenList
                  : valueID == 3
                      ? state.kecamatanList
                      : state.kelurahanList;
          finishLoading = load.length > 0 ? true : false;
          print(finishLoading);

          return load;
        }

        return FutureBuilder(
            future: loadingData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (finishLoading == false) {
                return Center(child: SpinKitWave(color: defaultcolor2));
              } else if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    padding: EdgeInsetsDirectional.all(0),
                    itemBuilder: (context, index) => CustomButton(
                        mainAxisAlignment: MainAxisAlignment.start,
                        styleId: 1,
                        textWidth: 0.6,
                        bColor: Colors.transparent,
                        btnText: snapshot.data[index]['name'],
                        fColor: fontColor1(),
                        onPressed: () {
                          setState(() {
                            if (valueID == 1) {
                              provinsiValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                              kotaValue = ['', ''];
                              kecamatanValue = ['', ''];
                              kelurahanValue = ['', ''];
                              kotaReadonly = false;
                            } else if (valueID == 2) {
                              kotaValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                              kecamatanValue = ['', ''];
                              kelurahanValue = ['', ''];
                            } else if (valueID == 3) {
                              kecamatanValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                              kelurahanValue = ['', ''];
                            } else if (valueID == 4) {
                              kelurahanValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                            }
                          });
                          Navigator.pop(context);
                        }));
              } else {
                return Center(child: CustomText(txt: 'Loading'));
              }
            });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, widget.title!, true,
          rightWidget: CustomButton(
            styleId: 3,
            onPressed: () => buttonFunc(),
            btnText: "Update",
          )),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: widget.callId == 2
            ? CustomTextField(
                hint: "Email",
                // controller: txtcemail,
                maxlines: 5,
              )
            : widget.callId == 3
                ? CustomTextField(
                    hint: "No. NIK",
                    // controller: txtcnik,
                    maxlines: 5,
                  )
                : Column(
                    children: [
                      CustomTextField(
                        hint: "No.SIP",
                        controller: txtcnosip,
                        maxlines: 5,
                      ),
                      CustomDateField(
                          textEditingController: txtcberlakudari,
                          hint: Str().valodfrom),
                      CustomDateField(
                          textEditingController: txtcberlakusampai,
                          hint: Str().valoduntil),
                      CustomDynamicDropDown(
                        context,
                        onTapFunction: () => context
                            .read<RegionSelectorBloc>()
                            .add(SelectProvinsi()),
                        hint: Str().province,
                        height: 1,
                        value: provinsiValue[1],
                        dialogTitle: "Pilih Provinsi",
                        showIfCondition: true,
                        contentWidget: listViewContent(valueID: 1),
                      ),
                      CustomDynamicDropDown(
                        context,
                        hint: Str().city,
                        onTapFunction: () {
                          if (provinsiValue.length > 0) {
                            context.read<RegionSelectorBloc>().add(
                                SelectKabupaten(provinsiID: provinsiValue[0]));
                          }
                        },
                        showIfCondition: provinsiValue[0].toString().length > 0
                            ? true
                            : false,
                        ifConditionFalseText: "Pilih Provinsi Dahulu",
                        height: 1,
                        disableOnTap: kotaReadonly,
                        value: kotaValue[1],
                        dialogTitle: "Pilih Kota / Kabupaten",
                        contentWidget: listViewContent(valueID: 2),
                      ),
                    ],
                  ),
      ),
    );
  }
}
