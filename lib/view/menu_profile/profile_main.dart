import 'package:obatin/importPackage.dart';

class ProfileMain extends StatefulWidget {
  @override
  _ProfileMainState createState() => _ProfileMainState();
}

class _ProfileMainState extends State<ProfileMain> {
  bool allowreview = false;
  settingsBtn(
      {@required String? title,
      String? info,
      @required String? svgName,
      Widget? gotopage,
      VoidCallback? func,
      Widget? suffixWidget}) {
    return CustomButton(
        styleId: 5,
        verpad: 0.02,
        btnText: title,
        btnText2: info != null ? info : null,
        suffixWidget: suffixWidget != null ? suffixWidget : null,
        prefixWidget: CustomSVG(
          svgName: svgName,
          color: defaultcolor3,
          size: 0.06,
          hormargin: 0.02,
        ),
        onPressed: () {
          if (gotopage != null) {
            goToPage(context, page: gotopage);
          } else {
            func!();
          }
        });
  }

  allowratingSwitchFunc() {
    setState(() {
      allowreview = !allowreview;
    });
  }

  alertDialog() {
    button(IconData icon, String txt) {
      return CustomButton(
        styleId: 3,
        vermargin: 0.02,
        height: 0.1,
        onPressed: () {},
        prefixIcon: icon,
        btnText: txt,
      );
    }

    return CustomAlertDialog(context,
        title: Str().setting$changeprofilephoto,
        contentWidget: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            button(Icons.add_a_photo_outlined, Str().setting$opencamera),
            button(Icons.image_outlined, Str().setting$uploadphoto),
          ],
        ));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SettingsSwitchBloc, SettingsSwitchState>(
      builder: (context, darkmodeState) => Scaffold(
        appBar: CustomAppBar(context, Str().profile, true),
        backgroundColor: bgColor(),
        body: SingleChildScrollView(
          child: Column(
            children: [
              profileUserInfo(context),
              Container(
                margin: EdgeInsets.only(top: getWidth(context, 0.05)),
                child: SingleChildScrollView(
                  padding: EdgeInsets.only(bottom: getWidth(context, 0.02)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal: defaultPaddingContent(context)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(
                                txt: Str().setting$practiceNservicetitle,
                                bold: true,
                                color: Colors.grey),
                            settingsBtn(
                                title: Str().setting$practiceNservice,
                                info: Str().setting$practiceNservicedesc,
                                svgName: svg_iconpracticesetting,
                                gotopage: ProfileService()),
                            CustomText(
                                txt: Str().setting$userinfotitle,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            settingsBtn(
                                title: Str().setting$editdescription,
                                info: Str().setting$editdescriptiondesc,
                                svgName: svg_icondescription,
                                gotopage: ProfileChangeDescription()),
                            settingsBtn(
                                title: Str().setting$allowreviewNrating,
                                svgName: svg_iconreview,
                                func: () => allowratingSwitchFunc(),
                                suffixWidget: CustomSwitch(
                                    rightmargin: 0.03,
                                    onToggle: () => allowratingSwitchFunc(),
                                    value: allowreview)),
                            settingsBtn(
                                title: Str().setting$bankaccount,
                                info: Str().setting$bankaccountdesc,
                                svgName: svg_iconbankaccount,
                                gotopage: ProfileBankAccount()),
                            CustomText(
                                txt: Str().setting$accountsettingtitle,
                                bold: true,
                                color: Colors.grey,
                                toppad: 0.02),
                            settingsBtn(
                                title: Str().setting$notification,
                                info: Str().setting$notificationdesc,
                                svgName: svg_iconnotifsettings,
                                gotopage: ProfileNotification())
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
