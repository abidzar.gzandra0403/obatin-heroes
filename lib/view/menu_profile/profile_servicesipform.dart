import 'package:obatin/importPackage.dart';
import 'package:obatin/view/OSM/openStreetMapMain.dart';

class ProfileServiceSipForm extends StatefulWidget {
  final int? callId;
  final String? noSip;
  final int? dataid;
  final String? alamat;
  final String? berlakudari;
  final String? berlakusampai;
  ProfileServiceSipForm(
      {@required this.callId,
      this.noSip,
      this.dataid,
      this.alamat,
      this.berlakudari,
      this.berlakusampai});
  @override
  _ProfileServiceSipFormState createState() => _ProfileServiceSipFormState();
}

class _ProfileServiceSipFormState extends State<ProfileServiceSipForm> {
  TextEditingController txtcNomorSip = new TextEditingController();
  TextEditingController txtcberlakudari = new TextEditingController();
  TextEditingController txtcberlakusampai = new TextEditingController();
  List provinsiValue = ['', ''];
  List kotaValue = ['', ''];
  bool kotaReadonly = true;

  @override
  void initState() {
    super.initState();
  }

  initValue() {}

  buttonFunc() async {
    if (txtcNomorSip.text.isEmpty ||
        provinsiValue.isEmpty ||
        kotaValue.isEmpty) {
      CustomBottomDialog(context, txt: "Mohon Lengkapi Form");
    } else {
      context.read<SipSikBloc>().add(
            CreateSipSik(context,
                nosip: txtcNomorSip.text,
                berlakudari: txtcberlakudari.text,
                berlakusampai: txtcberlakusampai.text,
                kotaberlaku: kotaValue[1],
                propinsiberlaku: provinsiValue[1]),
          );
    }
  }

  listViewContent({@required int? valueID, String? id}) {
    bool finishLoading = false;
    return BlocBuilder<RegionSelectorBloc, RegionSelectorState>(
      builder: (context, state) {
        loadingData() async {
          List load = valueID == 1
              ? state.provinsiList
              : valueID == 2
                  ? state.kabupatenList
                  : valueID == 3
                      ? state.kecamatanList
                      : state.kelurahanList;
          finishLoading = load.length > 0 ? true : false;
          print(finishLoading);

          return load;
        }

        return FutureBuilder(
            future: loadingData(),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (finishLoading == false) {
                return Center(child: SpinKitWave(color: defaultcolor2));
              } else if (snapshot.hasData) {
                return ListView.builder(
                    itemCount: snapshot.data.length,
                    padding: EdgeInsetsDirectional.all(0),
                    itemBuilder: (context, index) => CustomButton(
                        mainAxisAlignment: MainAxisAlignment.start,
                        styleId: 1,
                        textWidth: 0.6,
                        bColor: Colors.transparent,
                        btnText: snapshot.data[index]['name'],
                        fColor: fontColor1(),
                        onPressed: () {
                          setState(() {
                            if (valueID == 1) {
                              provinsiValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                              kotaValue = ['', ''];
                              kotaReadonly = false;
                            } else if (valueID == 2) {
                              kotaValue = [
                                snapshot.data[index]['id']!,
                                snapshot.data[index]['name']!
                              ];
                            }
                          });
                          Navigator.pop(context);
                        }));
              } else {
                return Center(child: CustomText(txt: 'Loading'));
              }
            });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor(),
      appBar: CustomAppBar(context, Str().setting$addSIP, true),
      body: SingleChildScrollView(
        padding:
            EdgeInsets.symmetric(horizontal: defaultPaddingContent(context)),
        child: Column(
          children: [
            CustomTextField(
              hint: Str().setting$licensenumber,
              controller: txtcNomorSip,
            ),
            CustomDateField(
                textEditingController: txtcberlakudari, hint: Str().valodfrom),
            CustomDateField(
                textEditingController: txtcberlakusampai,
                hint: Str().valoduntil),
            CustomDynamicDropDown(
              context,
              onTapFunction: () =>
                  context.read<RegionSelectorBloc>().add(SelectProvinsi()),
              hint: Str().province,
              height: 1,
              value: provinsiValue[1],
              dialogTitle: "Pilih Provinsi",
              showIfCondition: true,
              contentWidget: listViewContent(valueID: 1),
            ),
            CustomDynamicDropDown(
              context,
              hint: Str().city,
              onTapFunction: () {
                if (provinsiValue.length > 0) {
                  context
                      .read<RegionSelectorBloc>()
                      .add(SelectKabupaten(provinsiID: provinsiValue[0]));
                }
              },
              showIfCondition:
                  provinsiValue[0].toString().length > 0 ? true : false,
              ifConditionFalseText: "Pilih Provinsi Dahulu",
              height: 1,
              disableOnTap: kotaReadonly,
              value: kotaValue[1],
              dialogTitle: "Pilih Kota / Kabupaten",
              contentWidget: listViewContent(valueID: 2),
            ),
            CustomButton(
              styleId: 3,
              shapeId: 2,
              onPressed: () => goToPage(context, page: OpenStreetMapMain()),
              btnText: Str().setlocationmap,
            ),
            CustomText(
                toppad: 0.02,
                botpad: 0.02,
                txt:
                    "Mohon diperhatikan :\n\nPihak OBAT-in akan melakukan verifikasi terlebih dahulu maksimal 3 x 24 Jam (tidak termasuk hari libur).\n\nPastikan masa berlaku surat izin masih aktif, apabila masa berlaku nomor surat izin sudah kadaluarsa maka proses verifikasi gagal.\n\nPastikan nomor surat izin yang anda masukkan sudah benar agar proses verfikasi lebih cepat.\n\nAnda dapat menambahkan Surat Izin (SIP/SIK) Maksimal 3, setiap satu surat izin anda dapat membuka satu praktik."),
            CustomButton(
              vermargin: 0.05,
              styleId: 1,
              onPressed: () => buttonFunc(),
              btnText: Str().save,
            )
          ],
        ),
      ),
    );
  }
}
