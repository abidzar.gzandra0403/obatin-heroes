class ModelUserData {
  int? selectSIP;
  ModelUserData({this.selectSIP});
  static const String tableName = "userdata";
  static const String createQuery =
      " CREATE TABLE IF NOT EXISTS $tableName(dataid INTEGER PRIMARY KEY AUTOINCREMENT, id_pengguna TEXT, id_nakes TEXT, id_level TEXT, no_str TEXT, str_berlaku_dari DATE, str_berlaku_sampai DATE, email TEXT, nik TEXT, nama TEXT, tempat_lahir TEXT, tanggal_lahir DATE, alamat TEXT, kelurahan TEXT, kecamatan TEXT, kota TEXT, provinsi TEXT) ";

  static const String getData = "SELECT * FROM $tableName";
}

// class ModelUserDataMap {
//   int? dataid;
//   String? idpengguna;
//   String? idnakes;
//   String? idlevel;
//   String? nostr;
//   String? strberlakudari;
//   String? strberlakusampai;
//   String? email;
//   String? nik;
//   String? nama;
//   String? tempatlahir;
//   String? tanggallahir;
//   String? alamat;
//   String? kelurahan;
//   String? kecamatan;
//   String? kota;
//   String? provinsi;

//   ModelUserDataMap(
//       {this.dataid,
//       this.idpengguna,
//       this.idnakes,
//       this.idlevel,
//       this.nostr,
//       this.strberlakudari,
//       this.strberlakusampai,
//       this.email,
//       this.nik,
//       this.nama,
//       this.tempatlahir,
//       this.tanggallahir,
//       this.alamat,
//       this.kelurahan,
//       this.kecamatan,
//       this.kota,
//       this.provinsi});

//   factory ModelUserDataMap.fromJson(Map<String, dynamic> json) {
//     return ModelUserDataMap(
//       dataid: json['dataid'],
//       idpengguna: json['id_pengguna'],
//       idnakes:json['id_nakes'],
//       idlevel:json['id_level'],
//       nostr: json['no_str'],
//       strberlakudari:json['str_berlaku_dari'],
//       strberlakusampai: json['str_berlaku_sampai'],
//       email: json['email'],
//       nik: json['nik'],
//       nama: json['nama'],
//       tempatlahir: json['tempat_lahir'],
//       tanggallahir: json['tanggal_lahir'],
//       alamat: json['alamat'],
//       kelurahan: json['kelurahan'],
//       kecamatan: json['kecamatan'],
//       kota: json['kota'],
//       provinsi: json['provinsi'],
//     );
//   }

//   Map<String, dynamic> toJson() {
//     final Map<String, dynamic> data = new Map<String, dynamic>();
//     data['dataid'] = this.dataid;
//     data['id_pengguna'] = this.idpengguna;
//     data['id_nakes'] = this.idnakes;
//     data['id_level'] = this.idlevel;
//     data['no_str'] = this.nostr;
//     data['str_berlaku_dari'] = this.strberlakudari;
//     data['str_berlaku_sampai'] = this.strberlakusampai;
//     data['email'] = this.email;
//     data['nik'] = this.nik;
//     data['nama'] = this.nama;
//     data['tempat_lahir'] = this.tempatlahir;
//     data['tanggal_lahir'] = this.tanggallahir;
//     data['alamat'] = this.alamat;
//     data['kelurahan'] = this.kelurahan;
//     data['kecamatan'] = this.kecamatan;
//     data['kota'] = this.kota;
//     data['provinsi'] = this.provinsi;
//     return data;
//   }
// }
