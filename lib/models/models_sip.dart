class ModelSipData {
  int? selectSIP;
  ModelSipData({this.selectSIP});
  static const String tableName = "sip";
  static const String createQuery =
      " CREATE TABLE IF NOT EXISTS $tableName ( dataid INTEGER PRIMARY KEY AUTOINCREMENT, idSip TEXT, nosip TEXT, berlakudari DATE, berlakusampai DATE, kotaberlaku TEXT, propinsiberlaku TEXT ) ";

  static const String getData = "SELECT * FROM $tableName";
}
