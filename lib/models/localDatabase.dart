import 'package:flutter/cupertino.dart';
import 'package:obatin/models/models_sip.dart';
import 'package:obatin/models/models_userdata.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart' as sqlite;
import 'package:sqflite/sqlite_api.dart';
import 'package:path/path.dart';
import 'dart:io' as io;

//=========================================================================================
//    DATABASE LOKAL MENGGUNAKAN SQFLITE
//=========================================================================================
class LocalDatabase {
  static LocalDatabase _localDB = LocalDatabase._instance();

  factory LocalDatabase() {
    return _localDB;
  }

  LocalDatabase._instance();

  final tables = [
    ModelSipData.createQuery,
    ModelUserData.createQuery,
  ];

  Future<Database> openDB() async {
    io.Directory directory = await getApplicationDocumentsDirectory();
    String path = join(directory.path, "localdatabase");
    var db = await sqlite.openDatabase(path, onCreate: (db, version) {
      tables.forEach((table) async {
        await db.execute(table).then((value) {
          print("berashil ");
        }).catchError((err) {
          print("errornya ${err.toString()}");
        });
      });
      print('Table Created');
    }, version: 1);
    return db;
  }

  //=========================================================================================
  // MEMBACA DATA TABLE DI DATABASE =========================================================
  //=========================================================================================

  Future<List> readData(String tableName) async {
    final db = await openDB();
    var result = await db.query(tableName);
    return result.toList();
  }

  //=========================================================================================
  // INPUT DATA KE TABLE ====================================================================
  //=========================================================================================

  createData(String table, Map<String, Object> data) {
    openDB().then((db) {
      db.insert(table, data, conflictAlgorithm: ConflictAlgorithm.replace);
    }).catchError((err) {
      print("error $err");
    });
  }

  //=========================================================================================
  // UPDATE DATA KE TABLE ===================================================================
  //=========================================================================================

  updateData(String tableName,
      {@required int? dataid,
      @required Map<String, Object>? updateData}) async {
    final db = await openDB();
    var result =
        await db.update(tableName, updateData!, where: 'dataid = $dataid');
    print(result);
  }

  //=========================================================================================
  // HAPUS DATA DARI TABLE ==================================================================
  //=========================================================================================

  deleteData(String tableName, {int? dataid}) async {
    final db = await openDB();
    var result = dataid != null
        ? await db.delete(tableName, where: 'dataid = $dataid')
        : await db.delete(tableName);
    print(result);
  }
}
