import 'package:obatin/models/system_models/settingData.dart';

class Str {
  //General String
  static lang<String>(String ind, String eng) {
    return langID == 1 ? ind! : eng!;
  }

  var login = lang("Login", "Login");
  var welcome = lang("Selamat Datang", "Welcome");
  var email = lang("Email", "Email");
  var password = lang("Kata Sandi", "Password");
  var passwordconfirm = lang("Konfirmasi Kata Sandi", "Confirm Password");
  var newpasswordconfirm =
      lang("Konfirmasi Kata Sandi Baru", "Confirm New Password");
  var oldpassword = lang("Kata Sandi Lama", "Old Password");
  var search = lang("Cari", "Search");
  var forgotpassword = lang("Lupa Kata Sandi?", "Forget Password?");
  var resetpassword = lang("Reset Password", "Reset Password");
  var newpassword = lang("Password Baru", "New Password");
  var allchangenotbesave =
      lang("Semua perubahan tidak akan disimpan", "All changes not be saved");
  var hi = lang("Hai", "Hi");
  var submit = lang("Submit", "Submit");
  var exitapp = lang("Keluar Aplikasi?", "Exit Apps?");
  var yes = lang("Ya", "Yes");
  var no = lang("Tidak", "No");
  var cancel = lang("Batal", "Cancel");
  var accept = lang("Terima", "Accept");
  var decline = lang("Tolak", "Decline");
  var description = lang("Deskripsi", "Description");
  var next = lang("Selanjutnya", "Next");
  var finish = lang("Selesai", "Finish");
  var skip = lang("Lewati", "Skip");
  var back = lang("Kembali", "Back");
  var nohaveaccount = lang("Tidak Punya Akun?", "No Have Account?");
  var createaccount = lang("Buat Akun", "Create Account");
  var notmatch = lang("Tidak Cocok", "Not Match");
  var register = lang("Daftar", "Register");
  var verification = lang("Verifikasi", "Verification");
  var seeall = lang("Lihat Semua", "See All");
  var all = lang("Semua", "All");
  var clickfordetail =
      lang("Ketuk Untuk Lihat Detail", "Click For See Details");
  var service = lang("Layanan", "Service");
  var practice = lang("Praktik", "Practice");
  var activity = lang("Aktifitas", "Activity");
  var inbox = lang("Kotak Masuk", "Inbox");
  var settings = lang("Pengaturan", "Settings");
  var support = lang("Bantuan", "Support");
  var latest = lang("Terkini", "Latest");
  var moredetail = lang("Selengkapnya", "More Detail");
  var call = lang("Hubungi", "Call");
  var chat = lang("Chat", "Chat");
  var male = lang("Pria", "Male");
  var female = lang("Wanita", "Female");
  var searchmessage = lang("Cari Pesan", "Search Chat");
  var searchpatient = lang("Cari Pasien", "Search Patient");
  var searchpartner = lang("Cari Mitra", "Search Partner");
  var pay = lang("Bayar", "Pay");
  var topup = lang("Top Up", "Top Up");
  var filter = lang("Filter", "Filter");
  var edit = lang("Ubah", "Change");
  var darkmode = lang("Mode Gelap", "Dark Mode");
  var lightmode = lang("Mode Terang", "Light Mode");
  var dark = lang("Gelap", "Dark");
  var light = lang("Terang", "Light");
  var price = lang("Tarif", "Price");
  var free = lang("Gratis", "Free");
  var delete = lang("Hapus", "Delete");
  var save = lang("Simpan", "Save");
  var saved = lang("Tersimpan", "Saved");
  var savechanges = lang("Simpan Perubahan", "Save Changes");
  var confirmation = lang("Konfirmasi", "Confirmation");
  var primary = lang("Utama", "Primary");
  var sure = lang("Baik", "Sure");
  var sent = lang("Terkirim", "Sent");
  var send = lang("Kirim", "Send");
  var pleaseinputform = lang("Mohon lengkapi form", "Please fill the form");
  var add = lang("Tambah", "Add");
  var done = lang("Selesai", "Done");
  var village = lang("Kelurahan", "Village");
  var subdistrict = lang("Kecamatan", "Sub-District");
  var city = lang("Kota", "City");
  var province = lang("Provinsi", "Province");
  var from = lang("Dari", "From");
  var nullDesc = lang(
      "Yuk ceritakan secara singkat tentang diri anda dan pengalaman kepada calon pasien, yakinkan anda adalah pahlawan bagi mereka. Ketuk menambahkan deskripsi.",
      "Let's briefly tell about yourself and your experience to prospective patients, make sure you are a hero to them. Tap add a description.");
  //
  //DAY WEEK STRING ==========================================================================================
  //
  var sunday = lang("Minggu", "Sunday");
  var monday = lang("Senin", "Monday");
  var tuesday = lang("Selasa", "Tuesday");
  var wednesday = lang("Rabu", "Wednesday");
  var thursday = lang("Kamis", "Thursday");
  var friday = lang("Jumat", "Friday");
  var saturday = lang("Sabtu", "Saturday");
  //
  //INTRO  =================================================================================================
  //
  var intro1title = lang(
      "Anda Adalah Pahlawan dari Misi Besar Integrated Health System. Kami Menyebut Anda, OBAT-in Heroes",
      "You Are the Hero of the Great Mission of the Integrated Health System. We Call You, OBAT-in Heroes ");
  var intro2title = lang(
      "Anda Bebas Menentukan Jenis Layanan, Waktu Layanan dan Tarif Layanan",
      "Free to Set the Type of Service, Service Time and Service Fee");
  var intro3title = lang(
      "Anda Dapat Membuka Layanan Praktik atau Menjual Produk Kesehatan ke Konsumen",
      "You Can Open Practice Services or Sell Health Products to Consumers ");
  var intro1desc = lang(
      "Dokter • Dokter Spesialis • Dokter Konsultan • Dokter Gigi • Dokter Gigi Spesialis • Dokter Gigi Konsultan • Perawat • Bidan • Fisioterapis • Akupunkturis • Psikolog • Ahli Gizi • Terapis Wicara • Terapis Okupasi • dan lainnya",
      "Doctor • Specialist Doctor • Consultant Doctor • Dentist • Specialist Dentist • Consultant Dentist • Nurse • Midwife • Physiotherapist • Acupuncturist • Psychologist • Nutritionist • Speech Therapist • Occupational Therapist • and others");
  var intro2desc = lang("On Location • Visit Pasien • Telemedicine • Sukarela",
      "On Location • Visit Patient • Telemedicine • Volunteer");
  var intro3desc = lang(
      "Apotek • Alat Kesehatan • PKRT • Optik • Kebutuhan Pendidikan Kesehatan • Produk Ergonomis • Produk UMKM Kesehatan",
      "Pharmacies • Medical Devices • Household Health Supplies • Optics • Health Education Needs • Ergonomic Products • Health MSME Products");
  //
  // Register
  //
  var fullname = lang("Nama Lengkap", "Full Name");
  var prefixtitle = lang("Gelar Depan", "Front Title");
  var suffixtitle = lang("Gelar Belakang", "Back Title");
  var idtype = lang("Jenis Kartu Identitas", "ID Card Type");
  var idnumber = lang("Nomor Kartu Identitas", "ID Card Number");
  var addresbyid = lang("Alamat Kartu Identitas", "ID Card Address");
  var sameaddreswithid = lang("Alamat sekarang sama dengan kartu identitas?",
      "The current address is the same as the ID card? ");
  var mitraplustype = lang("Jenis MITRA Plus", "MITRA Plus Type");
  var uploaddocument = lang("Unggah Dokumen", "Upload Document");
  var idcardimage = lang("Foto Kartu Identitas", "ID Card Scan");
  var selfiewithidcard =
      lang("Selfie Dengan Kartu Identitas", "Selfie With ID Card");
  var practicelicensedocument =
      lang("Dokumen Surat Izin Praktik", "Practice License Document");
  var registeras = lang("Daftar Sebagai", "Register As");
  var phonenumber = lang("Nomor Telepon", "Phone Number");
  var phonecheck =
      lang("Pastikan nomor anda aktif", "Make sure your number is active");
  var enterotp = lang("Masukkan 4 kode digit", "Enter 4 digits code");
  var resend = lang("Kirim Ulang", "Resend");
  var resendotp = lang("Kirim Ulang Kode OTP", "Resend OTP Code");
  var registersuccesstitle =
      lang("Pendaftaran Berhasil Terkirim", "Registration Successfully Sent");
  var registersuccessdesc = lang(
      "Pihak OBAT-in akan memverifikasi pendaftaran kamu dalam 1 x 24 Jam.",
      "We will verify your registration within 1 x 24 hours.");
  var success = lang("Berhasil", "Success");
  var successchangepassword =
      lang("Kata Sandi Berhasil Diubah", "Password Changed Succesfully");
  //
  // Practice Services String
  //
  var doctor = lang("Dokter", "Doctor");
  var healthworker = lang("Tenaga Kesehatan", "Health Worker");
  var healthproductseller =
      lang("Penjual Produk Kesehatan", "Health Product Seller");
  var specialization = lang("Spesialisasi", "Specialization");
  var valodfrom = lang("Berlaku Dari", "Valid From");
  var valoduntil = lang("Berlaku Sampai", "Valid Until");
  var specialist = lang("Spesialis", "Specialist");
  var generaldoctor = lang("Dokter Umum", "General Practitioner");
  var specialistdoctor = lang("Dokter Spesialis", "Medical Specialist");
  var consultantdoctor = lang("Dokter Konsultan", "Consultant Doctor");
  var dentist = lang("Dokter Gigi Umum", "General Dentist");
  var specialistdentist = lang("Dokter Gigi Spesialis", "Specialist Dentist");
  var consultantdentist = lang("Dokter Gigi Konsultan", "Consultant Dentist");
  var nearestdoctor = lang("Dokter Terdekat", "Nearest Doctor");
  var makeappointment = lang("Buat Janji", "Make an appointment");
  var consultnow = lang("Konsultasi Sekarang", "Consult Now");
  var ongoing = lang("Sedang Berlansung", "On Going");
  var incoming = lang("Mendatang", "In Coming");
  var newrequest = lang("Permintaan Baru", "New Request");
  var onlocationreq = lang("Permintaan On Location", "On Location Request");
  var visitpatientreq =
      lang("Permintaan Visit Pasien", "Visit Patient Request");
  var telemedicinereq = lang("Permintaan Telemedicine", "Telemedicine Request");
  var volunteerreq = lang("Permintaan Sukarela", "Volunteer Request");
  var onlocation = lang("On Location", "On Location");
  var visitpatient = lang("Visit Pasien", "Visit Patient");
  var telemedicine = lang("Telemedicine", "Telemedicine");
  var volunteer = lang("Sukarela", "Volunteer");
  var registnumber = lang("No Registrasi", "Registration Number");
  var bookingtime = lang("Waktu Pemesanan", "Booking Time");
  var pricedetail = lang("Detail Harga", "Price Detail");
  var referral = lang("Rujuk", "Refer");
  var referraldesc = lang(
      "Memerlukan rujukan ke Dokter atau Nakes lain? (Dapatkan insentif 5% untuk setiap rujukan yang anda lakukan).",
      "Need a referral to another doctor or health worker? (Get 5% incentive for every referral you make). ");
  var complete = lang("Selesai", "Complete");
  var address = lang("Alamat", "Address");
  var servicedonedesc = lang(
      "Telah menyelesaikan layanan kepada pasien? jika sudah, mohon klik konfirmasi selesai.",
      "Have completed the service with patient? Please click complete confirmation button.");
  var servicedonedialogtitle =
      lang("Telah Menyelesaikan Layanan?", "Completed Service?");
  var servicedonedialog = lang(
      "Kami akan meminta konfirmasi kepada pasien apakah benar layanan telah selesai, Mohon menunggu.",
      "We will ask for confirmation to the patient whether the service has been completed, please wait");
  var neworder = lang("Pesanan Baru", "New Order");
  var waitingschedule = lang("Menunggu Jadwal", "Waiting Schedule");
  var waitingpayment = lang("Menunggu Pembayaran", "Waiting Payment");
  var reschedule = lang("Reschedule", "Reschedule");
  var ordercomplete = lang("Pesanan Selesai", "Completed Order");
  var orderrejected = lang("Pesanan Ditolak", "Order Rejected");
  var ordercomplain = lang("Pesanan Dikomplain", "Complained Order");
  var ordercanceled = lang("Pesanan Dibatalkan", "Order Canceled");
  var referencelist = lang("Daftar Rujukan", "Reference List");
  var makereferrals = lang("Melakukan Rujukan", "Make Referrals");
  var receivereferrals = lang("Menerima Rujukan", "Receive Referrals");
  var locationroutes = lang("Rute Lokasi", "Location Route");
  var servicetype = lang("Jenis Layanan", "Service Type");
  var sortbytime = lang("Urut Berdasarkan Waktu", "Sort By Time");
  var sortbydate = lang("Urut Berdasarkan Tanggal", "Sort By Date");
  var newest = lang("Terbaru", "Newest");
  var oldest = lang("Terlama", "Oldest");
  var standardservice = lang("Layanan Standar", "Standard Service");
  var volunteerservice = lang("Layanan Sukarela", "Volunteer Service");
  var setrateallstandardservice = lang(
      "Setel Tarif Semua Layanan Standar", "Set Rates All Standard Services");
  var setworktimeallstandardservice = lang(
      "Setel Jam Kerja Semua Layanan Standar",
      "Set Working Hours All Standard Services");
  var setworktimeallstandardvolunteer = lang(
      "Setel Jam Kerja Semua Layanan Sukarela",
      "Set Working Hours All Voluntary Services");
  var addpracticelicense = lang("Tambah Izin Praktik", "Add Practice License");
  var changeschedule = lang("Perubahan", "Reschedule");
  var complaint = lang("Komplain", "Complain");
  var cancellation = lang("Pembatalan", "Cancellation");
  var referto = lang("Rujuk Ke", "Refer To");
  var referfrom = lang("Rujukan Dari", "Reference From");
  var profile = lang("Profil", "Profile");
  //
  // Inbox ============================================================================================
  //
  var discuss = lang("Diskusi", "Discuss");
  var review = lang("Ulasan", "Review");
  var history = lang("Riwayat", "History");
  var writemessage = lang("Tulis Pesan", "Write Message");
  var seerating = lang("Lihat Penilaian", "See Ratings");
  //
  // Notification =====================================================================================
  //
  var notification = lang("Notifikasi", "Notification");
  var notificationDesc = lang("Atur segala jenis pesan notifikasi",
      "Set any kind of notification message");
  var unread = lang("Belum Dibaca", "Unread");
  var alreadyread = lang("Sudah Dibaca", "Already Read");
  //
  // Settings =========================================================================================
  //
  var setting$practiceNservicetitle =
      lang("Praktik & Layanan", "Practice & Service");
  var setting$practiceNservice =
      lang("Pengaturan Praktik dan Layanan", "Practice and Service Settings");
  var setting$practiceNservicedesc = lang(
      "Perbaharui informasi SIP serta layanan yang anda aktifkan.",
      "Update practice license information and activated services.");
  var setting$titleNfullname =
      lang("Gelar dan Nama Lengkap", "Title and Full Name");
  var setting$titleNfullnamedesc = lang(
      "Perbaharui nama anda dan gelar akademik atau profesi terbaru.",
      "Update your name and latest academic or professional degree. ");
  var setting$userinfotitle = lang("Informasi Pengguna", "User Information");
  var setting$editdescription = lang("Ubah Deskripsi", "Edit Description");
  var setting$editdescriptiondesc = lang(
      "Perbaharui deskripsi pribadi anda agar pasien lebih mengenal anda.",
      "Update your personal description so the patient knows you better.");
  var setting$allowreviewNrating =
      lang("Izinkan Review dan Rating", "Allow Review and Rating");
  var setting$bankaccount = lang("Rekening Bank", "Bank Account");
  var setting$bankaccountdesc = lang(
      "Atur akun rekening bank anda untuk melakukan penarikan saldo dompet digital.",
      "Set up your bank account to withdraw your digital wallet balance.");
  var setting$accountsettingtitle = lang("Pengaturan Akun", "Account Settings");
  var setting$changepassword = lang("Ubah Password", "Change Password");
  var setting$changepassworddesc =
      lang("Ubah kata sandi akun anda", "Change your password account");
  var setting$notification =
      lang("Pengaturan Notifikasi", "Notification Settings");
  var setting$notificationdesc = lang(
      "Atur notifikasi apa saja yang ingin ditampilkan.",
      "Set what notifications you want to display.");
  var setting$practiceinfo = lang("Informasi Praktik", "Practice Info");
  var setting$licensenumber = lang("Nomor Surat Izin", "License Number");
  var setting$yourservice = lang("Layanan Anda", "Your Service");
  var setting$changeaddress = lang("Ubah Alamat", "Change Address");
  var setting$deleteconfirmdialog = lang(
      "Nomor SIP/STR yang dipilih akan dihapus dan semua layanan akan di nonaktifkan, Yakin ingin menghapus?",
      "The selected practice license number will be deleted and all services will be disabled. Are you sure you want to delete?");
  var setting$verificationprocess =
      lang("Sedang Diverifikasi", "Verification Process");
  var setting$addSIP = lang("Tambah Surat Izin", "Add License");
  var setting$addlicense =
      lang("Tambah Surat Izin (maksimal 3)", "Add License (maximal 3)");
  var setlocationmap =
      lang("Set Lokasi Dengan Google Maps", "Set Location With Google Maps");
  var setting$descriptioninfo = lang(
      "Ceritakan secara singkat tentang diri dan profesi anda, maksimal 150 karakter.",
      "Tell us briefly about yourself and your profession, maximum 150 characters.");
  var setting$addlicensenotes = lang(
      "Anda harus menambahkan Surat Izin Praktik atau Surat Tanda Registrasi untuk membuka layanan praktik",
      "You must add a Practice License or Registration Certificate to open a practice service");
  var setting$yourservicenotes = lang(
      "Pengaturan layanan praktik anda akan muncul disini.",
      "Your practice service settings will appear here. ");
  var setting$service = lang("Pengaturan Layanan", "Service Settings");
  var setting$changefullnametitle =
      lang("Ubah Gelar dan Nama Lengkap", "Change Title and Full Name");
  var setting$changedesc = lang("Ubah Deskripsi", "Change Description");
  var setting$yourdescription = lang("Deskripsi Anda", "Your Description");
  var setting$addbankaccount = lang("Tambah Rekening Bank", "Add Account");
  var setting$accountfullname =
      lang("Nama Lengkap Pemilik Rekening", "Account Owner Full Name");
  var setting$accountnumber = lang("Nomor Rekening", "Account Number");
  var setting$bankname = lang("Nama Bank", "Bank Name");
  var setting$canceladdaccountdialog =
      lang("Batal Tambah Rekening?", "Cancel Add Account?");
  var setting$changepassworddialog =
      lang("Ubah Passoword?", "Change Password?");
  var setting$changepassworddialogdesc = lang(
      "Pastikan kamu benar-benar ingat password baru mu ya",
      "Make sure you remember new password");
  var setting$accountverification = lang(
      "Nomor rekening akan kami verifikasi. Mohon menunggu ya.",
      "We will verify the account number. Please wait. ");
  var setting$cancelchangepassworddialog =
      lang("Batal Ubah Password?", "Cancel Change Password?");
  var setting$notificationsetting =
      lang("Pengaturan Notifikasi", "Notification Settings");
  var setting$obatinnotif = lang("Notifikasi OBAT-in", "OBAT-in Notification");
  var setting$promonotif = lang("Notifikasi Promo", "Promo Notification");
  var setting$updatenotif = lang("Notifikasi Pembaruan", "Update Notification");
  var setting$obatinnotifdesc = lang(
      "Anda dapat mengaktifkan / nonaktifkan notifikasi dari OBAT-in",
      "You can enable/disable notifications from OBAT-in");
  var setting$promonotifdesc = lang(
      "Anda dapat mengaktifkan / nonaktifkan notifikasi promo dan penawaran menarik",
      "You can enable/disable notifications from promo and offers");
  var setting$updatenotifdesc = lang(
      "Anda dapat mengaktifkan / nonaktifkan notifikasi pembaruan dan berita terbaru",
      "You can enable/disable notification of updates and latest news");
  var setting$changeprofilephoto =
      lang("Ubah Foto Profil", "Change Profile Photo");
  var setting$opencamera = lang("Buka Kamera", "Open Camera");
  var setting$uploadphoto = lang("Unggah Foto", "Upload Photo");
  //
  //Support =========================================================================================
  //
  var support$supportcenter =
      lang("Pusat Bantuan OBAT-in Heroes", "OBAT-in Heroes Support Center");
  var support$supportcenterSobat =
      lang("Pusat Bantuan Sobat-in", "Sobat-in Heroes Support Center");
  var support$supportcenterSobatDesc =
      lang("Pusat Bantuan Sobat-in", "Sobat-in Heroes Support Center");
  var support$supportcenterdesc = lang(
      "Temukan jawaban dari setiap pertanyaan dan kendala yang anda alami.",
      "Find answers to every question and obstacle.");
  var support$complain = lang("Komplain", "Complain");
  var support$complaindesc = lang(
      "Ajukan komplain atas kinerja penyedia layanan.",
      "Submission of complaints on the performance of service providers.");
  var support$customercomplain = lang("Aduan Konsumen", "Consumer Complain");
  var support$customercomplaindesc = lang(
      "Direktorat Jenderal Perlindungan Konsumen dan Tertib Niaga Kementrian Perdagangan Republik Indonesia.",
      "Directorate General of Consumer Protection and Order of Commerce, Ministry of Trade of the Republic of Indonesia.");
  var support$aboutobatin = lang("Kenali OBAT-in dan OBAT-in Heroes",
      "Know About OBAT-in and OBAT-in Heroes");
  var support$aboutobatindesc = lang(
      "Kenali lebih detail mengenai OBAT-in dan OBAT-in Heroes.",
      "Get to know more details about OBAT-in and OBAT-in Heroes.");
  var support$aboutobatinSobat =
      lang("Kenali OBAT-in & SOBAT-in", "Know About  OBAT-in & SOBAT-in");
  var support$aboutobatinSobatDesc = lang(
      "Ketahui lebih detail mengenai OBAT-in & OBAT-in Heroes",
      "Find out more details about OBAT-in & OBAT-in Heroes");
  var support$termNcondition = lang("Syarat & Ketentuan", "Term & Conditions");
  var support$termNconditiondesc = lang(
      "Pahami syarat dan ketentuan yang berlaku di OBAT-in.",
      "Understand the terms and conditions that apply at OBAT-in.");
  var support$privacypolicy = lang("Kebijakan Privasi", "Privacy Policy");
  var support$privacypolicydesc = lang(
      "Pahami informasi yang kami kumpulkan, memperbarui, mengelola, mengekspor dan menghapus informasi anda.",
      "Understand the information we collect, update, manage, export and delete your information.");
  var support$intellectualrights =
      lang("Hak Kekayaan Intelektual", "Intellectual Property Rights");
  var support$intellectualrightsdesc = lang(
      "Pahami perlindungan secara hukum atas kekayaan intelektual sesuai dengan peraturan yang berlaku",
      "Understand the legal protection of intellectual property in accordance with applicable regulations.");
  var support$reviewapp = lang("Ulas Aplikasi", "Review Apps");
  var support$reviewappdesc = lang(
      "Berikan ulasan mengenai aplikasi OBAT-in, ulasan anda sangat bermanfaat bagi kami.",
      "Write a review about OBAT-in, your review is very useful for us.");
  var support$officialobatinwebsite =
      lang("Situs Resmi OBAT-in", "OBAT-in Official Website");
  var support$officialobatinwebsitedesc = lang(
      "Ketuk untuk membuka situs web resmi OBAT-in",
      "Tap to open OBAT-in official website.");
  var support$welcometojelasin =
      lang("Selamat Datang di JELAS-in", "Welcome to JELAS-in");
  var support$introductionmistero = lang(
      "Perkenalkan saya Mister'O yang akan menjawab semua pertanyaan kamu.",
      "I am Mister'O who will answer all your questions.");
  var support$anyhelpmistero =
      lang("Ada yang bisa Mister'O bantu?", "Can Mister'O help you?");
  var support$searchtopic = lang("Cari Topik", "Search Topic");
  var support$selecttopicmistero = lang(
      "Pilih topik yang ingin Mister'O JELAS-in",
      "Select the topic you want to Mister'O explained");
  var support$frequentquestion =
      lang("Pertanyaan yang sering muncul", "Frequently asked questions ");
  var support$yourquestionnotclear = lang(
      "Pertanyaanmu belum terjawab?", "Your question has not been answered?");
  var support$misterocanhelp = lang(
      "Mister'O siap bantu lebih lanjut", "Mister'O ready to help further");
  var support$writecomplain = lang("Tulis Kendala", "Write Problem");
  var support$canobatinhelp =
      lang("Hai, ada yang bisa OBAT-in bantu?", "Hi, need OBAT-in help?");
  var support$tellyourproblem = lang(
      "Ceritakan masalahmu disini, kami siap mendengarkan",
      "Tell us your problem here, we are ready to listen");
  var support$problemdetail = lang("Detail Masalah", "Problem Detail");
  var support$mincharacter = lang("Min. 30 Karakter", "Min. 30 Character");
  var support$transactionproblem =
      lang("Transaksi Bermasalah", "Problem Transaction");
  var support$selecttransaction = lang("Pilih Transaksi", "Select Transaction");
  var support$attachment = lang("Lampiran (Opsional)", "Attactment (Optional)");
  var support$attachmentdesc = lang(
      "Lampirkan bukti pembayaran atau bukti pendukung lainnya. (Maksimal 5 foto atau 4 foto dengan 1 video)",
      "Attach receipts or other evidence. (Maximum 5 photos or 4 photos with 1 video)");
  var support$format1 = lang(
      "*Format foto .jpg, .jpeg atau .png dengan ukuran maksimal 1 MB",
      "*Photo format .jpg, .jpeg or .png with a maximum size of 1 MB");
  var support$format2 = lang(
      "*Format video .mp4 atau .avi dengan ukuran maksimal 10 MB",
      "*Video format .mp4 or .avi with a maximum size of 10 MB ");
  var support$complaincheckdialog = lang(
      "Pihak OBAT-in akan memeriksa keluhan mu sesegera mungkin, mohon menunggu ya.",
      "OBAT-in will check your complaint as soon as possible, please wait.");

  //Kenalin
  var kenalin$myaccount = lang("Akun Saya", "My Account");
  var kenalin$changephotoprofile =
      lang("Ubah Foto Profil", "Change Profile Photo");
  var kenalin$updatebio = lang("Ubah Biodata", "Update Biodata");
  var kenalin$opencamera = lang("Buka Kamera", "Open Camera");
  var kenalin$uploadphoto = lang("Unggah Foto", "Upload Photo");

  //
  //Sobat-IN  =========================================================================================
  //
  var features = lang("Fitur SOBAT-in", "SOBAT-IN Features");
  var profile$aboutsobatin = lang("Seputar SOBAT-in", "About SOBAT-in");
  var profile$Advantagessobatin =
      lang("Seputar SOBAT-in", "Advantages of SOBAT-in");
  var profile$TitleAdvantagessobatin = lang(
      "Penasaran kan? yuk klik buat info detailnya",
      "Curious right? let's click for detailed info");
  var profile$TitleAjakin = lang(
      "Ajak-ajak yang lain yuk buat bergabung, komisi nya lumayan loh",
      "Let's invite others to join, the commission is pretty good");
  var profile$TitleSharein = lang("Bisa dapat komisi bisa juga untuk amal",
      "You can get a commission or it can be for charity");
  var profile$Accountsettings = lang("Pengaturan Akun", "Account settings");
  var profile$AddressList = lang("Daftar Alamat", "Address List");
  var profile$Address = lang("Alamat", "Address");
  var profile$TitleAddressList = lang(
      "Perbaharui deskripsi pribadi anda agar pasien lebih mengenal anda",
      "Update your personal description so patients know you better");
  var profile$InstantPayment = lang("Pembayaran Instan", "Instant Payment");
  var profile$InstantPaymentDesc = lang(
      "Kartu Kredit & Kartu Debit Instan terdaftar",
      "Registered Instant Credit & Debit Card");
  var profile$AccountSecurity = lang("Keamanan Akun", "Account Security");
  var profile$AccountSecurityDesc = lang(
      "Kata sandi, PIN & verifikasi data diri",
      "Password, PIN & personal data verification");
  var profile$sobatadvantages =
      lang("Keuntungan SOBAT-in", "SOBAT-in Advantages");
  var profile$passiveincome = lang("Pendapatan Pasif", "Passive Income");
  var profile$passiveincomesub = lang(
      "Dapatkan penghasilan tambahan selama sebulan penuh tanpa bekerja",
      "Get extra income for a whole month without working");
  var profile$passiveincomedesc1 = lang(
      "Penghasilan tambahan (passive income) selama satu bulan penuh dari hasil AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes bergabung di OBAT-in",
      "Additional income (passive income) for one full month from the results of AJAK-in Partners Plus Sellers of Health Products and/or Doctors / Health Workers / Healthcare Facilities joining OBAT-in");
  var profile$passiveincomedesc2 = lang(
      "Passive income berupa komisi sebesar 1% selama sebulan penuh dari setiap transaksi pihak yang berhasil di AJAK-in",
      "Passive income in the form of a commission of 1% for a full month from every successful party transaction in the AJAK-in");
  var profile$passiveincomedesc3 = lang(
      "Sekali berhasil AJAK-in, untungnya sebulan penuh tanpa ngapa-ngapain lagi",
      "Once you successfully AJAK-in, luckily a whole month without doing anything else");
  var profile$passiveincomedesc4 = lang(
      "Yuk banyak-banyak AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter dan/atau Nakes dan/atau Fasyankes bergabung di OBAT-in",
      "Let's AJAK-in more and more Partners Plus Sellers of Health Products and/or Doctors and/or Healthcare and/or Healthcare Facilities to join the OBAT-in");
  //
  var profile$activeincome = lang("Pendapatan Aktif", "Active Income");
  var profile$activeincomesub = lang(
      "Makin banyak pennghasilan tambahan dengan sedikit bekerja ",
      "More extra income with less work");
  var profile$activeincomedesc1 = lang(
      "Penghasilan tambahan (active income) dari hasil SHARE-in produk dan atau jasa Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes OBAT-in",
      "Additional income (active income) from the results of SHARE-in products and or services of Partner Plus Sellers of Health Products and/or Doctors / Health Workers / Medical Health OBAT-in");
  var profile$activeincomedesc2 = lang(
      "Active income berupa komisi 2% dari harga jual produk di OBAT-in yang berhasil dibeli melalui link yang SOBAT-in SHARE-in",
      "Active income in the form of a 2% commission from the selling price of the product at the MEDICINE-in that was successfully purchased through the link that SOBAT-in-SHARE-in");
  var profile$activeincomedesc3 = lang(
      "Active income berupa komisi 5% dari tarif jasa Dokter/Nakes/Fasyankes di OBAT-in yang berhasil “dibeli” melalui link yang SOBAT-in SHARE-in",
      "Active income in the form of a 5% commission from the tariff for Doctors/Nakes/Fasyankes services on in-drugs that are successfully (purchased) via the link that SOBAT-in SHARE-in");
  var profile$activeincomedesc4 = lang(
      "SHARE-in Produk dan/atau Jasa di OBAT-in ke WA atau sosmed mu sebanyak-banyaknya",
      "SHARE-in Products and/or Services in OBAT-in to your WA or social media as much as possible");
  var profile$activeincomedesc5 = lang(
      "Makin sering SHARE-in, makin banyak peluang dapat tambahan penghasilan.",
      "The more SHARE-in, the more opportunities to earn additional income.");

  //
  var profile$cashback = lang("Uang kembali", "Cashback");
  var profile$cashbacksub = lang(
      "Pasti dapat cashback dari belanja produk dan jasa OBAT-in",
      "Definitely get cashback from shopping for in-drug products and services");
  var profile$cashbacksubdesc1 = lang(
      "Pasti dapat cashback dari setiap pembelian produk dan/atau jasa di OBAT-in untuk diri sendiri maupun orang lain melalui akun SOBAT-in",
      "You can definitely get cashback from every purchase of products and/or services at OBAT-in for yourself and others through your SOBAT-in account");
  var profile$cashbacksubdesc2 = lang(
      "Cashback 2% dari harga jual produk Mitra Plus yang dibeli di OBAT-in",
      "2% cashback from the selling price of Mitra Plus products purchased at OBAT-in");
  var profile$cashbacksubdesc3 = lang(
      "Cashback 5% dari tarif  jasa Dokter/Nakes/Fasyankes di OBAT-in, kecuali Layanan Sukarela",
      "5% cashback from the tariff for Doctor/Nakes/Fasyankes services at OBAT-in, except for Voluntary Services");
  var profile$cashbacksubdesc4 = lang(
      "Cashback di dapatkan setelah barang diterima dan/atau jasa telah selesai dilayani tanpa komplain",
      "Cashback is received after the goods are received and/or the service has been served without complaint");
  var profile$cashbacksubdesc5 = lang(
      "Mari belanja produk dan/atau gunakan jasa Dokter/Nakes/Fasyankes di aplikasi OBAT-in dan dapatkan cashback nya",
      "Let's shop for products and/or use the services of Doctors/Nakes/Fasyankes in the OBAT-in application and get the cashback");
  //

  var profile$charity = lang("Amal", "Charity");
  var profile$charitysub = lang("Lakukan amal dengan mudah tanpa keluar uang",
      "Do charity easily without spending money");
  var profile$charitydesc1 = lang(
      "Membantu banyak orang yang tidak mampu disekitar SOBAT-in untuk berobat kepada Dokter/Nakes/Fasyankes melalui Layanan Sukarela (gratis tanpa dipungut biaya). SOBAT-in dapat memberitahu dan atau mendaftarkan pasien tersebut melalui akun SOBAT-in",
      "Helping many underprivileged people around SOBAT-in to seek treatment from Doctors/Nakes/Fasyankes through Volunteer Services (free of charge). SOBAT-in can notify and or register the patient through the SOBAT-in account");
  var profile$charitydesc2 = lang("OBAT-in mempunyai Jenis Layanan Sukarela",
      "OBAT-in has a Voluntary Service Type");
  var profile$charitydesc3 = lang(
      "SOBAT-in, SHARE-in Jenis layanan tersebut ke orang yang membutuhkan",
      "SOBAT-in, SHARE-in This type of service to people in need");
  var profile$charitydesc4 = lang(
      "OBAT-in percaya diluar sana masih banyak Dokter/Nakes/ Fasyankes baik yang ingin beramal",
      "OBAT-in believes that there are still many good Doctors/Nakes/Fasyankes who want to do charity");
  var profile$charitydesc5 = lang(
      "OBAT-in juga percaya, SOBAT-in mau membantu SHARE-in agar membentuk lingkaran amal yang tidak terputus",
      "OBAT-in also believes, PAL-in wants to help SHARE-in to form an unbroken circle of charity");
  var profile$charitydesc6 = lang(
      "AJAK-in lebih banyak Dokter/Nakes/ Fasyankes untuk bergabung, agar makin banyak Dokter/Nakes/ Fasyankes yang mengaktifkan Jenis Layanan Sukarela",
      "AJAK-in more Doctors/Nakes/Fasyankes to join, so that more Doctors/Nakes/Fasyankes activate the Voluntary Service Type");

  var profile$ajakinsub = lang(
      "Ajak-ajak yang lain yuk buat bergabung, komisi nya lumayan loh",
      "Invite others to join, the commission is pretty good");
  var profile$ajakindesc1 = lang(
      "Passive income berupa komisi sebesar 1% selama sebulan penuh dari setiap transaksi pihak yang berhasil di AJAK-in",
      "Passive income in the form of a commission of 1% for a full month from every successful party transaction in the AJAK-in");
  var profile$ajakindesc2 = lang(
      "Ajak-ajak yang lain yuk buat bergabung, komisi nya lumayan loh",
      "Invite others to join, the commission is pretty good");
  var profile$ajakindesc3 = lang(
      "Sekali berhasil AJAK-in, untungnya sebulan penuh tanpa ngapa-ngapain lagi",
      "Once you successfully AJAK-in, luckily a whole month without doing anything else");
  var profile$ajakindesc4 = lang(
      "Yuk banyak-banyak AJAK-in Mitra Plus Penjual Produk Kesehatan dan/atau Dokter dan/atau Nakes dan/atau Fasyankes bergabung di OBAT-in.",
      "Let's AJAK-in Mitra Plus Sellers of Health Products and/or Doctors and/or Healthcare and/or Healthcare Facilities to join in the OBAT-in.");

  var profile$share = lang("Bisa dapat komisi, bisa juga untuk charity (amal)",
      "Can get a commission, can also be for charity (amal)");
  var profile$sharecommission = lang("KOMISI :", "COMMISSION :");

  var profile$sharedesc1 = lang(
      "Penghasilan tambahan (active income) dari hasil SHARE-in produk dan atau jasa Mitra Plus Penjual Produk Kesehatan dan/atau Dokter / Nakes / Fasyankes OBAT-in",
      "Additional income (active income) from the results of SHARE-in products and or services of Partner Plus Sellers of Health Products and/or Doctors / Health Workers / OBAT-inFacilities");
  var profile$sharedesc2 = lang(
      "Active income berupa komisi 2% dari harga jual produk di OBAT-in yang berhasil dibeli melalui link yang SOBAT-in SHARE-in",
      "Active income in the form of a 2% commission from the selling price of the product at the OBAT-in that was successfully purchased through the link that SOBAT-in-SHARE-in");
  var profile$sharedesc3 = lang(
      "Active income berupa komisi 5% dari tarif jasa Dokter/Nakes/Fasyankes di OBAT-in yang berhasil “dibeli” melalui link yang SOBAT-in SHARE-in",
      "Active income in the form of a 5% commission from the tariff for Doctors/Nakes/Fasyankes services on OBAT-in  that have been successfully purchased via the link that SOBAT-in SHARE-in");
  var profile$sharedesc4 = lang(
      "SHARE-in Produk dan/atau Jasa di OBAT-in ke WA atau sosmed mu sebanyak-banyaknya",
      "SHARE-in Products and/or Services in OBAT-in to your WA or social media as much as possible");
  var profile$sharedesc5 = lang(
      "Makin sering SHARE-in, makin banyak peluang dapat tambahan penghasilan",
      "The more SHARE-in, the more opportunities to get additional income");

  var profile$sharecharity = lang("Amal :", "Charity :");
  var profile$sharecharitydesc1 = lang(
      "Membantu banyak orang yang tidak mampu disekitar SOBAT-in untuk berobat kepada Dokter/Nakes/Fasyankes melalui Layanan Sukarela (gratis tanpa dipungut biaya). SOBAT-in dapat memberitahu dan atau mendaftarkan pasien tersebut melalui akun SOBAT-in",
      "Helping many underprivileged people around SOBAT-in to seek treatment from Doctors/Nakes/Fasyankes through Volunteer Services (free of charge). PAL-in can notify and or register the patient through the SOBAT-in account");
  var profile$sharecharitydesc2 = lang(
      "OBAT-in mempunyai Jenis Layanan Sukarela, Dokter/Nakes/ Fasyankes dapat mengaktifkan layanan ini",
      "OBAT-in have Voluntary Service Types, Doctors/Nakes/Fasyankes can activate this service");
  var profile$sharecharitydesc3 = lang(
      "SOBAT-in, SHARE-in Jenis layanan tersebut ke orang yang membutuhkan",
      "SOBAT-in, SHARE-in This type of service to people in need");
  var profile$sharecharitydesc4 = lang(
      "OBAT-in percaya diluar sana masih banyak Dokter/Nakes/ Fasyankes baik yang ingin beramal",
      "OBAT-in believes that there are still many good Doctors/Nakes/Fasyankes who want to do charity");
  var profile$sharecharitydesc5 = lang(
      "OBAT-in juga percaya, SOBAT-in mau membantu SHARE-in agar membentuk lingkaran amal yang tidak terputus",
      "OBAT-in also believes, SOBAT-in wants to help SHARE-in to form an unbroken circle of charity");
  var profile$sharecharitydesc6 = lang(
      "AJAK-in lebih banyak Dokter/Nakes/ Fasyankes untuk bergabung, agar makin banyak Dokter/Nakes/ Fasyankes yang mengaktifkan Jenis Layanan Sukarela.",
      "AJAK-in more Doctors/Nakes/Fasyankes to join, so that more Doctors/Nakes/Fasyankes activate the Voluntary Service Type.");

  //Sobat-IN Activity  =========================================================================================
  //
  var activity$Title = lang("Aktivitas", "Activity");
  var activity$TitleCashback =
      lang("List Belanja Produk & Jasa", "Product & Service Shopping List");
  var activity$ProductCashbackList =
      lang("List Cashback Belanja Produk", "Product Shopping Cashback List");
  var activity$ProductCashbackListDesc = lang(
      "Belanja di toko Mitra Plus OBAT-in, cek cashback nya disini",
      "Shop at the Mitra Plus drug-in store, check the cashback here");
  var activity$serviceCashbackList =
      lang("List Cashback Penggunaan Jasa", "service Shopping Cashback List");
  var activity$serviceCashbackListDesc = lang(
      "Konsultasi / berobat di OBAT-in, cek cashback nya disini",
      "Consultation / treatment at the in-drug, check the cashback here");
  var activity$Ajakinlist = lang("List AJAK-in & Jasa", "Ajakin-in list");
  var activity$orderlist = lang("Daftar Pesanan", "Order List");
  var activity$referencelist = lang("Daftar Rujukan", "reference List");
  var activity$Shareinlist = lang("List Share-in & Jasa", "Share-in list");
  var activity$HealthProducts =
      lang("Penjual Produk Kesehatan", "Seller of Health Products");
  var activity$HealthProductsDesc = lang(
      "Ini daftar yang sudah berhasil di AJAK-in",
      "This is a list that has been successfully AJAK-in");

  var activity$PlusDoctor = lang("Mitra Plus Dokter", "Partner Plus Doctor");
  var activity$PlusNakes = lang("Mitra Plus Nakes", "Partner Plus Nakes");
  var activity$PlusFasyankes =
      lang("Mitra Plus Fasyankes", "Partner Plus Fasyankes");
  var activity$cashback = lang("Bonus Cashback Sobat-In", "Sobat-In Cashback");
  var activity$cashbackdesc =
      lang("List Cashback Sobat-In", "Sobat-In Cashback List");

  //Sobat-IN Profil  =========================================================================================
  //
  var profile$completeaddress = lang("Alamat Lengkap", "Complete Address");
  var profile$MakeMainAddress =
      lang("Jadikan Alamat Utama", "Make Main Address");
  var profile$oldpassword = lang("Password Lama", "Old Password");
  var profile$changepasswordyouraccoun =
      lang("Ubah Password akun anda", "Change your account password");
  var profile$setpindesc = lang(
      "Setel PIN untuk mengunci akun anda saat membuka aplikasi.",
      "Set a PIN to lock your account when opening the app.");
  var profile$notivpromo = lang(
      "Anda dapat mengaktifkan / nonaktifkan notifikasi penawaran menarik",
      "You can enable/disable attractive offer notifications");
  var profil$FeatureUpdateNotification =
      lang("Notifikasi Pembaruan Fitur", "Feature Update Notification");
  var profile$FeatureUpdateNotivicationDesc = lang(
      "Anda dapat mengaktifkan / nonaktifkan notifikasi pembaruan dan penambahan fitur baru",
      "You can enable/disable notification of updates and addition of new features");
  var profile$account = lang("Akun", "Account");
  var profile$order = lang("Pesanan", "Order");
  var profile$payment = lang("Pembayaran", "Payment");
  var profile$Delivery = lang("Pengiriman", "Delivery");
  var profile$Promotion = lang("Promosi", "Promotion");
  var profile$Gettoknow =
      lang("Mengenal OBAT-in dan OBAT-in Heroes", "Get to know OBAT-in Heroes");
  var profile$openbrowser = lang("Buka Di Browser", "Open in Browser");

  var beingserved = lang("Sedang Dilayani", "being served");
  var beingserveddesc = lang(
      "Pengguna yang saat ini sedang di layani", "Users currently serving");
  var newshare = lang("Share-In Terbaru", "Latest Share-In");
  var newsharedesc = lang(
      "Semua aktivitas SHARE-in yang dilakukan tercatat disini",
      "All share-in activities recorded here");
  var waitingscheduleDesc = lang(
      "Menunggu konfirmasi pembayaran dari Pengguna ",
      "Waiting for payment confirmation from User");
  var waitingpaymentDesc = lang(
      "Pengguna sudah membayar, tunggu jadwal kirim/dilayani",
      "The user has paid, wait for the delivery/service schedule");
  var rescheduleDesc = lang(
      "Daftar Pasien yang mungkin / sudah di ubah jadwalnya ",
      "List of Patients who may / have been rescheduled");
  var ordercompleteDesc = lang(
      "Pengguna yang sudah selesai di layani tanpa komplain",
      "Users who have finished serving without complaints");
  var ordercomplainDesc = lang(
      "Pesanan yang di komplain Pengguna", "Orders that users complain about");
  var ordercanceledDesc =
      lang("Pesanan yang dibatalkan Pengguna", "User Canceled Order");
}
