import 'package:flutter/material.dart';
import 'package:obatin/view/common/defaultvalue.dart';

class BannerContainer extends StatelessWidget {
  final String? imgName;
  BannerContainer({@required this.imgName});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: getWidth(context, 0.06)),
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(getWidth(context, 0.03))),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(getWidth(context, 0.02)),
        child: Image.asset(
          "assets/images/promoexample/" + imgName!,
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}
