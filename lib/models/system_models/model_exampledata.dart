import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class ExampleData {
  String? id;
  String? name;

  ExampleData({this.id, this.name});
  // factory
  factory ExampleData.getData(Map<String, dynamic> object) {
    var jsonReq = object["request"];
    var jsonBody = jsonReq["header"];
    // var jsonUrl = jsonBody["urlencoded"] as List;
    print(jsonBody);
    return ExampleData(name: object["id"]);
  }
  static Future<List<ExampleData>> connectAPI(String id) async {
    String url = "https://www.postman.com/collections/f4e0fd92de966d37de65/";
    var apiResult = await http.get(
      Uri.parse(url),
    );
    var jsonObj = json.decode(apiResult.body);
    return (jsonObj as List).map((p) => ExampleData.getData(p)).toList();
  }
}
