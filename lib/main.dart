import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:obatin/controller/bloc/service_bloc/deskripsi_nakes_blog.dart';
import 'package:obatin/controller/bloc/sqflite_bloc/sqflite_sipsik_bloc.dart';
import 'package:obatin/controller/bloc/sqflite_bloc/sqflite_userdata_bloc.dart';
import 'package:obatin/controller/bloc/service_bloc/region_selector_bloc.dart';
import 'package:obatin/controller/bloc/service_bloc/sipsik_bloc.dart';
import 'package:obatin/controller/bloc/service_bloc/user_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/register_checkpass_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/settingsswitch_bloc.dart';
import 'package:obatin/controller/bloc/system_bloc/servicebool_bloc.dart';
import 'package:obatin/view/splashscreen.dart';

bool allowreview = false;

void main() {
  // SystemChrome.setPreferredOrientations(
  //     [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(ObatinMobile());
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

class ObatinMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => PasswordCheckerBloc(),
          ),
          BlocProvider(
            create: (context) => SettingsSwitchBloc(),
          ),
          BlocProvider(
            create: (context) => ServiceBoolBloc(),
          ),
          BlocProvider(
            create: (context) => SQFLITESipSikBloc(),
          ),
          BlocProvider(
            create: (context) => UserBloc(),
          ),
          BlocProvider(
            create: (context) => SQFLITEUserDataBloc(),
          ),
          BlocProvider(
            create: (context) => RegionSelectorBloc(),
          ),
          BlocProvider(
            create: (context) => SQFLITESipSikBloc(),
          ),
          BlocProvider(
            create: (context) => SipSikBloc(),
          ),
          BlocProvider(
            create: (context) => SipSikBloc(),
          ),
          BlocProvider(
            create: (context) => DeskripsiBloc(),
          ),
        ],
        child: MaterialApp(
          builder: (context, child) {
            return ScrollConfiguration(
                behavior: MyBehavior(),
                child: MediaQuery(
                  data: MediaQuery.of(context).copyWith(
                    textScaleFactor: 1.0,
                  ), //set desired text scale factor here
                  child: child!,
                ));
          },
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: SplashScreen(),
        ));
  }
}
